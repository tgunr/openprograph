/* A Marten Project Source File */
/*

%vpl_projectname_cfilename%
Copyright: 2005 Andescotia LLC

*/

#include "%vpl_projectname_hfilename%"

#define vpl_MAIN %vpl_projectname_mainmethod%
#define vpl_INIT %vpl_projectname_initmethod%

extern Nat4	*pClassConstantToIndex;
extern Nat4	*pMethodConstantToIndex;
extern Nat4	*pValueConstantToIndex;

V_Environment %vpl_projectenvironment% = NULL;

Nat4 gClassConstantToIndex[%vpl_gindex_Class%];
Nat4 gMethodConstantToIndex[%vpl_gindex_Method%];
Nat4 gValueConstantToIndex[%vpl_gindex_Value%];

%vpl_prims_declare%

%vpl_sections_declare%

vpl_Status %vpl_mapprojectfunction%(V_Environment environment);
vpl_Status %vpl_mapprojectfunction%(V_Environment environment)
{
	Int4	result = 0;

    pClassConstantToIndex=gClassConstantToIndex;
    pMethodConstantToIndex=gMethodConstantToIndex;
    pValueConstantToIndex=gValueConstantToIndex;

%vpl_project_map%

    return kNOERROR;
}

vpl_Status %vpl_loadprojectfunction%(V_Environment environment);
vpl_Status %vpl_loadprojectfunction%(V_Environment environment)
{
	Int4	result = 0;

%vpl_prims_load%

%vpl_sections_load%

    return %vpl_mapprojectfunction%(environment);
}

void %vpl_initprojectfunction%(void);
void %vpl_initprojectfunction%(void)
{
    if( %vpl_projectenvironment% == NULL ){
        %vpl_projectenvironment% = VPLEnvironmentCreate(%vpl_projectname_escapec%, kvpl_StackInline, kFALSE);

        %vpl_loadprojectfunction%(%vpl_projectenvironment%);

        VPLEnvironmentInit(%vpl_projectenvironment%, vpl_INIT);
    }
}

int main(int argc,char *argv[])
{
    %vpl_initprojectfunction%();
        
    return VPLEnvironmentMain(%vpl_projectenvironment%,vpl_MAIN,argc,argv);
}
