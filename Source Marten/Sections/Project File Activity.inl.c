/* A VPL Section File */
/*

Project File Activity.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Name[] = {
0000000000, 0X00000024, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000004, 0X53617665, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Canceled_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Completed_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Completion_20_Callback[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Attachments[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Progress[] = {
0000000000, 0X00000104, 0X00000040, 0X0000000B, 0X00000014, 0X00000088, 0X00000120, 0X00000084,
0X00000100, 0X00000080, 0X000000D8, 0X0000007C, 0X00000078, 0X00000074, 0X00000054, 0X0000005C,
0X00A70008, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000074, 0X00000008, 0X00000060,
0X41637469, 0X76697479, 0X2050726F, 0X67726573, 0X73000000, 0X00000094, 0X000000AC, 0X000000C4,
0X000000EC, 0X0000010C, 0X0000012C, 0000000000, 0000000000, 0X02D30004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0X06390004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000, 0X02D20006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000E0, 0X00000008,
0X556E7469, 0X746C6564, 0000000000, 0X02D30006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000108, 0000000000, 0000000000, 0X02D30006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000128, 0000000000, 0000000000, 0X02DD0004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Stages[] = {
0000000000, 0X000000C4, 0X00000030, 0X00000007, 0X00000014, 0X000000D8, 0X00000054, 0X000000A4,
0X00000050, 0X0000006C, 0X0000004C, 0X00000044, 0X00000007, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000004C, 0X00000003, 0X00000058, 0X00000090, 0X000000C4, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000074, 0X00000018, 0X50464153, 0X20557064, 0X61746520,
0X57696E64, 0X6F772053, 0X74616765, 0000000000, 0X46690006, 0000000000, 0000000000, 0000000000,
0000000000, 0X000000AC, 0X00000014, 0X50464153, 0X2046696E, 0X64205479, 0X70652046, 0X696C6573,
0000000000, 0X46690006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000E0, 0X00000010,
0X50464153, 0X20577269, 0X74652046, 0X696C6573, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Completed_20_Stages[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Process_20_Items[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_File_20_Type[] = {
0000000000, 0X00000024, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000006, 0X4F626A65, 0X63740000
	};
	Nat4 tempAttribute_Project_20_Save_20_Activity_2F_Window_20_Type[] = {
0000000000, 0X00000024, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000004, 0X53617665, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Type_20_Files_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X02380006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000D, 0X46696E64, 0X696E6720, 0X46696C65,
0X73000000
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Type_20_Files_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Type_20_Files_2F_Folder_20_Search[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Name[] = {
0000000000, 0X00000030, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X4C6F0006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000013, 0X53656C65, 0X6374696E, 0X67204C6F,
0X63617469, 0X6F6E7300
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Stack[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Write_20_Files_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000C, 0X53617669, 0X6E672046, 0X696C6573,
0000000000
	};
	Nat4 tempAttribute_PFAS_20_Write_20_Files_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Write_20_Files_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Write_20_Files_2F_Stack[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Write_20_Files_2F_Done[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Update_20_Window_20_Stage_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000E, 0X50726570, 0X6172696E, 0X67204461,
0X74610000
	};
	Nat4 tempAttribute_PFAS_20_Update_20_Window_20_Stage_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PFAS_20_Update_20_Window_20_Stage_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};


Nat4 VPLC_Project_20_Save_20_Activity_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Project_20_Save_20_Activity_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 549 280 }{ 240 353 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Name,environment);
	tempAttribute = attribute_add("Canceled?",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Canceled_3F_,environment);
	tempAttribute = attribute_add("Completed?",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Completed_3F_,environment);
	tempAttribute = attribute_add("Completion Callback",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Completion_20_Callback,environment);
	tempAttribute = attribute_add("Attachments",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Attachments,environment);
	tempAttribute = attribute_add("Progress",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Progress,environment);
	tempAttribute = attribute_add("Stages",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Stages,environment);
	tempAttribute = attribute_add("Current Stage",tempClass,NULL,environment);
	tempAttribute = attribute_add("Completed Stages",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Completed_20_Stages,environment);
	tempAttribute = attribute_add("Process Items",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Process_20_Items,environment);
	tempAttribute = attribute_add("File Type",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_File_20_Type,environment);
	tempAttribute = attribute_add("Window Type",tempClass,tempAttribute_Project_20_Save_20_Activity_2F_Window_20_Type,environment);
	tempAttribute = attribute_add("Project Data",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Activity Staged");
	return kNOERROR;
}

/* Start Universals: { 153 283 }{ 200 300 } */
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),TERMINAL(1),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(3),TERMINAL(2),ROOT(4));

result = vpx_constant(PARAMETERS,"Object",ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"Save",ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(6),TERMINAL(7),ROOT(8));

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do,2,0,TERMINAL(0),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_Sync_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_Sync_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),TERMINAL(1),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(3),TERMINAL(2),ROOT(4));

result = vpx_constant(PARAMETERS,"Object",ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"Save",ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(6),TERMINAL(7),ROOT(8));

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_Sync(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_Sync_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do,2,0,TERMINAL(0),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_As_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_As_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),TERMINAL(1),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(3),TERMINAL(2),ROOT(4));

result = vpx_constant(PARAMETERS,"Object",ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"Save As",ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(6),TERMINAL(7),ROOT(8));

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_As(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_As_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do,2,0,TERMINAL(0),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_C_20_Save_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_C_20_Save_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),TERMINAL(1),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(3),TERMINAL(2),ROOT(4));

result = vpx_constant(PARAMETERS,"C",ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"Save",ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(6),TERMINAL(7),ROOT(8));

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_C_20_Save(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Project_20_Save_20_Activity_2F_C_20_Save_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do,2,0,TERMINAL(0),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Title,2,0,TERMINAL(1),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Alias,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Alias_20_Record,1,1,TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod__7E_Set_20_Proxy_20_Alias,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(3),ROOT(4));
TERMINATEONFAILURE

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3_case_1_local_6(PARAMETERS,TERMINAL(1),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Synchronize,1,0,TERMINAL(0));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Synchronize,1,0,TERMINAL(0));

result = vpx_constant(PARAMETERS,"( )",ROOT(2));

result = vpx_persistent(PARAMETERS,kVPXValue_Problem_20_Operations,1,0,TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Run_20_End_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Run_20_End_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex)
{
HEADER(4)
result = kSuccess;

result = vpx_persistent(PARAMETERS,kVPXValue_Problem_20_Operations,0,1,ROOT(0));

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_constant(PARAMETERS,"Operation Data",ROOT(1));

result = vpx_method_Get_20_Application(PARAMETERS,ROOT(2));

result = vpx_constant(PARAMETERS,"Unresolved External Procedures",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Display_20_List,4,0,TERMINAL(2),TERMINAL(0),TERMINAL(1),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Run_20_End_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Run_20_End_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Project_20_Save_20_Activity_2F_Run_20_End_case_1_local_3_case_1_local_2(PARAMETERS);

result = vpx_constant(PARAMETERS,"( )",ROOT(1));

result = vpx_persistent(PARAMETERS,kVPXValue_Problem_20_Operations,1,0,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Save_20_Activity_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_End,1,0,TERMINAL(0));

result = vpx_method_Project_20_Save_20_Activity_2F_Run_20_End_case_1_local_3(PARAMETERS,TERMINAL(0));

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_constant(PARAMETERS,"( )",ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(2),TERMINAL(3),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

/* Stop Universals */



Nat4 VPLC_PFAS_20_Find_20_Type_20_Files_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PFAS_20_Find_20_Type_20_Files_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 195 301 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PFAS_20_Find_20_Type_20_Files_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PFAS_20_Find_20_Type_20_Files_2F_Count_20_Completed,environment);
	tempAttribute = attribute_add("Folder Search",tempClass,tempAttribute_PFAS_20_Find_20_Type_20_Files_2F_Folder_20_Search,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Current_20_Search,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Folder,1,1,TERMINAL(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Display_20_Name,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(4));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_String,1,1,TERMINAL(4),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(4));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"\"\"",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Found_20_Items,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(4),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Count,3,0,TERMINAL(1),TERMINAL(5),TERMINAL(2));

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5_case_1_local_5(PARAMETERS,TERMINAL(0),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(2),TERMINAL(6));

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Folder_20_Search,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Search_20_Pass,1,1,TERMINAL(3),ROOT(4));

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1_local_5(PARAMETERS,TERMINAL(3),TERMINAL(0),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"Save As",TERMINAL(3));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(0))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
OUTPUT(1,NONE)
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
OUTPUT(1,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1))
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"Object",TERMINAL(1));
NEXTCASEONFAILURE

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(2)
REPEATBEGINWITHCOUNTER
result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1_local_3(PARAMETERS,LIST(0),ROOT(2),ROOT(3));
LISTROOT(2,0)
LISTROOT(3,1)
REPEATFINISH
LISTROOTFINISH(2,0)
LISTROOTFINISH(3,1)
LISTROOTEND
} else {
ROOTEMPTY(2)
ROOTEMPTY(3)
}

result = kSuccess;

OUTPUT(0,TERMINAL(2))
OUTPUT(1,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"C",TERMINAL(1));
NEXTCASEONFAILURE

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_C_20_File_20_Name,1,1,LIST(0),ROOT(2));
LISTROOT(2,0)
REPEATFINISH
LISTROOTFINISH(2,0)
LISTROOTEND
} else {
ROOTEMPTY(2)
}

result = kSuccess;

OUTPUT(0,TERMINAL(0))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( )",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(0))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1))
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Parent,1,1,TERMINAL(3),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_pack,1,1,TERMINAL(4),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( )",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_VPL_20_Add_20_Search_20_Locations(PARAMETERS,TERMINAL(0),ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(15)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_4(PARAMETERS,TERMINAL(3),TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_instantiate(PARAMETERS,kVPXClass_Folder_20_Search,1,1,NONE,ROOT(8));

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_6(PARAMETERS,TERMINAL(1),ROOT(9));

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2_local_7(PARAMETERS,TERMINAL(9),ROOT(10));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Setup_20_Search,3,0,TERMINAL(8),TERMINAL(10),TERMINAL(7));

result = vpx_set(PARAMETERS,kVPXValue_Folder_20_Search,TERMINAL(0),TERMINAL(8),ROOT(11));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(2),TERMINAL(6),ROOT(12));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(7),ROOT(13));

result = vpx_set(PARAMETERS,kVPXValue_Count_20_Max,TERMINAL(11),TERMINAL(13),ROOT(14));

result = kSuccess;

FOOTERWITHNONE(15)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_instantiate(PARAMETERS,kVPXClass_PFAS_20_Find_20_Files_20_Manually,1,1,NONE,ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Stack,TERMINAL(2),TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Push_20_Next_20_Stage,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"Save As",TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(2),ROOT(4),ROOT(5));

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_1_local_5(PARAMETERS,TERMINAL(2),TERMINAL(5));

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_list_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"Object",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"C",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_C_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name_20_As_20_String,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_match(PARAMETERS,"0",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_get_2D_nth,2,1,TERMINAL(0),TERMINAL(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(5),TERMINAL(1),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_set_2D_nth_21_,3,1,TERMINAL(0),TERMINAL(6),TERMINAL(4),ROOT(7));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTER(8)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));

result = vpx_match(PARAMETERS,"0",TERMINAL(3));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_instantiate(PARAMETERS,kVPXClass_PFAS_20_Find_20_Files_20_Manually,1,1,NONE,ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Stack,TERMINAL(2),TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Push_20_Next_20_Stage,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(14)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Found_20_Items,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(1),ROOT(6),ROOT(7));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(5))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_5(PARAMETERS,LIST(5),TERMINAL(7),ROOT(8));
LISTROOT(8,0)
REPEATFINISH
LISTROOTFINISH(8,0)
LISTROOTEND
} else {
ROOTEMPTY(8)
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(3))) ){
REPEATBEGINWITHCOUNTER
LOOPTERMINALBEGIN(1)
LOOPTERMINAL(0,5,9)
result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_6(PARAMETERS,LOOP(0),LIST(3),TERMINAL(8),ROOT(9));
REPEATFINISH
} else {
ROOTNULL(9,TERMINAL(5))
}

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(1),TERMINAL(9),ROOT(10));

result = vpx_get(PARAMETERS,kVPXValue_Search_20_Items,TERMINAL(2),ROOT(11),ROOT(12));

if( (repeatLimit = vpx_multiplex(2,TERMINAL(9),TERMINAL(8))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_9(PARAMETERS,LIST(9),TERMINAL(12),LIST(8),ROOT(13));
LISTROOT(13,0)
REPEATFINISH
LISTROOTFINISH(13,0)
LISTROOTEND
} else {
ROOTEMPTY(13)
}

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4_case_1_local_10(PARAMETERS,TERMINAL(10),TERMINAL(13));

result = kSuccess;

FOOTERSINGLECASE(14)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Folder_20_Search,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
TERMINATEONFAILURE

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2_local_4(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Dispose,1,0,TERMINAL(3));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Folder_20_Search,TERMINAL(2),TERMINAL(4),ROOT(5));

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_End,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

/* Stop Universals */



Nat4 VPLC_PFAS_20_Find_20_Files_20_Manually_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PFAS_20_Find_20_Files_20_Manually_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Count_20_Completed,environment);
	tempAttribute = attribute_add("Stack",tempClass,tempAttribute_PFAS_20_Find_20_Files_20_Manually_2F_Stack,environment);
	tempAttribute = attribute_add("Current Item",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 417 97 }{ 200 300 } */
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Current_20_Item,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_constant(PARAMETERS,"FALSE",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Item_20_Run,3,0,TERMINAL(0),TERMINAL(3),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Stack,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"(  )",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_Stack,TERMINAL(2),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Item_20_Start,3,0,TERMINAL(0),TERMINAL(4),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTER(8)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Stack,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(2),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Count_20_Max,TERMINAL(0),TERMINAL(3),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_20_Begin_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Stop(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Activity,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Up_20_Count,3,0,TERMINAL(0),NONE,TERMINAL(2));

result = vpx_constant(PARAMETERS,"NULL",ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Current_20_Item,TERMINAL(1),TERMINAL(3),ROOT(4));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(1),TERMINAL(3));

result = vpx_set(PARAMETERS,kVPXValue_Current_20_Item,TERMINAL(2),TERMINAL(0),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_UniChar,1,2,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Length,1,1,TERMINAL(0),ROOT(4));

result = vpx_extconstant(PARAMETERS,kTextEncodingUnknown,ROOT(5));

result = vpx_instantiate(PARAMETERS,kVPXClass_MacOS_20_File_20_Reference,1,1,NONE,ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_New_20_From_20_Unicode,5,0,TERMINAL(6),TERMINAL(1),TERMINAL(2),TERMINAL(4),TERMINAL(5));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERWITHNONE(7)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_To_20_CFString(PARAMETERS,TERMINAL(1),ROOT(2));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1_local_3(PARAMETERS,TERMINAL(2),TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(2));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_File,2,2,TERMINAL(0),TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(15)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(1),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"C",TERMINAL(4));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(2),ROOT(5));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(5));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Parent,1,1,TERMINAL(5),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(6));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_C_20_File_20_Name,1,1,TERMINAL(2),ROOT(7));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1_local_9(PARAMETERS,TERMINAL(6),TERMINAL(7),ROOT(8));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(8));
NEXTCASEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(1),ROOT(9),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(10),TERMINAL(2),ROOT(11));

result = vpx_match(PARAMETERS,"0",TERMINAL(11));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(2),TERMINAL(8),ROOT(12));

result = vpx_call_primitive(PARAMETERS,VPLP_set_2D_nth_21_,3,1,TERMINAL(10),TERMINAL(12),TERMINAL(11),ROOT(13));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(9),TERMINAL(13),ROOT(14));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Item_20_Stop,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(15)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"Object",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_File_20_Type,1,1,TERMINAL(0),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
OUTPUT(1,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"C",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_C_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = vpx_constant(PARAMETERS,"\'TEXT\'",ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
OUTPUT(1,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1))
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0,root1);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADERWITHNONE(11)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kWindowModalityAppModal,ROOT(3));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_constant(PARAMETERS,"/Nav File Callback",ROOT(5));

result = vpx_method_New_20_Callback(PARAMETERS,TERMINAL(5),TERMINAL(0),NONE,ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(1),ROOT(7),ROOT(8));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2_local_7(PARAMETERS,TERMINAL(2),TERMINAL(8),ROOT(9),ROOT(10));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(9));
NEXTCASEONSUCCESS

result = vpx_method_Nav_20_Put_20_File(PARAMETERS,TERMINAL(9),TERMINAL(10),TERMINAL(4),TERMINAL(3),TERMINAL(4),TERMINAL(6));
NEXTCASEONFAILURE

result = kSuccess;

FOOTERWITHNONE(11)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_3(PARAMETERS,TERMINAL(1),TERMINAL(2),TERMINAL(0));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1_local_4(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(1));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Item_20_Stop,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"Object",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"C",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_C_20_File_20_Name,1,1,TERMINAL(0),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADERWITHNONE(12)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kWindowModalityAppModal,ROOT(3));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_constant(PARAMETERS,"/Nav File Callback",ROOT(5));

result = vpx_method_New_20_Callback(PARAMETERS,TERMINAL(5),TERMINAL(1),NONE,ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_File_20_Type,1,1,TERMINAL(0),ROOT(7));

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(2),ROOT(8),ROOT(9));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2_case_1_local_8(PARAMETERS,TERMINAL(0),TERMINAL(9),ROOT(10));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(10));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(0),ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(2),TERMINAL(11));

result = vpx_method_Nav_20_Put_20_File(PARAMETERS,TERMINAL(10),TERMINAL(7),TERMINAL(4),TERMINAL(3),TERMINAL(4),TERMINAL(6));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASEWITHNONE(12)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3_local_2(PARAMETERS,TERMINAL(1),TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_File,2,2,TERMINAL(0),TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_extmatch(PARAMETERS,dupFNErr,TERMINAL(2));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_UniChar,1,2,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Length,1,1,TERMINAL(0),ROOT(4));

result = vpx_extconstant(PARAMETERS,kTextEncodingUnknown,ROOT(5));

result = vpx_instantiate(PARAMETERS,kVPXClass_MacOS_20_File_20_Reference,1,1,NONE,ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_New_20_From_20_Unicode,5,0,TERMINAL(6),TERMINAL(1),TERMINAL(2),TERMINAL(4),TERMINAL(5));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERWITHNONE(7)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_To_20_CFString(PARAMETERS,TERMINAL(1),ROOT(2));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2_local_3(PARAMETERS,TERMINAL(2),TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(2));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(17)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Canceled_3F_,1,1,TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Reply_20_Selection,1,1,TERMINAL(1),ROOT(3));
TERMINATEONFAILURE

result = vpx_match(PARAMETERS,"(  )",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File_20_Name,1,1,TERMINAL(1),ROOT(5));

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2_case_1_local_8(PARAMETERS,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(6));
TERMINATEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Current_20_Item,TERMINAL(0),ROOT(7),ROOT(8));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(8));
TERMINATEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Activity,TERMINAL(0),ROOT(9),ROOT(10));

result = vpx_get(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(10),ROOT(11),ROOT(12));

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(12),TERMINAL(8),ROOT(13));

result = vpx_match(PARAMETERS,"0",TERMINAL(13));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_pack,2,1,TERMINAL(8),TERMINAL(6),ROOT(14));

result = vpx_call_primitive(PARAMETERS,VPLP_set_2D_nth_21_,3,1,TERMINAL(12),TERMINAL(14),TERMINAL(13),ROOT(15));

result = vpx_set(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(11),TERMINAL(15),ROOT(16));

result = kSuccess;

FOOTERSINGLECASE(17)
}

enum opTrigger vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Item_20_Stop,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

/* Stop Universals */



Nat4 VPLC_PFAS_20_Write_20_Files_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PFAS_20_Write_20_Files_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PFAS_20_Write_20_Files_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PFAS_20_Write_20_Files_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PFAS_20_Write_20_Files_2F_Count_20_Completed,environment);
	tempAttribute = attribute_add("Stack",tempClass,tempAttribute_PFAS_20_Write_20_Files_2F_Stack,environment);
	tempAttribute = attribute_add("Done",tempClass,tempAttribute_PFAS_20_Write_20_Files_2F_Done,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 239 199 }{ 200 300 } */
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Exporting Inline Files",ROOT(0));

result = vpx_persistent(PARAMETERS,kVPXValue_Export_20_Mode,0,1,ROOT(1));

result = vpx_match(PARAMETERS,"Experimental",TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Exporting Runtime Files",ROOT(0));

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(1)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"C",TERMINAL(3));
TERMINATEONFAILURE

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2_case_1_local_4(PARAMETERS,ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Name,TERMINAL(0),TERMINAL(4),ROOT(5));

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Process_20_Items,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(3),ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Count_20_Max,TERMINAL(0),TERMINAL(4),ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_Stack,TERMINAL(5),TERMINAL(3),ROOT(6));

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Next_20_Name,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_list_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
OUTPUT(1,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(0))
OUTPUT(1,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
OUTPUT(1,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1))
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Synchronize,1,0,TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Now,1,0,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CGrafPtr,1,1,TERMINAL(2),ROOT(3));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

QDFlushPortBuffer( GETPOINTER(0,OpaqueGrafPtr,*,ROOT(5),TERMINAL(3)),GETPOINTER(0,OpaqueRgnHandle,*,ROOT(6),TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(7)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Full_20_Path,1,1,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"Save As",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Full_20_Path,1,1,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(0),ROOT(2));

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2_local_3(PARAMETERS,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_MacOS_20_File_20_Reference,1,1,NONE,ROOT(2));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_New_20_From_20_Path_20_String,4,0,TERMINAL(2),TERMINAL(1),NONE,TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_File,2,0,TERMINAL(0),TERMINAL(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = kSuccess;

FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_match(PARAMETERS,"Save As",TERMINAL(3));
TERMINATEONFAILURE

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_1_local_4(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_MacOS_20_File_20_Reference,1,1,NONE,ROOT(2));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_New_20_From_20_Path_20_String,4,0,TERMINAL(2),TERMINAL(1),NONE,TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_File,2,0,TERMINAL(0),TERMINAL(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = kSuccess;

FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_2_local_2(PARAMETERS,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_match(PARAMETERS,"Object",TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_4(PARAMETERS,TERMINAL(1),ROOT(4));

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_5(PARAMETERS,TERMINAL(0),TERMINAL(3),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Save_20_File,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1_local_7(PARAMETERS,TERMINAL(0),TERMINAL(5),TERMINAL(4),TERMINAL(3));

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_match(PARAMETERS,"C",TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_C_20_Code_20_Export,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3))
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADERWITHNONE(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(3),ROOT(4));
TERMINATEONFAILURE

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_3(PARAMETERS,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Up_20_Count,3,0,TERMINAL(1),NONE,TERMINAL(2));

result = vpx_get(PARAMETERS,kVPXValue_File_20_Type,TERMINAL(2),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Window_20_Type,TERMINAL(2),ROOT(7),ROOT(8));

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7_case_1_local_7(PARAMETERS,TERMINAL(3),TERMINAL(4),TERMINAL(6),TERMINAL(8));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(9)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Stack,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"( )",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_set(PARAMETERS,kVPXValue_Stack,TERMINAL(3),TERMINAL(6),ROOT(7));

result = vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1_local_7(PARAMETERS,TERMINAL(5),TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Next_20_Name,2,0,TERMINAL(7),TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(8)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_list_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(3)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(1)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Stack,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"( )",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1_local_5(PARAMETERS,TERMINAL(3),ROOT(5));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(5),ROOT(6));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTER(7)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"\"\"",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Synchronize,1,0,TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Now,1,0,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CGrafPtr,1,1,TERMINAL(2),ROOT(3));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

QDFlushPortBuffer( GETPOINTER(0,OpaqueGrafPtr,*,ROOT(5),TERMINAL(3)),GETPOINTER(0,OpaqueRgnHandle,*,ROOT(6),TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(7)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(1),TERMINAL(2));

result = vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name_case_1_local_4(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(3)
}

/* Stop Universals */



Nat4 VPLC_PFAS_20_Update_20_Window_20_Stage_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PFAS_20_Update_20_Window_20_Stage_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 343 426 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PFAS_20_Update_20_Window_20_Stage_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PFAS_20_Update_20_Window_20_Stage_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PFAS_20_Update_20_Window_20_Stage_2F_Count_20_Completed,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Begin_20_Window,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3_case_1_local_4(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin_case_1_local_3(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

/* Stop Universals */






Nat4	loadClasses_Project_20_File_20_Activity(V_Environment environment);
Nat4	loadClasses_Project_20_File_20_Activity(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("Project Save Activity",environment);
	if(result == NULL) return kERROR;
	VPLC_Project_20_Save_20_Activity_class_load(result,environment);
	result = class_new("PFAS Find Type Files",environment);
	if(result == NULL) return kERROR;
	VPLC_PFAS_20_Find_20_Type_20_Files_class_load(result,environment);
	result = class_new("PFAS Find Files Manually",environment);
	if(result == NULL) return kERROR;
	VPLC_PFAS_20_Find_20_Files_20_Manually_class_load(result,environment);
	result = class_new("PFAS Write Files",environment);
	if(result == NULL) return kERROR;
	VPLC_PFAS_20_Write_20_Files_class_load(result,environment);
	result = class_new("PFAS Update Window Stage",environment);
	if(result == NULL) return kERROR;
	VPLC_PFAS_20_Update_20_Window_20_Stage_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_Project_20_File_20_Activity(V_Environment environment);
Nat4	loadUniversals_Project_20_File_20_Activity(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("Project Save Activity/Object Save",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save,NULL);

	result = method_new("Project Save Activity/Object Save Sync",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_Sync,NULL);

	result = method_new("Project Save Activity/Object Save As",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Object_20_Save_20_As,NULL);

	result = method_new("Project Save Activity/C Save",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_C_20_Save,NULL);

	result = method_new("Project Save Activity/Initialize Progress Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Initialize_20_Progress_20_Window,NULL);

	result = method_new("Project Save Activity/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Run,NULL);

	result = method_new("Project Save Activity/Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Begin,NULL);

	result = method_new("Project Save Activity/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Save_20_Activity_2F_Run_20_End,NULL);

	result = method_new("PFAS Find Type Files/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run,NULL);

	result = method_new("PFAS Find Type Files/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_Begin,NULL);

	result = method_new("PFAS Find Type Files/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Type_20_Files_2F_Run_20_End,NULL);

	result = method_new("PFAS Find Files Manually/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run,NULL);

	result = method_new("PFAS Find Files Manually/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Run_20_Begin,NULL);

	result = method_new("PFAS Find Files Manually/Item Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Run,NULL);

	result = method_new("PFAS Find Files Manually/Item Stop",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Stop,NULL);

	result = method_new("PFAS Find Files Manually/Item Start",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Item_20_Start,NULL);

	result = method_new("PFAS Find Files Manually/Nav File Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Find_20_Files_20_Manually_2F_Nav_20_File_20_Callback,NULL);

	result = method_new("PFAS Write Files/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Write_20_Files_2F_Run_20_Begin,NULL);

	result = method_new("PFAS Write Files/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Write_20_Files_2F_Run,NULL);

	result = method_new("PFAS Write Files/Next Name",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Write_20_Files_2F_Next_20_Name,NULL);

	result = method_new("PFAS Update Window Stage/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PFAS_20_Update_20_Window_20_Stage_2F_Run_20_Begin,NULL);

	return kNOERROR;

}



Nat4	loadPersistents_Project_20_File_20_Activity(V_Environment environment);
Nat4	loadPersistents_Project_20_File_20_Activity(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	return kNOERROR;

}

Nat4	load_Project_20_File_20_Activity(V_Environment environment);
Nat4	load_Project_20_File_20_Activity(V_Environment environment)
{

	loadClasses_Project_20_File_20_Activity(environment);
	loadUniversals_Project_20_File_20_Activity(environment);
	loadPersistents_Project_20_File_20_Activity(environment);
	return kNOERROR;

}

