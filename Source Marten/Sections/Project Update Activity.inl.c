/* A VPL Section File */
/*

Project Update Activity.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Name[] = {
0000000000, 0X00000038, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000001A, 0X55706461, 0X74652052, 0X756E7469,
0X6D652041, 0X70706C69, 0X63617469, 0X6F6E0000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Canceled_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Completed_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Completion_20_Callback[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Attachments[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Progress[] = {
0000000000, 0X00000104, 0X00000040, 0X0000000B, 0X00000014, 0X00000088, 0X00000120, 0X00000084,
0X00000100, 0X00000080, 0X000000D8, 0X0000007C, 0X00000078, 0X00000074, 0X00000054, 0X0000005C,
0X00A70008, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000074, 0X00000008, 0X00000060,
0X41637469, 0X76697479, 0X2050726F, 0X67726573, 0X73000000, 0X00000094, 0X000000AC, 0X000000C4,
0X000000EC, 0X0000010C, 0X0000012C, 0000000000, 0000000000, 0X02D30004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000002, 0X02D20006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000E0, 0X00000008,
0X556E7469, 0X746C6564, 0000000000, 0X02D30006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000108, 0000000000, 0000000000, 0X02D30006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000128, 0000000000, 0000000000, 0X02DD0004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Stages[] = {
0000000000, 0X000000F4, 0X00000038, 0X00000009, 0X00000014, 0X0000010C, 0X00000060, 0X000000DC,
0X0000005C, 0X000000A8, 0X00000058, 0X00000078, 0X00000054, 0X0000004C, 0X00000007, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000054, 0X00000004, 0X00000064, 0X00000094, 0X000000C8,
0X000000F8, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000080, 0X00000012,
0X50554153, 0X20557064, 0X61746520, 0X56616C75, 0X65730000, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X000000B0, 0X00000014, 0X50554153, 0X20557064, 0X61746520, 0X56505A20,
0X46696C65, 0000000000, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000E4,
0X00000013, 0X50554153, 0X20557064, 0X61746520, 0X42756E64, 0X6C657300, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000114, 0X00000015, 0X50554153, 0X20557064, 0X61746520,
0X5265736F, 0X75726365, 0X73000000
	};
	Nat4 tempAttribute_Project_20_Update_20_Activity_2F_Completed_20_Stages[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Values_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000F, 0X55706461, 0X74696E67, 0X2056616C,
0X75657300
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Values_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Values_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Values_2F_Values[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_VPZ_20_File_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000E, 0X53617669, 0X6E672041, 0X72636869,
0X76650000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_VPZ_20_File_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_VPZ_20_File_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Bundles_2F_Name[] = {
0000000000, 0X00000030, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000012, 0X55706461, 0X74696E67, 0X204C6962,
0X72617269, 0X65730000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Bundles_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Bundles_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Bundles_2F_Values[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Resources_2F_Name[] = {
0000000000, 0X00000030, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000012, 0X55706461, 0X74696E67, 0X20526573,
0X6F757263, 0X65730000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Resources_2F_Count_20_Max[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Resources_2F_Count_20_Completed[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_PUAS_20_Update_20_Resources_2F_Values[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};


Nat4 VPLC_Project_20_Update_20_Activity_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Project_20_Update_20_Activity_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 328 283 }{ 254 372 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Name,environment);
	tempAttribute = attribute_add("Canceled?",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Canceled_3F_,environment);
	tempAttribute = attribute_add("Completed?",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Completed_3F_,environment);
	tempAttribute = attribute_add("Completion Callback",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Completion_20_Callback,environment);
	tempAttribute = attribute_add("Attachments",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Attachments,environment);
	tempAttribute = attribute_add("Progress",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Progress,environment);
	tempAttribute = attribute_add("Stages",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Stages,environment);
	tempAttribute = attribute_add("Current Stage",tempClass,NULL,environment);
	tempAttribute = attribute_add("Completed Stages",tempClass,tempAttribute_Project_20_Update_20_Activity_2F_Completed_20_Stages,environment);
	tempAttribute = attribute_add("Project Data",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Activity Staged");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Error_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Error_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex)
{
HEADER(2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Play Sounds",ROOT(0));

result = vpx_method_CFPrefs_20_Get_20_App_20_Value(PARAMETERS,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Error_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Error_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex)
{
HEADER(1)
result = kSuccess;

result = vpx_method_Project_20_Update_20_Activity_2F_Error_case_1_local_3_case_1_local_2(PARAMETERS);
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"1",ROOT(0));

SystemSoundPlay( GETINTEGER(TERMINAL(0)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Error(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Cancel,1,0,TERMINAL(0));

result = vpx_method_Project_20_Update_20_Activity_2F_Error_case_1_local_3(PARAMETERS);

result = vpx_call_primitive(PARAMETERS,VPLP_show,1,0,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Get_20_Project_20_Helper(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Synchronize,1,0,TERMINAL(0));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Run_20_End_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Run_20_End_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex)
{
HEADER(2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Play Sounds",ROOT(0));

result = vpx_method_CFPrefs_20_Get_20_App_20_Value(PARAMETERS,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Run_20_End_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Run_20_End_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex)
{
HEADER(1)
result = kSuccess;

result = vpx_method_Project_20_Update_20_Activity_2F_Run_20_End_case_1_local_2_case_1_local_2(PARAMETERS);
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"22",ROOT(0));

SystemSoundPlay( GETINTEGER(TERMINAL(0)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Project_20_Update_20_Activity_2F_Run_20_End_case_1_local_2(PARAMETERS);

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_End,1,0,TERMINAL(0));

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Title,2,0,TERMINAL(1),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Alias,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Alias_20_Record,1,1,TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod__7E_Set_20_Proxy_20_Alias,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Project_20_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_File,1,1,TERMINAL(3),ROOT(4));
TERMINATEONFAILURE

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3_case_1_local_6(PARAMETERS,TERMINAL(1),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

/* Stop Universals */



Nat4 VPLC_PUAS_20_Update_20_Values_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PUAS_20_Update_20_Values_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PUAS_20_Update_20_Values_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PUAS_20_Update_20_Values_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PUAS_20_Update_20_Values_2F_Count_20_Completed,environment);
	tempAttribute = attribute_add("Values",tempClass,tempAttribute_PUAS_20_Update_20_Values_2F_Values,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Values,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"(  )",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(2),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(7));

result = vpx_method_PUAS_20_Update_20_Values_2F_Run_case_1_local_7(PARAMETERS,TERMINAL(4),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Value,1,0,TERMINAL(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Up_20_Count,3,0,TERMINAL(0),NONE,TERMINAL(1));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTERWITHNONE(8)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_Values_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PUAS_20_Update_20_Values_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Project_20_Helper,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Persistents,1,1,TERMINAL(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_All_20_Attributes,1,1,TERMINAL(2),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP__28_join_29_,2,1,TERMINAL(4),TERMINAL(3),ROOT(5));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(1),TERMINAL(5),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(5),ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Count_20_Max,TERMINAL(6),TERMINAL(7),ROOT(8));

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PUAS_20_Update_20_Values_2F_Run_20_Begin_case_1_local_2(PARAMETERS,TERMINAL(1),TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Values_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( )",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(0),TERMINAL(2),ROOT(3));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_End,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

/* Stop Universals */



Nat4 VPLC_PUAS_20_Update_20_VPZ_20_File_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PUAS_20_Update_20_VPZ_20_File_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PUAS_20_Update_20_VPZ_20_File_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PUAS_20_Update_20_VPZ_20_File_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PUAS_20_Update_20_VPZ_20_File_2F_Count_20_Completed,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(9)
INPUT(0,0)
result = kSuccess;

result = vpx_method_New_20_FSRef(PARAMETERS,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"Application.vpz",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Bundle_20_Resource_20_FSRef,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod__7E_FSGetCatalogInfo,5,4,TERMINAL(3),NONE,NONE,TERMINAL(4),TERMINAL(4),ROOT(5),ROOT(6),ROOT(7),ROOT(8));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(7))
OUTPUT(1,TERMINAL(8))
FOOTERWITHNONE(9)
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADERWITHNONE(19)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kFSCatInfoNone,ROOT(1));

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

PUTINTEGER(CFStringGetSystemEncoding(),3);
result = kSuccess;

result = vpx_constant(PARAMETERS,"Application.vpz",ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_string_2D_to_2D_pointer,1,1,TERMINAL(4),ROOT(5));

PUTPOINTER(__CFString,*,CFStringCreateWithCString( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(2)),GETCONSTPOINTER(char,*,TERMINAL(5)),GETINTEGER(TERMINAL(3))),6);
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(7));

PUTPOINTER(__CFURL,*,CFURLCreateFromFSRef( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(7)),GETCONSTPOINTER(FSRef,*,TERMINAL(0))),8);
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(8));
FAILONSUCCESS

PUTPOINTER(__CFBundle,*,CFBundleCreate( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(7)),GETCONSTPOINTER(__CFURL,*,TERMINAL(8))),9);
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(9));
FAILONSUCCESS

PUTPOINTER(__CFURL,*,CFBundleCopyResourceURL( GETPOINTER(0,__CFBundle,*,ROOT(11),TERMINAL(9)),GETCONSTPOINTER(__CFString,*,TERMINAL(6)),GETCONSTPOINTER(__CFString,*,TERMINAL(2)),GETCONSTPOINTER(__CFString,*,TERMINAL(2))),10);
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(10));
FAILONSUCCESS

PUTINTEGER(CFURLGetFSRef( GETCONSTPOINTER(__CFURL,*,TERMINAL(10)),GETPOINTER(144,FSRef,*,ROOT(13),NONE)),12);
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(12));
FAILONFAILURE

PUTINTEGER(FSGetCatalogInfo( GETCONSTPOINTER(FSRef,*,TERMINAL(13)),GETINTEGER(TERMINAL(1)),GETPOINTER(148,FSCatalogInfo,*,ROOT(15),TERMINAL(2)),GETPOINTER(512,HFSUniStr255,*,ROOT(16),TERMINAL(2)),GETPOINTER(72,FSSpec,*,ROOT(17),NONE),GETPOINTER(144,FSRef,*,ROOT(18),NONE)),14);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(8)));
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(6)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(17))
OUTPUT(1,TERMINAL(18))
FOOTERWITHNONE(19)
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1))
vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1_local_3(PARAMETERS,TERMINAL(3),ROOT(4),ROOT(5));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_open_2D_file,1,1,TERMINAL(4),ROOT(6));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_write_2D_object,2,0,TERMINAL(6),TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_close_2D_file,1,0,TERMINAL(6));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(7)
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Couldn\'t update executable!",ROOT(2));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(2));

result = vpx_method_Beep(PARAMETERS);

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(16)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Project_20_Helper,1,1,TERMINAL(1),ROOT(3));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_constant(PARAMETERS,"NULL",ROOT(8));

result = vpx_constant(PARAMETERS,"NULL",ROOT(9));

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(3),ROOT(10),ROOT(11));

result = vpx_set(PARAMETERS,kVPXValue_Environment,TERMINAL(10),TERMINAL(9),ROOT(12));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(6),TERMINAL(8),ROOT(13));

result = vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1_local_11(PARAMETERS,TERMINAL(13),TERMINAL(11));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(13),TERMINAL(7),ROOT(14));

result = vpx_set(PARAMETERS,kVPXValue_Environment,TERMINAL(12),TERMINAL(11),ROOT(15));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(16)
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

/* Stop Universals */



Nat4 VPLC_PUAS_20_Update_20_Bundles_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PUAS_20_Update_20_Bundles_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PUAS_20_Update_20_Bundles_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PUAS_20_Update_20_Bundles_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PUAS_20_Update_20_Bundles_2F_Count_20_Completed,environment);
	tempAttribute = attribute_add("Values",tempClass,tempAttribute_PUAS_20_Update_20_Bundles_2F_Values,environment);
	tempAttribute = attribute_add("File Ref",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 464 308 }{ 200 300 } */
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"Replace",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_FSRef,1,1,TERMINAL(5),ROOT(7));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_copy_2D_object,3,0,TERMINAL(7),TERMINAL(1),TERMINAL(6));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(8)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"\"Problem with copying \"",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name_20_As_20_String,1,1,TERMINAL(5),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Full_20_Path,1,1,TERMINAL(5),ROOT(8));

result = vpx_constant(PARAMETERS,"\". \n\"",ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP__22_join_22_,4,1,TERMINAL(6),TERMINAL(7),TERMINAL(9),TERMINAL(8),ROOT(10));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(10));

result = kSuccess;

FOOTER(11)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(10)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Values,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"(  )",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_File_20_Ref,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(3),ROOT(6),ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(2),TERMINAL(7),ROOT(8));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(9));

result = vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_8(PARAMETERS,TERMINAL(6),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Up_20_Count,3,0,TERMINAL(0),NONE,TERMINAL(1));

result = vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1_local_10(PARAMETERS,TERMINAL(6),TERMINAL(5));

result = kSuccess;

OUTPUT(0,TERMINAL(9))
FOOTERWITHNONE(10)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PUAS_20_Update_20_Bundles_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(9)
INPUT(0,0)
result = kSuccess;

result = vpx_method_New_20_FSRef(PARAMETERS,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"Application.vpz",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Bundle_20_Resource_20_FSRef,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod__7E_FSGetCatalogInfo,5,4,TERMINAL(3),NONE,NONE,NONE,TERMINAL(4),ROOT(5),ROOT(6),ROOT(7),ROOT(8));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(8))
FOOTERSINGLECASEWITHNONE(9)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(16)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Project_20_Helper,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin_case_1_local_2_case_1_local_5(PARAMETERS,TERMINAL(6),ROOT(7));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"Primitives Data",ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_List_20_Datum,2,1,TERMINAL(2),TERMINAL(8),ROOT(9));

result = vpx_get(PARAMETERS,kVPXValue_List,TERMINAL(9),ROOT(10),ROOT(11));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(1),TERMINAL(11),ROOT(12));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(11),ROOT(13));

result = vpx_set(PARAMETERS,kVPXValue_Count_20_Max,TERMINAL(12),TERMINAL(13),ROOT(14));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Ref,TERMINAL(14),TERMINAL(7),ROOT(15));

result = kSuccess;

FOOTERSINGLECASE(16)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin_case_1_local_2(PARAMETERS,TERMINAL(1),TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( )",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(0),TERMINAL(2),ROOT(3));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_End,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Ref,TERMINAL(3),TERMINAL(4),ROOT(5));

result = kSuccess;

FOOTERSINGLECASE(6)
}

/* Stop Universals */



Nat4 VPLC_PUAS_20_Update_20_Resources_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_PUAS_20_Update_20_Resources_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_PUAS_20_Update_20_Resources_2F_Name,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
	tempAttribute = attribute_add("Count Max",tempClass,tempAttribute_PUAS_20_Update_20_Resources_2F_Count_20_Max,environment);
	tempAttribute = attribute_add("Count Completed",tempClass,tempAttribute_PUAS_20_Update_20_Resources_2F_Count_20_Completed,environment);
	tempAttribute = attribute_add("Values",tempClass,tempAttribute_PUAS_20_Update_20_Resources_2F_Values,environment);
	tempAttribute = attribute_add("File Ref",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Project File Activity Stage");
	return kNOERROR;
}

/* Start Universals: { 464 308 }{ 200 300 } */
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"Replace",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_FSRef,1,1,TERMINAL(5),ROOT(7));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_copy_2D_object,3,0,TERMINAL(7),TERMINAL(1),TERMINAL(6));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(8)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"\"Problem with copying \"",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name_20_As_20_String,1,1,TERMINAL(5),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Full_20_Path,1,1,TERMINAL(5),ROOT(8));

result = vpx_constant(PARAMETERS,"\". \n\"",ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP__22_join_22_,4,1,TERMINAL(6),TERMINAL(7),TERMINAL(9),TERMINAL(8),ROOT(10));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(10));

result = kSuccess;

FOOTER(11)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(10)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Values,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"(  )",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_File_20_Ref,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(3),ROOT(6),ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(2),TERMINAL(7),ROOT(8));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(9));

result = vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_8(PARAMETERS,TERMINAL(6),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Up_20_Count,3,0,TERMINAL(0),NONE,TERMINAL(1));

result = vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1_local_10(PARAMETERS,TERMINAL(6),TERMINAL(5));

result = kSuccess;

OUTPUT(0,TERMINAL(9))
FOOTERWITHNONE(10)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_PUAS_20_Update_20_Resources_2F_Run_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(9)
INPUT(0,0)
result = kSuccess;

result = vpx_method_New_20_FSRef(PARAMETERS,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"Application.vpz",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Bundle_20_Resource_20_FSRef,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod__7E_FSGetCatalogInfo,5,4,TERMINAL(3),NONE,NONE,NONE,TERMINAL(4),ROOT(5),ROOT(6),ROOT(7),ROOT(8));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(8))
FOOTERSINGLECASEWITHNONE(9)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(16)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Project_20_Helper,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_File,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin_case_1_local_2_case_1_local_5(PARAMETERS,TERMINAL(6),ROOT(7));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"Resource File Data",ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_List_20_Datum,2,1,TERMINAL(2),TERMINAL(8),ROOT(9));
TERMINATEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_List,TERMINAL(9),ROOT(10),ROOT(11));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(1),TERMINAL(11),ROOT(12));

result = vpx_call_primitive(PARAMETERS,VPLP__28_length_29_,1,1,TERMINAL(11),ROOT(13));

result = vpx_set(PARAMETERS,kVPXValue_Count_20_Max,TERMINAL(12),TERMINAL(13),ROOT(14));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Ref,TERMINAL(14),TERMINAL(7),ROOT(15));

result = kSuccess;

FOOTERSINGLECASE(16)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin_case_1_local_2(PARAMETERS,TERMINAL(1),TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_End_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_End_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Project_20_Helper,1,1,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Resources_20_Changed,1,0,TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( )",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Values,TERMINAL(0),TERMINAL(2),ROOT(3));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Run_20_End,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_File_20_Ref,TERMINAL(3),TERMINAL(4),ROOT(5));

result = vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_End_case_1_local_7(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(6)
}

/* Stop Universals */






Nat4	loadClasses_Project_20_Update_20_Activity(V_Environment environment);
Nat4	loadClasses_Project_20_Update_20_Activity(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("Project Update Activity",environment);
	if(result == NULL) return kERROR;
	VPLC_Project_20_Update_20_Activity_class_load(result,environment);
	result = class_new("PUAS Update Values",environment);
	if(result == NULL) return kERROR;
	VPLC_PUAS_20_Update_20_Values_class_load(result,environment);
	result = class_new("PUAS Update VPZ File",environment);
	if(result == NULL) return kERROR;
	VPLC_PUAS_20_Update_20_VPZ_20_File_class_load(result,environment);
	result = class_new("PUAS Update Bundles",environment);
	if(result == NULL) return kERROR;
	VPLC_PUAS_20_Update_20_Bundles_class_load(result,environment);
	result = class_new("PUAS Update Resources",environment);
	if(result == NULL) return kERROR;
	VPLC_PUAS_20_Update_20_Resources_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_Project_20_Update_20_Activity(V_Environment environment);
Nat4	loadUniversals_Project_20_Update_20_Activity(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("Project Update Activity/Error",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Update_20_Activity_2F_Error,NULL);

	result = method_new("Project Update Activity/Get Project Helper",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Update_20_Activity_2F_Get_20_Project_20_Helper,NULL);

	result = method_new("Project Update Activity/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Update_20_Activity_2F_Run,NULL);

	result = method_new("Project Update Activity/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Update_20_Activity_2F_Run_20_End,NULL);

	result = method_new("Project Update Activity/Initialize Progress Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Project_20_Update_20_Activity_2F_Initialize_20_Progress_20_Window,NULL);

	result = method_new("PUAS Update Values/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Values_2F_Run,NULL);

	result = method_new("PUAS Update Values/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Values_2F_Run_20_Begin,NULL);

	result = method_new("PUAS Update Values/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Values_2F_Run_20_End,NULL);

	result = method_new("PUAS Update VPZ File/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_VPZ_20_File_2F_Run,NULL);

	result = method_new("PUAS Update Bundles/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Bundles_2F_Run,NULL);

	result = method_new("PUAS Update Bundles/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_Begin,NULL);

	result = method_new("PUAS Update Bundles/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Bundles_2F_Run_20_End,NULL);

	result = method_new("PUAS Update Resources/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Resources_2F_Run,NULL);

	result = method_new("PUAS Update Resources/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_Begin,NULL);

	result = method_new("PUAS Update Resources/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_PUAS_20_Update_20_Resources_2F_Run_20_End,NULL);

	return kNOERROR;

}



Nat4	loadPersistents_Project_20_Update_20_Activity(V_Environment environment);
Nat4	loadPersistents_Project_20_Update_20_Activity(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	return kNOERROR;

}

Nat4	load_Project_20_Update_20_Activity(V_Environment environment);
Nat4	load_Project_20_Update_20_Activity(V_Environment environment)
{

	loadClasses_Project_20_Update_20_Activity(environment);
	loadUniversals_Project_20_Update_20_Activity(environment);
	loadPersistents_Project_20_Update_20_Activity(environment);
	return kNOERROR;

}

