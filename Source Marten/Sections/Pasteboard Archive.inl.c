/* A VPL Section File */
/*

Pasteboard Archive.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_Object_20_Archive_20_Abstract_2F_Name[] = {
0000000000, 0X00000020, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Object_20_Archive_20_Abstract_2F_Archive_20_Size[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_List_20_Item_20_Archive_2F_Name[] = {
0000000000, 0X00000020, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0000000000, 0000000000
	};
	Nat4 tempAttribute_List_20_Item_20_Archive_2F_Archive_20_Size[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Value_20_Item_20_Archive_2F_Name[] = {
0000000000, 0X00000020, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Value_20_Item_20_Archive_2F_Archive_20_Size[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Operation_20_Item_20_Archive_2F_Name[] = {
0000000000, 0X00000020, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Operation_20_Item_20_Archive_2F_Archive_20_Size[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Identifier[] = {
0000000000, 0X00000040, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000022, 0X636F6D2E, 0X616E6465, 0X73636F74,
0X69612E6D, 0X61727465, 0X6E2E6172, 0X63686976, 0X652E6C69, 0X73740000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Sender_20_Only_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Sender_20_Translated_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Not_20_Saved_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Request_20_Only_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_System_20_Translated_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Promised_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Exclude_20_Types[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Archive_20_Types[] = {
0000000000, 0X00000050, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000011, 0X4C697374,
0X20497465, 0X6D204172, 0X63686976, 0X65000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Identifier[] = {
0000000000, 0X00000040, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000023, 0X636F6D2E, 0X616E6465, 0X73636F74,
0X69612E6D, 0X61727465, 0X6E2E6172, 0X63686976, 0X652E7661, 0X6C756500
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Sender_20_Only_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Sender_20_Translated_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Not_20_Saved_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Request_20_Only_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_System_20_Translated_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Promised_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Exclude_20_Types[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Archive_20_Types[] = {
0000000000, 0X00000050, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000012, 0X56616C75,
0X65204974, 0X656D2041, 0X72636869, 0X76650000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Identifier[] = {
0000000000, 0X00000048, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000028, 0X636F6D2E, 0X616E6465, 0X73636F74,
0X69612E6D, 0X61727465, 0X6E2E6172, 0X63686976, 0X652E6F70, 0X65726174, 0X696F6E73, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Sender_20_Only_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Sender_20_Translated_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Not_20_Saved_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Request_20_Only_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_System_20_Translated_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Promised_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Exclude_20_Types[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Archive_20_Types[] = {
0000000000, 0X00000054, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000016, 0X4F706572,
0X6174696F, 0X6E204974, 0X656D2041, 0X72636869, 0X76650000
	};


Nat4 VPLC_Object_20_Archive_20_Abstract_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Object_20_Archive_20_Abstract_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Object_20_Archive_20_Abstract_2F_Name,environment);
	tempAttribute = attribute_add("Archive",tempClass,NULL,environment);
	tempAttribute = attribute_add("Archive Size",tempClass,tempAttribute_Object_20_Archive_20_Abstract_2F_Archive_20_Size,environment);
	tempAttribute = attribute_add("Text",tempClass,NULL,environment);
	tempAttribute = attribute_add("URL Text",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 404 233 }{ 369 329 } */
enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Archive_20_Preflight,3,2,TERMINAL(0),TERMINAL(1),TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_object_2D_to_2D_archive,1,2,TERMINAL(3),ROOT(5),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Archive_20_Postflight,4,0,TERMINAL(0),TERMINAL(3),TERMINAL(4),TERMINAL(2));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
OUTPUT(1,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1),TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Archive,2,0,TERMINAL(0),TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Archive_20_Size,2,0,TERMINAL(0),TERMINAL(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Archive_20_MetaData,3,0,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Close_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Archive,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Archive_20_Size,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Extract_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Archive,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_archive_2D_to_2D_object,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Archive_20_Preflight(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
OUTPUT(1,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Archive_20_Postflight(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive_20_MetaData(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Name(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Name,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Name(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Name,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Archive,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Archive,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Text,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Archive_20_Size(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Archive_20_Size,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Archive_20_Size(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Archive_20_Size,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Clean_20_Up(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close_20_Archive,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_URL_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_URL_20_Text,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_URL_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_URL_20_Text,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

/* Stop Universals */



Nat4 VPLC_List_20_Item_20_Archive_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_List_20_Item_20_Archive_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_List_20_Item_20_Archive_2F_Name,environment);
	tempAttribute = attribute_add("Archive",tempClass,NULL,environment);
	tempAttribute = attribute_add("Archive Size",tempClass,tempAttribute_List_20_Item_20_Archive_2F_Archive_20_Size,environment);
	tempAttribute = attribute_add("Text",tempClass,NULL,environment);
	tempAttribute = attribute_add("URL Text",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Object Archive Abstract");
	return kNOERROR;
}

/* Start Universals: { 349 457 }{ 200 300 } */
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_4_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_4_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
FAILONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_detach_2D_l,1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
FAILONFAILURE

result = vpx_method_Get_20_Item_20_Helper(PARAMETERS,TERMINAL(1),ROOT(3));
FAILONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_type,1,1,TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_4_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Name,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(9)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Project,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(5));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_To_20_Text,2,1,TERMINAL(7),TERMINAL(3),ROOT(8));

result = kSuccess;

OUTPUT(0,TERMINAL(8))
FOOTER(9)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Value Entry Text",ROOT(1));

result = vpx_constant(PARAMETERS,"Name",ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Content,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_method_Find_20_Instance(PARAMETERS,TERMINAL(4),TERMINAL(2),TERMINAL(1),ROOT(5),ROOT(6));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(6));
FAILONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Text,1,1,TERMINAL(6),ROOT(7));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTER(8)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Value Item",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1_local_4(PARAMETERS,TERMINAL(0),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Helper Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Path_20_Name,1,1,TERMINAL(0),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"List Item",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_method_Get_20_Item_20_Helper(PARAMETERS,TERMINAL(3),ROOT(4));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Path_20_Name,1,1,TERMINAL(4),ROOT(5));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Item Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Path_20_Name,1,1,TERMINAL(3),ROOT(4));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_5(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_5(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2_case_5(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2_case_1_local_2(PARAMETERS,LIST(0),ROOT(1));
FAILONFAILURE
LISTROOT(1,0)
REPEATFINISH
LISTROOTFINISH(1,0)
LISTROOTEND
} else {
ROOTEMPTY(1)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Helper Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(2)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Get_20_Item_20_Helper(PARAMETERS,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUTF8,ROOT(3));

PUTPOINTER(__CFString,*,CFURLCreateStringByAddingPercentEscapes( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(2)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_method_From_20_CFStringRef(PARAMETERS,TERMINAL(4),ROOT(5));

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"\"\"",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_URL,1,1,TERMINAL(3),ROOT(4));

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1_local_5(PARAMETERS,TERMINAL(4),ROOT(5));

result = vpx_match(PARAMETERS,"\"\"",TERMINAL(5));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
FAILONSUCCESS

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2_case_1_local_3(PARAMETERS,LIST(0),ROOT(1));
LISTROOT(1,0)
REPEATFINISH
LISTROOTFINISH(1,0)
LISTROOTEND
} else {
ROOTEMPTY(1)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_URL_20_Text,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method__28_Check_29_(PARAMETERS,TERMINAL(1),ROOT(3));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Create_20_Archive,3,0,TERMINAL(0),TERMINAL(3),TERMINAL(2));

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_4(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_6(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Archive_20_Preflight(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(2)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Isolate_20_First,1,2,LIST(1),ROOT(3),ROOT(4));
LISTROOT(3,0)
LISTROOT(4,1)
REPEATFINISH
LISTROOTFINISH(3,0)
LISTROOTFINISH(4,1)
LISTROOTEND
} else {
ROOTEMPTY(3)
ROOTEMPTY(4)
}

result = kSuccess;

OUTPUT(0,TERMINAL(3))
OUTPUT(1,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_List_20_Item_20_Archive_2F_Archive_20_Postflight(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(2,TERMINAL(1),TERMINAL(2))) ){
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Integrate_20_First,2,0,LIST(1),LIST(2));
REPEATFINISH
} else {
}

result = kSuccess;

FOOTERSINGLECASE(4)
}

/* Stop Universals */



Nat4 VPLC_Value_20_Item_20_Archive_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Value_20_Item_20_Archive_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Value_20_Item_20_Archive_2F_Name,environment);
	tempAttribute = attribute_add("Archive",tempClass,NULL,environment);
	tempAttribute = attribute_add("Archive Size",tempClass,tempAttribute_Value_20_Item_20_Archive_2F_Archive_20_Size,environment);
	tempAttribute = attribute_add("Text",tempClass,NULL,environment);
	tempAttribute = attribute_add("URL Text",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Object Archive Abstract");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
FAILONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_constant(PARAMETERS,"archive",ROOT(4));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Value_20_Data,3,1,TERMINAL(3),LIST(0),TERMINAL(4),ROOT(5));
LISTROOT(5,0)
REPEATFINISH
LISTROOTFINISH(5,0)
LISTROOTEND
} else {
ROOTEMPTY(5)
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_To_20_Text,2,1,TERMINAL(3),LIST(0),ROOT(6));
LISTROOT(6,0)
REPEATFINISH
LISTROOTFINISH(6,0)
LISTROOTEND
} else {
ROOTEMPTY(6)
}

result = kSuccess;

OUTPUT(0,TERMINAL(5))
OUTPUT(1,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5_case_1_local_2(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(0),TERMINAL(3));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method__28_Check_29_(PARAMETERS,TERMINAL(1),ROOT(3));

result = vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(2),ROOT(4),ROOT(5));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Create_20_Archive,3,0,TERMINAL(0),TERMINAL(4),TERMINAL(2));

result = vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive_case_1_local_5(PARAMETERS,TERMINAL(0),TERMINAL(5),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Value_20_Item_20_Archive_2F_Extract_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
FAILONFAILURE

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Extract_20_Archive,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));
FAILONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP_list_3F_,1,0,TERMINAL(2));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"archive",ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Environment,TERMINAL(1),ROOT(4),ROOT(5));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(2))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Object_20_To_20_Value,3,1,TERMINAL(5),LIST(2),TERMINAL(3),ROOT(6));
LISTROOT(6,0)
REPEATFINISH
LISTROOTFINISH(6,0)
LISTROOTEND
} else {
ROOTEMPTY(6)
}

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

/* Stop Universals */



Nat4 VPLC_Operation_20_Item_20_Archive_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Operation_20_Item_20_Archive_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Operation_20_Item_20_Archive_2F_Name,environment);
	tempAttribute = attribute_add("Archive",tempClass,NULL,environment);
	tempAttribute = attribute_add("Archive Size",tempClass,tempAttribute_Operation_20_Item_20_Archive_2F_Archive_20_Size,environment);
	tempAttribute = attribute_add("Text",tempClass,NULL,environment);
	tempAttribute = attribute_add("URL Text",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Object Archive Abstract");
	return kNOERROR;
}

/* Start Universals: { 620 1117 }{ 200 300 } */
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1,V_Object *root2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1,V_Object *root2)
{
HEADERWITHNONE(6)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_type,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"Comment Item",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Isolate_20_First,1,2,TERMINAL(3),ROOT(4),ROOT(5));

result = kSuccess;

OUTPUT(0,NONE)
OUTPUT(1,TERMINAL(4))
OUTPUT(2,TERMINAL(5))
FOOTERWITHNONE(6)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1,V_Object *root2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1,V_Object *root2)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(0))
OUTPUT(1,NONE)
OUTPUT(2,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1,V_Object *root2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1,V_Object *root2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1,root2))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0,root1,root2);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Outputs,TERMINAL(4),ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(2),ROOT(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_9_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_9_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"0",TERMINAL(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Root,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Root,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_9_case_1_local_4(PARAMETERS,TERMINAL(2),TERMINAL(4));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(1),TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"0",TERMINAL(2));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(21)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Synchros,TERMINAL(6),ROOT(7),ROOT(8));

result = vpx_get(PARAMETERS,kVPXValue_Inputs,TERMINAL(7),ROOT(9),ROOT(10));

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(9),ROOT(11),ROOT(12));

result = vpx_constant(PARAMETERS,"NULL",ROOT(13));

result = vpx_set(PARAMETERS,kVPXValue_Owner,TERMINAL(11),TERMINAL(13),ROOT(14));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(10))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_9(PARAMETERS,LIST(10),TERMINAL(1),ROOT(15));
LISTROOT(15,0)
REPEATFINISH
LISTROOTFINISH(15,0)
LISTROOTEND
} else {
ROOTEMPTY(15)
}

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Isolate_20_First,1,2,TERMINAL(4),ROOT(16),ROOT(17));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,4,1,TERMINAL(17),TERMINAL(12),TERMINAL(15),TERMINAL(8),ROOT(18));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(8))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1_local_12(PARAMETERS,LIST(8),TERMINAL(2),ROOT(19));
LISTROOT(19,0)
REPEATFINISH
LISTROOTFINISH(19,0)
LISTROOTEND
} else {
ROOTEMPTY(19)
}

result = vpx_set(PARAMETERS,kVPXValue_Synchros,TERMINAL(14),TERMINAL(19),ROOT(20));

result = kSuccess;

OUTPUT(0,TERMINAL(16))
OUTPUT(1,TERMINAL(18))
FOOTER(21)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_13_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_13_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"0",TERMINAL(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Root,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Root,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_13_case_1_local_4(PARAMETERS,TERMINAL(2),TERMINAL(4));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP__28_in_29_,2,1,TERMINAL(1),TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"0",TERMINAL(2));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(26)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Controller,TERMINAL(5),ROOT(7),ROOT(8));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(7),ROOT(9),ROOT(10));

result = vpx_constant(PARAMETERS,"NULL",ROOT(11));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(9),TERMINAL(11),ROOT(12));

result = vpx_get(PARAMETERS,kVPXValue_Synchros,TERMINAL(6),ROOT(13),ROOT(14));

result = vpx_get(PARAMETERS,kVPXValue_Inputs,TERMINAL(13),ROOT(15),ROOT(16));

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(15),ROOT(17),ROOT(18));

result = vpx_constant(PARAMETERS,"NULL",ROOT(19));

result = vpx_set(PARAMETERS,kVPXValue_Owner,TERMINAL(17),TERMINAL(19),ROOT(20));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(16))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_13(PARAMETERS,LIST(16),TERMINAL(1),ROOT(21));
LISTROOT(21,0)
REPEATFINISH
LISTROOTFINISH(21,0)
LISTROOTEND
} else {
ROOTEMPTY(21)
}

result = vpx_call_primitive(PARAMETERS,VPLP_pack,5,1,TERMINAL(10),TERMINAL(8),TERMINAL(18),TERMINAL(21),TERMINAL(14),ROOT(22));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(14))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2_local_15(PARAMETERS,LIST(14),TERMINAL(2),ROOT(23));
LISTROOT(23,0)
REPEATFINISH
LISTROOTFINISH(23,0)
LISTROOTEND
} else {
ROOTEMPTY(23)
}

result = vpx_set(PARAMETERS,kVPXValue_Synchros,TERMINAL(20),TERMINAL(23),ROOT(24));

result = vpx_set(PARAMETERS,kVPXValue_Controller,TERMINAL(12),TERMINAL(11),ROOT(25));

result = kSuccess;

OUTPUT(0,TERMINAL(25))
OUTPUT(1,TERMINAL(22))
FOOTER(26)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0,root1))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0,root1);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,4,TERMINAL(1),ROOT(2),ROOT(3),ROOT(4),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Integrate_20_First,2,0,TERMINAL(0),TERMINAL(2));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(0),ROOT(6),ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Owner,TERMINAL(7),TERMINAL(3),ROOT(8));

result = vpx_get(PARAMETERS,kVPXValue_Inputs,TERMINAL(8),ROOT(9),ROOT(10));

if( (repeatLimit = vpx_multiplex(2,TERMINAL(10),TERMINAL(4))) ){
REPEATBEGINWITHCOUNTER
result = vpx_set(PARAMETERS,kVPXValue_Root,LIST(10),LIST(4),ROOT(11));
REPEATFINISH
} else {
ROOTNULL(11,NULL)
}

result = vpx_set(PARAMETERS,kVPXValue_Synchros,TERMINAL(9),TERMINAL(5),ROOT(12));

result = kSuccess;

FOOTER(13)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(16)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,5,TERMINAL(1),ROOT(2),ROOT(3),ROOT(4),ROOT(5),ROOT(6));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(2),ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Controller,TERMINAL(7),TERMINAL(3),ROOT(8));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(8),ROOT(9),ROOT(10));

result = vpx_set(PARAMETERS,kVPXValue_Owner,TERMINAL(10),TERMINAL(4),ROOT(11));

result = vpx_get(PARAMETERS,kVPXValue_Inputs,TERMINAL(11),ROOT(12),ROOT(13));

if( (repeatLimit = vpx_multiplex(2,TERMINAL(13),TERMINAL(5))) ){
REPEATBEGINWITHCOUNTER
result = vpx_set(PARAMETERS,kVPXValue_Root,LIST(13),LIST(5),ROOT(14));
REPEATFINISH
} else {
ROOTNULL(14,NULL)
}

result = vpx_set(PARAMETERS,kVPXValue_Synchros,TERMINAL(12),TERMINAL(6),ROOT(15));

result = kSuccess;

FOOTER(16)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Integrate_20_First,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Archive_20_MetaData,3,0,TERMINAL(0),TERMINAL(1),TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Helper Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Path_20_Name,1,1,TERMINAL(0),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2_case_1_local_2(PARAMETERS,LIST(0),ROOT(1));
FAILONFAILURE
LISTROOT(1,0)
REPEATFINISH
LISTROOTFINISH(1,0)
LISTROOTEND
} else {
ROOTEMPTY(1)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Helper Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Get_20_Item_20_Helper(PARAMETERS,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUTF8,ROOT(3));

PUTPOINTER(__CFString,*,CFURLCreateStringByAddingPercentEscapes( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(2)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_method_From_20_CFStringRef(PARAMETERS,TERMINAL(4),ROOT(5));

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"\"\"",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_URL,1,1,TERMINAL(3),ROOT(4));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1_local_5(PARAMETERS,TERMINAL(4),ROOT(5));

result = vpx_match(PARAMETERS,"\"\"",TERMINAL(5));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
FAILONSUCCESS

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2_case_1_local_3(PARAMETERS,LIST(0),ROOT(1));
LISTROOT(1,0)
REPEATFINISH
LISTROOTFINISH(1,0)
LISTROOTEND
} else {
ROOTEMPTY(1)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_URL_20_Text,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_get(PARAMETERS,kVPXValue_Owner,LIST(1),ROOT(3),ROOT(4));
LISTROOT(4,0)
REPEATFINISH
LISTROOTFINISH(4,0)
LISTROOTEND
} else {
ROOTNULL(3,NULL)
ROOTEMPTY(4)
}

result = vpx_method_Put_20_URLs_20_On_20_Scrap(PARAMETERS,TERMINAL(4));

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUTF8,ROOT(3));

PUTPOINTER(__CFString,*,CFURLCreateStringByAddingPercentEscapes( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(2)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_method_From_20_CFStringRef(PARAMETERS,TERMINAL(4),ROOT(5));

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"\"\"",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_URL,1,1,TERMINAL(2),ROOT(3));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5_case_1_local_4(PARAMETERS,TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADERWITHNONE(14)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

PUTINTEGER(GetCurrentScrap( GETPOINTER(0,OpaqueScrapRef,**,ROOT(4),NONE)),3);
result = kSuccess;

result = vpx_constant(PARAMETERS,"\'TEXT\'",ROOT(5));

result = vpx_extconstant(PARAMETERS,kScrapFlavorMaskNone,ROOT(6));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4_local_5(PARAMETERS,LIST(1),ROOT(7));
LISTROOT(7,0)
REPEATFINISH
LISTROOTFINISH(7,0)
LISTROOTEND
} else {
ROOTEMPTY(7)
}

result = vpx_constant(PARAMETERS,"( 13 )",ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_from_2D_ascii,1,1,TERMINAL(8),ROOT(9));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(7),TERMINAL(9),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP_byte_2D_length,1,1,TERMINAL(10),ROOT(11));

PUTINTEGER(PutScrapFlavor( GETPOINTER(0,OpaqueScrapRef,*,ROOT(13),TERMINAL(4)),GETINTEGER(TERMINAL(5)),GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(11)),GETCONSTPOINTER(void,*,TERMINAL(10))),12);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(14)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(15)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(3)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_2(PARAMETERS,LIST(1),ROOT(3),ROOT(4),ROOT(5));
LISTROOT(3,0)
LISTROOT(4,1)
LISTROOT(5,2)
REPEATFINISH
LISTROOTFINISH(3,0)
LISTROOTFINISH(4,1)
LISTROOTFINISH(5,2)
LISTROOTEND
} else {
ROOTEMPTY(3)
ROOTEMPTY(4)
ROOTEMPTY(5)
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(3))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_3(PARAMETERS,LIST(3),ROOT(6));
LISTROOT(6,0)
REPEATFINISH
LISTROOTFINISH(6,0)
LISTROOTEND
} else {
ROOTEMPTY(6)
}

result = vpx_method_Lists_20_To_20_List(PARAMETERS,TERMINAL(6),ROOT(7));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(3))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_5(PARAMETERS,LIST(3),ROOT(8));
LISTROOT(8,0)
REPEATFINISH
LISTROOTFINISH(8,0)
LISTROOTEND
} else {
ROOTEMPTY(8)
}

if( (repeatLimit = vpx_multiplex(1,TERMINAL(3))) ){
LISTROOTBEGIN(2)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_6(PARAMETERS,LIST(3),TERMINAL(7),TERMINAL(8),ROOT(9),ROOT(10));
LISTROOT(9,0)
LISTROOT(10,1)
REPEATFINISH
LISTROOTFINISH(9,0)
LISTROOTFINISH(10,1)
LISTROOTEND
} else {
ROOTEMPTY(9)
ROOTEMPTY(10)
}

result = vpx_call_primitive(PARAMETERS,VPLP__28_join_29_,2,1,TERMINAL(9),TERMINAL(4),ROOT(11));

result = vpx_call_primitive(PARAMETERS,VPLP_object_2D_to_2D_archive,1,2,TERMINAL(11),ROOT(12),ROOT(13));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Archive,2,0,TERMINAL(0),TERMINAL(12));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Archive_20_Size,2,0,TERMINAL(0),TERMINAL(13));

if( (repeatLimit = vpx_multiplex(2,TERMINAL(9),TERMINAL(10))) ){
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_11(PARAMETERS,LIST(9),LIST(10));
REPEATFINISH
} else {
}

if( (repeatLimit = vpx_multiplex(2,TERMINAL(4),TERMINAL(5))) ){
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_12(PARAMETERS,LIST(4),LIST(5));
REPEATFINISH
} else {
}

result = vpx_call_primitive(PARAMETERS,VPLP__28_join_29_,2,1,TERMINAL(8),TERMINAL(4),ROOT(14));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive_case_1_local_14(PARAMETERS,TERMINAL(0),TERMINAL(14),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(15)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Helper Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Path_20_Name,1,1,TERMINAL(0),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Name,1,1,TERMINAL(0),ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2_case_1_local_2(PARAMETERS,LIST(0),ROOT(1));
FAILONFAILURE
LISTROOT(1,0)
REPEATFINISH
LISTROOTFINISH(1,0)
LISTROOTEND
} else {
ROOTEMPTY(1)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(0),TERMINAL(2));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Helper Data",ROOT(1));

result = vpx_method_Is_20_Or_20_Is_20_Descendant_3F_(PARAMETERS,TERMINAL(0),TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Get_20_Item_20_Helper(PARAMETERS,TERMINAL(0),ROOT(1));
FAILONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUTF8,ROOT(3));

PUTPOINTER(__CFString,*,CFURLCreateStringByAddingPercentEscapes( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(2)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETCONSTPOINTER(__CFString,*,TERMINAL(1)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_method_From_20_CFStringRef(PARAMETERS,TERMINAL(4),ROOT(5));

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"\"\"",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_2(PARAMETERS,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Owner,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_URL,1,1,TERMINAL(3),ROOT(4));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1_local_5(PARAMETERS,TERMINAL(4),ROOT(5));

result = vpx_match(PARAMETERS,"\"\"",TERMINAL(5));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(4)
INPUT(0,0)
result = kSuccess;

result = vpx_match(PARAMETERS,"( )",TERMINAL(0));
FAILONSUCCESS

if( (repeatLimit = vpx_multiplex(1,TERMINAL(0))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2_case_1_local_3(PARAMETERS,LIST(0),ROOT(1));
LISTROOT(1,0)
REPEATFINISH
LISTROOTFINISH(1,0)
LISTROOTEND
} else {
ROOTEMPTY(1)
}

result = vpx_match(PARAMETERS,"( )",TERMINAL(1));
FAILONSUCCESS

result = vpx_method_Get_20_Return_20_String(PARAMETERS,NONE,ROOT(2));

result = vpx_method__28_To_20_String_29_(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASEWITHNONE(4)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3_case_1_local_2(PARAMETERS,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_URL_20_Text,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Archive_20_Preflight(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0,V_Object *root1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(1,TERMINAL(1))) ){
LISTROOTBEGIN(2)
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Isolate_20_First,1,2,LIST(1),ROOT(3),ROOT(4));
LISTROOT(3,0)
LISTROOT(4,1)
REPEATFINISH
LISTROOTFINISH(3,0)
LISTROOTFINISH(4,1)
LISTROOTEND
} else {
ROOTEMPTY(3)
ROOTEMPTY(4)
}

result = kSuccess;

OUTPUT(0,TERMINAL(3))
OUTPUT(1,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Operation_20_Item_20_Archive_2F_Archive_20_Postflight(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

if( (repeatLimit = vpx_multiplex(2,TERMINAL(1),TERMINAL(2))) ){
REPEATBEGINWITHCOUNTER
result = vpx_call_data_method(PARAMETERS,kVPXMethod_Integrate_20_First,2,0,LIST(1),LIST(2));
REPEATFINISH
} else {
}

result = kSuccess;

FOOTERSINGLECASE(4)
}

/* Stop Universals */



Nat4 VPLC_Pasteboard_20_Flavor_20_Archive_20_List_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Pasteboard_20_Flavor_20_Archive_20_List_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Identifier",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Identifier,environment);
	tempAttribute = attribute_add("Sender Only?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Sender_20_Only_3F_,environment);
	tempAttribute = attribute_add("Sender Translated?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Sender_20_Translated_3F_,environment);
	tempAttribute = attribute_add("Not Saved?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Not_20_Saved_3F_,environment);
	tempAttribute = attribute_add("Request Only?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Request_20_Only_3F_,environment);
	tempAttribute = attribute_add("System Translated?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_System_20_Translated_3F_,environment);
	tempAttribute = attribute_add("Promised?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Promised_3F_,environment);
	tempAttribute = attribute_add("Extracted Value",tempClass,NULL,environment);
	tempAttribute = attribute_add("Exclude Types",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Exclude_20_Types,environment);
	tempAttribute = attribute_add("Archive Types",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_List_2F_Archive_20_Types,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Pasteboard Flavor Archive");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
/* Stop Universals */



Nat4 VPLC_Pasteboard_20_Flavor_20_Archive_20_Value_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Pasteboard_20_Flavor_20_Archive_20_Value_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Identifier",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Identifier,environment);
	tempAttribute = attribute_add("Sender Only?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Sender_20_Only_3F_,environment);
	tempAttribute = attribute_add("Sender Translated?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Sender_20_Translated_3F_,environment);
	tempAttribute = attribute_add("Not Saved?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Not_20_Saved_3F_,environment);
	tempAttribute = attribute_add("Request Only?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Request_20_Only_3F_,environment);
	tempAttribute = attribute_add("System Translated?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_System_20_Translated_3F_,environment);
	tempAttribute = attribute_add("Promised?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Promised_3F_,environment);
	tempAttribute = attribute_add("Extracted Value",tempClass,NULL,environment);
	tempAttribute = attribute_add("Exclude Types",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Exclude_20_Types,environment);
	tempAttribute = attribute_add("Archive Types",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Archive_20_Types,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Pasteboard Flavor Archive");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
/* Stop Universals */



Nat4 VPLC_Pasteboard_20_Flavor_20_Archive_20_Operations_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Pasteboard_20_Flavor_20_Archive_20_Operations_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 442 521 }{ 222 449 } */
	tempAttribute = attribute_add("Identifier",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Identifier,environment);
	tempAttribute = attribute_add("Sender Only?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Sender_20_Only_3F_,environment);
	tempAttribute = attribute_add("Sender Translated?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Sender_20_Translated_3F_,environment);
	tempAttribute = attribute_add("Not Saved?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Not_20_Saved_3F_,environment);
	tempAttribute = attribute_add("Request Only?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Request_20_Only_3F_,environment);
	tempAttribute = attribute_add("System Translated?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_System_20_Translated_3F_,environment);
	tempAttribute = attribute_add("Promised?",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Promised_3F_,environment);
	tempAttribute = attribute_add("Extracted Value",tempClass,NULL,environment);
	tempAttribute = attribute_add("Exclude Types",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Exclude_20_Types,environment);
	tempAttribute = attribute_add("Archive Types",tempClass,tempAttribute_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Archive_20_Types,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Pasteboard Flavor Archive");
	return kNOERROR;
}

/* Start Universals: { 60 60 }{ 200 300 } */
/* Stop Universals */






Nat4	loadClasses_Pasteboard_20_Archive(V_Environment environment);
Nat4	loadClasses_Pasteboard_20_Archive(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("Object Archive Abstract",environment);
	if(result == NULL) return kERROR;
	VPLC_Object_20_Archive_20_Abstract_class_load(result,environment);
	result = class_new("List Item Archive",environment);
	if(result == NULL) return kERROR;
	VPLC_List_20_Item_20_Archive_class_load(result,environment);
	result = class_new("Value Item Archive",environment);
	if(result == NULL) return kERROR;
	VPLC_Value_20_Item_20_Archive_class_load(result,environment);
	result = class_new("Operation Item Archive",environment);
	if(result == NULL) return kERROR;
	VPLC_Operation_20_Item_20_Archive_class_load(result,environment);
	result = class_new("Pasteboard Flavor Archive List",environment);
	if(result == NULL) return kERROR;
	VPLC_Pasteboard_20_Flavor_20_Archive_20_List_class_load(result,environment);
	result = class_new("Pasteboard Flavor Archive Value",environment);
	if(result == NULL) return kERROR;
	VPLC_Pasteboard_20_Flavor_20_Archive_20_Value_class_load(result,environment);
	result = class_new("Pasteboard Flavor Archive Operations",environment);
	if(result == NULL) return kERROR;
	VPLC_Pasteboard_20_Flavor_20_Archive_20_Operations_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_Pasteboard_20_Archive(V_Environment environment);
Nat4	loadUniversals_Pasteboard_20_Archive(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("Object Archive Abstract/Create Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive,NULL);

	result = method_new("Object Archive Abstract/Close Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Close_20_Archive,NULL);

	result = method_new("Object Archive Abstract/Extract Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Extract_20_Archive,NULL);

	result = method_new("Object Archive Abstract/Archive Preflight",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Archive_20_Preflight,NULL);

	result = method_new("Object Archive Abstract/Archive Postflight",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Archive_20_Postflight,NULL);

	result = method_new("Object Archive Abstract/Create Archive MetaData",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Create_20_Archive_20_MetaData,NULL);

	result = method_new("Object Archive Abstract/Get Name",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Name,NULL);

	result = method_new("Object Archive Abstract/Set Name",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Name,NULL);

	result = method_new("Object Archive Abstract/Get Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Archive,NULL);

	result = method_new("Object Archive Abstract/Set Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Archive,NULL);

	result = method_new("Object Archive Abstract/Get Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Text,NULL);

	result = method_new("Object Archive Abstract/Set Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Text,NULL);

	result = method_new("Object Archive Abstract/Get Archive Size",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_Archive_20_Size,NULL);

	result = method_new("Object Archive Abstract/Set Archive Size",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_Archive_20_Size,NULL);

	result = method_new("Object Archive Abstract/Clean Up",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Clean_20_Up,"Destructor");

	result = method_new("Object Archive Abstract/Get URL Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Get_20_URL_20_Text,NULL);

	result = method_new("Object Archive Abstract/Set URL Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Object_20_Archive_20_Abstract_2F_Set_20_URL_20_Text,NULL);

	result = method_new("List Item Archive/Create Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_List_20_Item_20_Archive_2F_Create_20_Archive,NULL);

	result = method_new("List Item Archive/Archive Preflight",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_List_20_Item_20_Archive_2F_Archive_20_Preflight,NULL);

	result = method_new("List Item Archive/Archive Postflight",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_List_20_Item_20_Archive_2F_Archive_20_Postflight,NULL);

	result = method_new("Value Item Archive/Create Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Value_20_Item_20_Archive_2F_Create_20_Archive,NULL);

	result = method_new("Value Item Archive/Extract Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Value_20_Item_20_Archive_2F_Extract_20_Archive,NULL);

	result = method_new("Operation Item Archive/Create Operations Archive",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Operations_20_Archive,NULL);

	result = method_new("Operation Item Archive/Create Archive MetaData",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Operation_20_Item_20_Archive_2F_Create_20_Archive_20_MetaData,NULL);

	result = method_new("Operation Item Archive/Archive Preflight",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Operation_20_Item_20_Archive_2F_Archive_20_Preflight,NULL);

	result = method_new("Operation Item Archive/Archive Postflight",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Operation_20_Item_20_Archive_2F_Archive_20_Postflight,NULL);

	return kNOERROR;

}


	Nat4 tempPersistent_Pasteboard_20_Flavor_20_Archive_20_List_2F_Known_20_IDs[] = {
0000000000, 0X00000060, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000022, 0X636F6D2E,
0X616E6465, 0X73636F74, 0X69612E6D, 0X61727465, 0X6E2E6172, 0X63686976, 0X652E6C69, 0X73740000
	};
	Nat4 tempPersistent_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Known_20_IDs[] = {
0000000000, 0X00000060, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000023, 0X636F6D2E,
0X616E6465, 0X73636F74, 0X69612E6D, 0X61727465, 0X6E2E6172, 0X63686976, 0X652E7661, 0X6C756500
	};
	Nat4 tempPersistent_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Known_20_IDs[] = {
0000000000, 0X00000068, 0X00000020, 0X00000003, 0X00000014, 0X00000054, 0X0000003C, 0X00000034,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000003C, 0X00000001, 0X00000040,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000005C, 0X00000028, 0X636F6D2E,
0X616E6465, 0X73636F74, 0X69612E6D, 0X61727465, 0X6E2E6172, 0X63686976, 0X652E6F70, 0X65726174,
0X696F6E73, 0000000000
	};

Nat4	loadPersistents_Pasteboard_20_Archive(V_Environment environment);
Nat4	loadPersistents_Pasteboard_20_Archive(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	tempPersistent = create_persistentNode("Pasteboard Flavor Archive List/Known IDs",tempPersistent_Pasteboard_20_Flavor_20_Archive_20_List_2F_Known_20_IDs,environment);
	add_node(dictionary,tempPersistent->objectName,tempPersistent);

	tempPersistent = create_persistentNode("Pasteboard Flavor Archive Value/Known IDs",tempPersistent_Pasteboard_20_Flavor_20_Archive_20_Value_2F_Known_20_IDs,environment);
	add_node(dictionary,tempPersistent->objectName,tempPersistent);

	tempPersistent = create_persistentNode("Pasteboard Flavor Archive Operations/Known IDs",tempPersistent_Pasteboard_20_Flavor_20_Archive_20_Operations_2F_Known_20_IDs,environment);
	add_node(dictionary,tempPersistent->objectName,tempPersistent);

	return kNOERROR;

}

Nat4	load_Pasteboard_20_Archive(V_Environment environment);
Nat4	load_Pasteboard_20_Archive(V_Environment environment)
{

	loadClasses_Pasteboard_20_Archive(environment);
	loadUniversals_Pasteboard_20_Archive(environment);
	loadPersistents_Pasteboard_20_Archive(environment);
	return kNOERROR;

}

