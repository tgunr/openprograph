/* A VPL Section File */
/*

VPLControls.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_VPLControl_2F_Frame[] = {
0000000000, 0X0000008C, 0X00000028, 0X00000005, 0X00000014, 0X00000050, 0X0000004C, 0X00000048,
0X00000044, 0X0000003C, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000044,
0X00000004, 0X00000054, 0X0000006C, 0X00000084, 0X0000009C, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLControl_2F_Selected[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLControl_2F_Highlight[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLControl_2F_Visible_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLControl_2F_Procedure_20_ID[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLControl_2F_Title[] = {
0000000000, 0X00000030, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000011, 0X5C22436F, 0X6E74726F, 0X6C205469,
0X746C655C, 0X22000000
	};
	Nat4 tempAttribute_VPLControl_2F_Visible[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLControl_2F_Value[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLControl_2F_Minimum[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLControl_2F_Maximum[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Frame[] = {
0000000000, 0X0000008C, 0X00000028, 0X00000005, 0X00000014, 0X00000050, 0X0000004C, 0X00000048,
0X00000044, 0X0000003C, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000044,
0X00000004, 0X00000054, 0X0000006C, 0X00000084, 0X0000009C, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Selected[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Highlight[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Visible_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Procedure_20_ID[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000182
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Title[] = {
0000000000, 0X00000028, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X0D530006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000A, 0X5363726F, 0X6C6C2042, 0X61720000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Visible[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Value[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Minimum[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Maximum[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000064
	};
	Nat4 tempAttribute_VPLScroll_20_Bar_2F_Action_20_Callback[] = {
0000000000, 0X000002BC, 0X00000084, 0X0000001C, 0X00000014, 0X000000E4, 0X00000308, 0X000000D8,
0X000002D8, 0X000000D4, 0X000002A4, 0X000000D0, 0X00000270, 0X000000CC, 0X000000C8, 0X000000C4,
0X00000208, 0X000001E4, 0X000001EC, 0X00000168, 0X000001C0, 0X000001A8, 0X00000180, 0X00000188,
0X00000164, 0X0000015C, 0X000000C0, 0X00000130, 0X000000BC, 0X000000FC, 0X000000B4, 0X00000098,
0X000000A0, 0X00190008, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000B4, 0X0000000D,
0X000000A4, 0X4D657468, 0X6F642043, 0X616C6C62, 0X61636B00, 0X000000E8, 0000000000, 0X0000011C,
0X00000148, 0X00000224, 0X00000240, 0X0000025C, 0X00000290, 0X000002C4, 0X000002F4, 0000000000,
0000000000, 0X00000328, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000104,
0X00000016, 0X41637469, 0X6F6E204D, 0X6574686F, 0X64204361, 0X6C6C6261, 0X636B0000, 0X00000006,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000138, 0X0000000F, 0X2F446F20, 0X4C697665,
0X20416374, 0X696F6E00, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000164,
0X00000002, 0X0000016C, 0X000001D0, 0X00150008, 0000000000, 0000000000, 0000000000, 0000000000,
0X000001A8, 0X00000001, 0X0000018C, 0X41747472, 0X69627574, 0X6520496E, 0X70757420, 0X53706563,
0X69666965, 0X72000000, 0X000001AC, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X000001C8, 0X00000005, 0X4F776E65, 0X72000000, 0X00170008, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000208, 0X00000001, 0X000001F0, 0X41747461, 0X63686D65, 0X6E742053, 0X70656369,
0X66696572, 0000000000, 0X0000020C, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000002, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000006,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000278, 0X00000016, 0X2F446F20, 0X43616C6C,
0X6261636B, 0X204E6F20, 0X52657375, 0X6C740000, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X000002AC, 0X00000014, 0X436F6E74, 0X726F6C41, 0X6374696F, 0X6E50726F, 0X63507472,
0000000000, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000002E0, 0X00000013,
0X4E657743, 0X6F6E7472, 0X6F6C4163, 0X74696F6E, 0X55505000, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000310, 0X00000017, 0X44697370, 0X6F736543, 0X6F6E7472, 0X6F6C4163,
0X74696F6E, 0X55505000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000
	};


Nat4 VPLC_VPLControl_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLControl_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 336 385 }{ 205 376 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Item Record",tempClass,NULL,environment);
	tempAttribute = attribute_add("Name",tempClass,NULL,environment);
	tempAttribute = attribute_add("Frame",tempClass,tempAttribute_VPLControl_2F_Frame,environment);
	tempAttribute = attribute_add("Selected",tempClass,tempAttribute_VPLControl_2F_Selected,environment);
	tempAttribute = attribute_add("Highlight",tempClass,tempAttribute_VPLControl_2F_Highlight,environment);
	tempAttribute = attribute_add("Visible?",tempClass,tempAttribute_VPLControl_2F_Visible_3F_,environment);
	tempAttribute = attribute_add("Procedure ID",tempClass,tempAttribute_VPLControl_2F_Procedure_20_ID,environment);
	tempAttribute = attribute_add("Title",tempClass,tempAttribute_VPLControl_2F_Title,environment);
	tempAttribute = attribute_add("Visible",tempClass,tempAttribute_VPLControl_2F_Visible,environment);
	tempAttribute = attribute_add("Value",tempClass,tempAttribute_VPLControl_2F_Value,environment);
	tempAttribute = attribute_add("Minimum",tempClass,tempAttribute_VPLControl_2F_Minimum,environment);
	tempAttribute = attribute_add("Maximum",tempClass,tempAttribute_VPLControl_2F_Maximum,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"VPLWindow Item");
	return kNOERROR;
}

/* Start Universals: { 266 218 }{ 299 337 } */
enum opTrigger vpx_method_VPLControl_2F_Close_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLControl_2F_Close_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

DisposeControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(1),TERMINAL(0)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLControl_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_method_VPLControl_2F_Close_case_1_local_3(PARAMETERS,TERMINAL(2));

result = vpx_constant(PARAMETERS,"NULL",ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Draw_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Draw_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

ShowControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(2),TERMINAL(0)));
result = kSuccess;

Draw1Control( GETPOINTER(0,OpaqueControlRef,*,ROOT(3),TERMINAL(0)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLControl_2F_Draw_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Draw_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Draw_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Visible_3F_,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(5));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLControl_2F_Draw_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Draw_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLControl_2F_Draw(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLControl_2F_Draw_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLControl_2F_Draw_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLControl_2F_Get_20_Value_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Get_20_Value_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

PUTINTEGER(GetControlValue( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(2))),3);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Value,TERMINAL(1),TERMINAL(3),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(6)
}

enum opTrigger vpx_method_VPLControl_2F_Get_20_Value_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Get_20_Value_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Value,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLControl_2F_Get_20_Value(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLControl_2F_Get_20_Value_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLControl_2F_Get_20_Value_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLControl_2F_Hilite_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Hilite_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

HiliteControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(2),TERMINAL(0)),GETINTEGER(TERMINAL(1)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLControl_2F_Hilite_20_Control(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Hilite_20_Control_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLControl_2F_Move_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Move_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(1),ROOT(2),ROOT(3));

MoveControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(0)),GETINTEGER(TERMINAL(3)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Move_20_Control(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Move_20_Control_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLControl_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(26)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Procedure_20_ID,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Maximum,TERMINAL(3),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Minimum,TERMINAL(5),ROOT(7),ROOT(8));

result = vpx_get(PARAMETERS,kVPXValue_Value,TERMINAL(7),ROOT(9),ROOT(10));

result = vpx_get(PARAMETERS,kVPXValue_Visible,TERMINAL(9),ROOT(11),ROOT(12));

result = vpx_get(PARAMETERS,kVPXValue_Title,TERMINAL(11),ROOT(13),ROOT(14));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(13),ROOT(15),ROOT(16));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(16),ROOT(17));

result = vpx_call_primitive(PARAMETERS,VPLP_string_2D_to_2D_pointer,1,1,TERMINAL(14),ROOT(18));

CopyCStringToPascal( GETCONSTPOINTER(char,*,TERMINAL(18)),GETPOINTER(256,unsigned char,*,ROOT(19),NONE));
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(20));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(2),ROOT(21));

PUTPOINTER(OpaqueControlRef,*,NewControl( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(23),TERMINAL(21)),GETCONSTPOINTER(Rect,*,TERMINAL(17)),GETCONSTPOINTER(unsigned char,*,TERMINAL(19)),GETINTEGER(TERMINAL(12)),GETINTEGER(TERMINAL(10)),GETINTEGER(TERMINAL(8)),GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(20))),22);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(15),TERMINAL(22),ROOT(24));

ShowControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(25),TERMINAL(22)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASEWITHNONE(26)
}

enum opTrigger vpx_method_VPLControl_2F_Process_20_Click(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Local_20_Where,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Track_20_Control,2,1,TERMINAL(0),TERMINAL(3),ROOT(4));

result = vpx_match(PARAMETERS,"0",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Click_20_Control_20_Part,2,0,TERMINAL(0),TERMINAL(4));
TERMINATEONFAILURE

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Frame_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Set_20_Frame_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(10)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,4,TERMINAL(1),ROOT(2),ROOT(3),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_iminus,2,1,TERMINAL(4),TERMINAL(2),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_iminus,2,1,TERMINAL(5),TERMINAL(3),ROOT(7));

MoveControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(8),TERMINAL(0)),GETINTEGER(TERMINAL(3)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

SizeControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(9),TERMINAL(0)),GETINTEGER(TERMINAL(7)),GETINTEGER(TERMINAL(6)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(10)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Frame(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Set_20_Frame,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Set_20_Frame_case_1_local_4(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Maximum_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Set_20_Maximum_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

SetControlMaximum( GETPOINTER(0,OpaqueControlRef,*,ROOT(2),TERMINAL(0)),GETINTEGER(TERMINAL(1)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Maximum(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Set_20_Maximum_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = vpx_set(PARAMETERS,kVPXValue_Maximum,TERMINAL(2),TERMINAL(1),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Minimum_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Set_20_Minimum_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

SetControlMinimum( GETPOINTER(0,OpaqueControlRef,*,ROOT(2),TERMINAL(0)),GETINTEGER(TERMINAL(1)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Minimum(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Set_20_Minimum_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = vpx_set(PARAMETERS,kVPXValue_Minimum,TERMINAL(2),TERMINAL(1),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Title_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Set_20_Title_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_string_2D_to_2D_pointer,1,1,TERMINAL(1),ROOT(2));

CopyCStringToPascal( GETCONSTPOINTER(char,*,TERMINAL(2)),GETPOINTER(256,unsigned char,*,ROOT(3),NONE));
result = kSuccess;

SetControlTitle( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(0)),GETCONSTPOINTER(unsigned char,*,TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Title(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Set_20_Title_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = vpx_set(PARAMETERS,kVPXValue_Title,TERMINAL(2),TERMINAL(1),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Value_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Set_20_Value_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

SetControlValue( GETPOINTER(0,OpaqueControlRef,*,ROOT(2),TERMINAL(0)),GETINTEGER(TERMINAL(1)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLControl_2F_Set_20_Value(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Set_20_Value_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = vpx_set(PARAMETERS,kVPXValue_Value,TERMINAL(2),TERMINAL(1),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Size_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLControl_2F_Size_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(1),ROOT(2),ROOT(3));

SizeControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(0)),GETINTEGER(TERMINAL(3)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Size_20_Control(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Size_20_Control_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
NEXTCASEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(1),ROOT(2));

result = vpx_constant(PARAMETERS,"0",ROOT(3));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

PUTINTEGER(HandleControlClick( GETPOINTER(0,OpaqueControlRef,*,ROOT(6),TERMINAL(0)),GETSTRUCTURE(4,Point,TERMINAL(2)),GETINTEGER(TERMINAL(3)),GETPOINTER(0,void,*,ROOT(7),TERMINAL(4))),5);
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(8)
}

enum opTrigger vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_VPLControl_2F_Track_20_Control(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLControl_2F_Track_20_Control_case_1_local_3(PARAMETERS,TERMINAL(3),TERMINAL(1),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLControl_2F_Get_20_ControlRef(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Size,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(1),ROOT(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kControlUpButtonPart,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP__2D_,2,1,TERMINAL(1),TERMINAL(3),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kControlDownButtonPart,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP__2B_,2,1,TERMINAL(1),TERMINAL(3),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kControlIndicatorPart,TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(5)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kControlPageUpPart,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP__2D_,2,1,TERMINAL(1),TERMINAL(4),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_5(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_5(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kControlPageDownPart,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_primitive(PARAMETERS,VPLP__2B_,2,1,TERMINAL(1),TERMINAL(4),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_6(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_6(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
INPUT(4,4)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,TERMINAL(1))
FOOTER(5)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object terminal4,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0))
if(vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0))
if(vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0))
if(vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0))
if(vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_5(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0))
vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5_case_6(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,terminal4,root0);
return outcome;
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Maximum,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Minimum,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(5),TERMINAL(1),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_min,2,1,TERMINAL(6),TERMINAL(3),ROOT(7));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_VPLControl_2F_Click_20_Control_20_Part(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Value,1,1,TERMINAL(0),ROOT(2));

result = vpx_constant(PARAMETERS,"18",ROOT(3));

result = vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_4(PARAMETERS,TERMINAL(0),ROOT(4));

result = vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_5(PARAMETERS,TERMINAL(0),TERMINAL(2),TERMINAL(1),TERMINAL(3),TERMINAL(4),ROOT(5));
FAILONFAILURE

result = vpx_method_VPLControl_2F_Click_20_Control_20_Part_case_1_local_6(PARAMETERS,TERMINAL(0),TERMINAL(5),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Value,2,0,TERMINAL(0),TERMINAL(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do_20_Click_20_Method,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLControl_2F_Do_20_Click_20_Method(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Value,1,1,TERMINAL(0),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Scroll_20_V,2,0,TERMINAL(2),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

/* Stop Universals */



Nat4 VPLC_VPLScroll_20_Bar_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLScroll_20_Bar_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 350 409 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Item Record",tempClass,NULL,environment);
	tempAttribute = attribute_add("Name",tempClass,NULL,environment);
	tempAttribute = attribute_add("Frame",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Frame,environment);
	tempAttribute = attribute_add("Selected",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Selected,environment);
	tempAttribute = attribute_add("Highlight",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Highlight,environment);
	tempAttribute = attribute_add("Visible?",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Visible_3F_,environment);
	tempAttribute = attribute_add("Procedure ID",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Procedure_20_ID,environment);
	tempAttribute = attribute_add("Title",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Title,environment);
	tempAttribute = attribute_add("Visible",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Visible,environment);
	tempAttribute = attribute_add("Value",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Value,environment);
	tempAttribute = attribute_add("Minimum",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Minimum,environment);
	tempAttribute = attribute_add("Maximum",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Maximum,environment);
	tempAttribute = attribute_add("Action Callback",tempClass,tempAttribute_VPLScroll_20_Bar_2F_Action_20_Callback,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"VPLControl");
	return kNOERROR;
}

/* Start Universals: { 602 720 }{ 200 300 } */
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Install_20_Action_20_Proc(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Action_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_ControlRef,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Pointer,2,1,TERMINAL(2),TERMINAL(0),ROOT(4));

SetControlAction( GETPOINTER(0,OpaqueControlRef,*,ROOT(5),TERMINAL(3)),GETPOINTER(0,void,*,ROOT(6),TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Remove_20_Action_20_Proc(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Action_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_ControlRef,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(2));

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

SetControlAction( GETPOINTER(0,OpaqueControlRef,*,ROOT(5),TERMINAL(3)),GETPOINTER(0,void,*,ROOT(6),TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(0),TERMINAL(1));
FAILONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Install_20_Action_20_Proc,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Remove_20_Action_20_Proc,1,0,TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Click_20_Control_20_Part,2,0,TERMINAL(0),TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Now,1,0,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Value,1,1,TERMINAL(0),ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Scroll_20_V,2,0,TERMINAL(4),TERMINAL(2));

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Action_20_Callback,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_UPP_20_Pointer,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(0));
NEXTCASEONSUCCESS

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(1),ROOT(3));

result = vpx_constant(PARAMETERS,"0",ROOT(4));

result = vpx_constant(PARAMETERS,"-1",ROOT(5));

PUTINTEGER(HandleControlClick( GETPOINTER(0,OpaqueControlRef,*,ROOT(7),TERMINAL(0)),GETSTRUCTURE(4,Point,TERMINAL(3)),GETINTEGER(TERMINAL(4)),GETPOINTER(0,void,*,ROOT(8),TERMINAL(2))),6);
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTER(9)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Item_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_3(PARAMETERS,TERMINAL(0),ROOT(4));

result = vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1_local_4(PARAMETERS,TERMINAL(3),TERMINAL(1),TERMINAL(4),ROOT(5));

result = vpx_match(PARAMETERS,"129",TERMINAL(5));
NEXTCASEONFAILURE

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLScroll_20_Bar_2F_Track_20_Control(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_VPLScroll_20_Bar_2F_Track_20_Control_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

/* Stop Universals */






Nat4	loadClasses_VPLControls(V_Environment environment);
Nat4	loadClasses_VPLControls(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("VPLControl",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLControl_class_load(result,environment);
	result = class_new("VPLScroll Bar",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLScroll_20_Bar_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_VPLControls(V_Environment environment);
Nat4	loadUniversals_VPLControls(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("VPLControl/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Close,NULL);

	result = method_new("VPLControl/Draw",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Draw,NULL);

	result = method_new("VPLControl/Get Value",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Get_20_Value,NULL);

	result = method_new("VPLControl/Hilite Control",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Hilite_20_Control,NULL);

	result = method_new("VPLControl/Move Control",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Move_20_Control,NULL);

	result = method_new("VPLControl/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Open,NULL);

	result = method_new("VPLControl/Process Click",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Process_20_Click,NULL);

	result = method_new("VPLControl/Set Frame",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Set_20_Frame,NULL);

	result = method_new("VPLControl/Set Maximum",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Set_20_Maximum,NULL);

	result = method_new("VPLControl/Set Minimum",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Set_20_Minimum,NULL);

	result = method_new("VPLControl/Set Title",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Set_20_Title,NULL);

	result = method_new("VPLControl/Set Value",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Set_20_Value,NULL);

	result = method_new("VPLControl/Size Control",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Size_20_Control,NULL);

	result = method_new("VPLControl/Track Control",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Track_20_Control,NULL);

	result = method_new("VPLControl/Get ControlRef",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Get_20_ControlRef,NULL);

	result = method_new("VPLControl/Click Control Part",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Click_20_Control_20_Part,NULL);

	result = method_new("VPLControl/Do Click Method",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLControl_2F_Do_20_Click_20_Method,NULL);

	result = method_new("VPLScroll Bar/Install Action Proc",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLScroll_20_Bar_2F_Install_20_Action_20_Proc,NULL);

	result = method_new("VPLScroll Bar/Remove Action Proc",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLScroll_20_Bar_2F_Remove_20_Action_20_Proc,NULL);

	result = method_new("VPLScroll Bar/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLScroll_20_Bar_2F_Open,NULL);

	result = method_new("VPLScroll Bar/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLScroll_20_Bar_2F_Close,NULL);

	result = method_new("VPLScroll Bar/Do Live Action",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLScroll_20_Bar_2F_Do_20_Live_20_Action,NULL);

	result = method_new("VPLScroll Bar/Track Control",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLScroll_20_Bar_2F_Track_20_Control,NULL);

	return kNOERROR;

}



Nat4	loadPersistents_VPLControls(V_Environment environment);
Nat4	loadPersistents_VPLControls(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	return kNOERROR;

}

Nat4	load_VPLControls(V_Environment environment);
Nat4	load_VPLControls(V_Environment environment)
{

	loadClasses_VPLControls(environment);
	loadUniversals_VPLControls(environment);
	loadPersistents_VPLControls(environment);
	return kNOERROR;

}

