/* A VPL Section File */
/*

VPLEdit Text.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_VPLEdit_20_Text_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000D, 0X5C224564, 0X69742054, 0X6578745C,
0X22000000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Frame[] = {
0000000000, 0X0000008C, 0X00000028, 0X00000005, 0X00000014, 0X00000050, 0X0000004C, 0X00000048,
0X00000044, 0X0000003C, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000044,
0X00000004, 0X00000054, 0X0000006C, 0X00000084, 0X0000009C, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Selected[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Highlight[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Visible_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Dirty[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Justification[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEdit_20_Text_2F_Editable_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X02380003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000E, 0X5C22456E, 0X74727920, 0X54657874,
0X5C220000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Frame[] = {
0000000000, 0X0000008C, 0X00000028, 0X00000005, 0X00000014, 0X00000050, 0X0000004C, 0X00000048,
0X00000044, 0X0000003C, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000044,
0X00000004, 0X00000054, 0X0000006C, 0X00000084, 0X0000009C, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Selected[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Highlight[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Visible_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Dirty[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Justification[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_VPLEntry_20_Text_2F_Editable_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X02380003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_VPLHIText_20_Item_20_Editor_2F_Event_20_Handler[] = {
0000000000, 0X00000344, 0X000000A4, 0X00000024, 0X00000014, 0X000003B4, 0X000003B0, 0X000003A8,
0X00000390, 0X00000388, 0X00000110, 0X0000010C, 0X00000340, 0X000000F8, 0X00000314, 0X000000F4,
0X000002E8, 0X000002D0, 0X000002A8, 0X000002B0, 0X00000290, 0X00000288, 0X000000EC, 0X00000260,
0X00000248, 0X00000220, 0X00000228, 0X000001A4, 0X000001FC, 0X000001E4, 0X000001BC, 0X000001C4,
0X000001A0, 0X00000198, 0X000000E8, 0X00000168, 0X000000E4, 0X00000134, 0X000000DC, 0X000000B8,
0X000000C0, 0X00270008, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000DC, 0X00000011,
0X000000C4, 0X43617262, 0X6F6E2045, 0X76656E74, 0X2043616C, 0X6C626163, 0X6B000000, 0X00000120,
0000000000, 0X00000154, 0X00000184, 0X00000274, 0000000000, 0X00000300, 0X0000032C, 0000000000,
0000000000, 0000000000, 0000000000, 0X0000035C, 0X00000374, 0000000000, 0000000000, 0000000000,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000013C, 0X00000014, 0X6B457665,
0X6E745465, 0X7874496E, 0X70757443, 0X6C617373, 0000000000, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000170, 0X00000010, 0X2F434520, 0X48616E64, 0X6C652045, 0X76656E74,
0000000000, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X000001A0, 0X00000002,
0X000001A8, 0X0000020C, 0X00150008, 0000000000, 0000000000, 0000000000, 0000000000, 0X000001E4,
0X00000001, 0X000001C8, 0X41747472, 0X69627574, 0X6520496E, 0X70757420, 0X53706563, 0X69666965,
0X72000000, 0X000001E8, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000204,
0X00000005, 0X4F776E65, 0X72000000, 0X00150008, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000248, 0X00000001, 0X0000022C, 0X41747472, 0X69627574, 0X6520496E, 0X70757420, 0X53706563,
0X69666965, 0X72000000, 0X0000024C, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000268, 0X00000009, 0X54686520, 0X4576656E, 0X74000000, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000290, 0X00000001, 0X00000294, 0X00160008, 0000000000, 0000000000,
0000000000, 0000000000, 0X000002D0, 0X00000001, 0X000002B4, 0X41747472, 0X69627574, 0X65204F75,
0X74707574, 0X20537065, 0X63696669, 0X65720000, 0X000002D4, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X000002F0, 0X0000000F, 0X43616C6C, 0X6261636B, 0X20526573, 0X756C7400,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000031C, 0X0000000F, 0X2F446F20,
0X43452043, 0X616C6C62, 0X61636B00, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000348, 0X00000013, 0X4576656E, 0X7448616E, 0X646C6572, 0X50726F63, 0X50747200, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000390, 0X00000001, 0X00000394, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X000003B0, 0X00000002, 0X000003B8, 0X000003D0, 0X00000004, 0000000000,
0000000000, 0000000000, 0000000000, 0X74657874, 0X00000004, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000002
	};
	Nat4 tempAttribute_VPLHICombo_20_Box_20_Editor_2F_Event_20_Handler[] = {
0000000000, 0X00000344, 0X000000A4, 0X00000024, 0X00000014, 0X000003B4, 0X000003B0, 0X000003A8,
0X00000390, 0X00000388, 0X00000110, 0X0000010C, 0X00000340, 0X000000F8, 0X00000314, 0X000000F4,
0X000002E8, 0X000002D0, 0X000002A8, 0X000002B0, 0X00000290, 0X00000288, 0X000000EC, 0X00000260,
0X00000248, 0X00000220, 0X00000228, 0X000001A4, 0X000001FC, 0X000001E4, 0X000001BC, 0X000001C4,
0X000001A0, 0X00000198, 0X000000E8, 0X00000168, 0X000000E4, 0X00000134, 0X000000DC, 0X000000B8,
0X000000C0, 0X00270008, 0000000000, 0000000000, 0000000000, 0000000000, 0X000000DC, 0X00000011,
0X000000C4, 0X43617262, 0X6F6E2045, 0X76656E74, 0X2043616C, 0X6C626163, 0X6B000000, 0X00000120,
0000000000, 0X00000154, 0X00000184, 0X00000274, 0000000000, 0X00000300, 0X0000032C, 0000000000,
0000000000, 0000000000, 0000000000, 0X0000035C, 0X00000374, 0000000000, 0000000000, 0000000000,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000013C, 0X00000014, 0X6B457665,
0X6E745465, 0X7874496E, 0X70757443, 0X6C617373, 0000000000, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000170, 0X00000010, 0X2F434520, 0X48616E64, 0X6C652045, 0X76656E74,
0000000000, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X000001A0, 0X00000002,
0X000001A8, 0X0000020C, 0X00150008, 0000000000, 0000000000, 0000000000, 0000000000, 0X000001E4,
0X00000001, 0X000001C8, 0X41747472, 0X69627574, 0X6520496E, 0X70757420, 0X53706563, 0X69666965,
0X72000000, 0X000001E8, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000204,
0X00000005, 0X4F776E65, 0X72000000, 0X00150008, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000248, 0X00000001, 0X0000022C, 0X41747472, 0X69627574, 0X6520496E, 0X70757420, 0X53706563,
0X69666965, 0X72000000, 0X0000024C, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000268, 0X00000009, 0X54686520, 0X4576656E, 0X74000000, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000290, 0X00000001, 0X00000294, 0X00160008, 0000000000, 0000000000,
0000000000, 0000000000, 0X000002D0, 0X00000001, 0X000002B4, 0X41747472, 0X69627574, 0X65204F75,
0X74707574, 0X20537065, 0X63696669, 0X65720000, 0X000002D4, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X000002F0, 0X0000000F, 0X43616C6C, 0X6261636B, 0X20526573, 0X756C7400,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000031C, 0X0000000F, 0X2F446F20,
0X43452043, 0X616C6C62, 0X61636B00, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000348, 0X00000013, 0X4576656E, 0X7448616E, 0X646C6572, 0X50726F63, 0X50747200, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000390, 0X00000001, 0X00000394, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X000003B0, 0X00000002, 0X000003B8, 0X000003D0, 0X00000004, 0000000000,
0000000000, 0000000000, 0000000000, 0X74657874, 0X00000004, 0000000000, 0000000000, 0000000000,
0000000000, 0X00000002
	};


Nat4 VPLC_VPLEdit_20_Text_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLEdit_20_Text_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 321 347 }{ 180 350 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Item Record",tempClass,NULL,environment);
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_VPLEdit_20_Text_2F_Name,environment);
	tempAttribute = attribute_add("Frame",tempClass,tempAttribute_VPLEdit_20_Text_2F_Frame,environment);
	tempAttribute = attribute_add("Selected",tempClass,tempAttribute_VPLEdit_20_Text_2F_Selected,environment);
	tempAttribute = attribute_add("Highlight",tempClass,tempAttribute_VPLEdit_20_Text_2F_Highlight,environment);
	tempAttribute = attribute_add("Visible?",tempClass,tempAttribute_VPLEdit_20_Text_2F_Visible_3F_,environment);
	tempAttribute = attribute_add("Text Editor",tempClass,NULL,environment);
	tempAttribute = attribute_add("Text",tempClass,NULL,environment);
	tempAttribute = attribute_add("Dirty",tempClass,tempAttribute_VPLEdit_20_Text_2F_Dirty,environment);
	tempAttribute = attribute_add("Justification",tempClass,tempAttribute_VPLEdit_20_Text_2F_Justification,environment);
	tempAttribute = attribute_add("Editable?",tempClass,tempAttribute_VPLEdit_20_Text_2F_Editable_3F_,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"VPLWindow Item");
	return kNOERROR;
}

/* Start Universals: { 207 278 }{ 482 383 } */
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Activate_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Activate_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Activate(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Activate_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Close_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Close_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Close_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Port_20_Bounds,1,1,TERMINAL(4),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Draw,2,0,TERMINAL(3),TERMINAL(5));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
FAILONSUCCESS

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_erase_2D_rect,1,0,TERMINAL(4));

result = vpx_method_VPLEdit_20_Text_2F_Draw_case_1_local_5(PARAMETERS,TERMINAL(2),TERMINAL(1));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(2),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(4),ROOT(6),ROOT(7));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(7));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP__22_length_22_,1,1,TERMINAL(7),ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(5),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_string_2D_to_2D_pointer,1,1,TERMINAL(7),ROOT(10));

TETextBox( GETCONSTPOINTER(void,*,TERMINAL(10)),GETINTEGER(TERMINAL(8)),GETCONSTPOINTER(Rect,*,TERMINAL(9)),GETINTEGER(TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTER(11)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(18)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(2),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(4),ROOT(6),ROOT(7));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(7));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(5),ROOT(8));

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(7),ROOT(9));

result = vpx_extconstant(PARAMETERS,kThemeCurrentPortFont,ROOT(10));

result = vpx_extconstant(PARAMETERS,kThemeStateActive,ROOT(11));

result = vpx_constant(PARAMETERS,"NULL",ROOT(12));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(13));

result = vpx_extconstant(PARAMETERS,kThemeSmallEmphasizedSystemFont,ROOT(14));

result = vpx_extconstant(PARAMETERS,kThemeSystemFontDetailEmphasized,ROOT(15));

PUTINTEGER(DrawThemeTextBox( GETCONSTPOINTER(__CFString,*,TERMINAL(9)),GETINTEGER(TERMINAL(14)),GETINTEGER(TERMINAL(11)),GETINTEGER(TERMINAL(13)),GETCONSTPOINTER(Rect,*,TERMINAL(8)),GETINTEGER(TERMINAL(3)),GETPOINTER(0,CGContext,*,ROOT(17),TERMINAL(12))),16);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(9)));
result = kSuccess;

result = kSuccess;

FOOTER(18)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
if(vpx_method_VPLEdit_20_Text_2F_Draw_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Draw_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Enter_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Enter_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(12)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Editable_3F_,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(3),ROOT(5),ROOT(6));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(6));
NEXTCASEONSUCCESS

result = vpx_instantiate(PARAMETERS,kVPXClass_VPLHIText_20_Editor,1,1,NONE,ROOT(7));

result = vpx_set(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(3),TERMINAL(7),ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(7),TERMINAL(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(7),TERMINAL(6));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(9));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(5),TERMINAL(9));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(10));

result = vpx_set(PARAMETERS,kVPXValue_Dirty,TERMINAL(5),TERMINAL(10),ROOT(11));

result = kSuccess;

FOOTERWITHNONE(12)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Enter_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Enter_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Dirty,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Enter(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Enter_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLEdit_20_Text_2F_Enter_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(10)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_instantiate(PARAMETERS,kVPXClass_VPLHIText_20_Editor,1,1,NONE,ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(3),TERMINAL(4));

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(1),ROOT(5),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(3),TERMINAL(6));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(5),TERMINAL(7));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(8));

result = vpx_set(PARAMETERS,kVPXValue_Dirty,TERMINAL(5),TERMINAL(8),ROOT(9));

result = kSuccess;

FOOTERWITHNONE(10)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Dirty,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_equals,2,0,TERMINAL(3),TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"FALSE",ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Dirty,TERMINAL(2),TERMINAL(4),ROOT(5));

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Dirty,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Text,1,1,TERMINAL(2),ROOT(3));

result = vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2_case_1_local_5(PARAMETERS,TERMINAL(1),TERMINAL(3));

result = vpx_set(PARAMETERS,kVPXValue_Text,TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Redraw,2,0,TERMINAL(4),NONE);

result = kSuccess;

FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Exit_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Exit(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Exit_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Exit_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Get_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Get_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Text,1,1,TERMINAL(2),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Text,TERMINAL(1),TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(5)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Get_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Get_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Get_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Get_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLEdit_20_Text_2F_Get_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Hilite(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Activate,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Hilite,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Idle_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Idle_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Idle,2,0,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Idle(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Get_20_Port(PARAMETERS,ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(0));

result = vpx_method_VPLEdit_20_Text_2F_Idle_case_1_local_4(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_Set_20_Port(PARAMETERS,TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Editable_3F_,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(3));
NEXTCASEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(4),ROOT(5));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(5));
FAILONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Process_20_Click,2,0,TERMINAL(5),TERMINAL(1));
FAILONFAILURE

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_2_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_2_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(10)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Target_20_Self,1,0,TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Select_20_Data,2,1,TERMINAL(0),TERMINAL(1),ROOT(4));

result = vpx_match(PARAMETERS,"( )",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,1,TERMINAL(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Name,TERMINAL(3),ROOT(6),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Item_20_Name,2,1,TERMINAL(5),TERMINAL(7),ROOT(8));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(9));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Target,2,0,TERMINAL(8),TERMINAL(9));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Process_20_Click,2,0,TERMINAL(8),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(10)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(10)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_Get_20_Target(PARAMETERS,ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP__3D_,2,0,TERMINAL(2),TERMINAL(0));
NEXTCASEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(4),ROOT(7),ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Dispatch_20_Click,2,1,TERMINAL(4),TERMINAL(1),ROOT(9));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(9));
TERMINATEONSUCCESS

result = vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_2_local_9(PARAMETERS,TERMINAL(8),TERMINAL(6),TERMINAL(1),TERMINAL(0));

result = kSuccess;

FOOTER(10)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Application,1,1,TERMINAL(0),ROOT(1));

result = vpx_get(PARAMETERS,kVPXValue_Target,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_equals,2,0,TERMINAL(3),TERMINAL(0));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Target,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Process_20_Click,2,0,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_2(PARAMETERS,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(0));

result = vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3_local_4(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Click(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
if(vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Process_20_Click_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_2_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Key_20_Down,2,0,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_2_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Process_20_Key(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Process_20_Key_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Text_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Text_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Text,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_method_VPLEdit_20_Text_2F_Set_20_Text_case_1_local_3(PARAMETERS,TERMINAL(2),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Value(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Application,1,1,TERMINAL(0),ROOT(1));

result = vpx_get(PARAMETERS,kVPXValue_Target,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_equals,2,0,TERMINAL(3),TERMINAL(0));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Target,2,0,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_constant(PARAMETERS,"0",ROOT(3));

result = vpx_constant(PARAMETERS,"32767",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Selection,3,0,TERMINAL(2),TERMINAL(3),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Select_20_All(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(0));

result = vpx_method_VPLEdit_20_Text_2F_Select_20_All_case_1_local_4(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"1.0",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(1),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(2));

result = vpx_constant(PARAMETERS,"2",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Fill_20_Rounded_20_Rect,4,0,TERMINAL(1),TERMINAL(0),TERMINAL(3),TERMINAL(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
FAILONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Port_20_Bounds,1,1,TERMINAL(4),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Draw_20_CG,2,0,TERMINAL(3),TERMINAL(5));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(2));

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
result = kSuccess;

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_constant(PARAMETERS,"0",ROOT(3));

PUTPOINTER(__CFString,*,CFStringCreateMutable( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(2)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

result = vpx_method_To_20_CString(PARAMETERS,TERMINAL(0),ROOT(5));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUTF8,ROOT(6));

CFStringAppendCString( GETPOINTER(0,__CFString,*,ROOT(7),TERMINAL(4)),GETCONSTPOINTER(char,*,TERMINAL(5)),GETINTEGER(TERMINAL(6)));
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(1)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(8)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kCFStringEncodingMacRoman,ROOT(1));

result = vpx_method_Beep(PARAMETERS);

result = vpx_method_Create_20_CFStringRef(PARAMETERS,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_constant(PARAMETERS,"Encoding warning:",ROOT(3));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(3));

CFShow( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_constant(PARAMETERS,"0",ROOT(5));

PUTPOINTER(__CFString,*,CFStringCreateMutableCopy( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(4)),GETINTEGER(TERMINAL(5)),GETCONSTPOINTER(__CFString,*,TERMINAL(2))),6);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTER(7)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Encoding failure",ROOT(1));

result = vpx_method__22_Check_22_(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_method_Debug_20_OSError(PARAMETERS,TERMINAL(1),TERMINAL(2));

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CGContextRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = vpx_constant(PARAMETERS,"1.0",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Stroke_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(2),ROOT(3));

result = vpx_constant(PARAMETERS,"-1",ROOT(4));

result = vpx_constant(PARAMETERS,"0",ROOT(5));

PUTSTRUCTURE(16,CGRect,CGRectOffset( GETSTRUCTURE(16,CGRect,TERMINAL(3)),GETREAL(TERMINAL(5)),GETREAL(TERMINAL(4))),6);
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
TERMINATEONSUCCESS

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_4(PARAMETERS,TERMINAL(4),ROOT(5));
TERMINATEONFAILURE

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_5(PARAMETERS,TERMINAL(2),ROOT(6));

result = vpx_extconstant(PARAMETERS,kHIThemeOrientationNormal,ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_HIThemeTextInfo,1,1,TERMINAL(0),ROOT(8));

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2_local_8(PARAMETERS,TERMINAL(0),ROOT(9));

PUTINTEGER(HIThemeDrawTextBox( GETCONSTPOINTER(void,*,TERMINAL(5)),GETCONSTPOINTER(CGRect,*,TERMINAL(9)),GETPOINTER(40,HIThemeTextInfo,*,ROOT(11),TERMINAL(8)),GETPOINTER(0,CGContext,*,ROOT(12),TERMINAL(6)),GETINTEGER(TERMINAL(7))),10);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(5)));
result = kSuccess;

result = kSuccess;

FOOTER(13)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(12)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(3),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(5),ROOT(7),ROOT(8));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(8));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP__22_length_22_,1,1,TERMINAL(8),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(6),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP_string_2D_to_2D_pointer,1,1,TERMINAL(8),ROOT(11));

TETextBox( GETCONSTPOINTER(void,*,TERMINAL(11)),GETINTEGER(TERMINAL(9)),GETCONSTPOINTER(Rect,*,TERMINAL(10)),GETINTEGER(TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(12)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
result = kSuccess;

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_constant(PARAMETERS,"0",ROOT(3));

PUTPOINTER(__CFString,*,CFStringCreateMutable( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(2)),GETINTEGER(TERMINAL(3))),4);
result = kSuccess;

result = vpx_method_To_20_CString(PARAMETERS,TERMINAL(0),ROOT(5));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUTF8,ROOT(6));

CFStringAppendCString( GETPOINTER(0,__CFString,*,ROOT(7),TERMINAL(4)),GETCONSTPOINTER(char,*,TERMINAL(5)),GETINTEGER(TERMINAL(6)));
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(1)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(8)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kCFStringEncodingMacRoman,ROOT(1));

result = vpx_method_Beep(PARAMETERS);

result = vpx_method_Create_20_CFStringRef(PARAMETERS,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_constant(PARAMETERS,"Encoding warning:",ROOT(3));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(3));

CFShow( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_constant(PARAMETERS,"0",ROOT(5));

PUTPOINTER(__CFString,*,CFStringCreateMutableCopy( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(4)),GETINTEGER(TERMINAL(5)),GETCONSTPOINTER(__CFString,*,TERMINAL(2))),6);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTER(7)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Encoding failure",ROOT(1));

result = vpx_method__22_Check_22_(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_method_Debug_20_OSError(PARAMETERS,TERMINAL(1),TERMINAL(2));

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADERWITHNONE(13)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,4,TERMINAL(3),ROOT(4),ROOT(5),ROOT(6),ROOT(7));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(7),TERMINAL(5),ROOT(8));

result = vpx_extconstant(PARAMETERS,truncMiddle,ROOT(9));

PUTINTEGER(TruncateThemeText( GETPOINTER(0,__CFString,*,ROOT(11),TERMINAL(0)),GETINTEGER(TERMINAL(1)),GETINTEGER(TERMINAL(2)),GETINTEGER(TERMINAL(8)),GETINTEGER(TERMINAL(9)),GETPOINTER(1,unsigned char,*,ROOT(12),NONE)),10);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(13)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CGContextRef,1,1,TERMINAL(0),ROOT(1));

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = vpx_constant(PARAMETERS,"1.0",ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Fill_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Stroke_20_RGB_20_Color,5,0,TERMINAL(0),TERMINAL(2),TERMINAL(2),TERMINAL(2),TERMINAL(3));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(21)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(3),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(5),ROOT(7),ROOT(8));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(8));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(6),ROOT(9));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(10));

result = vpx_constant(PARAMETERS,"NULL",ROOT(11));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(12));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(13));

result = vpx_constant(PARAMETERS,"list item",ROOT(14));

result = vpx_method_Set_20_Operation_20_Font(PARAMETERS,TERMINAL(14),TERMINAL(13),TERMINAL(12),ROOT(15),ROOT(16));

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_13(PARAMETERS,TERMINAL(8),ROOT(17));
TERMINATEONFAILURE

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_14(PARAMETERS,TERMINAL(17),TERMINAL(15),TERMINAL(16),TERMINAL(6));

result = vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4_local_15(PARAMETERS,TERMINAL(2),ROOT(18));

PUTINTEGER(DrawThemeTextBox( GETCONSTPOINTER(__CFString,*,TERMINAL(17)),GETINTEGER(TERMINAL(15)),GETINTEGER(TERMINAL(16)),GETINTEGER(TERMINAL(10)),GETCONSTPOINTER(Rect,*,TERMINAL(9)),GETINTEGER(TERMINAL(4)),GETPOINTER(0,CGContext,*,ROOT(20),TERMINAL(18))),19);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(17)));
result = kSuccess;

result = kSuccess;

FOOTER(21)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Draw_20_CG(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
if(vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2))
vpx_method_VPLEdit_20_Text_2F_Draw_20_CG_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Key_20_Down(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Process_20_Key,2,0,TERMINAL(0),TERMINAL(1));
FAILONFAILURE

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F__case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F__case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
NEXTCASEONSUCCESS

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Was_20_Content_20_Hit_3F_,3,1,TERMINAL(3),TERMINAL(1),TERMINAL(2),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTER(6)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F__case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F__case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(10)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Measure_20_Text,1,2,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_constant(PARAMETERS,"0",ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_pack,4,1,TERMINAL(5),TERMINAL(5),TERMINAL(4),TERMINAL(3),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Location,1,1,TERMINAL(0),ROOT(7));

result = vpx_method_Offset_20_Rectangle(PARAMETERS,TERMINAL(6),TERMINAL(7),ROOT(8));

result = vpx_method_Point_20_In_20_Rect(PARAMETERS,TERMINAL(1),TERMINAL(8),ROOT(9));

result = kSuccess;

OUTPUT(0,TERMINAL(9))
FOOTER(10)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F__case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F__case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"list item",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Type,TERMINAL(4),ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_2_local_3(PARAMETERS,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_6_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_6_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(0),ROOT(2));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(3));

result = vpx_persistent(PARAMETERS,kVPXValue_Minimum_20_Size_20_Text,0,1,ROOT(4));

result = vpx_method_Measure_20_Text(PARAMETERS,TERMINAL(4),TERMINAL(1),TERMINAL(3),ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(5),ROOT(6));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(2),ROOT(7),ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(6),ROOT(9),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(7),TERMINAL(9),ROOT(11));

result = vpx_call_primitive(PARAMETERS,VPLP_max,2,1,TERMINAL(8),TERMINAL(10),ROOT(12));

result = kSuccess;

OUTPUT(0,TERMINAL(11))
OUTPUT(1,TERMINAL(12))
FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0,V_Object *root1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method__22_Check_22_(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(3));

result = vpx_method_Measure_20_Text(PARAMETERS,TERMINAL(2),TERMINAL(1),TERMINAL(3),ROOT(4));

result = vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_6_case_1_local_5(PARAMETERS,TERMINAL(4),TERMINAL(1),ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
OUTPUT(1,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Measure_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0,V_Object *root1)
{
HEADER(8)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_constant(PARAMETERS,"\"\"",ROOT(3));

result = vpx_method_Replace_20_NULL(PARAMETERS,TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_5(PARAMETERS,TERMINAL(0),ROOT(5));

result = vpx_method_VPLEdit_20_Text_2F_Measure_20_Text_case_1_local_6(PARAMETERS,TERMINAL(4),TERMINAL(5),ROOT(6),ROOT(7));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
OUTPUT(1,TERMINAL(6))
FOOTERSINGLECASE(8)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Text_20_Editor,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame_20_In_20_Window,1,1,TERMINAL(0),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Frame,2,0,TERMINAL(3),TERMINAL(1));

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Set_20_Frame(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Set_20_Frame,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_method_VPLEdit_20_Text_2F_Set_20_Frame_case_1_local_3(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLEdit_20_Text_2F_Create_20_HIThemeTextInfo(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(25)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(1));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

result = vpx_constant(PARAMETERS,"list item",ROOT(3));

result = vpx_method_Set_20_Operation_20_Font(PARAMETERS,TERMINAL(3),TERMINAL(2),TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"31",ROOT(6));

result = vpx_constant(PARAMETERS,"HIThemeTextInfo",ROOT(7));

result = vpx_call_primitive(PARAMETERS,VPLP_make_2D_external,2,1,TERMINAL(7),TERMINAL(6),ROOT(8));

result = vpx_extconstant(PARAMETERS,kHIThemeTextInfoVersionZero,ROOT(9));

result = vpx_extset(PARAMETERS,"version",2,1,TERMINAL(8),TERMINAL(9),ROOT(10));

result = vpx_extset(PARAMETERS,"state",2,1,TERMINAL(10),TERMINAL(5),ROOT(11));

result = vpx_extset(PARAMETERS,"fontID",2,1,TERMINAL(11),TERMINAL(4),ROOT(12));

result = vpx_extconstant(PARAMETERS,kHIThemeTextHorizontalFlushCenter,ROOT(13));

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(14),ROOT(15));

result = vpx_extset(PARAMETERS,"horizontalFlushness",2,1,TERMINAL(12),TERMINAL(15),ROOT(16));

result = vpx_extconstant(PARAMETERS,kHIThemeTextVerticalFlushCenter,ROOT(17));

result = vpx_extset(PARAMETERS,"verticalFlushness",2,1,TERMINAL(16),TERMINAL(17),ROOT(18));

result = vpx_constant(PARAMETERS,"0",ROOT(19));

result = vpx_extset(PARAMETERS,"options",2,1,TERMINAL(18),TERMINAL(19),ROOT(20));

result = vpx_extconstant(PARAMETERS,kHIThemeTextTruncationMiddle,ROOT(21));

result = vpx_extset(PARAMETERS,"truncationPosition",2,1,TERMINAL(20),TERMINAL(21),ROOT(22));

result = vpx_constant(PARAMETERS,"1",ROOT(23));

result = vpx_extset(PARAMETERS,"truncationMaxLines",2,1,TERMINAL(22),TERMINAL(23),ROOT(24));

result = kSuccess;

OUTPUT(0,TERMINAL(8))
FOOTERSINGLECASE(25)
}

/* Stop Universals */



Nat4 VPLC_VPLEntry_20_Text_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLEntry_20_Text_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 316 350 }{ 169 366 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Item Record",tempClass,NULL,environment);
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_VPLEntry_20_Text_2F_Name,environment);
	tempAttribute = attribute_add("Frame",tempClass,tempAttribute_VPLEntry_20_Text_2F_Frame,environment);
	tempAttribute = attribute_add("Selected",tempClass,tempAttribute_VPLEntry_20_Text_2F_Selected,environment);
	tempAttribute = attribute_add("Highlight",tempClass,tempAttribute_VPLEntry_20_Text_2F_Highlight,environment);
	tempAttribute = attribute_add("Visible?",tempClass,tempAttribute_VPLEntry_20_Text_2F_Visible_3F_,environment);
	tempAttribute = attribute_add("Text Editor",tempClass,NULL,environment);
	tempAttribute = attribute_add("Text",tempClass,NULL,environment);
	tempAttribute = attribute_add("Dirty",tempClass,tempAttribute_VPLEntry_20_Text_2F_Dirty,environment);
	tempAttribute = attribute_add("Justification",tempClass,tempAttribute_VPLEntry_20_Text_2F_Justification,environment);
	tempAttribute = attribute_add("Editable?",tempClass,tempAttribute_VPLEntry_20_Text_2F_Editable_3F_,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"VPLEdit Text");
	return kNOERROR;
}

/* Start Universals: { 356 343 }{ 50 381 } */
enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kReturnCharCode,TERMINAL(0));
TERMINATEONSUCCESS

result = vpx_extmatch(PARAMETERS,kEnterCharCode,TERMINAL(0));
TERMINATEONSUCCESS

result = kSuccess;
FAILONSUCCESS

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Key,1,1,TERMINAL(1),ROOT(2));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_1_local_4(PARAMETERS,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
FAILONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_View,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Target,3,0,TERMINAL(4),TERMINAL(6),TERMINAL(3));

result = kSuccess;

FOOTER(7)
}

enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Process_20_Key,2,0,TERMINAL(0),TERMINAL(1));
FAILONFAILURE

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLEntry_20_Text_2F_Process_20_Key(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLEntry_20_Text_2F_Process_20_Key_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

/* Stop Universals */



Nat4 VPLC_VPLText_20_Editor_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLText_20_Editor_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 294 343 }{ 200 300 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Edit Record",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 205 742 }{ 225 335 } */
enum opTrigger vpx_method_VPLText_20_Editor_2F_Old_20_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(10)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(4),ROOT(5));

PUTPOINTER(TERec,**,TENew( GETCONSTPOINTER(Rect,*,TERMINAL(5)),GETCONSTPOINTER(Rect,*,TERMINAL(5))),6);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(2),TERMINAL(6),ROOT(7));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(8));

TEAutoView( GETINTEGER(TERMINAL(8)),GETPOINTER(32104,TERec,**,ROOT(9),TERMINAL(6)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(10)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Activate_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Activate_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

TEActivate( GETPOINTER(32104,TERec,**,ROOT(4),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Activate_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Activate_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

TEDeactivate( GETPOINTER(32104,TERec,**,ROOT(4),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Activate(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLText_20_Editor_2F_Activate_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLText_20_Editor_2F_Activate_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Close_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Close_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

TEDispose( GETPOINTER(32104,TERec,**,ROOT(3),TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_method_VPLText_20_Editor_2F_Close_case_1_local_4(PARAMETERS,TERMINAL(2));

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(2),TERMINAL(1),ROOT(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Draw(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(1),ROOT(4));

TEUpdate( GETCONSTPOINTER(Rect,*,TERMINAL(4)),GETPOINTER(32104,TERec,**,ROOT(5),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Get_20_Application(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Application,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Get_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Get_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_te_2D_get_2D_text,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Get_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Get_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Get_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLText_20_Editor_2F_Get_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLText_20_Editor_2F_Get_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Idle(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

TEIdle( GETPOINTER(32104,TERec,**,ROOT(4),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Key_20_Down_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Key_20_Down_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Key,1,1,TERMINAL(1),ROOT(4));
TERMINATEONFAILURE

TEKey( GETINTEGER(TERMINAL(4)),GETPOINTER(32104,TERec,**,ROOT(5),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Key_20_Down(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLText_20_Editor_2F_Key_20_Down_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Process_20_Click_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLText_20_Editor_2F_Process_20_Click_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(9)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Local_20_Where,1,1,TERMINAL(1),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(4),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Shift_20_Key,1,1,TERMINAL(1),ROOT(6));

TEClick( GETSTRUCTURE(4,Point,TERMINAL(5)),GETINTEGER(TERMINAL(6)),GETPOINTER(32104,TERec,**,ROOT(7),TERMINAL(3)));
result = kSuccess;

TEIdle( GETPOINTER(32104,TERec,**,ROOT(8),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Process_20_Click(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLText_20_Editor_2F_Process_20_Click_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Set_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP__22_length_22_,1,1,TERMINAL(1),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_string_2D_to_2D_pointer,1,1,TERMINAL(1),ROOT(5));

TESetText( GETCONSTPOINTER(void,*,TERMINAL(5)),GETINTEGER(TERMINAL(4)),GETPOINTER(32104,TERec,**,ROOT(6),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(4),ROOT(5));

PUTPOINTER(TERec,**,TENew( GETCONSTPOINTER(Rect,*,TERMINAL(5)),GETCONSTPOINTER(Rect,*,TERMINAL(5))),6);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(2),TERMINAL(6),ROOT(7));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(8));

TEAutoView( GETINTEGER(TERMINAL(8)),GETPOINTER(32104,TERec,**,ROOT(9),TERMINAL(6)));
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(1),ROOT(10),ROOT(11));

TESetAlignment( GETINTEGER(TERMINAL(11)),GETPOINTER(32104,TERec,**,ROOT(12),TERMINAL(6)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_VPLText_20_Editor_2F_Set_20_Selection(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Record,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(4));
TERMINATEONSUCCESS

TESetSelect( GETINTEGER(TERMINAL(1)),GETINTEGER(TERMINAL(2)),GETPOINTER(32104,TERec,**,ROOT(5),TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(6)
}

/* Stop Universals */



Nat4 VPLC_VPLHIText_20_Editor_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLHIText_20_Editor_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 294 343 }{ 200 300 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Edit Control",tempClass,NULL,environment);
	tempAttribute = attribute_add("Window",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 361 886 }{ 323 334 } */
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Activate_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Activate_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

PUTINTEGER(ActivateControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(5),TERMINAL(3))),4);
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Activate_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Activate_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

PUTINTEGER(DeactivateControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(5),TERMINAL(3))),4);
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Activate(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Activate_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Editor_2F_Activate_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Close_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Close_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

HideControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(3),TERMINAL(2)));
result = kSuccess;

DisposeControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Close_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Close_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Window,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
TERMINATEONSUCCESS

DisposeWindow( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(3),TERMINAL(2)));
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(4));

result = vpx_set(PARAMETERS,kVPXValue_Window,TERMINAL(0),TERMINAL(4),ROOT(5));

result = kSuccess;

FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_method_VPLHIText_20_Editor_2F_Close_case_1_local_4(PARAMETERS,TERMINAL(2));

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(2),TERMINAL(1),ROOT(3));

result = vpx_method_VPLHIText_20_Editor_2F_Close_case_1_local_6(PARAMETERS,TERMINAL(3));

result = vpx_method_Set_20_Arrow_20_Cursor(PARAMETERS);

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Draw_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Draw_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Draw_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Draw_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

Draw1Control( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Draw(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Draw_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Editor_2F_Draw_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Application(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Application,1,1,TERMINAL(2),ROOT(3));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(22)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_extconstant(PARAMETERS,kControlLabelPart,ROOT(3));

result = vpx_extconstant(PARAMETERS,kControlEditTextCFStringTag,ROOT(4));

result = vpx_constant(PARAMETERS,"4",ROOT(5));

result = vpx_constant(PARAMETERS,"void",ROOT(6));

result = vpx_constant(PARAMETERS,"1",ROOT(7));

result = vpx_method_Create_20_Block_20_Type(PARAMETERS,TERMINAL(6),TERMINAL(5),TERMINAL(7),ROOT(8));

result = vpx_extconstant(PARAMETERS,kControlEditTextPart,ROOT(9));

PUTINTEGER(GetControlData( GETPOINTER(0,OpaqueControlRef,*,ROOT(11),TERMINAL(2)),GETINTEGER(TERMINAL(9)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(5)),GETPOINTER(0,void,*,ROOT(12),TERMINAL(8)),GETPOINTER(4,long int,*,ROOT(13),NONE)),10);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(10));
NEXTCASEONFAILURE

result = vpx_method_Get_20_UInt32(PARAMETERS,TERMINAL(8),NONE,ROOT(14),ROOT(15));

result = vpx_call_primitive(PARAMETERS,VPLP_to_2D_pointer,1,1,TERMINAL(15),ROOT(16));

result = vpx_method_Block_20_Free(PARAMETERS,TERMINAL(8));

result = vpx_constant(PARAMETERS,"char",ROOT(17));

result = vpx_call_primitive(PARAMETERS,VPLP_set_2D_external_2D_type,2,0,TERMINAL(16),TERMINAL(17));

PUTINTEGER(IsPointerValid( GETPOINTER(1,char,*,ROOT(19),TERMINAL(16))),18);
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(18));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"__CFString",ROOT(20));

result = vpx_call_primitive(PARAMETERS,VPLP_set_2D_external_2D_type,2,0,TERMINAL(16),TERMINAL(20));

result = vpx_method_From_20_CFStringRef(PARAMETERS,TERMINAL(16),ROOT(21));

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(16)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(21))
FOOTERWITHNONE(22)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(20)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_extconstant(PARAMETERS,kControlLabelPart,ROOT(3));

result = vpx_extconstant(PARAMETERS,kControlEditTextTextTag,ROOT(4));

result = vpx_constant(PARAMETERS,"1024",ROOT(5));

result = vpx_constant(PARAMETERS,"void",ROOT(6));

result = vpx_constant(PARAMETERS,"1",ROOT(7));

result = vpx_method_Create_20_Block_20_Type(PARAMETERS,TERMINAL(6),TERMINAL(5),TERMINAL(7),ROOT(8));

result = vpx_extconstant(PARAMETERS,kControlEditTextPart,ROOT(9));

PUTINTEGER(GetControlData( GETPOINTER(0,OpaqueControlRef,*,ROOT(11),TERMINAL(2)),GETINTEGER(TERMINAL(9)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(5)),GETPOINTER(0,void,*,ROOT(12),TERMINAL(8)),GETPOINTER(4,long int,*,ROOT(13),NONE)),10);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(10));
NEXTCASEONFAILURE

result = vpx_method_Get_20_UInt32(PARAMETERS,TERMINAL(13),NONE,ROOT(14),ROOT(15));

result = vpx_constant(PARAMETERS,"0",ROOT(16));

result = vpx_call_primitive(PARAMETERS,VPLP_get_2D_text,3,3,TERMINAL(8),TERMINAL(16),TERMINAL(15),ROOT(17),ROOT(18),ROOT(19));

result = kSuccess;

OUTPUT(0,TERMINAL(19))
FOOTERWITHNONE(20)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(24)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(1));
NEXTCASEONFAILURE

result = vpx_extconstant(PARAMETERS,kTXNStartOffset,ROOT(2));

result = vpx_extconstant(PARAMETERS,kTXNEndOffset,ROOT(3));

result = vpx_extconstant(PARAMETERS,kTXNUnicodeTextData,ROOT(4));

result = vpx_extconstant(PARAMETERS,kTXNTextData,ROOT(5));

PUTINTEGER(TXNGetDataEncoded( GETPOINTER(0,OpaqueTXNObject,*,ROOT(7),TERMINAL(1)),GETINTEGER(TERMINAL(2)),GETINTEGER(TERMINAL(3)),GETPOINTER(1,char,***,ROOT(8),NONE),GETINTEGER(TERMINAL(5))),6);
result = kSuccess;

PUTINTEGER(TXNGetData( GETPOINTER(0,OpaqueTXNObject,*,ROOT(10),TERMINAL(1)),GETINTEGER(TERMINAL(2)),GETINTEGER(TERMINAL(3)),GETPOINTER(1,char,***,ROOT(11),NONE)),9);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(9));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(12));

result = vpx_extconstant(PARAMETERS,kCFStringEncodingUnicode,ROOT(13));

result = vpx_method_Coerce_20_Pointer(PARAMETERS,TERMINAL(8),ROOT(14));

result = vpx_constant(PARAMETERS,"char",ROOT(15));

result = vpx_call_primitive(PARAMETERS,VPLP_set_2D_external_2D_type,2,0,TERMINAL(14),TERMINAL(15));

PUTPOINTER(__CFString,*,CFStringCreateWithCString( GETCONSTPOINTER(__CFAllocator,*,TERMINAL(12)),GETCONSTPOINTER(char,*,TERMINAL(8)),GETINTEGER(TERMINAL(13))),16);
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(16));
NEXTCASEONSUCCESS

result = vpx_method_From_20_CFStringRef(PARAMETERS,TERMINAL(16),ROOT(17));

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(16)));
result = kSuccess;

PUTINTEGER(GetHandleSize( GETPOINTER(1,char,**,ROOT(19),TERMINAL(8))),18);
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(20));

result = vpx_call_primitive(PARAMETERS,VPLP_get_2D_text,3,3,TERMINAL(8),TERMINAL(20),TERMINAL(18),ROOT(21),ROOT(22),ROOT(23));

result = kSuccess;

OUTPUT(0,TERMINAL(23))
FOOTERWITHNONE(24)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLHIText_20_Editor_2F_Get_20_Text_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_method_To_20_CFStringRef(PARAMETERS,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
NEXTCASEONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kCFStringEncodingMacRoman,ROOT(1));

result = vpx_method_Beep(PARAMETERS);

result = vpx_method_Create_20_CFStringRef(PARAMETERS,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_constant(PARAMETERS,"Encoding was MacRoman:",ROOT(3));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(3));

CFShow( GETCONSTPOINTER(void,*,TERMINAL(2)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"Encoding was unknown:",ROOT(1));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(1));

result = vpx_method__22_Check_22_(PARAMETERS,TERMINAL(0),ROOT(2));

result = vpx_method_Debug_20_Console(PARAMETERS,TERMINAL(2));

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"void",ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_make_2D_external,2,1,TERMINAL(2),TERMINAL(0),ROOT(3));

result = vpx_constant(PARAMETERS,"0",ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_put_2D_integer,4,2,TERMINAL(3),TERMINAL(4),TERMINAL(0),TERMINAL(1),ROOT(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_4(PARAMETERS,TERMINAL(1),ROOT(4));
TERMINATEONFAILURE

result = vpx_extconstant(PARAMETERS,kControlLabelPart,ROOT(5));

result = vpx_extconstant(PARAMETERS,kControlEditTextCFStringTag,ROOT(6));

result = vpx_constant(PARAMETERS,"4",ROOT(7));

result = vpx_method_Coerce_20_Integer(PARAMETERS,TERMINAL(4),ROOT(8));

result = vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1_local_9(PARAMETERS,TERMINAL(7),TERMINAL(8),ROOT(9));

result = vpx_extconstant(PARAMETERS,kControlEditTextPart,ROOT(10));

PUTINTEGER(SetControlData( GETPOINTER(0,OpaqueControlRef,*,ROOT(12),TERMINAL(3)),GETINTEGER(TERMINAL(10)),GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(7)),GETCONSTPOINTER(void,*,TERMINAL(9))),11);
result = kSuccess;

CFRelease( GETCONSTPOINTER(void,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(13)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(11)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_extconstant(PARAMETERS,kTXNStartOffset,ROOT(3));

result = vpx_extconstant(PARAMETERS,kTXNEndOffset,ROOT(4));

result = vpx_method_To_20_CFString(PARAMETERS,TERMINAL(1),ROOT(5));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_UniChar,1,2,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Release,1,0,TERMINAL(5));

result = vpx_extconstant(PARAMETERS,kTXNUnicodeTextData,ROOT(8));

PUTINTEGER(TXNSetData( GETPOINTER(0,OpaqueTXNObject,*,ROOT(10),TERMINAL(2)),GETINTEGER(TERMINAL(8)),GETCONSTPOINTER(void,*,TERMINAL(6)),GETINTEGER(TERMINAL(7)),GETINTEGER(TERMINAL(3)),GETINTEGER(TERMINAL(4))),9);
result = kSuccess;

result = kSuccess;

FOOTER(11)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Editor_2F_Set_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Idle(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

TXNIdle( GETPOINTER(0,OpaqueTXNObject,*,ROOT(3),TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Record,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(4));
TERMINATEONFAILURE

TXNKeyDown( GETPOINTER(0,OpaqueTXNObject,*,ROOT(5),TERMINAL(4)),GETCONSTPOINTER(EventRecord,*,TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(7)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Record,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

result = vpx_extget(PARAMETERS,"message",1,2,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_extconstant(PARAMETERS,keyCodeMask,ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP_bit_2D_and,2,1,TERMINAL(4),TERMINAL(5),ROOT(6));

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(12)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Key,1,1,TERMINAL(1),ROOT(4));
TERMINATEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Record,TERMINAL(1),ROOT(5),ROOT(6));

result = vpx_extget(PARAMETERS,"modifiers",1,2,TERMINAL(6),ROOT(7),ROOT(8));

result = vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_2_local_7(PARAMETERS,TERMINAL(1),ROOT(9));

PUTINTEGER(HandleControlKey( GETPOINTER(0,OpaqueControlRef,*,ROOT(11),TERMINAL(3)),GETINTEGER(TERMINAL(9)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(8))),10);
result = kSuccess;

result = kSuccess;

FOOTER(12)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2_local_12(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

PUTINTEGER(HIViewSetNeedsDisplay( GETPOINTER(0,OpaqueControlRef,*,ROOT(3),TERMINAL(0)),GETINTEGER(TERMINAL(1))),2);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2_local_28(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2_local_28(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

PUTINTEGER(HIViewSetNeedsDisplay( GETPOINTER(0,OpaqueControlRef,*,ROOT(3),TERMINAL(0)),GETINTEGER(TERMINAL(1))),2);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(38)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(2));

result = vpx_get(PARAMETERS,kVPXValue_Selected,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(2),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(6),ROOT(7),ROOT(8));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(8),ROOT(9),ROOT(10));

result = vpx_get(PARAMETERS,kVPXValue_Type,TERMINAL(10),ROOT(11),ROOT(12));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Text,1,1,TERMINAL(0),ROOT(13));

result = vpx_method_Measure_20_Text(PARAMETERS,TERMINAL(13),TERMINAL(12),TERMINAL(4),ROOT(14));

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(15),ROOT(16));

result = vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2_local_12(PARAMETERS,TERMINAL(16));

PUTPOINTER(Rect,*,GetControlBounds( GETPOINTER(0,OpaqueControlRef,*,ROOT(18),TERMINAL(16)),GETPOINTER(8,Rect,*,ROOT(19),NONE)),17);
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,TERMINAL(19),ROOT(20));

result = vpx_method_Rect_20_To_20_Ints(PARAMETERS,TERMINAL(20),ROOT(21),ROOT(22),ROOT(23),ROOT(24));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(24),TERMINAL(22),ROOT(25));

result = vpx_extget(PARAMETERS,"h",1,2,TERMINAL(14),ROOT(26),ROOT(27));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(27),TERMINAL(25),ROOT(28));

result = vpx_constant(PARAMETERS,"8",ROOT(29));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(28),TERMINAL(29),ROOT(30));

result = vpx_method_Half(PARAMETERS,TERMINAL(30),ROOT(31));

result = vpx_method_Int(PARAMETERS,TERMINAL(31),ROOT(32));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(22),TERMINAL(32),ROOT(33));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(24),TERMINAL(32),ROOT(34));

result = vpx_method_Ints_20_To_20_Rect(PARAMETERS,TERMINAL(21),TERMINAL(33),TERMINAL(23),TERMINAL(34),ROOT(35));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(35),ROOT(36));

SetControlBounds( GETPOINTER(0,OpaqueControlRef,*,ROOT(37),TERMINAL(16)),GETCONSTPOINTER(Rect,*,TERMINAL(36)));
result = kSuccess;

result = vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2_local_28(PARAMETERS,TERMINAL(16));

result = kSuccess;

FOOTERWITHNONE(38)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Key_20_Down(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = vpx_method_VPLHIText_20_Editor_2F_Key_20_Down_case_1_local_3(PARAMETERS,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Record,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(4));
TERMINATEONFAILURE

TXNClick( GETPOINTER(0,OpaqueTXNObject,*,ROOT(5),TERMINAL(4)),GETCONSTPOINTER(EventRecord,*,TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(14)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Local_20_Where,1,1,TERMINAL(1),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Record,TERMINAL(1),ROOT(6),ROOT(7));

result = vpx_extget(PARAMETERS,"modifiers",1,2,TERMINAL(7),ROOT(8),ROOT(9));

result = vpx_constant(PARAMETERS,"NULL",ROOT(10));

PUTINTEGER(HandleControlClick( GETPOINTER(0,OpaqueControlRef,*,ROOT(12),TERMINAL(3)),GETSTRUCTURE(4,Point,TERMINAL(5)),GETINTEGER(TERMINAL(9)),GETPOINTER(0,void,*,ROOT(13),TERMINAL(10))),11);
result = kSuccess;

result = kSuccess;

FOOTER(14)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Process_20_Click(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_method_VPLHIText_20_Editor_2F_Process_20_Click_case_1_local_2(PARAMETERS,TERMINAL(0),TERMINAL(1));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Selection(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(3));
TERMINATEONFAILURE

TXNSelectAll( GETPOINTER(0,OpaqueTXNObject,*,ROOT(4),TERMINAL(3)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Get_20_TXNObject(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

PUTPOINTER(OpaqueTXNObject,*,HITextViewGetTXNObject( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(2))),3);
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(3)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kControlUseThemeFontIDMask,ROOT(0));

result = vpx_extconstant(PARAMETERS,kControlUseJustMask,ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_type,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"Operation Item",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Type,TERMINAL(5),ROOT(6),ROOT(7));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTER(8)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_type,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"Comment Item",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"comment",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"list item",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(19)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"ControlFontStyleRec",ROOT(1));

result = vpx_constant(PARAMETERS,"28",ROOT(2));

result = vpx_constant(PARAMETERS,"1",ROOT(3));

result = vpx_method_Create_20_Block_20_Type(PARAMETERS,TERMINAL(1),TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_6(PARAMETERS,ROOT(5));

result = vpx_extset(PARAMETERS,"flags",2,1,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(7));

result = vpx_get(PARAMETERS,kVPXValue_Selected,TERMINAL(0),ROOT(8),ROOT(9));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(10),ROOT(11));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13_case_1_local_11(PARAMETERS,TERMINAL(11),ROOT(12));

result = vpx_method_Set_20_Operation_20_Font(PARAMETERS,TERMINAL(12),TERMINAL(7),TERMINAL(9),ROOT(13),ROOT(14));

result = vpx_extset(PARAMETERS,"font",2,1,TERMINAL(6),TERMINAL(13),ROOT(15));

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(16),ROOT(17));

result = vpx_extset(PARAMETERS,"just",2,1,TERMINAL(15),TERMINAL(17),ROOT(18));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(19)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

TXNFocus( GETPOINTER(0,OpaqueTXNObject,*,ROOT(3),TERMINAL(1)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(29)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(3));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Content_20_HIViewRef,1,1,TERMINAL(3),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(3),ROOT(8));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(9));

result = vpx_constant(PARAMETERS,"NULL",ROOT(10));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame_20_In_20_Window,1,1,TERMINAL(1),ROOT(11));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(11),ROOT(12));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_13(PARAMETERS,TERMINAL(1),ROOT(13));

PUTINTEGER(CreateEditUnicodeTextControl( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(15),TERMINAL(8)),GETCONSTPOINTER(Rect,*,TERMINAL(12)),GETCONSTPOINTER(__CFString,*,TERMINAL(10)),GETINTEGER(TERMINAL(9)),GETCONSTPOINTER(ControlFontStyleRec,*,TERMINAL(13)),GETPOINTER(0,OpaqueControlRef,**,ROOT(16),NONE)),14);
result = kSuccess;

PUTINTEGER(HIViewAddSubview( GETPOINTER(0,OpaqueControlRef,*,ROOT(18),TERMINAL(7)),GETPOINTER(0,OpaqueControlRef,*,ROOT(19),TERMINAL(16))),17);
result = kSuccess;

PUTINTEGER(HIViewSetVisible( GETPOINTER(0,OpaqueControlRef,*,ROOT(21),TERMINAL(16)),GETINTEGER(TERMINAL(6))),20);
result = kSuccess;

PUTINTEGER(EnableControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(23),TERMINAL(16))),22);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(2),TERMINAL(16),ROOT(24));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_1_local_19(PARAMETERS,TERMINAL(24));

result = vpx_extconstant(PARAMETERS,kControlFocusNextPart,ROOT(25));

PUTINTEGER(SetKeyboardFocus( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(27),TERMINAL(8)),GETPOINTER(0,OpaqueControlRef,*,ROOT(28),TERMINAL(16)),GETINTEGER(TERMINAL(25))),26);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(29)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(5)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kOutputTextInUnicodeEncodingBit,ROOT(0));

result = vpx_extconstant(PARAMETERS,kTXNSingleLineOnlyBit,ROOT(1));

result = vpx_extconstant(PARAMETERS,kTXNMonostyledTextBit,ROOT(2));

result = vpx_extconstant(PARAMETERS,kTXNDoFontSubstitutionBit,ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,3,1,TERMINAL(3),TERMINAL(0),TERMINAL(1),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

TXNFocus( GETPOINTER(0,OpaqueTXNObject,*,ROOT(3),TERMINAL(1)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(24)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(3),ROOT(4));

result = vpx_constant(PARAMETERS,"0",ROOT(5));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(4),ROOT(6));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_7(PARAMETERS,ROOT(7));

PUTINTEGER(HITextViewCreate( GETCONSTPOINTER(CGRect,*,TERMINAL(6)),GETINTEGER(TERMINAL(5)),GETINTEGER(TERMINAL(7)),GETPOINTER(0,OpaqueControlRef,**,ROOT(9),NONE)),8);
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(10));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Content_20_HIViewRef,1,1,TERMINAL(11),ROOT(12));

PUTINTEGER(HIViewAddSubview( GETPOINTER(0,OpaqueControlRef,*,ROOT(14),TERMINAL(12)),GETPOINTER(0,OpaqueControlRef,*,ROOT(15),TERMINAL(9))),13);
result = kSuccess;

PUTINTEGER(HIViewSetVisible( GETPOINTER(0,OpaqueControlRef,*,ROOT(17),TERMINAL(9)),GETINTEGER(TERMINAL(10))),16);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(2),TERMINAL(9),ROOT(18));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_2_local_15(PARAMETERS,TERMINAL(18));

result = vpx_extconstant(PARAMETERS,kControlFocusNextPart,ROOT(19));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(11),ROOT(20));

PUTINTEGER(SetKeyboardFocus( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(22),TERMINAL(20)),GETPOINTER(0,OpaqueControlRef,*,ROOT(23),TERMINAL(9)),GETINTEGER(TERMINAL(19))),21);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(24)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(5)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kOutputTextInUnicodeEncodingBit,ROOT(0));

result = vpx_extconstant(PARAMETERS,kTXNSingleLineOnlyBit,ROOT(1));

result = vpx_extconstant(PARAMETERS,kTXNMonostyledTextBit,ROOT(2));

result = vpx_extconstant(PARAMETERS,kTXNDoFontSubstitutionBit,ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,3,1,TERMINAL(3),TERMINAL(0),TERMINAL(1),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_16(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_16(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(13)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,TERMINAL(0),ROOT(1));

result = vpx_constant(PARAMETERS,"2",ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_split_2D_nth,2,2,TERMINAL(1),TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(3),ROOT(5));

LocalToGlobal( GETPOINTER(4,Point,*,ROOT(6),TERMINAL(5)));
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(4),ROOT(7));

LocalToGlobal( GETPOINTER(4,Point,*,ROOT(8),TERMINAL(7)));
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(6),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(8),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP__28_join_29_,2,1,TERMINAL(9),TERMINAL(10),ROOT(11));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(11),ROOT(12));

result = kSuccess;

OUTPUT(0,TERMINAL(12))
FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_17_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_17_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(3)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kWindowHideOnSuspendAttribute,ROOT(0));

result = vpx_extconstant(PARAMETERS,kWindowIgnoreClicksAttribute,ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_17(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_17(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(15)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kOverlayWindowClass,ROOT(2));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_17_case_1_local_3(PARAMETERS,ROOT(3));

result = vpx_extconstant(PARAMETERS,kDocumentWindowClass,ROOT(4));

PUTINTEGER(CreateNewWindow( GETINTEGER(TERMINAL(2)),GETINTEGER(TERMINAL(3)),GETCONSTPOINTER(Rect,*,TERMINAL(0)),GETPOINTER(0,OpaqueWindowPtr,**,ROOT(6),NONE)),5);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(5));
FAILONFAILURE

SetPortWindowPort( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(7),TERMINAL(6)));
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(1),ROOT(8));

PUTPOINTER(OpaqueWindowGroupRef,*,GetWindowGroup( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(10),TERMINAL(8))),9);
result = kSuccess;

PUTINTEGER(SetWindowGroup( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(12),TERMINAL(6)),GETPOINTER(0,OpaqueWindowGroupRef,*,ROOT(13),TERMINAL(9))),11);
result = kSuccess;

ShowWindow( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(14),TERMINAL(6)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASEWITHNONE(15)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.09",ROOT(1));

result = vpx_constant(PARAMETERS,"0.93",ROOT(2));

result = vpx_constant(PARAMETERS,"0.12",ROOT(3));

result = vpx_constant(PARAMETERS,"0.4",ROOT(4));

CGContextSetRGBStrokeColor( GETPOINTER(0,CGContext,*,ROOT(5),TERMINAL(0)),GETREAL(TERMINAL(2)),GETREAL(TERMINAL(1)),GETREAL(TERMINAL(3)),GETREAL(TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(16)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.4",ROOT(1));

result = vpx_constant(PARAMETERS,"65535",ROOT(2));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_constant(PARAMETERS,"0",ROOT(4));

result = vpx_extconstant(PARAMETERS,kThemeBrushPrimaryHighlightColor,ROOT(5));

result = vpx_extconstant(PARAMETERS,kThemeBrushAlternatePrimaryHighlightColor,ROOT(6));

result = vpx_extconstant(PARAMETERS,kThemeBrushSecondaryHighlightColor,ROOT(7));

PUTINTEGER(GetThemeBrushAsColor( GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(3)),GETPOINTER(8,RGBColor,*,ROOT(9),NONE)),8);
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_RGB_2D_to_2D_list,1,1,TERMINAL(9),ROOT(10));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(10))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_primitive(PARAMETERS,VPLP_div,2,1,LIST(10),TERMINAL(2),ROOT(11));
LISTROOT(11,0)
REPEATFINISH
LISTROOTFINISH(11,0)
LISTROOTEND
} else {
ROOTEMPTY(11)
}

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,3,TERMINAL(11),ROOT(12),ROOT(13),ROOT(14));

CGContextSetRGBStrokeColor( GETPOINTER(0,CGContext,*,ROOT(15),TERMINAL(0)),GETREAL(TERMINAL(12)),GETREAL(TERMINAL(13)),GETREAL(TERMINAL(14)),GETREAL(TERMINAL(1)));
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(8));
NEXTCASEONFAILURE

result = kSuccess;

FOOTERWITHNONE(16)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.09",ROOT(1));

result = vpx_constant(PARAMETERS,"0.80",ROOT(2));

result = vpx_constant(PARAMETERS,"0.13",ROOT(3));

result = vpx_constant(PARAMETERS,"0.4",ROOT(4));

CGContextSetRGBStrokeColor( GETPOINTER(0,CGContext,*,ROOT(5),TERMINAL(0)),GETREAL(TERMINAL(1)),GETREAL(TERMINAL(3)),GETREAL(TERMINAL(2)),GETREAL(TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.53",ROOT(1));

result = vpx_constant(PARAMETERS,"0.25",ROOT(2));

CGContextSetGrayStrokeColor( GETPOINTER(0,CGContext,*,ROOT(3),TERMINAL(0)),GETREAL(TERMINAL(1)),GETREAL(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0))
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(28)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = vpx_method_Rect_20_Size(PARAMETERS,TERMINAL(0),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(3),ROOT(4));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(4))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_primitive(PARAMETERS,VPLP__2B_1,1,1,LIST(4),ROOT(5));
LISTROOT(5,0)
REPEATFINISH
LISTROOTFINISH(5,0)
LISTROOTEND
} else {
ROOTEMPTY(5)
}

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_method_New_20_CGRect(PARAMETERS,TERMINAL(2),TERMINAL(2),TERMINAL(7),TERMINAL(6),ROOT(8));

PUTPOINTER(OpaqueGrafPtr,*,GetWindowPort( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(10),TERMINAL(1))),9);
result = kSuccess;

PUTINTEGER(CreateCGContextForPort( GETPOINTER(0,OpaqueGrafPtr,*,ROOT(12),TERMINAL(9)),GETPOINTER(0,CGContext,**,ROOT(13),NONE)),11);
result = kSuccess;

result = vpx_constant(PARAMETERS,"3.0",ROOT(14));

CGContextSetLineWidth( GETPOINTER(0,CGContext,*,ROOT(15),TERMINAL(13)),GETREAL(TERMINAL(14)));
result = kSuccess;

PUTINTEGER(SyncCGContextOriginWithPort( GETPOINTER(0,CGContext,*,ROOT(17),TERMINAL(13)),GETPOINTER(0,OpaqueGrafPtr,*,ROOT(18),TERMINAL(9))),16);
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.0",ROOT(19));

result = vpx_call_primitive(PARAMETERS,VPLP__2D_1,1,1,TERMINAL(6),ROOT(20));

CGContextTranslateCTM( GETPOINTER(0,CGContext,*,ROOT(21),TERMINAL(13)),GETREAL(TERMINAL(19)),GETREAL(TERMINAL(20)));
result = kSuccess;

result = vpx_constant(PARAMETERS,"1.0",ROOT(22));

result = vpx_constant(PARAMETERS,"-1.0",ROOT(23));

CGContextScaleCTM( GETPOINTER(0,CGContext,*,ROOT(24),TERMINAL(13)),GETREAL(TERMINAL(22)),GETREAL(TERMINAL(23)));
result = kSuccess;

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18_case_1_local_19(PARAMETERS,TERMINAL(13));

result = vpx_extconstant(PARAMETERS,kCGLineCapRound,ROOT(25));

CGContextSetLineCap( GETPOINTER(0,CGContext,*,ROOT(26),TERMINAL(13)),GETINTEGER(TERMINAL(25)));
result = kSuccess;

CGContextRelease( GETPOINTER(0,CGContext,*,ROOT(27),TERMINAL(13)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASEWITHNONE(28)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

TXNFocus( GETPOINTER(0,OpaqueTXNObject,*,ROOT(3),TERMINAL(1)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(39)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Port,1,0,TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"0",ROOT(6));

result = vpx_constant(PARAMETERS,"( 0 0 100 100 )",ROOT(7));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(7),ROOT(8));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_10(PARAMETERS,ROOT(9));

PUTINTEGER(HITextViewCreate( GETCONSTPOINTER(CGRect,*,TERMINAL(8)),GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(9)),GETPOINTER(0,OpaqueControlRef,**,ROOT(11),NONE)),10);
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(12));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Content_20_HIViewRef,1,1,TERMINAL(3),ROOT(13));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Port_20_Bounds,1,1,TERMINAL(3),ROOT(14));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(14),ROOT(15));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_16(PARAMETERS,TERMINAL(15),ROOT(16));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_17(PARAMETERS,TERMINAL(16),TERMINAL(3),ROOT(17));
FAILONFAILURE

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_18(PARAMETERS,TERMINAL(16),TERMINAL(17));

PUTINTEGER(CreateRootControl( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(19),TERMINAL(17)),GETPOINTER(0,OpaqueControlRef,**,ROOT(20),NONE)),18);
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(3),ROOT(21));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(22));

result = vpx_constant(PARAMETERS,"NULL",ROOT(23));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(5),ROOT(24));

PUTINTEGER(CreateEditUnicodeTextControl( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(26),TERMINAL(21)),GETCONSTPOINTER(Rect,*,TERMINAL(24)),GETCONSTPOINTER(__CFString,*,TERMINAL(23)),GETINTEGER(TERMINAL(22)),GETCONSTPOINTER(ControlFontStyleRec,*,TERMINAL(23)),GETPOINTER(0,OpaqueControlRef,**,ROOT(27),NONE)),25);
result = kSuccess;

PUTINTEGER(HIViewAddSubview( GETPOINTER(0,OpaqueControlRef,*,ROOT(29),TERMINAL(13)),GETPOINTER(0,OpaqueControlRef,*,ROOT(30),TERMINAL(27))),28);
result = kSuccess;

PUTINTEGER(HIViewSetVisible( GETPOINTER(0,OpaqueControlRef,*,ROOT(32),TERMINAL(27)),GETINTEGER(TERMINAL(12))),31);
result = kSuccess;

PUTINTEGER(EnableControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(34),TERMINAL(27))),33);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(2),TERMINAL(27),ROOT(35));

result = vpx_method_VPLHIText_20_Editor_2F_Open_case_3_local_29(PARAMETERS,TERMINAL(35));

result = vpx_set(PARAMETERS,kVPXValue_Window,TERMINAL(35),TERMINAL(17),ROOT(36));

PUTINTEGER(AdvanceKeyboardFocus( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(38),TERMINAL(21))),37);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(39)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
if(vpx_method_VPLHIText_20_Editor_2F_Open_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Editor_2F_Open_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Draw_20_CG(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(7)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(2));
FAILONFAILURE

result = vpx_constant(PARAMETERS,"NULL",ROOT(3));

result = vpx_extconstant(PARAMETERS,kTXNDrawItemAllMask,ROOT(4));

PUTINTEGER(TXNDrawObject( GETPOINTER(0,OpaqueTXNObject,*,ROOT(6),TERMINAL(2)),GETCONSTPOINTER(CGRect,*,TERMINAL(3)),GETINTEGER(TERMINAL(4))),5);
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(7)
}

enum opTrigger vpx_method_VPLHIText_20_Editor_2F_Set_20_Frame(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(6)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
TERMINATEONSUCCESS

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(1),ROOT(4));

SetControlBounds( GETPOINTER(0,OpaqueControlRef,*,ROOT(5),TERMINAL(3)),GETCONSTPOINTER(Rect,*,TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(6)
}

/* Stop Universals */



Nat4 VPLC_VPLHIText_20_Item_20_Editor_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLHIText_20_Item_20_Editor_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Edit Control",tempClass,NULL,environment);
	tempAttribute = attribute_add("Window",tempClass,NULL,environment);
	tempAttribute = attribute_add("Event Handler",tempClass,tempAttribute_VPLHIText_20_Item_20_Editor_2F_Event_20_Handler,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"VPLHIText Editor");
	return kNOERROR;
}

/* Start Universals: { 301 593 }{ 200 300 } */
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Event_20_Class,1,1,TERMINAL(1),ROOT(2));

result = vpx_extmatch(PARAMETERS,kEventClassTextInput,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Event_20_Kind,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_CE_20_Text_20_Input_20_Event,3,1,TERMINAL(0),TERMINAL(3),TERMINAL(1),ROOT(4));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Return_20_Event_20_Not_20_Handled,1,1,TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kEventTextInputUnicodeForKeyEvent,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_CE_20_Unicode_20_For_20_Key,2,1,TERMINAL(0),TERMINAL(2),ROOT(3));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTER(4)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0))
vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(40)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Selected,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(7),ROOT(8),ROOT(9));

result = vpx_get(PARAMETERS,kVPXValue_Type,TERMINAL(9),ROOT(10),ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Text,1,1,TERMINAL(0),ROOT(12));

result = vpx_persistent(PARAMETERS,kVPXValue_Minimum_20_Size_20_Text,0,1,ROOT(13));

result = vpx_call_primitive(PARAMETERS,VPLP__22_join_22_,2,1,TERMINAL(12),TERMINAL(13),ROOT(14));

result = vpx_method_Measure_20_Text(PARAMETERS,TERMINAL(14),TERMINAL(11),TERMINAL(3),ROOT(15));

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(16),ROOT(17));

PUTPOINTER(Rect,*,GetControlBounds( GETPOINTER(0,OpaqueControlRef,*,ROOT(19),TERMINAL(17)),GETPOINTER(8,Rect,*,ROOT(20),NONE)),18);
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,TERMINAL(20),ROOT(21));

result = vpx_method_Rect_20_To_20_Ints(PARAMETERS,TERMINAL(21),ROOT(22),ROOT(23),ROOT(24),ROOT(25));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(25),TERMINAL(23),ROOT(26));

result = vpx_extget(PARAMETERS,"h",1,2,TERMINAL(15),ROOT(27),ROOT(28));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(28),TERMINAL(26),ROOT(29));

result = vpx_extget(PARAMETERS,"v",1,2,TERMINAL(15),ROOT(30),ROOT(31));

result = vpx_method_Half(PARAMETERS,TERMINAL(29),ROOT(32));

result = vpx_method_Int(PARAMETERS,TERMINAL(32),ROOT(33));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(23),TERMINAL(33),ROOT(34));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(25),TERMINAL(33),ROOT(35));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(22),TERMINAL(31),ROOT(36));

result = vpx_method_Ints_20_To_20_Rect(PARAMETERS,TERMINAL(22),TERMINAL(34),TERMINAL(36),TERMINAL(35),ROOT(37));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(37),ROOT(38));

SetControlBounds( GETPOINTER(0,OpaqueControlRef,*,ROOT(39),TERMINAL(17)),GETCONSTPOINTER(Rect,*,TERMINAL(38)));
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(40)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object *root0)
{
HEADER(13)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(4),ROOT(5),ROOT(6));

result = vpx_get(PARAMETERS,kVPXValue_Type,TERMINAL(6),ROOT(7),ROOT(8));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Text,1,1,TERMINAL(1),ROOT(9));

result = vpx_persistent(PARAMETERS,kVPXValue_Minimum_20_Size_20_Text,0,1,ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP__22_join_22_,2,1,TERMINAL(9),TERMINAL(10),ROOT(11));

result = vpx_method_Measure_20_Text(PARAMETERS,TERMINAL(11),TERMINAL(8),TERMINAL(2),ROOT(12));

result = kSuccess;

OUTPUT(0,TERMINAL(12))
FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(21)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame_20_In_20_Window,1,1,TERMINAL(0),ROOT(4));

result = vpx_method_Rect_20_To_20_Ints(PARAMETERS,TERMINAL(4),ROOT(5),ROOT(6),ROOT(7),ROOT(8));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(8),TERMINAL(6),ROOT(9));

result = vpx_extget(PARAMETERS,"h",1,2,TERMINAL(1),ROOT(10),ROOT(11));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(11),TERMINAL(9),ROOT(12));

result = vpx_extget(PARAMETERS,"v",1,2,TERMINAL(1),ROOT(13),ROOT(14));

result = vpx_method_Half(PARAMETERS,TERMINAL(12),ROOT(15));

result = vpx_method_Int(PARAMETERS,TERMINAL(15),ROOT(16));

result = vpx_call_primitive(PARAMETERS,VPLP__2D2D_,2,1,TERMINAL(6),TERMINAL(16),ROOT(17));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(8),TERMINAL(16),ROOT(18));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(5),TERMINAL(14),ROOT(19));

result = vpx_method_Ints_20_To_20_Rect(PARAMETERS,TERMINAL(5),TERMINAL(17),TERMINAL(19),TERMINAL(18),ROOT(20));

result = kSuccess;

OUTPUT(0,TERMINAL(20))
FOOTERSINGLECASE(21)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(8)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(1),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Selected,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2_local_6(PARAMETERS,TERMINAL(3),TERMINAL(0),TERMINAL(5),ROOT(6));

result = vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2_local_7(PARAMETERS,TERMINAL(3),TERMINAL(6),ROOT(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Frame,2,0,TERMINAL(3),TERMINAL(7));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Redraw,2,0,TERMINAL(3),NONE);

result = kSuccess;

FOOTERWITHNONE(8)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
if(vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Call_20_Next_20_Event_20_Handler,1,1,TERMINAL(1),ROOT(4));

result = vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1_local_4(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(2)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0))
vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Initialize_20_Control_20_Events,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_Close(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Destruct_20_Control_20_Events,1,0,TERMINAL(0));

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_Initialize_20_Control_20_Events(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Handler,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_As_20_Handler,2,0,TERMINAL(2),TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_Destruct_20_Control_20_Events(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Event_20_Handler,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHIText_20_Item_20_Editor_2F_Get_20_Carbon_20_EventTargetRef(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_match(PARAMETERS,"NULL",TERMINAL(2));
FAILONSUCCESS

PUTPOINTER(OpaqueEventTargetRef,*,GetControlEventTarget( GETPOINTER(0,OpaqueControlRef,*,ROOT(4),TERMINAL(2))),3);
result = kSuccess;

result = vpx_match(PARAMETERS,"NULL",TERMINAL(3));
FAILONSUCCESS

result = kSuccess;

OUTPUT(0,TERMINAL(3))
FOOTERSINGLECASE(5)
}

/* Stop Universals */



Nat4 VPLC_VPLHICombo_20_Box_20_Editor_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_VPLHICombo_20_Box_20_Editor_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Parent",tempClass,NULL,environment);
	tempAttribute = attribute_add("Edit Control",tempClass,NULL,environment);
	tempAttribute = attribute_add("Window",tempClass,NULL,environment);
	tempAttribute = attribute_add("Event Handler",tempClass,tempAttribute_VPLHICombo_20_Box_20_Editor_2F_Event_20_Handler,environment);
	tempAttribute = attribute_add("List Array",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"VPLHIText Item Editor");
	return kNOERROR;
}

/* Start Universals: { 182 694 }{ 200 300 } */
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(9)
INPUT(0,0)
result = kSuccess;

result = vpx_extget(PARAMETERS,"size",1,2,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_extget(PARAMETERS,"width",1,2,TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_constant(PARAMETERS,"32",ROOT(5));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_extset(PARAMETERS,"width",2,1,TERMINAL(3),TERMINAL(6),ROOT(7));

result = vpx_extset(PARAMETERS,"size",2,1,TERMINAL(1),TERMINAL(7),ROOT(8));

result = kSuccess;

OUTPUT(0,TERMINAL(8))
FOOTERSINGLECASE(9)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(3)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kControlUseThemeFontIDMask,ROOT(0));

result = vpx_extconstant(PARAMETERS,kControlUseJustMask,ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(8)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_type,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"Operation Item",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_get(PARAMETERS,kVPXValue_Data,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_get(PARAMETERS,kVPXValue_Helper,TERMINAL(3),ROOT(4),ROOT(5));

result = vpx_get(PARAMETERS,kVPXValue_Type,TERMINAL(5),ROOT(6),ROOT(7));

result = kSuccess;

OUTPUT(0,TERMINAL(7))
FOOTER(8)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_type,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"Comment Item",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_constant(PARAMETERS,"comment",ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"list item",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(19)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"ControlFontStyleRec",ROOT(1));

result = vpx_constant(PARAMETERS,"28",ROOT(2));

result = vpx_constant(PARAMETERS,"1",ROOT(3));

result = vpx_method_Create_20_Block_20_Type(PARAMETERS,TERMINAL(1),TERMINAL(2),TERMINAL(3),ROOT(4));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_6(PARAMETERS,ROOT(5));

result = vpx_extset(PARAMETERS,"flags",2,1,TERMINAL(4),TERMINAL(5),ROOT(6));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(7));

result = vpx_get(PARAMETERS,kVPXValue_Selected,TERMINAL(0),ROOT(8),ROOT(9));

result = vpx_get(PARAMETERS,kVPXValue_Parent,TERMINAL(0),ROOT(10),ROOT(11));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5_case_1_local_11(PARAMETERS,TERMINAL(11),ROOT(12));

result = vpx_method_Set_20_Operation_20_Font(PARAMETERS,TERMINAL(12),TERMINAL(7),TERMINAL(9),ROOT(13),ROOT(14));

result = vpx_extset(PARAMETERS,"font",2,1,TERMINAL(6),TERMINAL(13),ROOT(15));

result = vpx_get(PARAMETERS,kVPXValue_Justification,TERMINAL(0),ROOT(16),ROOT(17));

result = vpx_extset(PARAMETERS,"just",2,1,TERMINAL(15),TERMINAL(17),ROOT(18));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(19)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(3)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kHIComboBoxAutoSizeListAttribute,ROOT(0));

result = vpx_extconstant(PARAMETERS,kHIComboBoxAutoCompletionAttribute,ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_bit_2D_or,2,1,TERMINAL(1),TERMINAL(0),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(28)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Frame_20_In_20_Window,1,1,TERMINAL(1),ROOT(2));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(2),ROOT(3));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_4(PARAMETERS,TERMINAL(3),ROOT(4));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_5(PARAMETERS,TERMINAL(1),ROOT(5));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1_local_6(PARAMETERS,ROOT(6));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_List_20_ArrayRef,1,1,TERMINAL(0),ROOT(7));

result = vpx_constant(PARAMETERS,"NULL",ROOT(8));

PUTINTEGER(HIComboBoxCreate( GETCONSTPOINTER(CGRect,*,TERMINAL(4)),GETCONSTPOINTER(__CFString,*,TERMINAL(8)),GETCONSTPOINTER(ControlFontStyleRec,*,TERMINAL(5)),GETCONSTPOINTER(__CFArray,*,TERMINAL(7)),GETINTEGER(TERMINAL(6)),GETPOINTER(0,OpaqueControlRef,**,ROOT(10),NONE)),9);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(12));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(12));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(13));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Content_20_HIViewRef,1,1,TERMINAL(12),ROOT(14));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(12),ROOT(15));

PUTINTEGER(HIViewAddSubview( GETPOINTER(0,OpaqueControlRef,*,ROOT(17),TERMINAL(14)),GETPOINTER(0,OpaqueControlRef,*,ROOT(18),TERMINAL(10))),16);
result = kSuccess;

PUTINTEGER(HIViewSetVisible( GETPOINTER(0,OpaqueControlRef,*,ROOT(20),TERMINAL(10)),GETINTEGER(TERMINAL(13))),19);
result = kSuccess;

PUTINTEGER(EnableControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(22),TERMINAL(10))),21);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(11),TERMINAL(10),ROOT(23));

result = vpx_extconstant(PARAMETERS,kControlFocusNextPart,ROOT(24));

PUTINTEGER(SetKeyboardFocus( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(26),TERMINAL(15)),GETPOINTER(0,OpaqueControlRef,*,ROOT(27),TERMINAL(10)),GETINTEGER(TERMINAL(24))),25);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(28)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(5)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kOutputTextInUnicodeEncodingBit,ROOT(0));

result = vpx_extconstant(PARAMETERS,kTXNSingleLineOnlyBit,ROOT(1));

result = vpx_extconstant(PARAMETERS,kTXNMonostyledTextBit,ROOT(2));

result = vpx_extconstant(PARAMETERS,kTXNDoFontSubstitutionBit,ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,3,1,TERMINAL(3),TERMINAL(0),TERMINAL(1),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

TXNFocus( GETPOINTER(0,OpaqueTXNObject,*,ROOT(3),TERMINAL(1)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(24)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(3),ROOT(4));

result = vpx_constant(PARAMETERS,"0",ROOT(5));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(4),ROOT(6));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_7(PARAMETERS,ROOT(7));

PUTINTEGER(HITextViewCreate( GETCONSTPOINTER(CGRect,*,TERMINAL(6)),GETINTEGER(TERMINAL(5)),GETINTEGER(TERMINAL(7)),GETPOINTER(0,OpaqueControlRef,**,ROOT(9),NONE)),8);
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(10));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(11));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Content_20_HIViewRef,1,1,TERMINAL(11),ROOT(12));

PUTINTEGER(HIViewAddSubview( GETPOINTER(0,OpaqueControlRef,*,ROOT(14),TERMINAL(12)),GETPOINTER(0,OpaqueControlRef,*,ROOT(15),TERMINAL(9))),13);
result = kSuccess;

PUTINTEGER(HIViewSetVisible( GETPOINTER(0,OpaqueControlRef,*,ROOT(17),TERMINAL(9)),GETINTEGER(TERMINAL(10))),16);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(2),TERMINAL(9),ROOT(18));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2_local_15(PARAMETERS,TERMINAL(18));

result = vpx_extconstant(PARAMETERS,kControlFocusNextPart,ROOT(19));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(11),ROOT(20));

PUTINTEGER(SetKeyboardFocus( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(22),TERMINAL(20)),GETPOINTER(0,OpaqueControlRef,*,ROOT(23),TERMINAL(9)),GETINTEGER(TERMINAL(19))),21);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(24)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_10(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(5)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kOutputTextInUnicodeEncodingBit,ROOT(0));

result = vpx_extconstant(PARAMETERS,kTXNSingleLineOnlyBit,ROOT(1));

result = vpx_extconstant(PARAMETERS,kTXNMonostyledTextBit,ROOT(2));

result = vpx_extconstant(PARAMETERS,kTXNDoFontSubstitutionBit,ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,3,1,TERMINAL(3),TERMINAL(0),TERMINAL(1),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_16(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_16(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(13)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Rect_2D_to_2D_list,1,1,TERMINAL(0),ROOT(1));

result = vpx_constant(PARAMETERS,"2",ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_split_2D_nth,2,2,TERMINAL(1),TERMINAL(2),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(3),ROOT(5));

LocalToGlobal( GETPOINTER(4,Point,*,ROOT(6),TERMINAL(5)));
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Point,1,1,TERMINAL(4),ROOT(7));

LocalToGlobal( GETPOINTER(4,Point,*,ROOT(8),TERMINAL(7)));
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(6),ROOT(9));

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(8),ROOT(10));

result = vpx_call_primitive(PARAMETERS,VPLP__28_join_29_,2,1,TERMINAL(9),TERMINAL(10),ROOT(11));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(11),ROOT(12));

result = kSuccess;

OUTPUT(0,TERMINAL(12))
FOOTERSINGLECASE(13)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_17_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_17_case_1_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object *root0)
{
HEADER(3)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kWindowHideOnSuspendAttribute,ROOT(0));

result = vpx_extconstant(PARAMETERS,kWindowIgnoreClicksAttribute,ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP__2B2B_,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_17(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_17(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object *root0)
{
HEADERWITHNONE(15)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_extconstant(PARAMETERS,kOverlayWindowClass,ROOT(2));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_17_case_1_local_3(PARAMETERS,ROOT(3));

result = vpx_extconstant(PARAMETERS,kDocumentWindowClass,ROOT(4));

PUTINTEGER(CreateNewWindow( GETINTEGER(TERMINAL(2)),GETINTEGER(TERMINAL(3)),GETCONSTPOINTER(Rect,*,TERMINAL(0)),GETPOINTER(0,OpaqueWindowPtr,**,ROOT(6),NONE)),5);
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(5));
FAILONFAILURE

SetPortWindowPort( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(7),TERMINAL(6)));
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(1),ROOT(8));

PUTPOINTER(OpaqueWindowGroupRef,*,GetWindowGroup( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(10),TERMINAL(8))),9);
result = kSuccess;

PUTINTEGER(SetWindowGroup( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(12),TERMINAL(6)),GETPOINTER(0,OpaqueWindowGroupRef,*,ROOT(13),TERMINAL(9))),11);
result = kSuccess;

ShowWindow( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(14),TERMINAL(6)));
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(6))
FOOTERSINGLECASEWITHNONE(15)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.09",ROOT(1));

result = vpx_constant(PARAMETERS,"0.93",ROOT(2));

result = vpx_constant(PARAMETERS,"0.12",ROOT(3));

result = vpx_constant(PARAMETERS,"0.4",ROOT(4));

CGContextSetRGBStrokeColor( GETPOINTER(0,CGContext,*,ROOT(5),TERMINAL(0)),GETREAL(TERMINAL(2)),GETREAL(TERMINAL(1)),GETREAL(TERMINAL(3)),GETREAL(TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(16)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.4",ROOT(1));

result = vpx_constant(PARAMETERS,"65535",ROOT(2));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(3));

result = vpx_constant(PARAMETERS,"0",ROOT(4));

result = vpx_extconstant(PARAMETERS,kThemeBrushPrimaryHighlightColor,ROOT(5));

result = vpx_extconstant(PARAMETERS,kThemeBrushAlternatePrimaryHighlightColor,ROOT(6));

result = vpx_extconstant(PARAMETERS,kThemeBrushSecondaryHighlightColor,ROOT(7));

PUTINTEGER(GetThemeBrushAsColor( GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(4)),GETINTEGER(TERMINAL(3)),GETPOINTER(8,RGBColor,*,ROOT(9),NONE)),8);
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_RGB_2D_to_2D_list,1,1,TERMINAL(9),ROOT(10));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(10))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_primitive(PARAMETERS,VPLP_div,2,1,LIST(10),TERMINAL(2),ROOT(11));
LISTROOT(11,0)
REPEATFINISH
LISTROOTFINISH(11,0)
LISTROOTEND
} else {
ROOTEMPTY(11)
}

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,3,TERMINAL(11),ROOT(12),ROOT(13),ROOT(14));

CGContextSetRGBStrokeColor( GETPOINTER(0,CGContext,*,ROOT(15),TERMINAL(0)),GETREAL(TERMINAL(12)),GETREAL(TERMINAL(13)),GETREAL(TERMINAL(14)),GETREAL(TERMINAL(1)));
result = kSuccess;

result = vpx_extmatch(PARAMETERS,noErr,TERMINAL(8));
NEXTCASEONFAILURE

result = kSuccess;

FOOTERWITHNONE(16)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.09",ROOT(1));

result = vpx_constant(PARAMETERS,"0.80",ROOT(2));

result = vpx_constant(PARAMETERS,"0.13",ROOT(3));

result = vpx_constant(PARAMETERS,"0.4",ROOT(4));

CGContextSetRGBStrokeColor( GETPOINTER(0,CGContext,*,ROOT(5),TERMINAL(0)),GETREAL(TERMINAL(1)),GETREAL(TERMINAL(3)),GETREAL(TERMINAL(2)),GETREAL(TERMINAL(4)));
result = kSuccess;

result = kSuccess;

FOOTER(6)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_4(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.53",ROOT(1));

result = vpx_constant(PARAMETERS,"0.25",ROOT(2));

CGContextSetGrayStrokeColor( GETPOINTER(0,CGContext,*,ROOT(3),TERMINAL(0)),GETREAL(TERMINAL(1)),GETREAL(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0))
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19_case_4(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(28)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(2));

result = vpx_method_Rect_20_Size(PARAMETERS,TERMINAL(0),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_Point_2D_to_2D_list,1,1,TERMINAL(3),ROOT(4));

if( (repeatLimit = vpx_multiplex(1,TERMINAL(4))) ){
LISTROOTBEGIN(1)
REPEATBEGINWITHCOUNTER
result = vpx_call_primitive(PARAMETERS,VPLP__2B_1,1,1,LIST(4),ROOT(5));
LISTROOT(5,0)
REPEATFINISH
LISTROOTFINISH(5,0)
LISTROOTEND
} else {
ROOTEMPTY(5)
}

result = vpx_call_primitive(PARAMETERS,VPLP_unpack,1,2,TERMINAL(5),ROOT(6),ROOT(7));

result = vpx_method_New_20_CGRect(PARAMETERS,TERMINAL(2),TERMINAL(2),TERMINAL(7),TERMINAL(6),ROOT(8));

PUTPOINTER(OpaqueGrafPtr,*,GetWindowPort( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(10),TERMINAL(1))),9);
result = kSuccess;

PUTINTEGER(CreateCGContextForPort( GETPOINTER(0,OpaqueGrafPtr,*,ROOT(12),TERMINAL(9)),GETPOINTER(0,CGContext,**,ROOT(13),NONE)),11);
result = kSuccess;

result = vpx_constant(PARAMETERS,"3.0",ROOT(14));

CGContextSetLineWidth( GETPOINTER(0,CGContext,*,ROOT(15),TERMINAL(13)),GETREAL(TERMINAL(14)));
result = kSuccess;

PUTINTEGER(SyncCGContextOriginWithPort( GETPOINTER(0,CGContext,*,ROOT(17),TERMINAL(13)),GETPOINTER(0,OpaqueGrafPtr,*,ROOT(18),TERMINAL(9))),16);
result = kSuccess;

result = vpx_constant(PARAMETERS,"0.0",ROOT(19));

result = vpx_call_primitive(PARAMETERS,VPLP__2D_1,1,1,TERMINAL(6),ROOT(20));

CGContextTranslateCTM( GETPOINTER(0,CGContext,*,ROOT(21),TERMINAL(13)),GETREAL(TERMINAL(19)),GETREAL(TERMINAL(20)));
result = kSuccess;

result = vpx_constant(PARAMETERS,"1.0",ROOT(22));

result = vpx_constant(PARAMETERS,"-1.0",ROOT(23));

CGContextScaleCTM( GETPOINTER(0,CGContext,*,ROOT(24),TERMINAL(13)),GETREAL(TERMINAL(22)),GETREAL(TERMINAL(23)));
result = kSuccess;

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18_case_1_local_19(PARAMETERS,TERMINAL(13));

result = vpx_extconstant(PARAMETERS,kCGLineCapRound,ROOT(25));

CGContextSetLineCap( GETPOINTER(0,CGContext,*,ROOT(26),TERMINAL(13)),GETINTEGER(TERMINAL(25)));
result = kSuccess;

CGContextRelease( GETPOINTER(0,CGContext,*,ROOT(27),TERMINAL(13)));
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASEWITHNONE(28)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_TXNObject,1,1,TERMINAL(0),ROOT(1));
TERMINATEONFAILURE

result = vpx_constant(PARAMETERS,"TRUE",ROOT(2));

TXNFocus( GETPOINTER(0,OpaqueTXNObject,*,ROOT(3),TERMINAL(1)),GETINTEGER(TERMINAL(2)));
result = kSuccess;

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(39)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Parent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Port,1,0,TERMINAL(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Up_20_Drawing,1,0,TERMINAL(1));

result = vpx_get(PARAMETERS,kVPXValue_Frame,TERMINAL(1),ROOT(4),ROOT(5));

result = vpx_constant(PARAMETERS,"0",ROOT(6));

result = vpx_constant(PARAMETERS,"( 0 0 100 100 )",ROOT(7));

result = vpx_method_list_2D_to_2D_CGRect(PARAMETERS,TERMINAL(7),ROOT(8));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_10(PARAMETERS,ROOT(9));

PUTINTEGER(HITextViewCreate( GETCONSTPOINTER(CGRect,*,TERMINAL(8)),GETINTEGER(TERMINAL(6)),GETINTEGER(TERMINAL(9)),GETPOINTER(0,OpaqueControlRef,**,ROOT(11),NONE)),10);
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(12));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Content_20_HIViewRef,1,1,TERMINAL(3),ROOT(13));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Port_20_Bounds,1,1,TERMINAL(3),ROOT(14));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(14),ROOT(15));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_16(PARAMETERS,TERMINAL(15),ROOT(16));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_17(PARAMETERS,TERMINAL(16),TERMINAL(3),ROOT(17));
FAILONFAILURE

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_18(PARAMETERS,TERMINAL(16),TERMINAL(17));

PUTINTEGER(CreateRootControl( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(19),TERMINAL(17)),GETPOINTER(0,OpaqueControlRef,**,ROOT(20),NONE)),18);
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_WindowRef,1,1,TERMINAL(3),ROOT(21));

result = vpx_constant(PARAMETERS,"FALSE",ROOT(22));

result = vpx_constant(PARAMETERS,"NULL",ROOT(23));

result = vpx_call_primitive(PARAMETERS,VPLP_list_2D_to_2D_Rect,1,1,TERMINAL(5),ROOT(24));

PUTINTEGER(CreateEditUnicodeTextControl( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(26),TERMINAL(21)),GETCONSTPOINTER(Rect,*,TERMINAL(24)),GETCONSTPOINTER(__CFString,*,TERMINAL(23)),GETINTEGER(TERMINAL(22)),GETCONSTPOINTER(ControlFontStyleRec,*,TERMINAL(23)),GETPOINTER(0,OpaqueControlRef,**,ROOT(27),NONE)),25);
result = kSuccess;

PUTINTEGER(HIViewAddSubview( GETPOINTER(0,OpaqueControlRef,*,ROOT(29),TERMINAL(13)),GETPOINTER(0,OpaqueControlRef,*,ROOT(30),TERMINAL(27))),28);
result = kSuccess;

PUTINTEGER(HIViewSetVisible( GETPOINTER(0,OpaqueControlRef,*,ROOT(32),TERMINAL(27)),GETINTEGER(TERMINAL(12))),31);
result = kSuccess;

PUTINTEGER(EnableControl( GETPOINTER(0,OpaqueControlRef,*,ROOT(34),TERMINAL(27))),33);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Edit_20_Control,TERMINAL(2),TERMINAL(27),ROOT(35));

result = vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3_local_29(PARAMETERS,TERMINAL(35));

result = vpx_set(PARAMETERS,kVPXValue_Window,TERMINAL(35),TERMINAL(17),ROOT(36));

PUTINTEGER(AdvanceKeyboardFocus( GETPOINTER(0,OpaqueWindowPtr,*,ROOT(38),TERMINAL(21))),37);
result = kSuccess;

result = kSuccess;

FOOTERWITHNONE(39)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Control,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Initialize_20_Control_20_Events,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_List_20_Array,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_CF_20_Reference,1,1,TERMINAL(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTER(2)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_Array(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_List_20_Array,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Set_20_List_20_Array(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_List_20_Array,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_TXNObject(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERSINGLECASEWITHNONE(1)
}

/* Stop Universals */






Nat4	loadClasses_VPLEdit_20_Text(V_Environment environment);
Nat4	loadClasses_VPLEdit_20_Text(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("VPLEdit Text",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLEdit_20_Text_class_load(result,environment);
	result = class_new("VPLEntry Text",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLEntry_20_Text_class_load(result,environment);
	result = class_new("VPLText Editor",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLText_20_Editor_class_load(result,environment);
	result = class_new("VPLHIText Editor",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLHIText_20_Editor_class_load(result,environment);
	result = class_new("VPLHIText Item Editor",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLHIText_20_Item_20_Editor_class_load(result,environment);
	result = class_new("VPLHICombo Box Editor",environment);
	if(result == NULL) return kERROR;
	VPLC_VPLHICombo_20_Box_20_Editor_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_VPLEdit_20_Text(V_Environment environment);
Nat4	loadUniversals_VPLEdit_20_Text(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("VPLEdit Text/Activate",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Activate,NULL);

	result = method_new("VPLEdit Text/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Close,NULL);

	result = method_new("VPLEdit Text/Draw",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Draw,NULL);

	result = method_new("VPLEdit Text/Enter",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Enter,NULL);

	result = method_new("VPLEdit Text/OLD Enter",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_OLD_20_Enter,NULL);

	result = method_new("VPLEdit Text/Exit",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Exit,NULL);

	result = method_new("VPLEdit Text/Get Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Get_20_Text,NULL);

	result = method_new("VPLEdit Text/Hilite",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Hilite,NULL);

	result = method_new("VPLEdit Text/Idle",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Idle,NULL);

	result = method_new("VPLEdit Text/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Open,NULL);

	result = method_new("VPLEdit Text/Process Click",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Process_20_Click,NULL);

	result = method_new("VPLEdit Text/Process Key",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Process_20_Key,NULL);

	result = method_new("VPLEdit Text/Set Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Set_20_Text,NULL);

	result = method_new("VPLEdit Text/Set Value",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Set_20_Value,NULL);

	result = method_new("VPLEdit Text/Select All",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Select_20_All,NULL);

	result = method_new("VPLEdit Text/Draw CG",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Draw_20_CG,NULL);

	result = method_new("VPLEdit Text/Key Down",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Key_20_Down,NULL);

	result = method_new("VPLEdit Text/Set Up Drawing",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Set_20_Up_20_Drawing,NULL);

	result = method_new("VPLEdit Text/Was Content Hit?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Was_20_Content_20_Hit_3F_,NULL);

	result = method_new("VPLEdit Text/Measure Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Measure_20_Text,NULL);

	result = method_new("VPLEdit Text/Set Frame",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Set_20_Frame,NULL);

	result = method_new("VPLEdit Text/Create HIThemeTextInfo",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEdit_20_Text_2F_Create_20_HIThemeTextInfo,NULL);

	result = method_new("VPLEntry Text/Process Key",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLEntry_20_Text_2F_Process_20_Key,NULL);

	result = method_new("VPLText Editor/Old Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Old_20_Open,NULL);

	result = method_new("VPLText Editor/Activate",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Activate,NULL);

	result = method_new("VPLText Editor/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Close,NULL);

	result = method_new("VPLText Editor/Draw",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Draw,NULL);

	result = method_new("VPLText Editor/Get Application",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Get_20_Application,NULL);

	result = method_new("VPLText Editor/Get Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Get_20_Text,NULL);

	result = method_new("VPLText Editor/Idle",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Idle,NULL);

	result = method_new("VPLText Editor/Key Down",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Key_20_Down,NULL);

	result = method_new("VPLText Editor/Process Click",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Process_20_Click,NULL);

	result = method_new("VPLText Editor/Set Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Set_20_Text,NULL);

	result = method_new("VPLText Editor/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Open,NULL);

	result = method_new("VPLText Editor/Set Selection",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLText_20_Editor_2F_Set_20_Selection,NULL);

	result = method_new("VPLHIText Editor/Activate",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Activate,NULL);

	result = method_new("VPLHIText Editor/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Close,NULL);

	result = method_new("VPLHIText Editor/Draw",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Draw,NULL);

	result = method_new("VPLHIText Editor/Get Application",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Get_20_Application,NULL);

	result = method_new("VPLHIText Editor/Get Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Get_20_Text,NULL);

	result = method_new("VPLHIText Editor/Set Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Set_20_Text,NULL);

	result = method_new("VPLHIText Editor/Idle",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Idle,NULL);

	result = method_new("VPLHIText Editor/Key Down",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Key_20_Down,NULL);

	result = method_new("VPLHIText Editor/Process Click",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Process_20_Click,NULL);

	result = method_new("VPLHIText Editor/Set Selection",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Set_20_Selection,NULL);

	result = method_new("VPLHIText Editor/Get TXNObject",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Get_20_TXNObject,NULL);

	result = method_new("VPLHIText Editor/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Open,NULL);

	result = method_new("VPLHIText Editor/Draw CG",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Draw_20_CG,NULL);

	result = method_new("VPLHIText Editor/Set Frame",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Editor_2F_Set_20_Frame,NULL);

	result = method_new("VPLHIText Item Editor/CE Handle Event",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Handle_20_Event,NULL);

	result = method_new("VPLHIText Item Editor/CE Text Input Event",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Text_20_Input_20_Event,NULL);

	result = method_new("VPLHIText Item Editor/CE Unicode For Key",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_CE_20_Unicode_20_For_20_Key,NULL);

	result = method_new("VPLHIText Item Editor/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_Open,NULL);

	result = method_new("VPLHIText Item Editor/Close",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_Close,NULL);

	result = method_new("VPLHIText Item Editor/Initialize Control Events",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_Initialize_20_Control_20_Events,NULL);

	result = method_new("VPLHIText Item Editor/Destruct Control Events",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_Destruct_20_Control_20_Events,NULL);

	result = method_new("VPLHIText Item Editor/Get Carbon EventTargetRef",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHIText_20_Item_20_Editor_2F_Get_20_Carbon_20_EventTargetRef,NULL);

	result = method_new("VPLHICombo Box Editor/Open Control",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open_20_Control,NULL);

	result = method_new("VPLHICombo Box Editor/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHICombo_20_Box_20_Editor_2F_Open,NULL);

	result = method_new("VPLHICombo Box Editor/Get List ArrayRef",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_ArrayRef,NULL);

	result = method_new("VPLHICombo Box Editor/Get List Array",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_List_20_Array,NULL);

	result = method_new("VPLHICombo Box Editor/Set List Array",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHICombo_20_Box_20_Editor_2F_Set_20_List_20_Array,NULL);

	result = method_new("VPLHICombo Box Editor/Get TXNObject",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_VPLHICombo_20_Box_20_Editor_2F_Get_20_TXNObject,NULL);

	return kNOERROR;

}



Nat4	loadPersistents_VPLEdit_20_Text(V_Environment environment);
Nat4	loadPersistents_VPLEdit_20_Text(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	return kNOERROR;

}

Nat4	load_VPLEdit_20_Text(V_Environment environment);
Nat4	load_VPLEdit_20_Text(V_Environment environment)
{

	loadClasses_VPLEdit_20_Text(environment);
	loadUniversals_VPLEdit_20_Text(environment);
	loadPersistents_VPLEdit_20_Text(environment);
	return kNOERROR;

}

