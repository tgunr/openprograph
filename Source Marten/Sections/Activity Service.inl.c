/* A VPL Section File */
/*

Activity Service.inl.c
Copyright: 2004 Andescotia LLC

*/

#include "MartenInlineProject.h"




	Nat4 tempAttribute_Activity_20_Service_2F_Name[] = {
0000000000, 0X00000028, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000A, 0X41637469, 0X76697469, 0X65730000
	};
	Nat4 tempAttribute_Activity_20_Service_2F_Required_20_Services[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Service_2F_Initial_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0X00010000
	};
	Nat4 tempAttribute_Activity_20_Service_2F_Active_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Service_2F_Activities[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Abstract_2F_Name[] = {
0000000000, 0X00000030, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000011, 0X556E7469, 0X746C6564, 0X20416374,
0X69766974, 0X79000000
	};
	Nat4 tempAttribute_Activity_20_Abstract_2F_Canceled_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Abstract_2F_Completed_3F_[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000003, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Abstract_2F_Completion_20_Callback[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Abstract_2F_Attachments[] = {
0000000000, 0X0000001C, 0X00000014, 0000000000, 0X00000014, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Progress_2F_Start_20_Time[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X02D30004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Progress_2F_Wait_20_Time[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000003
	};
	Nat4 tempAttribute_Activity_20_Progress_2F_Activity_20_Name[] = {
0000000000, 0X00000028, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X02D20006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X00000008, 0X556E7469, 0X746C6564, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Progress_2F_Primary_20_Prompt[] = {
0000000000, 0X00000020, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X02D30006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Progress_2F_Secondary_20_Prompt[] = {
0000000000, 0X00000020, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X02D30006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Progress_2F_Progress_20_Percent[] = {
0000000000, 0X00000018, 0X00000014, 0000000000, 0X00000014, 0X02DD0004, 0000000000, 0000000000,
0000000000, 0000000000, 0000000000
	};
	Nat4 tempAttribute_Activity_20_Progress_20_Window_2F_Name[] = {
0000000000, 0X0000002C, 0X00000018, 0X00000001, 0X00000014, 0X0000002C, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000034, 0X0000000F, 0X556E7469, 0X746C6564, 0X2057696E,
0X646F7700
	};
	Nat4 tempAttribute_Activity_20_Progress_20_Window_2F_Window_20_Event_20_Handler[] = {
0000000000, 0X000005A8, 0X00000114, 0X00000040, 0X00000014, 0X00000688, 0X00000684, 0X0000067C,
0X00000418, 0X00000634, 0X00000630, 0X00000628, 0X00000414, 0X000005E0, 0X000005DC, 0X000005D4,
0X00000410, 0X0000058C, 0X00000588, 0X00000580, 0X0000040C, 0X00000538, 0X00000534, 0X0000052C,
0X00000408, 0X000004E4, 0X000004E0, 0X000004D8, 0X00000404, 0X00000490, 0X0000048C, 0X00000484,
0X00000400, 0X0000043C, 0X00000438, 0X00000430, 0X000003FC, 0X000003F4, 0X00000180, 0X0000017C,
0X000003AC, 0X00000168, 0X00000380, 0X00000164, 0X00000354, 0X0000033C, 0X00000314, 0X0000031C,
0X000002FC, 0X000002F4, 0X0000015C, 0X000002CC, 0X000002B4, 0X0000028C, 0X00000294, 0X00000210,
0X00000268, 0X00000250, 0X00000228, 0X00000230, 0X0000020C, 0X00000204, 0X00000158, 0X000001D4,
0X00000154, 0X000001A4, 0X0000014C, 0X00000128, 0X00000130, 0X00270008, 0000000000, 0000000000,
0000000000, 0000000000, 0X0000014C, 0X00000011, 0X00000134, 0X43617262, 0X6F6E2045, 0X76656E74,
0X2043616C, 0X6C626163, 0X6B000000, 0X00000190, 0000000000, 0X000001C0, 0X000001F0, 0X000002E0,
0000000000, 0X0000036C, 0X00000398, 0000000000, 0000000000, 0000000000, 0000000000, 0X000003C8,
0X000003E0, 0000000000, 0000000000, 0000000000, 0X00000006, 0000000000, 0000000000, 0000000000,
0000000000, 0X000001AC, 0X00000011, 0X6B457665, 0X6E745769, 0X6E646F77, 0X436C6173, 0X73000000,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X000001DC, 0X00000010, 0X2F434520,
0X48616E64, 0X6C652045, 0X76656E74, 0000000000, 0X00000007, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000020C, 0X00000002, 0X00000214, 0X00000278, 0X00150008, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000250, 0X00000001, 0X00000234, 0X41747472, 0X69627574, 0X6520496E,
0X70757420, 0X53706563, 0X69666965, 0X72000000, 0X00000254, 0X00000006, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000270, 0X00000005, 0X4F776E65, 0X72000000, 0X00150008, 0000000000,
0000000000, 0000000000, 0000000000, 0X000002B4, 0X00000001, 0X00000298, 0X41747472, 0X69627574,
0X6520496E, 0X70757420, 0X53706563, 0X69666965, 0X72000000, 0X000002B8, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X000002D4, 0X00000009, 0X54686520, 0X4576656E, 0X74000000,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X000002FC, 0X00000001, 0X00000300,
0X00160008, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000033C, 0X00000001, 0X00000320,
0X41747472, 0X69627574, 0X65204F75, 0X74707574, 0X20537065, 0X63696669, 0X65720000, 0X00000340,
0X00000006, 0000000000, 0000000000, 0000000000, 0000000000, 0X0000035C, 0X0000000F, 0X43616C6C,
0X6261636B, 0X20526573, 0X756C7400, 0X00000006, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000388, 0X0000000F, 0X2F446F20, 0X43452043, 0X616C6C62, 0X61636B00, 0X00000006, 0000000000,
0000000000, 0000000000, 0000000000, 0X000003B4, 0X00000013, 0X4576656E, 0X7448616E, 0X646C6572,
0X50726F63, 0X50747200, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X000003FC, 0X00000008, 0X0000041C,
0X00000470, 0X000004C4, 0X00000518, 0X0000056C, 0X000005C0, 0X00000614, 0X00000668, 0X00000007,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000438, 0X00000002, 0X00000440, 0X00000458,
0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X636D6473, 0X00000004, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000001, 0X00000007, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000048C, 0X00000002, 0X00000494, 0X000004AC, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0X636D6473, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000002, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X000004E0, 0X00000002,
0X000004E8, 0X00000500, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X77696E64,
0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000048, 0X00000007, 0000000000,
0000000000, 0000000000, 0000000000, 0X00000534, 0X00000002, 0X0000053C, 0X00000554, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0X77696E64, 0X00000004, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000005, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000,
0X00000588, 0X00000002, 0X00000590, 0X000005A8, 0X00000004, 0000000000, 0000000000, 0000000000,
0000000000, 0X77696E64, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000006,
0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X000005DC, 0X00000002, 0X000005E4,
0X000005FC, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X77696E64, 0X00000004,
0000000000, 0000000000, 0000000000, 0000000000, 0X00000002, 0X00000007, 0000000000, 0000000000,
0000000000, 0000000000, 0X00000630, 0X00000002, 0X00000638, 0X00000650, 0X00000004, 0000000000,
0000000000, 0000000000, 0000000000, 0X77696E64, 0X00000004, 0000000000, 0000000000, 0000000000,
0000000000, 0X0000001B, 0X00000007, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000684,
0X00000002, 0X0000068C, 0X000006A4, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000,
0X77696E64, 0X00000004, 0000000000, 0000000000, 0000000000, 0000000000, 0X00000050
	};


Nat4 VPLC_Activity_20_Service_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Activity_20_Service_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Activity_20_Service_2F_Name,environment);
	tempAttribute = attribute_add("Required Services",tempClass,tempAttribute_Activity_20_Service_2F_Required_20_Services,environment);
	tempAttribute = attribute_add("Initial?",tempClass,tempAttribute_Activity_20_Service_2F_Initial_3F_,environment);
	tempAttribute = attribute_add("Active?",tempClass,tempAttribute_Activity_20_Service_2F_Active_3F_,environment);
	tempAttribute = attribute_add("Activities",tempClass,tempAttribute_Activity_20_Service_2F_Activities,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Service Abstract");
	return kNOERROR;
}

/* Start Universals: { 542 1012 }{ 200 300 } */
/* Stop Universals */



Nat4 VPLC_Activity_20_Abstract_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Activity_20_Abstract_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 204 304 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Activity_20_Abstract_2F_Name,environment);
	tempAttribute = attribute_add("Canceled?",tempClass,tempAttribute_Activity_20_Abstract_2F_Canceled_3F_,environment);
	tempAttribute = attribute_add("Completed?",tempClass,tempAttribute_Activity_20_Abstract_2F_Completed_3F_,environment);
	tempAttribute = attribute_add("Completion Callback",tempClass,tempAttribute_Activity_20_Abstract_2F_Completion_20_Callback,environment);
	tempAttribute = attribute_add("Attachments",tempClass,tempAttribute_Activity_20_Abstract_2F_Attachments,environment);
	tempAttribute = attribute_add("Progress",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 295 764 }{ 390 306 } */
enum opTrigger vpx_method_Activity_20_Abstract_2F_Get_20_Progress(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Progress,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_User_20_Cancel(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Cancel,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Cancel(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Canceled_3F_,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADERWITHNONE(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Activity_20_Progress,1,1,NONE,ROOT(2));

result = vpx_method_Replace_20_NULL(PARAMETERS,TERMINAL(1),TERMINAL(2),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Progress,TERMINAL(0),TERMINAL(3),ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Begin,2,0,TERMINAL(3),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Finish_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Finish_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Finish,1,0,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Finish_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Finish_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Finish(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Finish_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Activity_20_Abstract_2F_Finish_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Synchronize_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Synchronize_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Synchronize,1,0,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Synchronize_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Synchronize_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Synchronize(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Synchronize_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Activity_20_Abstract_2F_Synchronize_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Progress_20_Percent,2,0,TERMINAL(2),TERMINAL(1));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Activity_20_Name,2,0,TERMINAL(2),TERMINAL(1));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Primary_20_Prompt,2,0,TERMINAL(2),TERMINAL(1));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Secondary_20_Prompt,2,0,TERMINAL(2),TERMINAL(1));

result = kSuccess;

FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Get_20_Canceled_3F_(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Canceled_3F_,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Async_20_Schedule_20_Callback,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Run,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
NEXTCASEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Canceled_3F_,1,1,TERMINAL(0),ROOT(3));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(3));
NEXTCASEONSUCCESS

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_2_local_3(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(0));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Run_20_End,1,0,TERMINAL(0));
TERMINATEONFAILURE

result = vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_2_local_3(PARAMETERS,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Async_20_Schedule_20_Callback(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"/Do Async Callback",ROOT(1));

result = vpx_method_New_20_Callback(PARAMETERS,TERMINAL(1),TERMINAL(0),NONE,ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Async_20_Repeat,1,1,TERMINAL(0),ROOT(3));

result = vpx_constant(PARAMETERS,"TRUE",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Async_20_Delay,1,1,TERMINAL(0),ROOT(5));

result = vpx_method_Execute_20_Callback_20_Deferred(PARAMETERS,TERMINAL(2),NONE,TERMINAL(5),TERMINAL(3),TERMINAL(4));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(6)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Async_20_Delay(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"0",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Async_20_Repeat(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(6)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"1",ROOT(1));

result = vpx_constant(PARAMETERS,"1000",ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_div,2,1,TERMINAL(1),TERMINAL(2),ROOT(3));

result = vpx_constant(PARAMETERS,"1.0",ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP__2A_,2,1,TERMINAL(3),TERMINAL(4),ROOT(5));

result = kSuccess;

OUTPUT(0,TERMINAL(5))
FOOTERSINGLECASE(6)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Sync_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Sync_case_1_local_4(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Run,1,1,TERMINAL(0),ROOT(1));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
TERMINATEONSUCCESS

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Canceled_3F_,1,1,TERMINAL(0),ROOT(2));

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(2));
TERMINATEONSUCCESS

result = vpx_method_Dispatch_20_Event(PARAMETERS);

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_20_Sync(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"FALSE",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Run_20_Begin,2,0,TERMINAL(0),TERMINAL(1));

REPEATBEGIN
result = vpx_method_Activity_20_Abstract_2F_Do_20_Sync_case_1_local_4(PARAMETERS,TERMINAL(0));
REPEATFINISH

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Run_20_End,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_match(PARAMETERS,"TRUE",TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do_20_Async,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Abstract_2F_Do_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Do_20_Sync,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Do(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Abstract_2F_Do_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1))
vpx_method_Activity_20_Abstract_2F_Do_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Run_20_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(0),ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Begin,2,0,TERMINAL(0),TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Run(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"TRUE",ROOT(1));

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERSINGLECASE(2)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Run_20_End(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Finish,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Activity_20_Abstract_2F_Initialize_20_Progress_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(2)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = kSuccess;

FOOTERSINGLECASE(2)
}

/* Stop Universals */



Nat4 VPLC_Activity_20_Progress_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Activity_20_Progress_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 252 332 }{ 200 300 } */
	tempAttribute = attribute_add("Start Time",tempClass,tempAttribute_Activity_20_Progress_2F_Start_20_Time,environment);
	tempAttribute = attribute_add("Wait Time",tempClass,tempAttribute_Activity_20_Progress_2F_Wait_20_Time,environment);
	tempAttribute = attribute_add("Activity Name",tempClass,tempAttribute_Activity_20_Progress_2F_Activity_20_Name,environment);
	tempAttribute = attribute_add("Primary Prompt",tempClass,tempAttribute_Activity_20_Progress_2F_Primary_20_Prompt,environment);
	tempAttribute = attribute_add("Secondary Prompt",tempClass,tempAttribute_Activity_20_Progress_2F_Secondary_20_Prompt,environment);
	tempAttribute = attribute_add("Progress Percent",tempClass,tempAttribute_Activity_20_Progress_2F_Progress_20_Percent,environment);
	tempAttribute = attribute_add("Window",tempClass,NULL,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
/* Stop Attributes */

/* NULL SUPER CLASS */
	return kNOERROR;
}

/* Start Universals: { 387 1012 }{ 359 303 } */
enum opTrigger vpx_method_Activity_20_Progress_2F_Get_20_Activity_20_Name(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Activity_20_Name,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Get_20_Primary_20_Prompt(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Primary_20_Prompt,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Get_20_Secondary_20_Prompt(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Secondary_20_Prompt,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Get_20_Progress_20_Percent(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Progress_20_Percent,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Get_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Window,TERMINAL(0),ROOT(1),ROOT(2));

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Begin(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

PUTREAL(CFAbsoluteTimeGetCurrent(),2);
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Start_20_Time,TERMINAL(0),TERMINAL(2),ROOT(3));

result = vpx_set(PARAMETERS,kVPXValue_Activity,TERMINAL(0),TERMINAL(1),ROOT(4));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Get_20_Duration(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

PUTREAL(CFAbsoluteTimeGetCurrent(),1);
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Start_20_Time,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP__2D_,2,1,TERMINAL(1),TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Begin_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADERWITHNONE(5)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Activity_20_Progress_20_Window,1,1,NONE,ROOT(1));

result = vpx_get(PARAMETERS,kVPXValue_Activity,TERMINAL(0),ROOT(2),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open,2,0,TERMINAL(1),TERMINAL(3));

result = vpx_set(PARAMETERS,kVPXValue_Window,TERMINAL(0),TERMINAL(1),ROOT(4));

result = kSuccess;

FOOTERSINGLECASEWITHNONE(5)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(2)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Progress,1,0,TERMINAL(1));

result = kSuccess;

FOOTER(2)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(5)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Duration,1,1,TERMINAL(0),ROOT(1));

result = vpx_method_Int(PARAMETERS,TERMINAL(1),ROOT(2));

result = vpx_get(PARAMETERS,kVPXValue_Wait_20_Time,TERMINAL(0),ROOT(3),ROOT(4));

result = vpx_call_primitive(PARAMETERS,VPLP__3C_,2,0,TERMINAL(4),TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Begin_20_Window,1,0,TERMINAL(0));

result = kSuccess;

FOOTER(5)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Synchronize(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_2F_Synchronize_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
if(vpx_method_Activity_20_Progress_2F_Synchronize_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Activity_20_Progress_2F_Synchronize_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Set_20_Progress_20_Percent(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Progress_20_Percent,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Set_20_Activity_20_Name(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Activity_20_Name,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Set_20_Primary_20_Prompt(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Primary_20_Prompt,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Set_20_Secondary_20_Prompt(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(3)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Secondary_20_Prompt,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Finish(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Finish_20_Window,1,0,TERMINAL(0));

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Activity,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_2F_Finish_20_Window(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Window,1,1,TERMINAL(0),ROOT(1));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(1));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Close,1,0,TERMINAL(1));

result = vpx_constant(PARAMETERS,"NULL",ROOT(2));

result = vpx_set(PARAMETERS,kVPXValue_Window,TERMINAL(0),TERMINAL(2),ROOT(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

/* Stop Universals */



Nat4 VPLC_Activity_20_Progress_20_Window_class_load(V_Class tempClass,V_Environment environment);
Nat4 VPLC_Activity_20_Progress_20_Window_class_load(V_Class tempClass,V_Environment environment)
{
	V_Value	tempAttribute = NULL;

/* Start Attributes: { 60 60 }{ 200 300 } */
	tempAttribute = attribute_add("Name",tempClass,tempAttribute_Activity_20_Progress_20_Window_2F_Name,environment);
	tempAttribute = attribute_add("WindowRef",tempClass,NULL,environment);
	tempAttribute = attribute_add("Window Event Handler",tempClass,tempAttribute_Activity_20_Progress_20_Window_2F_Window_20_Event_20_Handler,environment);
	tempAttribute = attribute_add("Activity",tempClass,NULL,environment);
/* Stop Attributes */

	class_superClass_to(tempClass,environment,"Window");
	return kNOERROR;
}

/* Start Universals: { 489 841 }{ 266 301 } */
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Open(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_set(PARAMETERS,kVPXValue_Activity,TERMINAL(0),TERMINAL(1),ROOT(2));

result = vpx_constant(PARAMETERS,"main",ROOT(3));

result = vpx_constant(PARAMETERS,"Progress Window",ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_Nib_20_Window,3,0,TERMINAL(0),TERMINAL(3),TERMINAL(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Open_20_Controls,1,0,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Show,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(5)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Open_20_Controls_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Open_20_Controls_case_1_local_2(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Activity,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Initialize_20_Progress_20_Window,2,0,TERMINAL(2),TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Open_20_Controls(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = vpx_method_Activity_20_Progress_20_Window_2F_Open_20_Controls_case_1_local_2(PARAMETERS,TERMINAL(0));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Update_20_Progress,1,0,TERMINAL(0));

result = kSuccess;

FOOTERSINGLECASE(1)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Dispose(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_call_context_super_method(PARAMETERS,kVPXMethod_Dispose,1,0,TERMINAL(0));

result = vpx_constant(PARAMETERS,"NULL",ROOT(1));

result = vpx_set(PARAMETERS,kVPXValue_Activity,TERMINAL(0),TERMINAL(1),ROOT(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( \'Prog\' 1 )",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Control,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Static_20_Text_20_Control,1,1,NONE,ROOT(1));

result = vpx_constant(PARAMETERS,"( \'Prog\' 1 )",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_From_20_ID,3,0,TERMINAL(1),TERMINAL(0),TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( \'Prog\' 2 )",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Control,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Static_20_Text_20_Control,1,1,NONE,ROOT(1));

result = vpx_constant(PARAMETERS,"( \'Prog\' 2 )",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_From_20_ID,3,0,TERMINAL(1),TERMINAL(0),TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( \'Prog\' 5 )",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Control,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Static_20_Text_20_Control,1,1,NONE,ROOT(1));

result = vpx_constant(PARAMETERS,"( \'Prog\' 5 )",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_From_20_ID,3,0,TERMINAL(1),TERMINAL(0),TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_constant(PARAMETERS,"( \'Prog\' 3 )",ROOT(1));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Control,2,1,TERMINAL(0),TERMINAL(1),ROOT(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(2))
FOOTER(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(3)
INPUT(0,0)
result = kSuccess;

result = vpx_instantiate(PARAMETERS,kVPXClass_Progress_20_Bar_20_Control,1,1,NONE,ROOT(1));

result = vpx_constant(PARAMETERS,"( \'Prog\' 3 )",ROOT(2));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Create_20_From_20_ID,3,0,TERMINAL(1),TERMINAL(0),TERMINAL(2));
NEXTCASEONFAILURE

result = kSuccess;

OUTPUT(0,TERMINAL(1))
FOOTERWITHNONE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_3(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADERWITHNONE(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;
FAILONSUCCESS

OUTPUT(0,NONE)
FOOTERWITHNONE(1)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
if(vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,root0))
vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar_case_3(environment, &outcome, inputRepeat, contextIndex,terminal0,root0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_6(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Activity_20_Text,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Activity_20_Name,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(2),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_7(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Primary_20_Text,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Primary_20_Prompt,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(2),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_8(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Secondary_20_Text,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Secondary_20_Prompt,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Text,2,0,TERMINAL(2),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_9(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1)
{
HEADER(4)
INPUT(0,0)
INPUT(1,1)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Find_20_Progress_20_Bar,1,1,TERMINAL(0),ROOT(2));
TERMINATEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress_20_Percent,1,1,TERMINAL(1),ROOT(3));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Set_20_Progress_20_Percent,2,0,TERMINAL(2),TERMINAL(3));

result = kSuccess;

FOOTERSINGLECASE(4)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(4)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Activity,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Get_20_Progress,1,1,TERMINAL(2),ROOT(3));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(3));
NEXTCASEONFAILURE

result = vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_6(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_7(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_8(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1_local_9(PARAMETERS,TERMINAL(0),TERMINAL(3));

result = kSuccess;

FOOTER(4)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

FOOTER(1)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0))
vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process_case_1(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_extmatch(PARAMETERS,kHICommandCancel,TERMINAL(1));
NEXTCASEONFAILURE

result = vpx_extconstant(PARAMETERS,noErr,ROOT(4));

result = vpx_call_data_method(PARAMETERS,kVPXMethod_HIC_20_Cancel,1,0,TERMINAL(0));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object *root0);
enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process_case_2(V_Environment environment, enum opTrigger *outcome, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object *root0)
{
HEADER(5)
INPUT(0,0)
INPUT(1,1)
INPUT(2,2)
INPUT(3,3)
result = kSuccess;

result = vpx_call_data_method(PARAMETERS,kVPXMethod_Return_20_Event_20_Not_20_Handled,1,1,TERMINAL(3),ROOT(4));

result = kSuccess;

OUTPUT(0,TERMINAL(4))
FOOTER(5)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object terminal1,V_Object terminal2,V_Object terminal3,V_Object *root0)
{
enum opTrigger outcome = kSuccess;
if(vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process_case_1(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,root0))
vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process_case_2(environment, &outcome, inputRepeat, contextIndex,terminal0,terminal1,terminal2,terminal3,root0);
return outcome;
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_HIC_20_Cancel(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0)
{
HEADER(3)
INPUT(0,0)
result = kSuccess;

result = vpx_get(PARAMETERS,kVPXValue_Activity,TERMINAL(0),ROOT(1),ROOT(2));

result = vpx_call_primitive(PARAMETERS,VPLP_instance_3F_,1,0,TERMINAL(2));
NEXTCASEONFAILURE

result = vpx_call_data_method(PARAMETERS,kVPXMethod_User_20_Cancel,1,0,TERMINAL(2));

result = kSuccess;

FOOTERSINGLECASE(3)
}

enum opTrigger vpx_method_Activity_20_Progress_20_Window_2F_Close_20_All(V_Environment environment, enum boolType *inputRepeat, Nat4 contextIndex,V_Object terminal0,V_Object *root0)
{
HEADER(1)
INPUT(0,0)
result = kSuccess;

result = kSuccess;

OUTPUT(0,TERMINAL(0))
FOOTERSINGLECASE(1)
}

/* Stop Universals */






Nat4	loadClasses_Activity_20_Service(V_Environment environment);
Nat4	loadClasses_Activity_20_Service(V_Environment environment)
{
	V_Class result = NULL;

	result = class_new("Activity Service",environment);
	if(result == NULL) return kERROR;
	VPLC_Activity_20_Service_class_load(result,environment);
	result = class_new("Activity Abstract",environment);
	if(result == NULL) return kERROR;
	VPLC_Activity_20_Abstract_class_load(result,environment);
	result = class_new("Activity Progress",environment);
	if(result == NULL) return kERROR;
	VPLC_Activity_20_Progress_class_load(result,environment);
	result = class_new("Activity Progress Window",environment);
	if(result == NULL) return kERROR;
	VPLC_Activity_20_Progress_20_Window_class_load(result,environment);
	return kNOERROR;

}

Nat4	loadUniversals_Activity_20_Service(V_Environment environment);
Nat4	loadUniversals_Activity_20_Service(V_Environment environment)
{
	V_Method result = NULL;

	result = method_new("Activity Abstract/Get Progress",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Get_20_Progress,NULL);

	result = method_new("Activity Abstract/User Cancel",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_User_20_Cancel,NULL);

	result = method_new("Activity Abstract/Cancel",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Cancel,NULL);

	result = method_new("Activity Abstract/Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Begin,NULL);

	result = method_new("Activity Abstract/Finish",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Finish,NULL);

	result = method_new("Activity Abstract/Synchronize",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Synchronize,NULL);

	result = method_new("Activity Abstract/Set Progress Percent",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Set_20_Progress_20_Percent,NULL);

	result = method_new("Activity Abstract/Set Activity Name",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Set_20_Activity_20_Name,NULL);

	result = method_new("Activity Abstract/Set Primary Prompt",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Set_20_Primary_20_Prompt,NULL);

	result = method_new("Activity Abstract/Set Secondary Prompt",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Set_20_Secondary_20_Prompt,NULL);

	result = method_new("Activity Abstract/Get Canceled?",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Get_20_Canceled_3F_,NULL);

	result = method_new("Activity Abstract/Do Async",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Do_20_Async,NULL);

	result = method_new("Activity Abstract/Do Async Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Do_20_Async_20_Callback,NULL);

	result = method_new("Activity Abstract/Async Schedule Callback",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Async_20_Schedule_20_Callback,NULL);

	result = method_new("Activity Abstract/Async Delay",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Async_20_Delay,NULL);

	result = method_new("Activity Abstract/Async Repeat",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Async_20_Repeat,NULL);

	result = method_new("Activity Abstract/Do Sync",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Do_20_Sync,NULL);

	result = method_new("Activity Abstract/Do",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Do,NULL);

	result = method_new("Activity Abstract/Run Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Run_20_Begin,NULL);

	result = method_new("Activity Abstract/Run",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Run,NULL);

	result = method_new("Activity Abstract/Run End",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Run_20_End,NULL);

	result = method_new("Activity Abstract/Initialize Progress Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Abstract_2F_Initialize_20_Progress_20_Window,NULL);

	result = method_new("Activity Progress/Get Activity Name",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Get_20_Activity_20_Name,NULL);

	result = method_new("Activity Progress/Get Primary Prompt",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Get_20_Primary_20_Prompt,NULL);

	result = method_new("Activity Progress/Get Secondary Prompt",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Get_20_Secondary_20_Prompt,NULL);

	result = method_new("Activity Progress/Get Progress Percent",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Get_20_Progress_20_Percent,NULL);

	result = method_new("Activity Progress/Get Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Get_20_Window,NULL);

	result = method_new("Activity Progress/Begin",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Begin,NULL);

	result = method_new("Activity Progress/Get Duration",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Get_20_Duration,NULL);

	result = method_new("Activity Progress/Begin Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Begin_20_Window,NULL);

	result = method_new("Activity Progress/Synchronize",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Synchronize,NULL);

	result = method_new("Activity Progress/Set Progress Percent",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Set_20_Progress_20_Percent,NULL);

	result = method_new("Activity Progress/Set Activity Name",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Set_20_Activity_20_Name,NULL);

	result = method_new("Activity Progress/Set Primary Prompt",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Set_20_Primary_20_Prompt,NULL);

	result = method_new("Activity Progress/Set Secondary Prompt",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Set_20_Secondary_20_Prompt,NULL);

	result = method_new("Activity Progress/Finish",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Finish,NULL);

	result = method_new("Activity Progress/Finish Window",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_2F_Finish_20_Window,NULL);

	result = method_new("Activity Progress Window/Open",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Open,NULL);

	result = method_new("Activity Progress Window/Open Controls",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Open_20_Controls,NULL);

	result = method_new("Activity Progress Window/Dispose",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Dispose,NULL);

	result = method_new("Activity Progress Window/Find Activity Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Activity_20_Text,NULL);

	result = method_new("Activity Progress Window/Find Primary Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Primary_20_Text,NULL);

	result = method_new("Activity Progress Window/Find Secondary Text",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Secondary_20_Text,NULL);

	result = method_new("Activity Progress Window/Find Progress Bar",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Find_20_Progress_20_Bar,NULL);

	result = method_new("Activity Progress Window/Update Progress",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Update_20_Progress,NULL);

	result = method_new("Activity Progress Window/CE HICommand Process",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_CE_20_HICommand_20_Process,NULL);

	result = method_new("Activity Progress Window/HIC Cancel",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_HIC_20_Cancel,NULL);

	result = method_new("Activity Progress Window/Close All",environment);
	if(result == NULL) return kERROR;
	vpx_method_initialize(environment,result,vpx_method_Activity_20_Progress_20_Window_2F_Close_20_All,NULL);

	return kNOERROR;

}



Nat4	loadPersistents_Activity_20_Service(V_Environment environment);
Nat4	loadPersistents_Activity_20_Service(V_Environment environment)
{
	V_Value tempPersistent = NULL;
	V_Dictionary dictionary = environment->persistentsTable;

	return kNOERROR;

}

Nat4	load_Activity_20_Service(V_Environment environment);
Nat4	load_Activity_20_Service(V_Environment environment)
{

	loadClasses_Activity_20_Service(environment);
	loadUniversals_Activity_20_Service(environment);
	loadPersistents_Activity_20_Service(environment);
	return kNOERROR;

}

