/*
	
	MacVPL.h
	Copyright 2003 Scott B. Anderson, All Rights Reserved.
	
*/

#ifndef VPXCARBON
#define VPXCARBON

Nat4	load_Carbon(V_Environment environment);

#endif
