#ifndef LIBC_C
#define LIBC_C 1

/* Standard Libc Headers */

//#include <MSLCarbonPrefix.h>
#include <ctype.h>
#include <libc.h>
#include <netdb.h>
#include <util.h>
#ifdef __MWERKS__
#include "VPL_Compiler.h"
#elif __GNUC__
#include <MartenEngine/MartenEngine.h>
#endif	

#endif /* LIBC_C */

