/*
	
	MacOSX_Constants.c
	Copyright 2003 Scott B. Anderson, All Rights Reserved.
	
*/

#ifdef __MWERKS__
#include "VPL_Compiler.h"
#endif	

	VPL_ExtConstant _kIOFBBlitBeamSyncSpin_C = {"kIOFBBlitBeamSyncSpin",kIOFBBlitBeamSyncSpin,NULL};
	VPL_ExtConstant _kIOFBBlitBeamSyncAlways_C = {"kIOFBBlitBeamSyncAlways",kIOFBBlitBeamSyncAlways,NULL};
	VPL_ExtConstant _kIOFBBlitBeamSync_C = {"kIOFBBlitBeamSync",kIOFBBlitBeamSync,NULL};
	VPL_ExtConstant _kIOFBSynchronizeFlushWrites_C = {"kIOFBSynchronizeFlushWrites",kIOFBSynchronizeFlushWrites,NULL};
	VPL_ExtConstant _kIOFBSynchronizeWaitBeamExit_C = {"kIOFBSynchronizeWaitBeamExit",kIOFBSynchronizeWaitBeamExit,NULL};
	VPL_ExtConstant _kIODisplayNoProductName_C = {"kIODisplayNoProductName",kIODisplayNoProductName,NULL};
	VPL_ExtConstant _kIODisplayOnlyPreferredName_C = {"kIODisplayOnlyPreferredName",kIODisplayOnlyPreferredName,NULL};
	VPL_ExtConstant _kIODisplayMatchingInfo_C = {"kIODisplayMatchingInfo",kIODisplayMatchingInfo,NULL};
	VPL_ExtConstant _kIOFBConnectStateOnline_C = {"kIOFBConnectStateOnline",kIOFBConnectStateOnline,NULL};
	VPL_ExtConstant _IOFBMessageCallbacksVersion_C = {"IOFBMessageCallbacksVersion",IOFBMessageCallbacksVersion,NULL};
	VPL_ExtConstant _kIOBlitColorSpaceTypes_C = {"kIOBlitColorSpaceTypes",kIOBlitColorSpaceTypes,NULL};
	VPL_ExtConstant _kIOBlitAllOptions_C = {"kIOBlitAllOptions",kIOBlitAllOptions,NULL};
	VPL_ExtConstant _kIOBlitBeamSyncSpin_C = {"kIOBlitBeamSyncSpin",kIOBlitBeamSyncSpin,NULL};
	VPL_ExtConstant _kIOBlitBeamSyncAlways_C = {"kIOBlitBeamSyncAlways",kIOBlitBeamSyncAlways,NULL};
	VPL_ExtConstant _kIOBlitBeamSync_C = {"kIOBlitBeamSync",kIOBlitBeamSync,NULL};
	VPL_ExtConstant _kIOBlitSurfaceDestination_C = {"kIOBlitSurfaceDestination",kIOBlitSurfaceDestination,NULL};
	VPL_ExtConstant _kIOBlitFramebufferDestination_C = {"kIOBlitFramebufferDestination",kIOBlitFramebufferDestination,NULL};
	VPL_ExtConstant _kIOBlitUnlockWithSwap_C = {"kIOBlitUnlockWithSwap",kIOBlitUnlockWithSwap,NULL};
	VPL_ExtConstant _kIOBlitReferenceSource_C = {"kIOBlitReferenceSource",kIOBlitReferenceSource,NULL};
	VPL_ExtConstant _kIOBlitBeamSyncSwaps_C = {"kIOBlitBeamSyncSwaps",kIOBlitBeamSyncSwaps,NULL};
	VPL_ExtConstant _kIOBlitFixedSource_C = {"kIOBlitFixedSource",kIOBlitFixedSource,NULL};
	VPL_ExtConstant _kIOBlitHasCGSSurface_C = {"kIOBlitHasCGSSurface",kIOBlitHasCGSSurface,NULL};
	VPL_ExtConstant _kIOBlitFlushWithSwap_C = {"kIOBlitFlushWithSwap",kIOBlitFlushWithSwap,NULL};
	VPL_ExtConstant _kIOBlitWaitCheck_C = {"kIOBlitWaitCheck",kIOBlitWaitCheck,NULL};
	VPL_ExtConstant _kIOBlitWaitAll_C = {"kIOBlitWaitAll",kIOBlitWaitAll,NULL};
	VPL_ExtConstant _kIOBlitWaitGlobal_C = {"kIOBlitWaitGlobal",kIOBlitWaitGlobal,NULL};
	VPL_ExtConstant _kIOBlitWaitAll2D_C = {"kIOBlitWaitAll2D",kIOBlitWaitAll2D,NULL};
	VPL_ExtConstant _kIOBlitWaitContext_C = {"kIOBlitWaitContext",kIOBlitWaitContext,NULL};
	VPL_ExtConstant _kIOBlitSynchronizeFlushHostWrites_C = {"kIOBlitSynchronizeFlushHostWrites",kIOBlitSynchronizeFlushHostWrites,NULL};
	VPL_ExtConstant _kIOBlitSynchronizeWaitBeamExit_C = {"kIOBlitSynchronizeWaitBeamExit",kIOBlitSynchronizeWaitBeamExit,NULL};
	VPL_ExtConstant _kIOBlitMemoryRequiresHostFlush_C = {"kIOBlitMemoryRequiresHostFlush",kIOBlitMemoryRequiresHostFlush,NULL};
	VPL_ExtConstant _kIO16BE4444PixelFormat_C = {"kIO16BE4444PixelFormat",kIO16BE4444PixelFormat,NULL};
	VPL_ExtConstant _kIO16LE4444PixelFormat_C = {"kIO16LE4444PixelFormat",kIO16LE4444PixelFormat,NULL};
	VPL_ExtConstant _kIO2vuyPixelFormat_C = {"kIO2vuyPixelFormat",kIO2vuyPixelFormat,NULL};
	VPL_ExtConstant _kIOYUV211PixelFormat_C = {"kIOYUV211PixelFormat",kIOYUV211PixelFormat,NULL};
	VPL_ExtConstant _kIOUYVY422PixelFormat_C = {"kIOUYVY422PixelFormat",kIOUYVY422PixelFormat,NULL};
	VPL_ExtConstant _kIOYVYU422PixelFormat_C = {"kIOYVYU422PixelFormat",kIOYVYU422PixelFormat,NULL};
	VPL_ExtConstant _kIOYUV411PixelFormat_C = {"kIOYUV411PixelFormat",kIOYUV411PixelFormat,NULL};
	VPL_ExtConstant _kIOYVU9PixelFormat_C = {"kIOYVU9PixelFormat",kIOYVU9PixelFormat,NULL};
	VPL_ExtConstant _kIOYUVUPixelFormat_C = {"kIOYUVUPixelFormat",kIOYUVUPixelFormat,NULL};
	VPL_ExtConstant _kIOYUVSPixelFormat_C = {"kIOYUVSPixelFormat",kIOYUVSPixelFormat,NULL};
	VPL_ExtConstant _kIO32RGBAPixelFormat_C = {"kIO32RGBAPixelFormat",kIO32RGBAPixelFormat,NULL};
	VPL_ExtConstant _kIO32ABGRPixelFormat_C = {"kIO32ABGRPixelFormat",kIO32ABGRPixelFormat,NULL};
	VPL_ExtConstant _kIO32BGRAPixelFormat_C = {"kIO32BGRAPixelFormat",kIO32BGRAPixelFormat,NULL};
	VPL_ExtConstant _kIO24BGRPixelFormat_C = {"kIO24BGRPixelFormat",kIO24BGRPixelFormat,NULL};
	VPL_ExtConstant _kIO16LE565PixelFormat_C = {"kIO16LE565PixelFormat",kIO16LE565PixelFormat,NULL};
	VPL_ExtConstant _kIO16BE565PixelFormat_C = {"kIO16BE565PixelFormat",kIO16BE565PixelFormat,NULL};
	VPL_ExtConstant _kIO16LE5551PixelFormat_C = {"kIO16LE5551PixelFormat",kIO16LE5551PixelFormat,NULL};
	VPL_ExtConstant _kIO16LE555PixelFormat_C = {"kIO16LE555PixelFormat",kIO16LE555PixelFormat,NULL};
	VPL_ExtConstant _kIO8IndexedGrayPixelFormat_C = {"kIO8IndexedGrayPixelFormat",kIO8IndexedGrayPixelFormat,NULL};
	VPL_ExtConstant _kIO4IndexedGrayPixelFormat_C = {"kIO4IndexedGrayPixelFormat",kIO4IndexedGrayPixelFormat,NULL};
	VPL_ExtConstant _kIO2IndexedGrayPixelFormat_C = {"kIO2IndexedGrayPixelFormat",kIO2IndexedGrayPixelFormat,NULL};
	VPL_ExtConstant _kIO1IndexedGrayPixelFormat_C = {"kIO1IndexedGrayPixelFormat",kIO1IndexedGrayPixelFormat,NULL};
	VPL_ExtConstant _kIO32ARGBPixelFormat_C = {"kIO32ARGBPixelFormat",kIO32ARGBPixelFormat,NULL};
	VPL_ExtConstant _kIO24RGBPixelFormat_C = {"kIO24RGBPixelFormat",kIO24RGBPixelFormat,NULL};
	VPL_ExtConstant _kIO16BE555PixelFormat_C = {"kIO16BE555PixelFormat",kIO16BE555PixelFormat,NULL};
	VPL_ExtConstant _kIO8IndexedPixelFormat_C = {"kIO8IndexedPixelFormat",kIO8IndexedPixelFormat,NULL};
	VPL_ExtConstant _kIO4IndexedPixelFormat_C = {"kIO4IndexedPixelFormat",kIO4IndexedPixelFormat,NULL};
	VPL_ExtConstant _kIO2IndexedPixelFormat_C = {"kIO2IndexedPixelFormat",kIO2IndexedPixelFormat,NULL};
	VPL_ExtConstant _kIO1MonochromePixelFormat_C = {"kIO1MonochromePixelFormat",kIO1MonochromePixelFormat,NULL};
	VPL_ExtConstant _kIOBlitDestFramebuffer_C = {"kIOBlitDestFramebuffer",kIOBlitDestFramebuffer,NULL};
	VPL_ExtConstant _kIOBlitSourceIsSame_C = {"kIOBlitSourceIsSame",kIOBlitSourceIsSame,NULL};
	VPL_ExtConstant _kIOBlitSourceCGSSurface_C = {"kIOBlitSourceCGSSurface",kIOBlitSourceCGSSurface,NULL};
	VPL_ExtConstant _kIOBlitSourceSolid_C = {"kIOBlitSourceSolid",kIOBlitSourceSolid,NULL};
	VPL_ExtConstant _kIOBlitSourceOOLPattern_C = {"kIOBlitSourceOOLPattern",kIOBlitSourceOOLPattern,NULL};
	VPL_ExtConstant _kIOBlitSourcePattern_C = {"kIOBlitSourcePattern",kIOBlitSourcePattern,NULL};
	VPL_ExtConstant _kIOBlitSourceOOLMemory_C = {"kIOBlitSourceOOLMemory",kIOBlitSourceOOLMemory,NULL};
	VPL_ExtConstant _kIOBlitSourceMemory_C = {"kIOBlitSourceMemory",kIOBlitSourceMemory,NULL};
	VPL_ExtConstant _kIOBlitSourceFramebuffer_C = {"kIOBlitSourceFramebuffer",kIOBlitSourceFramebuffer,NULL};
	VPL_ExtConstant _kIOBlitSourceDefault_C = {"kIOBlitSourceDefault",kIOBlitSourceDefault,NULL};
	VPL_ExtConstant _kIOBlitHighlightOperation_C = {"kIOBlitHighlightOperation",kIOBlitHighlightOperation,NULL};
	VPL_ExtConstant _kIOBlitBlendOperation_C = {"kIOBlitBlendOperation",kIOBlitBlendOperation,NULL};
	VPL_ExtConstant _kIOBlitXorOperation_C = {"kIOBlitXorOperation",kIOBlitXorOperation,NULL};
	VPL_ExtConstant _kIOBlitOrOperation_C = {"kIOBlitOrOperation",kIOBlitOrOperation,NULL};
	VPL_ExtConstant _kIOBlitCopyOperation_C = {"kIOBlitCopyOperation",kIOBlitCopyOperation,NULL};
	VPL_ExtConstant _kIOBlitTypeOperationType0_C = {"kIOBlitTypeOperationType0",kIOBlitTypeOperationType0,NULL};
	VPL_ExtConstant _kIOBlitTypeOperationTypeMask_C = {"kIOBlitTypeOperationTypeMask",kIOBlitTypeOperationTypeMask,NULL};
	VPL_ExtConstant _kIOBlitTypeOperationShift_C = {"kIOBlitTypeOperationShift",kIOBlitTypeOperationShift,NULL};
	VPL_ExtConstant _kIOBlitTypeOperationMask_C = {"kIOBlitTypeOperationMask",kIOBlitTypeOperationMask,NULL};
	VPL_ExtConstant _kIOBlitTypeDestKeyColorNotEqual_C = {"kIOBlitTypeDestKeyColorNotEqual",kIOBlitTypeDestKeyColorNotEqual,NULL};
	VPL_ExtConstant _kIOBlitTypeDestKeyColorEqual_C = {"kIOBlitTypeDestKeyColorEqual",kIOBlitTypeDestKeyColorEqual,NULL};
	VPL_ExtConstant _kIOBlitTypeSourceKeyColorNotEqual_C = {"kIOBlitTypeSourceKeyColorNotEqual",kIOBlitTypeSourceKeyColorNotEqual,NULL};
	VPL_ExtConstant _kIOBlitTypeSourceKeyColorEqual_C = {"kIOBlitTypeSourceKeyColorEqual",kIOBlitTypeSourceKeyColorEqual,NULL};
	VPL_ExtConstant _kIOBlitTypeDestKeyColorModeMask_C = {"kIOBlitTypeDestKeyColorModeMask",kIOBlitTypeDestKeyColorModeMask,NULL};
	VPL_ExtConstant _kIOBlitTypeSourceKeyColorModeMask_C = {"kIOBlitTypeSourceKeyColorModeMask",kIOBlitTypeSourceKeyColorModeMask,NULL};
	VPL_ExtConstant _kIOBlitTypeScale_C = {"kIOBlitTypeScale",kIOBlitTypeScale,NULL};
	VPL_ExtConstant _kIOBlitTypeColorSpaceConvert_C = {"kIOBlitTypeColorSpaceConvert",kIOBlitTypeColorSpaceConvert,NULL};
	VPL_ExtConstant _kIOBlitTypeMonoExpand_C = {"kIOBlitTypeMonoExpand",kIOBlitTypeMonoExpand,NULL};
	VPL_ExtConstant _kIOBlitTypeHideCursor_C = {"kIOBlitTypeHideCursor",kIOBlitTypeHideCursor,NULL};
	VPL_ExtConstant _kIOBlitTypeShowCursor_C = {"kIOBlitTypeShowCursor",kIOBlitTypeShowCursor,NULL};
	VPL_ExtConstant _kIOBlitTypeMoveCursor_C = {"kIOBlitTypeMoveCursor",kIOBlitTypeMoveCursor,NULL};
	VPL_ExtConstant _kIOBlitTypeCopyRegion_C = {"kIOBlitTypeCopyRegion",kIOBlitTypeCopyRegion,NULL};
	VPL_ExtConstant _kIOBlitTypeScanlines_C = {"kIOBlitTypeScanlines",kIOBlitTypeScanlines,NULL};
	VPL_ExtConstant _kIOBlitTypeLines_C = {"kIOBlitTypeLines",kIOBlitTypeLines,NULL};
	VPL_ExtConstant _kIOBlitTypeCopyRects_C = {"kIOBlitTypeCopyRects",kIOBlitTypeCopyRects,NULL};
	VPL_ExtConstant _kIOBlitTypeRects_C = {"kIOBlitTypeRects",kIOBlitTypeRects,NULL};
	VPL_ExtConstant _kIOBlitTypeVerbMask_C = {"kIOBlitTypeVerbMask",kIOBlitTypeVerbMask,NULL};
	VPL_ExtConstant _kIOAccelSurfaceLockInMask_C = {"kIOAccelSurfaceLockInMask",kIOAccelSurfaceLockInMask,NULL};
	VPL_ExtConstant _kIOAccelSurfaceLockInDontCare_C = {"kIOAccelSurfaceLockInDontCare",kIOAccelSurfaceLockInDontCare,NULL};
	VPL_ExtConstant _kIOAccelSurfaceLockInAccel_C = {"kIOAccelSurfaceLockInAccel",kIOAccelSurfaceLockInAccel,NULL};
	VPL_ExtConstant _kIOAccelSurfaceLockInBacking_C = {"kIOAccelSurfaceLockInBacking",kIOAccelSurfaceLockInBacking,NULL};
	VPL_ExtConstant _kIOAccelSurfaceFilterLinear_C = {"kIOAccelSurfaceFilterLinear",kIOAccelSurfaceFilterLinear,NULL};
	VPL_ExtConstant _kIOAccelSurfaceFilterNone_C = {"kIOAccelSurfaceFilterNone",kIOAccelSurfaceFilterNone,NULL};
	VPL_ExtConstant _kIOAccelSurfaceFilterDefault_C = {"kIOAccelSurfaceFilterDefault",kIOAccelSurfaceFilterDefault,NULL};
	VPL_ExtConstant _kIOAccelSurfaceFiltering_C = {"kIOAccelSurfaceFiltering",kIOAccelSurfaceFiltering,NULL};
	VPL_ExtConstant _kIOAccelSurfaceFixedSource_C = {"kIOAccelSurfaceFixedSource",kIOAccelSurfaceFixedSource,NULL};
	VPL_ExtConstant _kIOAccelSurfaceBeamSyncSwaps_C = {"kIOAccelSurfaceBeamSyncSwaps",kIOAccelSurfaceBeamSyncSwaps,NULL};
	VPL_ExtConstant _kIOAccelSurfaceStateIdleBit_C = {"kIOAccelSurfaceStateIdleBit",kIOAccelSurfaceStateIdleBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceStateNone_C = {"kIOAccelSurfaceStateNone",kIOAccelSurfaceStateNone,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeBlockingBit_C = {"kIOAccelSurfaceShapeBlockingBit",kIOAccelSurfaceShapeBlockingBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeWaitEnabledBit_C = {"kIOAccelSurfaceShapeWaitEnabledBit",kIOAccelSurfaceShapeWaitEnabledBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeAssemblyBit_C = {"kIOAccelSurfaceShapeAssemblyBit",kIOAccelSurfaceShapeAssemblyBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeStaleBackingBit_C = {"kIOAccelSurfaceShapeStaleBackingBit",kIOAccelSurfaceShapeStaleBackingBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeBeamSyncBit_C = {"kIOAccelSurfaceShapeBeamSyncBit",kIOAccelSurfaceShapeBeamSyncBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeFrameSyncBit_C = {"kIOAccelSurfaceShapeFrameSyncBit",kIOAccelSurfaceShapeFrameSyncBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeIdentityScaleBit_C = {"kIOAccelSurfaceShapeIdentityScaleBit",kIOAccelSurfaceShapeIdentityScaleBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeNonSimpleBit_C = {"kIOAccelSurfaceShapeNonSimpleBit",kIOAccelSurfaceShapeNonSimpleBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeNonBlockingBit_C = {"kIOAccelSurfaceShapeNonBlockingBit",kIOAccelSurfaceShapeNonBlockingBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceShapeNone_C = {"kIOAccelSurfaceShapeNone",kIOAccelSurfaceShapeNone,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeBeamSync_C = {"kIOAccelSurfaceModeBeamSync",kIOAccelSurfaceModeBeamSync,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeWindowedBit_C = {"kIOAccelSurfaceModeWindowedBit",kIOAccelSurfaceModeWindowedBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeStereoBit_C = {"kIOAccelSurfaceModeStereoBit",kIOAccelSurfaceModeStereoBit,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepthBits_C = {"kIOAccelSurfaceModeColorDepthBits",kIOAccelSurfaceModeColorDepthBits,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepthBGRA32_C = {"kIOAccelSurfaceModeColorDepthBGRA32",kIOAccelSurfaceModeColorDepthBGRA32,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepthYUV2_C = {"kIOAccelSurfaceModeColorDepthYUV2",kIOAccelSurfaceModeColorDepthYUV2,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepthYUV12_C = {"kIOAccelSurfaceModeColorDepthYUV12",kIOAccelSurfaceModeColorDepthYUV12,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepthYUV9_C = {"kIOAccelSurfaceModeColorDepthYUV9",kIOAccelSurfaceModeColorDepthYUV9,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepthYUV_C = {"kIOAccelSurfaceModeColorDepthYUV",kIOAccelSurfaceModeColorDepthYUV,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepth8888_C = {"kIOAccelSurfaceModeColorDepth8888",kIOAccelSurfaceModeColorDepth8888,NULL};
	VPL_ExtConstant _kIOAccelSurfaceModeColorDepth1555_C = {"kIOAccelSurfaceModeColorDepth1555",kIOAccelSurfaceModeColorDepth1555,NULL};
	VPL_ExtConstant _kIOAccelNumSurfaceMethods_C = {"kIOAccelNumSurfaceMethods",kIOAccelNumSurfaceMethods,NULL};
	VPL_ExtConstant _kIOAccelSurfaceSetShapeBackingAndLength_C = {"kIOAccelSurfaceSetShapeBackingAndLength",kIOAccelSurfaceSetShapeBackingAndLength,NULL};
	VPL_ExtConstant _kIOAccelSurfaceControl_C = {"kIOAccelSurfaceControl",kIOAccelSurfaceControl,NULL};
	VPL_ExtConstant _kIOAccelSurfaceWriteUnlock_C = {"kIOAccelSurfaceWriteUnlock",kIOAccelSurfaceWriteUnlock,NULL};
	VPL_ExtConstant _kIOAccelSurfaceWriteLock_C = {"kIOAccelSurfaceWriteLock",kIOAccelSurfaceWriteLock,NULL};
	VPL_ExtConstant _kIOAccelSurfaceReadUnlock_C = {"kIOAccelSurfaceReadUnlock",kIOAccelSurfaceReadUnlock,NULL};
	VPL_ExtConstant _kIOAccelSurfaceReadLock_C = {"kIOAccelSurfaceReadLock",kIOAccelSurfaceReadLock,NULL};
	VPL_ExtConstant _kIOAccelSurfaceQueryLock_C = {"kIOAccelSurfaceQueryLock",kIOAccelSurfaceQueryLock,NULL};
	VPL_ExtConstant _kIOAccelSurfaceFlush_C = {"kIOAccelSurfaceFlush",kIOAccelSurfaceFlush,NULL};
	VPL_ExtConstant _kIOAccelSurfaceSetShape_C = {"kIOAccelSurfaceSetShape",kIOAccelSurfaceSetShape,NULL};
	VPL_ExtConstant _kIOAccelSurfaceSetScale_C = {"kIOAccelSurfaceSetScale",kIOAccelSurfaceSetScale,NULL};
	VPL_ExtConstant _kIOAccelSurfaceSetIDMode_C = {"kIOAccelSurfaceSetIDMode",kIOAccelSurfaceSetIDMode,NULL};
	VPL_ExtConstant _kIOAccelSurfaceSetShapeBacking_C = {"kIOAccelSurfaceSetShapeBacking",kIOAccelSurfaceSetShapeBacking,NULL};
	VPL_ExtConstant _kIOAccelSurfaceRead_C = {"kIOAccelSurfaceRead",kIOAccelSurfaceRead,NULL};
	VPL_ExtConstant _kIOAccelSurfaceWriteUnlockOptions_C = {"kIOAccelSurfaceWriteUnlockOptions",kIOAccelSurfaceWriteUnlockOptions,NULL};
	VPL_ExtConstant _kIOAccelSurfaceWriteLockOptions_C = {"kIOAccelSurfaceWriteLockOptions",kIOAccelSurfaceWriteLockOptions,NULL};
	VPL_ExtConstant _kIOAccelSurfaceGetState_C = {"kIOAccelSurfaceGetState",kIOAccelSurfaceGetState,NULL};
	VPL_ExtConstant _kIOAccelSurfaceReadUnlockOptions_C = {"kIOAccelSurfaceReadUnlockOptions",kIOAccelSurfaceReadUnlockOptions,NULL};
	VPL_ExtConstant _kIOAccelSurfaceReadLockOptions_C = {"kIOAccelSurfaceReadLockOptions",kIOAccelSurfaceReadLockOptions,NULL};
	VPL_ExtConstant _kIOAccelNumSurfaceMemoryTypes_C = {"kIOAccelNumSurfaceMemoryTypes",kIOAccelNumSurfaceMemoryTypes,NULL};
	VPL_ExtConstant _kIOAccelNumClientTypes_C = {"kIOAccelNumClientTypes",kIOAccelNumClientTypes,NULL};
	VPL_ExtConstant _kIOAccelSurfaceClientType_C = {"kIOAccelSurfaceClientType",kIOAccelSurfaceClientType,NULL};
	VPL_ExtConstant _kIOAccelPrivateID_C = {"kIOAccelPrivateID",kIOAccelPrivateID,NULL};
	VPL_ExtConstant _kIOAccelKeycolorSurface_C = {"kIOAccelKeycolorSurface",kIOAccelKeycolorSurface,NULL};
	VPL_ExtConstant _kIOAccelVolatileSurface_C = {"kIOAccelVolatileSurface",kIOAccelVolatileSurface,NULL};
	VPL_ExtConstant _kIOFBVRAMMemory_C = {"kIOFBVRAMMemory",kIOFBVRAMMemory,NULL};
	VPL_ExtConstant _kIOFBCursorMemory_C = {"kIOFBCursorMemory",kIOFBCursorMemory,NULL};
	VPL_ExtConstant _kIOFBShmemCursorNumFramesShift_C = {"kIOFBShmemCursorNumFramesShift",kIOFBShmemCursorNumFramesShift,NULL};
	VPL_ExtConstant _kIOFBShmemCursorNumFramesMask_C = {"kIOFBShmemCursorNumFramesMask",kIOFBShmemCursorNumFramesMask,NULL};
	VPL_ExtConstant _kIOFBCurrentShmemVersion_C = {"kIOFBCurrentShmemVersion",kIOFBCurrentShmemVersion,NULL};
	VPL_ExtConstant _kIOFBTenPtTwoShmemVersion_C = {"kIOFBTenPtTwoShmemVersion",kIOFBTenPtTwoShmemVersion,NULL};
	VPL_ExtConstant _kIOFBTenPtOneShmemVersion_C = {"kIOFBTenPtOneShmemVersion",kIOFBTenPtOneShmemVersion,NULL};
	VPL_ExtConstant _kIOFBShmemVersionMask_C = {"kIOFBShmemVersionMask",kIOFBShmemVersionMask,NULL};
	VPL_ExtConstant _kIOFBHardwareCursorInVRAM_C = {"kIOFBHardwareCursorInVRAM",kIOFBHardwareCursorInVRAM,NULL};
	VPL_ExtConstant _kIOFBHardwareCursorActive_C = {"kIOFBHardwareCursorActive",kIOFBHardwareCursorActive,NULL};
	VPL_ExtConstant _kIOFBCursorHWCapable_C = {"kIOFBCursorHWCapable",kIOFBCursorHWCapable,NULL};
	VPL_ExtConstant _kIOFBCursorImageNew_C = {"kIOFBCursorImageNew",kIOFBCursorImageNew,NULL};
	VPL_ExtConstant _kIOFBMaxCursorDepth_C = {"kIOFBMaxCursorDepth",kIOFBMaxCursorDepth,NULL};
	VPL_ExtConstant _kIOFBNumCursorFramesShift_C = {"kIOFBNumCursorFramesShift",kIOFBNumCursorFramesShift,NULL};
	VPL_ExtConstant _kIOFBNumCursorFrames_C = {"kIOFBNumCursorFrames",kIOFBNumCursorFrames,NULL};
	VPL_ExtConstant _EVLEVEL_C = {"EVLEVEL",EVLEVEL,NULL};
	VPL_ExtConstant _EVMOVE_C = {"EVMOVE",EVMOVE,NULL};
	VPL_ExtConstant _EVSHOW_C = {"EVSHOW",EVSHOW,NULL};
	VPL_ExtConstant _EVHIDE_C = {"EVHIDE",EVHIDE,NULL};
	VPL_ExtConstant _EVNOP_C = {"EVNOP",EVNOP,NULL};
	VPL_ExtConstant _NX_RightButton_C = {"NX_RightButton",NX_RightButton,NULL};
	VPL_ExtConstant _NX_LeftButton_C = {"NX_LeftButton",NX_LeftButton,NULL};
	VPL_ExtConstant _NX_OneButton_C = {"NX_OneButton",NX_OneButton,NULL};
	VPL_ExtConstant _kDisplaySubPixelShapeElliptical_C = {"kDisplaySubPixelShapeElliptical",kDisplaySubPixelShapeElliptical,NULL};
	VPL_ExtConstant _kDisplaySubPixelShapeOval_C = {"kDisplaySubPixelShapeOval",kDisplaySubPixelShapeOval,NULL};
	VPL_ExtConstant _kDisplaySubPixelShapeRectangular_C = {"kDisplaySubPixelShapeRectangular",kDisplaySubPixelShapeRectangular,NULL};
	VPL_ExtConstant _kDisplaySubPixelShapeSquare_C = {"kDisplaySubPixelShapeSquare",kDisplaySubPixelShapeSquare,NULL};
	VPL_ExtConstant _kDisplaySubPixelShapeRound_C = {"kDisplaySubPixelShapeRound",kDisplaySubPixelShapeRound,NULL};
	VPL_ExtConstant _kDisplaySubPixelShapeUndefined_C = {"kDisplaySubPixelShapeUndefined",kDisplaySubPixelShapeUndefined,NULL};
	VPL_ExtConstant _kDisplaySubPixelConfigurationQuad_C = {"kDisplaySubPixelConfigurationQuad",kDisplaySubPixelConfigurationQuad,NULL};
	VPL_ExtConstant _kDisplaySubPixelConfigurationStripeOffset_C = {"kDisplaySubPixelConfigurationStripeOffset",kDisplaySubPixelConfigurationStripeOffset,NULL};
	VPL_ExtConstant _kDisplaySubPixelConfigurationStripe_C = {"kDisplaySubPixelConfigurationStripe",kDisplaySubPixelConfigurationStripe,NULL};
	VPL_ExtConstant _kDisplaySubPixelConfigurationDelta_C = {"kDisplaySubPixelConfigurationDelta",kDisplaySubPixelConfigurationDelta,NULL};
	VPL_ExtConstant _kDisplaySubPixelConfigurationUndefined_C = {"kDisplaySubPixelConfigurationUndefined",kDisplaySubPixelConfigurationUndefined,NULL};
	VPL_ExtConstant _kDisplaySubPixelLayoutQuadGBR_C = {"kDisplaySubPixelLayoutQuadGBR",kDisplaySubPixelLayoutQuadGBR,NULL};
	VPL_ExtConstant _kDisplaySubPixelLayoutQuadGBL_C = {"kDisplaySubPixelLayoutQuadGBL",kDisplaySubPixelLayoutQuadGBL,NULL};
	VPL_ExtConstant _kDisplaySubPixelLayoutBGR_C = {"kDisplaySubPixelLayoutBGR",kDisplaySubPixelLayoutBGR,NULL};
	VPL_ExtConstant _kDisplaySubPixelLayoutRGB_C = {"kDisplaySubPixelLayoutRGB",kDisplaySubPixelLayoutRGB,NULL};
	VPL_ExtConstant _kDisplaySubPixelLayoutUndefined_C = {"kDisplaySubPixelLayoutUndefined",kDisplaySubPixelLayoutUndefined,NULL};
	VPL_ExtConstant _kDisplayProductIDGeneric_C = {"kDisplayProductIDGeneric",kDisplayProductIDGeneric,NULL};
	VPL_ExtConstant _kDisplayVendorIDUnknown_C = {"kDisplayVendorIDUnknown",kDisplayVendorIDUnknown,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1360x768_60hz_C = {"kIOTimingIDVESA_1360x768_60hz",kIOTimingIDVESA_1360x768_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_848x480_60hz_C = {"kIOTimingIDVESA_848x480_60hz",kIOTimingIDVESA_848x480_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_0x0_0hz_Offline_C = {"kIOTimingIDApple_0x0_0hz_Offline",kIOTimingIDApple_0x0_0hz_Offline,NULL};
	VPL_ExtConstant _kIOTimingIDSony_1920x1200_76hz_C = {"kIOTimingIDSony_1920x1200_76hz",kIOTimingIDSony_1920x1200_76hz,NULL};
	VPL_ExtConstant _kIOTimingIDSony_1920x1080_72hz_C = {"kIOTimingIDSony_1920x1080_72hz",kIOTimingIDSony_1920x1080_72hz,NULL};
	VPL_ExtConstant _kIOTimingIDSony_1920x1080_60hz_C = {"kIOTimingIDSony_1920x1080_60hz",kIOTimingIDSony_1920x1080_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDSony_1600x1024_76hz_C = {"kIOTimingIDSony_1600x1024_76hz",kIOTimingIDSony_1600x1024_76hz,NULL};
	VPL_ExtConstant _kIOTimingIDFilmRate_48hz_C = {"kIOTimingIDFilmRate_48hz",kIOTimingIDFilmRate_48hz,NULL};
	VPL_ExtConstant _kIOTimingIDSMPTE240M_60hz_C = {"kIOTimingIDSMPTE240M_60hz",kIOTimingIDSMPTE240M_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1920x1440_75hz_C = {"kIOTimingIDVESA_1920x1440_75hz",kIOTimingIDVESA_1920x1440_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1920x1440_60hz_C = {"kIOTimingIDVESA_1920x1440_60hz",kIOTimingIDVESA_1920x1440_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1856x1392_75hz_C = {"kIOTimingIDVESA_1856x1392_75hz",kIOTimingIDVESA_1856x1392_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1856x1392_60hz_C = {"kIOTimingIDVESA_1856x1392_60hz",kIOTimingIDVESA_1856x1392_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1792x1344_75hz_C = {"kIOTimingIDVESA_1792x1344_75hz",kIOTimingIDVESA_1792x1344_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1792x1344_60hz_C = {"kIOTimingIDVESA_1792x1344_60hz",kIOTimingIDVESA_1792x1344_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1600x1200_85hz_C = {"kIOTimingIDVESA_1600x1200_85hz",kIOTimingIDVESA_1600x1200_85hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1600x1200_80hz_C = {"kIOTimingIDVESA_1600x1200_80hz",kIOTimingIDVESA_1600x1200_80hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1600x1200_75hz_C = {"kIOTimingIDVESA_1600x1200_75hz",kIOTimingIDVESA_1600x1200_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1600x1200_70hz_C = {"kIOTimingIDVESA_1600x1200_70hz",kIOTimingIDVESA_1600x1200_70hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1600x1200_65hz_C = {"kIOTimingIDVESA_1600x1200_65hz",kIOTimingIDVESA_1600x1200_65hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1600x1200_60hz_C = {"kIOTimingIDVESA_1600x1200_60hz",kIOTimingIDVESA_1600x1200_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1280x1024_85hz_C = {"kIOTimingIDVESA_1280x1024_85hz",kIOTimingIDVESA_1280x1024_85hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1280x1024_75hz_C = {"kIOTimingIDVESA_1280x1024_75hz",kIOTimingIDVESA_1280x1024_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1280x1024_60hz_C = {"kIOTimingIDVESA_1280x1024_60hz",kIOTimingIDVESA_1280x1024_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1280x960_85hz_C = {"kIOTimingIDVESA_1280x960_85hz",kIOTimingIDVESA_1280x960_85hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1280x960_60hz_C = {"kIOTimingIDVESA_1280x960_60hz",kIOTimingIDVESA_1280x960_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1280x960_75hz_C = {"kIOTimingIDVESA_1280x960_75hz",kIOTimingIDVESA_1280x960_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDApplePAL_FFconv_C = {"kIOTimingIDApplePAL_FFconv",kIOTimingIDApplePAL_FFconv,NULL};
	VPL_ExtConstant _kIOTimingIDApplePAL_STconv_C = {"kIOTimingIDApplePAL_STconv",kIOTimingIDApplePAL_STconv,NULL};
	VPL_ExtConstant _kIOTimingIDApplePAL_FF_C = {"kIOTimingIDApplePAL_FF",kIOTimingIDApplePAL_FF,NULL};
	VPL_ExtConstant _kIOTimingIDApplePAL_ST_C = {"kIOTimingIDApplePAL_ST",kIOTimingIDApplePAL_ST,NULL};
	VPL_ExtConstant _kIOTimingIDAppleNTSC_FFconv_C = {"kIOTimingIDAppleNTSC_FFconv",kIOTimingIDAppleNTSC_FFconv,NULL};
	VPL_ExtConstant _kIOTimingIDAppleNTSC_STconv_C = {"kIOTimingIDAppleNTSC_STconv",kIOTimingIDAppleNTSC_STconv,NULL};
	VPL_ExtConstant _kIOTimingIDAppleNTSC_FF_C = {"kIOTimingIDAppleNTSC_FF",kIOTimingIDAppleNTSC_FF,NULL};
	VPL_ExtConstant _kIOTimingIDAppleNTSC_ST_C = {"kIOTimingIDAppleNTSC_ST",kIOTimingIDAppleNTSC_ST,NULL};
	VPL_ExtConstant _kIOTimingIDApple_1152x870_75hz_C = {"kIOTimingIDApple_1152x870_75hz",kIOTimingIDApple_1152x870_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_1024x768_75hz_C = {"kIOTimingIDApple_1024x768_75hz",kIOTimingIDApple_1024x768_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1024x768_85hz_C = {"kIOTimingIDVESA_1024x768_85hz",kIOTimingIDVESA_1024x768_85hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1024x768_75hz_C = {"kIOTimingIDVESA_1024x768_75hz",kIOTimingIDVESA_1024x768_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1024x768_70hz_C = {"kIOTimingIDVESA_1024x768_70hz",kIOTimingIDVESA_1024x768_70hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_1024x768_60hz_C = {"kIOTimingIDVESA_1024x768_60hz",kIOTimingIDVESA_1024x768_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_800x600_85hz_C = {"kIOTimingIDVESA_800x600_85hz",kIOTimingIDVESA_800x600_85hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_800x600_75hz_C = {"kIOTimingIDVESA_800x600_75hz",kIOTimingIDVESA_800x600_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_800x600_72hz_C = {"kIOTimingIDVESA_800x600_72hz",kIOTimingIDVESA_800x600_72hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_800x600_60hz_C = {"kIOTimingIDVESA_800x600_60hz",kIOTimingIDVESA_800x600_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_800x600_56hz_C = {"kIOTimingIDVESA_800x600_56hz",kIOTimingIDVESA_800x600_56hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_832x624_75hz_C = {"kIOTimingIDApple_832x624_75hz",kIOTimingIDApple_832x624_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_640x818_75hz_C = {"kIOTimingIDApple_640x818_75hz",kIOTimingIDApple_640x818_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_640x870_75hz_C = {"kIOTimingIDApple_640x870_75hz",kIOTimingIDApple_640x870_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDGTF_640x480_120hz_C = {"kIOTimingIDGTF_640x480_120hz",kIOTimingIDGTF_640x480_120hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_640x480_85hz_C = {"kIOTimingIDVESA_640x480_85hz",kIOTimingIDVESA_640x480_85hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_640x480_75hz_C = {"kIOTimingIDVESA_640x480_75hz",kIOTimingIDVESA_640x480_75hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_640x480_72hz_C = {"kIOTimingIDVESA_640x480_72hz",kIOTimingIDVESA_640x480_72hz,NULL};
	VPL_ExtConstant _kIOTimingIDVESA_640x480_60hz_C = {"kIOTimingIDVESA_640x480_60hz",kIOTimingIDVESA_640x480_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_640x400_67hz_C = {"kIOTimingIDApple_640x400_67hz",kIOTimingIDApple_640x400_67hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_640x480_67hz_C = {"kIOTimingIDApple_640x480_67hz",kIOTimingIDApple_640x480_67hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_560x384_60hz_C = {"kIOTimingIDApple_560x384_60hz",kIOTimingIDApple_560x384_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_512x384_60hz_C = {"kIOTimingIDApple_512x384_60hz",kIOTimingIDApple_512x384_60hz,NULL};
	VPL_ExtConstant _kIOTimingIDApple_FixedRateLCD_C = {"kIOTimingIDApple_FixedRateLCD",kIOTimingIDApple_FixedRateLCD,NULL};
	VPL_ExtConstant _kIOTimingIDInvalid_C = {"kIOTimingIDInvalid",kIOTimingIDInvalid,NULL};
	VPL_ExtConstant _kIOFBOnlineInterruptType_C = {"kIOFBOnlineInterruptType",kIOFBOnlineInterruptType,NULL};
	VPL_ExtConstant _kIOFBOfflineInterruptType_C = {"kIOFBOfflineInterruptType",kIOFBOfflineInterruptType,NULL};
	VPL_ExtConstant _kIOFBChangedInterruptType_C = {"kIOFBChangedInterruptType",kIOFBChangedInterruptType,NULL};
	VPL_ExtConstant _kIOFBConnectInterruptType_C = {"kIOFBConnectInterruptType",kIOFBConnectInterruptType,NULL};
	VPL_ExtConstant _kIOFBFrameInterruptType_C = {"kIOFBFrameInterruptType",kIOFBFrameInterruptType,NULL};
	VPL_ExtConstant _kIOFBHBLInterruptType_C = {"kIOFBHBLInterruptType",kIOFBHBLInterruptType,NULL};
	VPL_ExtConstant _kIOFBVBLInterruptType_C = {"kIOFBVBLInterruptType",kIOFBVBLInterruptType,NULL};
	VPL_ExtConstant _kHardwareCursorDescriptorMinorVersion_C = {"kHardwareCursorDescriptorMinorVersion",kHardwareCursorDescriptorMinorVersion,NULL};
	VPL_ExtConstant _kHardwareCursorDescriptorMajorVersion_C = {"kHardwareCursorDescriptorMajorVersion",kHardwareCursorDescriptorMajorVersion,NULL};
	VPL_ExtConstant _kInvertingEncodedPixel_C = {"kInvertingEncodedPixel",kInvertingEncodedPixel,NULL};
	VPL_ExtConstant _kInvertingEncodingShift_C = {"kInvertingEncodingShift",kInvertingEncodingShift,NULL};
	VPL_ExtConstant _kTransparentEncodedPixel_C = {"kTransparentEncodedPixel",kTransparentEncodedPixel,NULL};
	VPL_ExtConstant _kTransparentEncodingShift_C = {"kTransparentEncodingShift",kTransparentEncodingShift,NULL};
	VPL_ExtConstant _kInvertingEncoding_C = {"kInvertingEncoding",kInvertingEncoding,NULL};
	VPL_ExtConstant _kTransparentEncoding_C = {"kTransparentEncoding",kTransparentEncoding,NULL};
	VPL_ExtConstant _kIOFBUserRequestProbe_C = {"kIOFBUserRequestProbe",kIOFBUserRequestProbe,NULL};
	VPL_ExtConstant _kIOFBSharedConnectType_C = {"kIOFBSharedConnectType",kIOFBSharedConnectType,NULL};
	VPL_ExtConstant _kIOFBServerConnectType_C = {"kIOFBServerConnectType",kIOFBServerConnectType,NULL};
	VPL_ExtConstant _kIOSyncOnRed_C = {"kIOSyncOnRed",kIOSyncOnRed,NULL};
	VPL_ExtConstant _kIOSyncOnGreen_C = {"kIOSyncOnGreen",kIOSyncOnGreen,NULL};
	VPL_ExtConstant _kIOSyncOnBlue_C = {"kIOSyncOnBlue",kIOSyncOnBlue,NULL};
	VPL_ExtConstant _kIOTriStateSyncs_C = {"kIOTriStateSyncs",kIOTriStateSyncs,NULL};
	VPL_ExtConstant _kIONoSeparateSyncControl_C = {"kIONoSeparateSyncControl",kIONoSeparateSyncControl,NULL};
	VPL_ExtConstant _kIOCSyncDisable_C = {"kIOCSyncDisable",kIOCSyncDisable,NULL};
	VPL_ExtConstant _kIOVSyncDisable_C = {"kIOVSyncDisable",kIOVSyncDisable,NULL};
	VPL_ExtConstant _kIOHSyncDisable_C = {"kIOHSyncDisable",kIOHSyncDisable,NULL};
	VPL_ExtConstant _kIOConnectionStereoSync_C = {"kIOConnectionStereoSync",kIOConnectionStereoSync,NULL};
	VPL_ExtConstant _kIOConnectionBuiltIn_C = {"kIOConnectionBuiltIn",kIOConnectionBuiltIn,NULL};
	VPL_ExtConstant _kConnectionVideoBest_C = {"kConnectionVideoBest",kConnectionVideoBest,NULL};
	VPL_ExtConstant _kConnectionOverscan_C = {"kConnectionOverscan",kConnectionOverscan,NULL};
	VPL_ExtConstant _kConnectionDisplayParameters_C = {"kConnectionDisplayParameters",kConnectionDisplayParameters,NULL};
	VPL_ExtConstant _kConnectionDisplayParameterCount_C = {"kConnectionDisplayParameterCount",kConnectionDisplayParameterCount,NULL};
	VPL_ExtConstant _kConnectionPostWake_C = {"kConnectionPostWake",kConnectionPostWake,NULL};
	VPL_ExtConstant _kConnectionPower_C = {"kConnectionPower",kConnectionPower,NULL};
	VPL_ExtConstant _kConnectionChanged_C = {"kConnectionChanged",kConnectionChanged,NULL};
	VPL_ExtConstant _kConnectionEnable_C = {"kConnectionEnable",kConnectionEnable,NULL};
	VPL_ExtConstant _kConnectionSupportsHLDDCSense_C = {"kConnectionSupportsHLDDCSense",kConnectionSupportsHLDDCSense,NULL};
	VPL_ExtConstant _kConnectionSupportsLLDDCSense_C = {"kConnectionSupportsLLDDCSense",kConnectionSupportsLLDDCSense,NULL};
	VPL_ExtConstant _kConnectionSupportsAppleSense_C = {"kConnectionSupportsAppleSense",kConnectionSupportsAppleSense,NULL};
	VPL_ExtConstant _kConnectionSyncFlags_C = {"kConnectionSyncFlags",kConnectionSyncFlags,NULL};
	VPL_ExtConstant _kConnectionSyncEnable_C = {"kConnectionSyncEnable",kConnectionSyncEnable,NULL};
	VPL_ExtConstant _kConnectionFlags_C = {"kConnectionFlags",kConnectionFlags,NULL};
	VPL_ExtConstant _kAndConnections_C = {"kAndConnections",kAndConnections,NULL};
	VPL_ExtConstant _kOrConnections_C = {"kOrConnections",kOrConnections,NULL};
	VPL_ExtConstant _kIOScaleCanBorderInsetOnly_C = {"kIOScaleCanBorderInsetOnly",kIOScaleCanBorderInsetOnly,NULL};
	VPL_ExtConstant _kIOScaleCanRotate_C = {"kIOScaleCanRotate",kIOScaleCanRotate,NULL};
	VPL_ExtConstant _kIOScaleCanSupportInset_C = {"kIOScaleCanSupportInset",kIOScaleCanSupportInset,NULL};
	VPL_ExtConstant _kIOScaleCanScaleInterlaced_C = {"kIOScaleCanScaleInterlaced",kIOScaleCanScaleInterlaced,NULL};
	VPL_ExtConstant _kIOScaleCanDownSamplePixels_C = {"kIOScaleCanDownSamplePixels",kIOScaleCanDownSamplePixels,NULL};
	VPL_ExtConstant _kIOScaleCanUpSamplePixels_C = {"kIOScaleCanUpSamplePixels",kIOScaleCanUpSamplePixels,NULL};
	VPL_ExtConstant _kIOScaleStretchOnly_C = {"kIOScaleStretchOnly",kIOScaleStretchOnly,NULL};
	VPL_ExtConstant _kIOSyncPositivePolarity_C = {"kIOSyncPositivePolarity",kIOSyncPositivePolarity,NULL};
	VPL_ExtConstant _kIOAnalogSignalLevel_0700_0000_C = {"kIOAnalogSignalLevel_0700_0000",kIOAnalogSignalLevel_0700_0000,NULL};
	VPL_ExtConstant _kIOAnalogSignalLevel_1000_0400_C = {"kIOAnalogSignalLevel_1000_0400",kIOAnalogSignalLevel_1000_0400,NULL};
	VPL_ExtConstant _kIOAnalogSignalLevel_0714_0286_C = {"kIOAnalogSignalLevel_0714_0286",kIOAnalogSignalLevel_0714_0286,NULL};
	VPL_ExtConstant _kIOAnalogSignalLevel_0700_0300_C = {"kIOAnalogSignalLevel_0700_0300",kIOAnalogSignalLevel_0700_0300,NULL};
	VPL_ExtConstant _kIOPALTiming_C = {"kIOPALTiming",kIOPALTiming,NULL};
	VPL_ExtConstant _kIONTSCTiming_C = {"kIONTSCTiming",kIONTSCTiming,NULL};
	VPL_ExtConstant _kIOInterlacedCEATiming_C = {"kIOInterlacedCEATiming",kIOInterlacedCEATiming,NULL};
	VPL_ExtConstant _kIOAnalogSetupExpected_C = {"kIOAnalogSetupExpected",kIOAnalogSetupExpected,NULL};
	VPL_ExtConstant _kIODigitalSignal_C = {"kIODigitalSignal",kIODigitalSignal,NULL};
	VPL_ExtConstant _kIORangeSupportsInterlacedCEATimingWithConfirm_C = {"kIORangeSupportsInterlacedCEATimingWithConfirm",kIORangeSupportsInterlacedCEATimingWithConfirm,NULL};
	VPL_ExtConstant _kIORangeSupportsInterlacedCEATiming_C = {"kIORangeSupportsInterlacedCEATiming",kIORangeSupportsInterlacedCEATiming,NULL};
	VPL_ExtConstant _kIORangeSupportsVSyncSerration_C = {"kIORangeSupportsVSyncSerration",kIORangeSupportsVSyncSerration,NULL};
	VPL_ExtConstant _kIORangeSupportsCompositeSync_C = {"kIORangeSupportsCompositeSync",kIORangeSupportsCompositeSync,NULL};
	VPL_ExtConstant _kIORangeSupportsSyncOnGreen_C = {"kIORangeSupportsSyncOnGreen",kIORangeSupportsSyncOnGreen,NULL};
	VPL_ExtConstant _kIORangeSupportsSeparateSyncs_C = {"kIORangeSupportsSeparateSyncs",kIORangeSupportsSeparateSyncs,NULL};
	VPL_ExtConstant _kIORangeSupportsSignal_0700_0000_C = {"kIORangeSupportsSignal_0700_0000",kIORangeSupportsSignal_0700_0000,NULL};
	VPL_ExtConstant _kIORangeSupportsSignal_1000_0400_C = {"kIORangeSupportsSignal_1000_0400",kIORangeSupportsSignal_1000_0400,NULL};
	VPL_ExtConstant _kIORangeSupportsSignal_0714_0286_C = {"kIORangeSupportsSignal_0714_0286",kIORangeSupportsSignal_0714_0286,NULL};
	VPL_ExtConstant _kIORangeSupportsSignal_0700_0300_C = {"kIORangeSupportsSignal_0700_0300",kIORangeSupportsSignal_0700_0300,NULL};
	VPL_ExtConstant _kIOScaleRotate270_C = {"kIOScaleRotate270",kIOScaleRotate270,NULL};
	VPL_ExtConstant _kIOScaleRotate180_C = {"kIOScaleRotate180",kIOScaleRotate180,NULL};
	VPL_ExtConstant _kIOScaleRotate90_C = {"kIOScaleRotate90",kIOScaleRotate90,NULL};
	VPL_ExtConstant _kIOScaleRotate0_C = {"kIOScaleRotate0",kIOScaleRotate0,NULL};
	VPL_ExtConstant _kIOScaleInvertY_C = {"kIOScaleInvertY",kIOScaleInvertY,NULL};
	VPL_ExtConstant _kIOScaleInvertX_C = {"kIOScaleInvertX",kIOScaleInvertX,NULL};
	VPL_ExtConstant _kIOScaleSwapAxes_C = {"kIOScaleSwapAxes",kIOScaleSwapAxes,NULL};
	VPL_ExtConstant _kIOScaleRotateFlags_C = {"kIOScaleRotateFlags",kIOScaleRotateFlags,NULL};
	VPL_ExtConstant _kIOScaleStretchToFit_C = {"kIOScaleStretchToFit",kIOScaleStretchToFit,NULL};
	VPL_ExtConstant _kIOScalingInfoValid_C = {"kIOScalingInfoValid",kIOScalingInfoValid,NULL};
	VPL_ExtConstant _kIODetailedTimingValid_C = {"kIODetailedTimingValid",kIODetailedTimingValid,NULL};
	VPL_ExtConstant _kIOMirrorForced_C = {"kIOMirrorForced",kIOMirrorForced,NULL};
	VPL_ExtConstant _kIOMirrorDefault_C = {"kIOMirrorDefault",kIOMirrorDefault,NULL};
	VPL_ExtConstant _kIOMirrorHWClipped_C = {"kIOMirrorHWClipped",kIOMirrorHWClipped,NULL};
	VPL_ExtConstant _kIOMirrorIsPrimary_C = {"kIOMirrorIsPrimary",kIOMirrorIsPrimary,NULL};
	VPL_ExtConstant _kIOClamshellStateAttribute_C = {"kIOClamshellStateAttribute",kIOClamshellStateAttribute,NULL};
	VPL_ExtConstant _kIODeferCLUTSetAttribute_C = {"kIODeferCLUTSetAttribute",kIODeferCLUTSetAttribute,NULL};
	VPL_ExtConstant _kIOVRAMSaveAttribute_C = {"kIOVRAMSaveAttribute",kIOVRAMSaveAttribute,NULL};
	VPL_ExtConstant _kIOSystemPowerAttribute_C = {"kIOSystemPowerAttribute",kIOSystemPowerAttribute,NULL};
	VPL_ExtConstant _kIOCursorControlAttribute_C = {"kIOCursorControlAttribute",kIOCursorControlAttribute,NULL};
	VPL_ExtConstant _kIOCapturedAttribute_C = {"kIOCapturedAttribute",kIOCapturedAttribute,NULL};
	VPL_ExtConstant _kIOMirrorDefaultAttribute_C = {"kIOMirrorDefaultAttribute",kIOMirrorDefaultAttribute,NULL};
	VPL_ExtConstant _kIOMirrorAttribute_C = {"kIOMirrorAttribute",kIOMirrorAttribute,NULL};
	VPL_ExtConstant _kIOHardwareCursorAttribute_C = {"kIOHardwareCursorAttribute",kIOHardwareCursorAttribute,NULL};
	VPL_ExtConstant _kIOPowerAttribute_C = {"kIOPowerAttribute",kIOPowerAttribute,NULL};
	VPL_ExtConstant _kSetCLUTWithLuminance_C = {"kSetCLUTWithLuminance",kSetCLUTWithLuminance,NULL};
	VPL_ExtConstant _kSetCLUTImmediately_C = {"kSetCLUTImmediately",kSetCLUTImmediately,NULL};
	VPL_ExtConstant _kSetCLUTByValue_C = {"kSetCLUTByValue",kSetCLUTByValue,NULL};
	VPL_ExtConstant _kIOFBSystemAperture_C = {"kIOFBSystemAperture",kIOFBSystemAperture,NULL};
	VPL_ExtConstant _kFramebufferDisableAltivecAccess_C = {"kFramebufferDisableAltivecAccess",kFramebufferDisableAltivecAccess,NULL};
	VPL_ExtConstant _kFramebufferSupportsGammaCorrection_C = {"kFramebufferSupportsGammaCorrection",kFramebufferSupportsGammaCorrection,NULL};
	VPL_ExtConstant _kFramebufferSupportsWritethruCache_C = {"kFramebufferSupportsWritethruCache",kFramebufferSupportsWritethruCache,NULL};
	VPL_ExtConstant _kFramebufferSupportsCopybackCache_C = {"kFramebufferSupportsCopybackCache",kFramebufferSupportsCopybackCache,NULL};
	VPL_ExtConstant _kDisplayModeDefaultFlag_C = {"kDisplayModeDefaultFlag",kDisplayModeDefaultFlag,NULL};
	VPL_ExtConstant _kDisplayModeSafeFlag_C = {"kDisplayModeSafeFlag",kDisplayModeSafeFlag,NULL};
	VPL_ExtConstant _kDisplayModeValidFlag_C = {"kDisplayModeValidFlag",kDisplayModeValidFlag,NULL};
	VPL_ExtConstant _kDisplayModeTelevisionFlag_C = {"kDisplayModeTelevisionFlag",kDisplayModeTelevisionFlag,NULL};
	VPL_ExtConstant _kDisplayModeNotGraphicsQualityFlag_C = {"kDisplayModeNotGraphicsQualityFlag",kDisplayModeNotGraphicsQualityFlag,NULL};
	VPL_ExtConstant _kDisplayModeStretchedFlag_C = {"kDisplayModeStretchedFlag",kDisplayModeStretchedFlag,NULL};
	VPL_ExtConstant _kDisplayModeNotPresetFlag_C = {"kDisplayModeNotPresetFlag",kDisplayModeNotPresetFlag,NULL};
	VPL_ExtConstant _kDisplayModeBuiltInFlag_C = {"kDisplayModeBuiltInFlag",kDisplayModeBuiltInFlag,NULL};
	VPL_ExtConstant _kDisplayModeSimulscanFlag_C = {"kDisplayModeSimulscanFlag",kDisplayModeSimulscanFlag,NULL};
	VPL_ExtConstant _kDisplayModeInterlacedFlag_C = {"kDisplayModeInterlacedFlag",kDisplayModeInterlacedFlag,NULL};
	VPL_ExtConstant _kDisplayModeRequiresPanFlag_C = {"kDisplayModeRequiresPanFlag",kDisplayModeRequiresPanFlag,NULL};
	VPL_ExtConstant _kDisplayModeNotResizeFlag_C = {"kDisplayModeNotResizeFlag",kDisplayModeNotResizeFlag,NULL};
	VPL_ExtConstant _kDisplayModeNeverShowFlag_C = {"kDisplayModeNeverShowFlag",kDisplayModeNeverShowFlag,NULL};
	VPL_ExtConstant _kDisplayModeAlwaysShowFlag_C = {"kDisplayModeAlwaysShowFlag",kDisplayModeAlwaysShowFlag,NULL};
	VPL_ExtConstant _kDisplayModeSafetyFlags_C = {"kDisplayModeSafetyFlags",kDisplayModeSafetyFlags,NULL};
	VPL_ExtConstant _kIOMonoInverseDirectPixels_C = {"kIOMonoInverseDirectPixels",kIOMonoInverseDirectPixels,NULL};
	VPL_ExtConstant _kIOMonoDirectPixels_C = {"kIOMonoDirectPixels",kIOMonoDirectPixels,NULL};
	VPL_ExtConstant _kIORGBDirectPixels_C = {"kIORGBDirectPixels",kIORGBDirectPixels,NULL};
	VPL_ExtConstant _kIOFixedCLUTPixels_C = {"kIOFixedCLUTPixels",kIOFixedCLUTPixels,NULL};
	VPL_ExtConstant _kIOCLUTPixels_C = {"kIOCLUTPixels",kIOCLUTPixels,NULL};
	VPL_ExtConstant _kIOMaxPixelBits_C = {"kIOMaxPixelBits",kIOMaxPixelBits,NULL};
	VPL_ExtConstant _kIODisplayModeIDReservedBase_C = {"kIODisplayModeIDReservedBase",kIODisplayModeIDReservedBase,NULL};
	VPL_ExtConstant _kIODisplayModeIDBootProgrammable_C = {"kIODisplayModeIDBootProgrammable",kIODisplayModeIDBootProgrammable,NULL};
	VPL_ExtConstant _kUSBAddExtraResetTimeMask_C = {"kUSBAddExtraResetTimeMask",kUSBAddExtraResetTimeMask,NULL};
	VPL_ExtConstant _kUSBAddExtraResetTimeBit_C = {"kUSBAddExtraResetTimeBit",kUSBAddExtraResetTimeBit,NULL};
	VPL_ExtConstant _kUSBGangOverCurrentNotificationType_C = {"kUSBGangOverCurrentNotificationType",kUSBGangOverCurrentNotificationType,NULL};
	VPL_ExtConstant _kUSBIndividualOverCurrentNotificationType_C = {"kUSBIndividualOverCurrentNotificationType",kUSBIndividualOverCurrentNotificationType,NULL};
	VPL_ExtConstant _kUSBNotEnoughPowerNotificationType_C = {"kUSBNotEnoughPowerNotificationType",kUSBNotEnoughPowerNotificationType,NULL};
	VPL_ExtConstant _kUSBNoUserNotificationType_C = {"kUSBNoUserNotificationType",kUSBNoUserNotificationType,NULL};
	VPL_ExtConstant _kUSBLowLatencyFrameListBuffer_C = {"kUSBLowLatencyFrameListBuffer",kUSBLowLatencyFrameListBuffer,NULL};
	VPL_ExtConstant _kUSBLowLatencyReadBuffer_C = {"kUSBLowLatencyReadBuffer",kUSBLowLatencyReadBuffer,NULL};
	VPL_ExtConstant _kUSBLowLatencyWriteBuffer_C = {"kUSBLowLatencyWriteBuffer",kUSBLowLatencyWriteBuffer,NULL};
	VPL_ExtConstant _kUSBLowLatencyIsochTransferKey_C = {"kUSBLowLatencyIsochTransferKey",kUSBLowLatencyIsochTransferKey,NULL};
	VPL_ExtConstant _kUSBHighSpeedMicrosecondsInFrame_C = {"kUSBHighSpeedMicrosecondsInFrame",kUSBHighSpeedMicrosecondsInFrame,NULL};
	VPL_ExtConstant _kUSBFullSpeedMicrosecondsInFrame_C = {"kUSBFullSpeedMicrosecondsInFrame",kUSBFullSpeedMicrosecondsInFrame,NULL};
	VPL_ExtConstant _kUSBDeviceSpeedHigh_C = {"kUSBDeviceSpeedHigh",kUSBDeviceSpeedHigh,NULL};
	VPL_ExtConstant _kUSBDeviceSpeedFull_C = {"kUSBDeviceSpeedFull",kUSBDeviceSpeedFull,NULL};
	VPL_ExtConstant _kUSBDeviceSpeedLow_C = {"kUSBDeviceSpeedLow",kUSBDeviceSpeedLow,NULL};
	VPL_ExtConstant _kIOUSBVendorIDAppleComputer_C = {"kIOUSBVendorIDAppleComputer",kIOUSBVendorIDAppleComputer,NULL};
	VPL_ExtConstant _kIOUSBFindInterfaceDontCare_C = {"kIOUSBFindInterfaceDontCare",kIOUSBFindInterfaceDontCare,NULL};
	VPL_ExtConstant _kUSBDefaultControlCompletionTimeoutMS_C = {"kUSBDefaultControlCompletionTimeoutMS",kUSBDefaultControlCompletionTimeoutMS,NULL};
	VPL_ExtConstant _kUSBDefaultControlNoDataTimeoutMS_C = {"kUSBDefaultControlNoDataTimeoutMS",kUSBDefaultControlNoDataTimeoutMS,NULL};
	VPL_ExtConstant _kIOUSBAnyProduct_C = {"kIOUSBAnyProduct",kIOUSBAnyProduct,NULL};
	VPL_ExtConstant _kIOUSBAnyVendor_C = {"kIOUSBAnyVendor",kIOUSBAnyVendor,NULL};
	VPL_ExtConstant _kIOUSBAnyProtocol_C = {"kIOUSBAnyProtocol",kIOUSBAnyProtocol,NULL};
	VPL_ExtConstant _kIOUSBAnySubClass_C = {"kIOUSBAnySubClass",kIOUSBAnySubClass,NULL};
	VPL_ExtConstant _kIOUSBAnyClass_C = {"kIOUSBAnyClass",kIOUSBAnyClass,NULL};
	VPL_ExtConstant _addPacketShift_C = {"addPacketShift",addPacketShift,NULL};
	VPL_ExtConstant _kSyncFrame_C = {"kSyncFrame",kSyncFrame,NULL};
	VPL_ExtConstant _kSetInterface_C = {"kSetInterface",kSetInterface,NULL};
	VPL_ExtConstant _kSetEndpointFeature_C = {"kSetEndpointFeature",kSetEndpointFeature,NULL};
	VPL_ExtConstant _kSetInterfaceFeature_C = {"kSetInterfaceFeature",kSetInterfaceFeature,NULL};
	VPL_ExtConstant _kSetDeviceFeature_C = {"kSetDeviceFeature",kSetDeviceFeature,NULL};
	VPL_ExtConstant _kSetDescriptor_C = {"kSetDescriptor",kSetDescriptor,NULL};
	VPL_ExtConstant _kSetConfiguration_C = {"kSetConfiguration",kSetConfiguration,NULL};
	VPL_ExtConstant _kSetAddress_C = {"kSetAddress",kSetAddress,NULL};
	VPL_ExtConstant _kGetEndpointStatus_C = {"kGetEndpointStatus",kGetEndpointStatus,NULL};
	VPL_ExtConstant _kGetInterfaceStatus_C = {"kGetInterfaceStatus",kGetInterfaceStatus,NULL};
	VPL_ExtConstant _kGetDeviceStatus_C = {"kGetDeviceStatus",kGetDeviceStatus,NULL};
	VPL_ExtConstant _kGetInterface_C = {"kGetInterface",kGetInterface,NULL};
	VPL_ExtConstant _kGetDescriptor_C = {"kGetDescriptor",kGetDescriptor,NULL};
	VPL_ExtConstant _kGetConfiguration_C = {"kGetConfiguration",kGetConfiguration,NULL};
	VPL_ExtConstant _kClearEndpointFeature_C = {"kClearEndpointFeature",kClearEndpointFeature,NULL};
	VPL_ExtConstant _kClearInterfaceFeature_C = {"kClearInterfaceFeature",kClearInterfaceFeature,NULL};
	VPL_ExtConstant _kClearDeviceFeature_C = {"kClearDeviceFeature",kClearDeviceFeature,NULL};
	VPL_ExtConstant _kUSBMaxHSIsocFrameCount_C = {"kUSBMaxHSIsocFrameCount",kUSBMaxHSIsocFrameCount,NULL};
	VPL_ExtConstant _kUSBMaxHSIsocEndpointReqCount_C = {"kUSBMaxHSIsocEndpointReqCount",kUSBMaxHSIsocEndpointReqCount,NULL};
	VPL_ExtConstant _kUSBMaxFSIsocEndpointReqCount_C = {"kUSBMaxFSIsocEndpointReqCount",kUSBMaxFSIsocEndpointReqCount,NULL};
	VPL_ExtConstant _kUSBRqRecipientMask_C = {"kUSBRqRecipientMask",kUSBRqRecipientMask,NULL};
	VPL_ExtConstant _kUSBRqTypeMask_C = {"kUSBRqTypeMask",kUSBRqTypeMask,NULL};
	VPL_ExtConstant _kUSBRqTypeShift_C = {"kUSBRqTypeShift",kUSBRqTypeShift,NULL};
	VPL_ExtConstant _kUSBRqDirnMask_C = {"kUSBRqDirnMask",kUSBRqDirnMask,NULL};
	VPL_ExtConstant _kUSBRqDirnShift_C = {"kUSBRqDirnShift",kUSBRqDirnShift,NULL};
	VPL_ExtConstant _kUSBNoPipeIdx_C = {"kUSBNoPipeIdx",kUSBNoPipeIdx,NULL};
	VPL_ExtConstant _kUSBDeviceMask_C = {"kUSBDeviceMask",kUSBDeviceMask,NULL};
	VPL_ExtConstant _kUSBEndPtShift_C = {"kUSBEndPtShift",kUSBEndPtShift,NULL};
	VPL_ExtConstant _kUSBInterfaceIDMask_C = {"kUSBInterfaceIDMask",kUSBInterfaceIDMask,NULL};
	VPL_ExtConstant _kUSBMaxInterfaces_C = {"kUSBMaxInterfaces",kUSBMaxInterfaces,NULL};
	VPL_ExtConstant _kUSBInterfaceIDShift_C = {"kUSBInterfaceIDShift",kUSBInterfaceIDShift,NULL};
	VPL_ExtConstant _kUSBMaxPipes_C = {"kUSBMaxPipes",kUSBMaxPipes,NULL};
	VPL_ExtConstant _kUSBPipeIDMask_C = {"kUSBPipeIDMask",kUSBPipeIDMask,NULL};
	VPL_ExtConstant _kUSBDeviceIDMask_C = {"kUSBDeviceIDMask",kUSBDeviceIDMask,NULL};
	VPL_ExtConstant _kUSBMaxDevice_C = {"kUSBMaxDevice",kUSBMaxDevice,NULL};
	VPL_ExtConstant _kUSBMaxDevices_C = {"kUSBMaxDevices",kUSBMaxDevices,NULL};
	VPL_ExtConstant _kUSBDeviceIDShift_C = {"kUSBDeviceIDShift",kUSBDeviceIDShift,NULL};
	VPL_ExtConstant _kUSBEndpointbmAttributesUsageTypeShift_C = {"kUSBEndpointbmAttributesUsageTypeShift",kUSBEndpointbmAttributesUsageTypeShift,NULL};
	VPL_ExtConstant _kUSBEndpointbmAttributesUsageTypeMask_C = {"kUSBEndpointbmAttributesUsageTypeMask",kUSBEndpointbmAttributesUsageTypeMask,NULL};
	VPL_ExtConstant _kUSBEndpointbmAttributesSynchronizationTypeShift_C = {"kUSBEndpointbmAttributesSynchronizationTypeShift",kUSBEndpointbmAttributesSynchronizationTypeShift,NULL};
	VPL_ExtConstant _kUSBEndpointbmAttributesSynchronizationTypeMask_C = {"kUSBEndpointbmAttributesSynchronizationTypeMask",kUSBEndpointbmAttributesSynchronizationTypeMask,NULL};
	VPL_ExtConstant _kUSBEndpointbmAttributesTransferTypeMask_C = {"kUSBEndpointbmAttributesTransferTypeMask",kUSBEndpointbmAttributesTransferTypeMask,NULL};
	VPL_ExtConstant _kUSBEndpointDirectionIn_C = {"kUSBEndpointDirectionIn",kUSBEndpointDirectionIn,NULL};
	VPL_ExtConstant _kUSBEndpointDirectionOut_C = {"kUSBEndpointDirectionOut",kUSBEndpointDirectionOut,NULL};
	VPL_ExtConstant _kUSBbEndpointDirectionMask_C = {"kUSBbEndpointDirectionMask",kUSBbEndpointDirectionMask,NULL};
	VPL_ExtConstant _kUSBbEndpointDirectionBit_C = {"kUSBbEndpointDirectionBit",kUSBbEndpointDirectionBit,NULL};
	VPL_ExtConstant _kUSBbEndpointAddressMask_C = {"kUSBbEndpointAddressMask",kUSBbEndpointAddressMask,NULL};
	VPL_ExtConstant _kUSBDFUManifestationTolerantBit_C = {"kUSBDFUManifestationTolerantBit",kUSBDFUManifestationTolerantBit,NULL};
	VPL_ExtConstant _kUSBDFUCanUploadBit_C = {"kUSBDFUCanUploadBit",kUSBDFUCanUploadBit,NULL};
	VPL_ExtConstant _kUSBDFUCanDownloadBit_C = {"kUSBDFUCanDownloadBit",kUSBDFUCanDownloadBit,NULL};
	VPL_ExtConstant _kUSBDFUAttributesMask_C = {"kUSBDFUAttributesMask",kUSBDFUAttributesMask,NULL};
	VPL_ExtConstant _KUSBInterfaceAssociationDescriptorProtocol_C = {"KUSBInterfaceAssociationDescriptorProtocol",KUSBInterfaceAssociationDescriptorProtocol,NULL};
	VPL_ExtConstant _kUSBBluetoothProgrammingInterfaceProtocol_C = {"kUSBBluetoothProgrammingInterfaceProtocol",kUSBBluetoothProgrammingInterfaceProtocol,NULL};
	VPL_ExtConstant _kUSB2ComplianceDeviceProtocol_C = {"kUSB2ComplianceDeviceProtocol",kUSB2ComplianceDeviceProtocol,NULL};
	VPL_ExtConstant _kUSBVendorSpecificProtocol_C = {"kUSBVendorSpecificProtocol",kUSBVendorSpecificProtocol,NULL};
	VPL_ExtConstant _kHIDMouseInterfaceProtocol_C = {"kHIDMouseInterfaceProtocol",kHIDMouseInterfaceProtocol,NULL};
	VPL_ExtConstant _kHIDKeyboardInterfaceProtocol_C = {"kHIDKeyboardInterfaceProtocol",kHIDKeyboardInterfaceProtocol,NULL};
	VPL_ExtConstant _kHIDNoInterfaceProtocol_C = {"kHIDNoInterfaceProtocol",kHIDNoInterfaceProtocol,NULL};
	VPL_ExtConstant _kUSBVideoInterfaceCollectionSubClass_C = {"kUSBVideoInterfaceCollectionSubClass",kUSBVideoInterfaceCollectionSubClass,NULL};
	VPL_ExtConstant _kUSBVideoStreamingSubClass_C = {"kUSBVideoStreamingSubClass",kUSBVideoStreamingSubClass,NULL};
	VPL_ExtConstant _kUSBVideoControlSubClass_C = {"kUSBVideoControlSubClass",kUSBVideoControlSubClass,NULL};
	VPL_ExtConstant _kUSBCommonClassSubClass_C = {"kUSBCommonClassSubClass",kUSBCommonClassSubClass,NULL};
	VPL_ExtConstant _kUSBRFControllerSubClass_C = {"kUSBRFControllerSubClass",kUSBRFControllerSubClass,NULL};
	VPL_ExtConstant _kUSBReprogrammableDiagnosticSubClass_C = {"kUSBReprogrammableDiagnosticSubClass",kUSBReprogrammableDiagnosticSubClass,NULL};
	VPL_ExtConstant _kUSBATMNetworkingSubClass_C = {"kUSBATMNetworkingSubClass",kUSBATMNetworkingSubClass,NULL};
	VPL_ExtConstant _kUSBCommEthernetNetworkingSubClass_C = {"kUSBCommEthernetNetworkingSubClass",kUSBCommEthernetNetworkingSubClass,NULL};
	VPL_ExtConstant _kUSBCommCAPISubClass_C = {"kUSBCommCAPISubClass",kUSBCommCAPISubClass,NULL};
	VPL_ExtConstant _kUSBCommMultiChannelSubClass_C = {"kUSBCommMultiChannelSubClass",kUSBCommMultiChannelSubClass,NULL};
	VPL_ExtConstant _kUSBCommTelephoneSubClass_C = {"kUSBCommTelephoneSubClass",kUSBCommTelephoneSubClass,NULL};
	VPL_ExtConstant _kUSBCommAbstractSubClass_C = {"kUSBCommAbstractSubClass",kUSBCommAbstractSubClass,NULL};
	VPL_ExtConstant _kUSBCommDirectLineSubClass_C = {"kUSBCommDirectLineSubClass",kUSBCommDirectLineSubClass,NULL};
	VPL_ExtConstant _kUSBHIDBootInterfaceSubClass_C = {"kUSBHIDBootInterfaceSubClass",kUSBHIDBootInterfaceSubClass,NULL};
	VPL_ExtConstant _kUSBMassStorageSCSISubClass_C = {"kUSBMassStorageSCSISubClass",kUSBMassStorageSCSISubClass,NULL};
	VPL_ExtConstant _kUSBMassStorageSFF8070iSubClass_C = {"kUSBMassStorageSFF8070iSubClass",kUSBMassStorageSFF8070iSubClass,NULL};
	VPL_ExtConstant _kUSBMassStorageUFISubClass_C = {"kUSBMassStorageUFISubClass",kUSBMassStorageUFISubClass,NULL};
	VPL_ExtConstant _kUSBMassStorageQIC157SubClass_C = {"kUSBMassStorageQIC157SubClass",kUSBMassStorageQIC157SubClass,NULL};
	VPL_ExtConstant _kUSBMassStorageATAPISubClass_C = {"kUSBMassStorageATAPISubClass",kUSBMassStorageATAPISubClass,NULL};
	VPL_ExtConstant _kUSBMassStorageRBCSubClass_C = {"kUSBMassStorageRBCSubClass",kUSBMassStorageRBCSubClass,NULL};
	VPL_ExtConstant _kUSBTestMeasurementSubClass_C = {"kUSBTestMeasurementSubClass",kUSBTestMeasurementSubClass,NULL};
	VPL_ExtConstant _kUSBIrDABridgeSubClass_C = {"kUSBIrDABridgeSubClass",kUSBIrDABridgeSubClass,NULL};
	VPL_ExtConstant _kUSBDFUSubClass_C = {"kUSBDFUSubClass",kUSBDFUSubClass,NULL};
	VPL_ExtConstant _kUSBMIDIStreamingSubClass_C = {"kUSBMIDIStreamingSubClass",kUSBMIDIStreamingSubClass,NULL};
	VPL_ExtConstant _kUSBAudioStreamingSubClass_C = {"kUSBAudioStreamingSubClass",kUSBAudioStreamingSubClass,NULL};
	VPL_ExtConstant _kUSBAudioControlSubClass_C = {"kUSBAudioControlSubClass",kUSBAudioControlSubClass,NULL};
	VPL_ExtConstant _kUSBHubSubClass_C = {"kUSBHubSubClass",kUSBHubSubClass,NULL};
	VPL_ExtConstant _kUSBCompositeSubClass_C = {"kUSBCompositeSubClass",kUSBCompositeSubClass,NULL};
	VPL_ExtConstant _kUSBDisplayClass_C = {"kUSBDisplayClass",kUSBDisplayClass,NULL};
	VPL_ExtConstant _kUSBVendorSpecificInterfaceClass_C = {"kUSBVendorSpecificInterfaceClass",kUSBVendorSpecificInterfaceClass,NULL};
	VPL_ExtConstant _kUSBApplicationSpecificInterfaceClass_C = {"kUSBApplicationSpecificInterfaceClass",kUSBApplicationSpecificInterfaceClass,NULL};
	VPL_ExtConstant _kUSBWirelessControllerInterfaceClass_C = {"kUSBWirelessControllerInterfaceClass",kUSBWirelessControllerInterfaceClass,NULL};
	VPL_ExtConstant _kUSBDiagnosticDeviceInterfaceClass_C = {"kUSBDiagnosticDeviceInterfaceClass",kUSBDiagnosticDeviceInterfaceClass,NULL};
	VPL_ExtConstant _kUSBVideoInterfaceClass_C = {"kUSBVideoInterfaceClass",kUSBVideoInterfaceClass,NULL};
	VPL_ExtConstant _kUSBContentSecurityInterfaceClass_C = {"kUSBContentSecurityInterfaceClass",kUSBContentSecurityInterfaceClass,NULL};
	VPL_ExtConstant _kUSBChipSmartCardInterfaceClass_C = {"kUSBChipSmartCardInterfaceClass",kUSBChipSmartCardInterfaceClass,NULL};
	VPL_ExtConstant _kUSBMassStorageInterfaceClass_C = {"kUSBMassStorageInterfaceClass",kUSBMassStorageInterfaceClass,NULL};
	VPL_ExtConstant _kUSBMassStorageClass_C = {"kUSBMassStorageClass",kUSBMassStorageClass,NULL};
	VPL_ExtConstant _kUSBPrintingInterfaceClass_C = {"kUSBPrintingInterfaceClass",kUSBPrintingInterfaceClass,NULL};
	VPL_ExtConstant _kUSBPrintingClass_C = {"kUSBPrintingClass",kUSBPrintingClass,NULL};
	VPL_ExtConstant _kUSBImageInterfaceClass_C = {"kUSBImageInterfaceClass",kUSBImageInterfaceClass,NULL};
	VPL_ExtConstant _kUSBPhysicalInterfaceClass_C = {"kUSBPhysicalInterfaceClass",kUSBPhysicalInterfaceClass,NULL};
	VPL_ExtConstant _kUSBHIDInterfaceClass_C = {"kUSBHIDInterfaceClass",kUSBHIDInterfaceClass,NULL};
	VPL_ExtConstant _kUSBHIDClass_C = {"kUSBHIDClass",kUSBHIDClass,NULL};
	VPL_ExtConstant _kUSBCommunicationDataInterfaceClass_C = {"kUSBCommunicationDataInterfaceClass",kUSBCommunicationDataInterfaceClass,NULL};
	VPL_ExtConstant _kUSBCommunicationControlInterfaceClass_C = {"kUSBCommunicationControlInterfaceClass",kUSBCommunicationControlInterfaceClass,NULL};
	VPL_ExtConstant _kUSBAudioInterfaceClass_C = {"kUSBAudioInterfaceClass",kUSBAudioInterfaceClass,NULL};
	VPL_ExtConstant _kUSBAudioClass_C = {"kUSBAudioClass",kUSBAudioClass,NULL};
	VPL_ExtConstant _kUSBVendorSpecificClass_C = {"kUSBVendorSpecificClass",kUSBVendorSpecificClass,NULL};
	VPL_ExtConstant _kUSBApplicationSpecificClass_C = {"kUSBApplicationSpecificClass",kUSBApplicationSpecificClass,NULL};
	VPL_ExtConstant _kUSBMiscellaneousClass_C = {"kUSBMiscellaneousClass",kUSBMiscellaneousClass,NULL};
	VPL_ExtConstant _kUSBWirelessControllerClass_C = {"kUSBWirelessControllerClass",kUSBWirelessControllerClass,NULL};
	VPL_ExtConstant _kUSBDiagnosticClass_C = {"kUSBDiagnosticClass",kUSBDiagnosticClass,NULL};
	VPL_ExtConstant _kUSBDataClass_C = {"kUSBDataClass",kUSBDataClass,NULL};
	VPL_ExtConstant _kUSBHubClass_C = {"kUSBHubClass",kUSBHubClass,NULL};
	VPL_ExtConstant _kUSBCommunicationClass_C = {"kUSBCommunicationClass",kUSBCommunicationClass,NULL};
	VPL_ExtConstant _kUSBCommClass_C = {"kUSBCommClass",kUSBCommClass,NULL};
	VPL_ExtConstant _kUSBCompositeClass_C = {"kUSBCompositeClass",kUSBCompositeClass,NULL};
	VPL_ExtConstant _kUSBScrollLockKey_C = {"kUSBScrollLockKey",kUSBScrollLockKey,NULL};
	VPL_ExtConstant _kUSBNumLockKey_C = {"kUSBNumLockKey",kUSBNumLockKey,NULL};
	VPL_ExtConstant _kUSBCapsLockKey_C = {"kUSBCapsLockKey",kUSBCapsLockKey,NULL};
	VPL_ExtConstant _kHIDReportProtocolValue_C = {"kHIDReportProtocolValue",kHIDReportProtocolValue,NULL};
	VPL_ExtConstant _kHIDBootProtocolValue_C = {"kHIDBootProtocolValue",kHIDBootProtocolValue,NULL};
	VPL_ExtConstant _kHIDRtFeatureReport_C = {"kHIDRtFeatureReport",kHIDRtFeatureReport,NULL};
	VPL_ExtConstant _kHIDRtOutputReport_C = {"kHIDRtOutputReport",kHIDRtOutputReport,NULL};
	VPL_ExtConstant _kHIDRtInputReport_C = {"kHIDRtInputReport",kHIDRtInputReport,NULL};
	VPL_ExtConstant _kHIDRqSetProtocol_C = {"kHIDRqSetProtocol",kHIDRqSetProtocol,NULL};
	VPL_ExtConstant _kHIDRqSetIdle_C = {"kHIDRqSetIdle",kHIDRqSetIdle,NULL};
	VPL_ExtConstant _kHIDRqSetReport_C = {"kHIDRqSetReport",kHIDRqSetReport,NULL};
	VPL_ExtConstant _kHIDRqGetProtocol_C = {"kHIDRqGetProtocol",kHIDRqGetProtocol,NULL};
	VPL_ExtConstant _kHIDRqGetIdle_C = {"kHIDRqGetIdle",kHIDRqGetIdle,NULL};
	VPL_ExtConstant _kHIDRqGetReport_C = {"kHIDRqGetReport",kHIDRqGetReport,NULL};
	VPL_ExtConstant _kUSBRel20_C = {"kUSBRel20",kUSBRel20,NULL};
	VPL_ExtConstant _kUSBRel11_C = {"kUSBRel11",kUSBRel11,NULL};
	VPL_ExtConstant _kUSBRel10_C = {"kUSBRel10",kUSBRel10,NULL};
	VPL_ExtConstant _kUSBAtrRemoteWakeup_C = {"kUSBAtrRemoteWakeup",kUSBAtrRemoteWakeup,NULL};
	VPL_ExtConstant _kUSBAtrSelfPowered_C = {"kUSBAtrSelfPowered",kUSBAtrSelfPowered,NULL};
	VPL_ExtConstant _kUSBAtrBusPowered_C = {"kUSBAtrBusPowered",kUSBAtrBusPowered,NULL};
	VPL_ExtConstant _kUSB100mA_C = {"kUSB100mA",kUSB100mA,NULL};
	VPL_ExtConstant _kUSB500mAAvailable_C = {"kUSB500mAAvailable",kUSB500mAAvailable,NULL};
	VPL_ExtConstant _kUSB100mAAvailable_C = {"kUSB100mAAvailable",kUSB100mAAvailable,NULL};
	VPL_ExtConstant _kUSBFeatureDeviceRemoteWakeup_C = {"kUSBFeatureDeviceRemoteWakeup",kUSBFeatureDeviceRemoteWakeup,NULL};
	VPL_ExtConstant _kUSBFeatureEndpointStall_C = {"kUSBFeatureEndpointStall",kUSBFeatureEndpointStall,NULL};
	VPL_ExtConstant _kUSBHUBDesc_C = {"kUSBHUBDesc",kUSBHUBDesc,NULL};
	VPL_ExtConstant _kUSBPhysicalDesc_C = {"kUSBPhysicalDesc",kUSBPhysicalDesc,NULL};
	VPL_ExtConstant _kUSBReportDesc_C = {"kUSBReportDesc",kUSBReportDesc,NULL};
	VPL_ExtConstant _kUSBHIDDesc_C = {"kUSBHIDDesc",kUSBHIDDesc,NULL};
	VPL_ExtConstant _kUSBInterfaceAssociationDesc_C = {"kUSBInterfaceAssociationDesc",kUSBInterfaceAssociationDesc,NULL};
	VPL_ExtConstant _kUSDebugDesc_C = {"kUSDebugDesc",kUSDebugDesc,NULL};
	VPL_ExtConstant _kUSBOnTheGoDesc_C = {"kUSBOnTheGoDesc",kUSBOnTheGoDesc,NULL};
	VPL_ExtConstant _kUSBInterfacePowerDesc_C = {"kUSBInterfacePowerDesc",kUSBInterfacePowerDesc,NULL};
	VPL_ExtConstant _kUSBOtherSpeedConfDesc_C = {"kUSBOtherSpeedConfDesc",kUSBOtherSpeedConfDesc,NULL};
	VPL_ExtConstant _kUSBDeviceQualifierDesc_C = {"kUSBDeviceQualifierDesc",kUSBDeviceQualifierDesc,NULL};
	VPL_ExtConstant _kUSBEndpointDesc_C = {"kUSBEndpointDesc",kUSBEndpointDesc,NULL};
	VPL_ExtConstant _kUSBInterfaceDesc_C = {"kUSBInterfaceDesc",kUSBInterfaceDesc,NULL};
	VPL_ExtConstant _kUSBStringDesc_C = {"kUSBStringDesc",kUSBStringDesc,NULL};
	VPL_ExtConstant _kUSBConfDesc_C = {"kUSBConfDesc",kUSBConfDesc,NULL};
	VPL_ExtConstant _kUSBDeviceDesc_C = {"kUSBDeviceDesc",kUSBDeviceDesc,NULL};
	VPL_ExtConstant _kUSBAnyDesc_C = {"kUSBAnyDesc",kUSBAnyDesc,NULL};
	VPL_ExtConstant _kUSBRqSyncFrame_C = {"kUSBRqSyncFrame",kUSBRqSyncFrame,NULL};
	VPL_ExtConstant _kUSBRqSetInterface_C = {"kUSBRqSetInterface",kUSBRqSetInterface,NULL};
	VPL_ExtConstant _kUSBRqGetInterface_C = {"kUSBRqGetInterface",kUSBRqGetInterface,NULL};
	VPL_ExtConstant _kUSBRqSetConfig_C = {"kUSBRqSetConfig",kUSBRqSetConfig,NULL};
	VPL_ExtConstant _kUSBRqGetConfig_C = {"kUSBRqGetConfig",kUSBRqGetConfig,NULL};
	VPL_ExtConstant _kUSBRqSetDescriptor_C = {"kUSBRqSetDescriptor",kUSBRqSetDescriptor,NULL};
	VPL_ExtConstant _kUSBRqGetDescriptor_C = {"kUSBRqGetDescriptor",kUSBRqGetDescriptor,NULL};
	VPL_ExtConstant _kUSBRqSetAddress_C = {"kUSBRqSetAddress",kUSBRqSetAddress,NULL};
	VPL_ExtConstant _kUSBRqReserved2_C = {"kUSBRqReserved2",kUSBRqReserved2,NULL};
	VPL_ExtConstant _kUSBRqSetFeature_C = {"kUSBRqSetFeature",kUSBRqSetFeature,NULL};
	VPL_ExtConstant _kUSBRqGetState_C = {"kUSBRqGetState",kUSBRqGetState,NULL};
	VPL_ExtConstant _kUSBRqClearFeature_C = {"kUSBRqClearFeature",kUSBRqClearFeature,NULL};
	VPL_ExtConstant _kUSBRqGetStatus_C = {"kUSBRqGetStatus",kUSBRqGetStatus,NULL};
	VPL_ExtConstant _kUSBOther_C = {"kUSBOther",kUSBOther,NULL};
	VPL_ExtConstant _kUSBEndpoint_C = {"kUSBEndpoint",kUSBEndpoint,NULL};
	VPL_ExtConstant _kUSBInterface_C = {"kUSBInterface",kUSBInterface,NULL};
	VPL_ExtConstant _kUSBDevice_C = {"kUSBDevice",kUSBDevice,NULL};
	VPL_ExtConstant _kUSBVendor_C = {"kUSBVendor",kUSBVendor,NULL};
	VPL_ExtConstant _kUSBClass_C = {"kUSBClass",kUSBClass,NULL};
	VPL_ExtConstant _kUSBStandard_C = {"kUSBStandard",kUSBStandard,NULL};
	VPL_ExtConstant _kUSBAnyDirn_C = {"kUSBAnyDirn",kUSBAnyDirn,NULL};
	VPL_ExtConstant _kUSBNone_C = {"kUSBNone",kUSBNone,NULL};
	VPL_ExtConstant _kUSBIn_C = {"kUSBIn",kUSBIn,NULL};
	VPL_ExtConstant _kUSBOut_C = {"kUSBOut",kUSBOut,NULL};
	VPL_ExtConstant _kUSBAnyType_C = {"kUSBAnyType",kUSBAnyType,NULL};
	VPL_ExtConstant _kUSBInterrupt_C = {"kUSBInterrupt",kUSBInterrupt,NULL};
	VPL_ExtConstant _kUSBBulk_C = {"kUSBBulk",kUSBBulk,NULL};
	VPL_ExtConstant _kUSBIsoc_C = {"kUSBIsoc",kUSBIsoc,NULL};
	VPL_ExtConstant _kUSBControl_C = {"kUSBControl",kUSBControl,NULL};
	VPL_ExtConstant _kIORegistryIterateParents_C = {"kIORegistryIterateParents",kIORegistryIterateParents,NULL};
	VPL_ExtConstant _kIORegistryIterateRecursively_C = {"kIORegistryIterateRecursively",kIORegistryIterateRecursively,NULL};
	VPL_ExtConstant _kOSAsyncRefSize_C = {"kOSAsyncRefSize",kOSAsyncRefSize,NULL};
	VPL_ExtConstant _kOSAsyncRefCount_C = {"kOSAsyncRefCount",kOSAsyncRefCount,NULL};
	VPL_ExtConstant _kIOInterestCalloutCount_C = {"kIOInterestCalloutCount",kIOInterestCalloutCount,NULL};
	VPL_ExtConstant _kIOInterestCalloutServiceIndex_C = {"kIOInterestCalloutServiceIndex",kIOInterestCalloutServiceIndex,NULL};
	VPL_ExtConstant _kIOInterestCalloutRefconIndex_C = {"kIOInterestCalloutRefconIndex",kIOInterestCalloutRefconIndex,NULL};
	VPL_ExtConstant _kIOInterestCalloutFuncIndex_C = {"kIOInterestCalloutFuncIndex",kIOInterestCalloutFuncIndex,NULL};
	VPL_ExtConstant _kIOMatchingCalloutCount_C = {"kIOMatchingCalloutCount",kIOMatchingCalloutCount,NULL};
	VPL_ExtConstant _kIOMatchingCalloutRefconIndex_C = {"kIOMatchingCalloutRefconIndex",kIOMatchingCalloutRefconIndex,NULL};
	VPL_ExtConstant _kIOMatchingCalloutFuncIndex_C = {"kIOMatchingCalloutFuncIndex",kIOMatchingCalloutFuncIndex,NULL};
	VPL_ExtConstant _kIOAsyncCalloutCount_C = {"kIOAsyncCalloutCount",kIOAsyncCalloutCount,NULL};
	VPL_ExtConstant _kIOAsyncCalloutRefconIndex_C = {"kIOAsyncCalloutRefconIndex",kIOAsyncCalloutRefconIndex,NULL};
	VPL_ExtConstant _kIOAsyncCalloutFuncIndex_C = {"kIOAsyncCalloutFuncIndex",kIOAsyncCalloutFuncIndex,NULL};
	VPL_ExtConstant _kIOAsyncReservedCount_C = {"kIOAsyncReservedCount",kIOAsyncReservedCount,NULL};
	VPL_ExtConstant _kIOAsyncReservedIndex_C = {"kIOAsyncReservedIndex",kIOAsyncReservedIndex,NULL};
	VPL_ExtConstant _kMaxAsyncArgs_C = {"kMaxAsyncArgs",kMaxAsyncArgs,NULL};
	VPL_ExtConstant _kOSAsyncCompleteMessageID_C = {"kOSAsyncCompleteMessageID",kOSAsyncCompleteMessageID,NULL};
	VPL_ExtConstant _kOSNotificationMessageID_C = {"kOSNotificationMessageID",kOSNotificationMessageID,NULL};
	VPL_ExtConstant _kLastIOKitNotificationType_C = {"kLastIOKitNotificationType",kLastIOKitNotificationType,NULL};
	VPL_ExtConstant _kIOServiceMessageNotificationType_C = {"kIOServiceMessageNotificationType",kIOServiceMessageNotificationType,NULL};
	VPL_ExtConstant _kIOAsyncCompletionNotificationType_C = {"kIOAsyncCompletionNotificationType",kIOAsyncCompletionNotificationType,NULL};
	VPL_ExtConstant _kIOServiceTerminatedNotificationType_C = {"kIOServiceTerminatedNotificationType",kIOServiceTerminatedNotificationType,NULL};
	VPL_ExtConstant _kIOServiceMatchedNotificationType_C = {"kIOServiceMatchedNotificationType",kIOServiceMatchedNotificationType,NULL};
	VPL_ExtConstant _kIOServicePublishNotificationType_C = {"kIOServicePublishNotificationType",kIOServicePublishNotificationType,NULL};
	VPL_ExtConstant _kFirstIOKitNotificationType_C = {"kFirstIOKitNotificationType",kFirstIOKitNotificationType,NULL};
	VPL_ExtConstant _IO_CopyBack_C = {"IO_CopyBack",IO_CopyBack,NULL};
	VPL_ExtConstant _IO_WriteThrough_C = {"IO_WriteThrough",IO_WriteThrough,NULL};
	VPL_ExtConstant _IO_CacheOff_C = {"IO_CacheOff",IO_CacheOff,NULL};
	VPL_ExtConstant _kTickScale_C = {"kTickScale",kTickScale,NULL};
	VPL_ExtConstant _kSecondScale_C = {"kSecondScale",kSecondScale,NULL};
	VPL_ExtConstant _kMillisecondScale_C = {"kMillisecondScale",kMillisecondScale,NULL};
	VPL_ExtConstant _kMicrosecondScale_C = {"kMicrosecondScale",kMicrosecondScale,NULL};
	VPL_ExtConstant _kNanosecondScale_C = {"kNanosecondScale",kNanosecondScale,NULL};
	VPL_ExtConstant _kIOMapUnique_C = {"kIOMapUnique",kIOMapUnique,NULL};
	VPL_ExtConstant _kIOMapReference_C = {"kIOMapReference",kIOMapReference,NULL};
	VPL_ExtConstant _kIOMapStatic_C = {"kIOMapStatic",kIOMapStatic,NULL};
	VPL_ExtConstant _kIOMapReadOnly_C = {"kIOMapReadOnly",kIOMapReadOnly,NULL};
	VPL_ExtConstant _kIOMapUserOptionsMask_C = {"kIOMapUserOptionsMask",kIOMapUserOptionsMask,NULL};
	VPL_ExtConstant _kIOMapWriteCombineCache_C = {"kIOMapWriteCombineCache",kIOMapWriteCombineCache,NULL};
	VPL_ExtConstant _kIOMapCopybackCache_C = {"kIOMapCopybackCache",kIOMapCopybackCache,NULL};
	VPL_ExtConstant _kIOMapWriteThruCache_C = {"kIOMapWriteThruCache",kIOMapWriteThruCache,NULL};
	VPL_ExtConstant _kIOMapInhibitCache_C = {"kIOMapInhibitCache",kIOMapInhibitCache,NULL};
	VPL_ExtConstant _kIOMapDefaultCache_C = {"kIOMapDefaultCache",kIOMapDefaultCache,NULL};
	VPL_ExtConstant _kIOMapCacheShift_C = {"kIOMapCacheShift",kIOMapCacheShift,NULL};
	VPL_ExtConstant _kIOMapCacheMask_C = {"kIOMapCacheMask",kIOMapCacheMask,NULL};
	VPL_ExtConstant _kIOMapAnywhere_C = {"kIOMapAnywhere",kIOMapAnywhere,NULL};
	VPL_ExtConstant _kIOWriteCombineCache_C = {"kIOWriteCombineCache",kIOWriteCombineCache,NULL};
	VPL_ExtConstant _kIOCopybackCache_C = {"kIOCopybackCache",kIOCopybackCache,NULL};
	VPL_ExtConstant _kIOWriteThruCache_C = {"kIOWriteThruCache",kIOWriteThruCache,NULL};
	VPL_ExtConstant _kIOInhibitCache_C = {"kIOInhibitCache",kIOInhibitCache,NULL};
	VPL_ExtConstant _kIODefaultCache_C = {"kIODefaultCache",kIODefaultCache,NULL};
	VPL_ExtConstant _kIODefaultMemoryType_C = {"kIODefaultMemoryType",kIODefaultMemoryType,NULL};
	VPL_ExtConstant _finalStage_C = {"finalStage",finalStage,NULL};
	VPL_ExtConstant _betaStage_C = {"betaStage",betaStage,NULL};
	VPL_ExtConstant _alphaStage_C = {"alphaStage",alphaStage,NULL};
	VPL_ExtConstant _developStage_C = {"developStage",developStage,NULL};
	VPL_ExtConstant _kUnknownType_C = {"kUnknownType",kUnknownType,NULL};
	VPL_ExtConstant _kVariableLengthArray_C = {"kVariableLengthArray",kVariableLengthArray,NULL};
	VPL_ExtConstant _kNilOptions_C = {"kNilOptions",kNilOptions,NULL};
	VPL_ExtConstant _noErr_C = {"noErr",noErr,NULL};


VPL_DictionaryNode VPX_MacOSX_IOKit_Constants[] =	{
	{"kIOFBBlitBeamSyncSpin", &_kIOFBBlitBeamSyncSpin_C},
	{"kIOFBBlitBeamSyncAlways", &_kIOFBBlitBeamSyncAlways_C},
	{"kIOFBBlitBeamSync", &_kIOFBBlitBeamSync_C},
	{"kIOFBSynchronizeFlushWrites", &_kIOFBSynchronizeFlushWrites_C},
	{"kIOFBSynchronizeWaitBeamExit", &_kIOFBSynchronizeWaitBeamExit_C},
	{"kIODisplayNoProductName", &_kIODisplayNoProductName_C},
	{"kIODisplayOnlyPreferredName", &_kIODisplayOnlyPreferredName_C},
	{"kIODisplayMatchingInfo", &_kIODisplayMatchingInfo_C},
	{"kIOFBConnectStateOnline", &_kIOFBConnectStateOnline_C},
	{"IOFBMessageCallbacksVersion", &_IOFBMessageCallbacksVersion_C},
	{"kIOBlitColorSpaceTypes", &_kIOBlitColorSpaceTypes_C},
	{"kIOBlitAllOptions", &_kIOBlitAllOptions_C},
	{"kIOBlitBeamSyncSpin", &_kIOBlitBeamSyncSpin_C},
	{"kIOBlitBeamSyncAlways", &_kIOBlitBeamSyncAlways_C},
	{"kIOBlitBeamSync", &_kIOBlitBeamSync_C},
	{"kIOBlitSurfaceDestination", &_kIOBlitSurfaceDestination_C},
	{"kIOBlitFramebufferDestination", &_kIOBlitFramebufferDestination_C},
	{"kIOBlitUnlockWithSwap", &_kIOBlitUnlockWithSwap_C},
	{"kIOBlitReferenceSource", &_kIOBlitReferenceSource_C},
	{"kIOBlitBeamSyncSwaps", &_kIOBlitBeamSyncSwaps_C},
	{"kIOBlitFixedSource", &_kIOBlitFixedSource_C},
	{"kIOBlitHasCGSSurface", &_kIOBlitHasCGSSurface_C},
	{"kIOBlitFlushWithSwap", &_kIOBlitFlushWithSwap_C},
	{"kIOBlitWaitCheck", &_kIOBlitWaitCheck_C},
	{"kIOBlitWaitAll", &_kIOBlitWaitAll_C},
	{"kIOBlitWaitGlobal", &_kIOBlitWaitGlobal_C},
	{"kIOBlitWaitAll2D", &_kIOBlitWaitAll2D_C},
	{"kIOBlitWaitContext", &_kIOBlitWaitContext_C},
	{"kIOBlitSynchronizeFlushHostWrites", &_kIOBlitSynchronizeFlushHostWrites_C},
	{"kIOBlitSynchronizeWaitBeamExit", &_kIOBlitSynchronizeWaitBeamExit_C},
	{"kIOBlitMemoryRequiresHostFlush", &_kIOBlitMemoryRequiresHostFlush_C},
	{"kIO16BE4444PixelFormat", &_kIO16BE4444PixelFormat_C},
	{"kIO16LE4444PixelFormat", &_kIO16LE4444PixelFormat_C},
	{"kIO2vuyPixelFormat", &_kIO2vuyPixelFormat_C},
	{"kIOYUV211PixelFormat", &_kIOYUV211PixelFormat_C},
	{"kIOUYVY422PixelFormat", &_kIOUYVY422PixelFormat_C},
	{"kIOYVYU422PixelFormat", &_kIOYVYU422PixelFormat_C},
	{"kIOYUV411PixelFormat", &_kIOYUV411PixelFormat_C},
	{"kIOYVU9PixelFormat", &_kIOYVU9PixelFormat_C},
	{"kIOYUVUPixelFormat", &_kIOYUVUPixelFormat_C},
	{"kIOYUVSPixelFormat", &_kIOYUVSPixelFormat_C},
	{"kIO32RGBAPixelFormat", &_kIO32RGBAPixelFormat_C},
	{"kIO32ABGRPixelFormat", &_kIO32ABGRPixelFormat_C},
	{"kIO32BGRAPixelFormat", &_kIO32BGRAPixelFormat_C},
	{"kIO24BGRPixelFormat", &_kIO24BGRPixelFormat_C},
	{"kIO16LE565PixelFormat", &_kIO16LE565PixelFormat_C},
	{"kIO16BE565PixelFormat", &_kIO16BE565PixelFormat_C},
	{"kIO16LE5551PixelFormat", &_kIO16LE5551PixelFormat_C},
	{"kIO16LE555PixelFormat", &_kIO16LE555PixelFormat_C},
	{"kIO8IndexedGrayPixelFormat", &_kIO8IndexedGrayPixelFormat_C},
	{"kIO4IndexedGrayPixelFormat", &_kIO4IndexedGrayPixelFormat_C},
	{"kIO2IndexedGrayPixelFormat", &_kIO2IndexedGrayPixelFormat_C},
	{"kIO1IndexedGrayPixelFormat", &_kIO1IndexedGrayPixelFormat_C},
	{"kIO32ARGBPixelFormat", &_kIO32ARGBPixelFormat_C},
	{"kIO24RGBPixelFormat", &_kIO24RGBPixelFormat_C},
	{"kIO16BE555PixelFormat", &_kIO16BE555PixelFormat_C},
	{"kIO8IndexedPixelFormat", &_kIO8IndexedPixelFormat_C},
	{"kIO4IndexedPixelFormat", &_kIO4IndexedPixelFormat_C},
	{"kIO2IndexedPixelFormat", &_kIO2IndexedPixelFormat_C},
	{"kIO1MonochromePixelFormat", &_kIO1MonochromePixelFormat_C},
	{"kIOBlitDestFramebuffer", &_kIOBlitDestFramebuffer_C},
	{"kIOBlitSourceIsSame", &_kIOBlitSourceIsSame_C},
	{"kIOBlitSourceCGSSurface", &_kIOBlitSourceCGSSurface_C},
	{"kIOBlitSourceSolid", &_kIOBlitSourceSolid_C},
	{"kIOBlitSourceOOLPattern", &_kIOBlitSourceOOLPattern_C},
	{"kIOBlitSourcePattern", &_kIOBlitSourcePattern_C},
	{"kIOBlitSourceOOLMemory", &_kIOBlitSourceOOLMemory_C},
	{"kIOBlitSourceMemory", &_kIOBlitSourceMemory_C},
	{"kIOBlitSourceFramebuffer", &_kIOBlitSourceFramebuffer_C},
	{"kIOBlitSourceDefault", &_kIOBlitSourceDefault_C},
	{"kIOBlitHighlightOperation", &_kIOBlitHighlightOperation_C},
	{"kIOBlitBlendOperation", &_kIOBlitBlendOperation_C},
	{"kIOBlitXorOperation", &_kIOBlitXorOperation_C},
	{"kIOBlitOrOperation", &_kIOBlitOrOperation_C},
	{"kIOBlitCopyOperation", &_kIOBlitCopyOperation_C},
	{"kIOBlitTypeOperationType0", &_kIOBlitTypeOperationType0_C},
	{"kIOBlitTypeOperationTypeMask", &_kIOBlitTypeOperationTypeMask_C},
	{"kIOBlitTypeOperationShift", &_kIOBlitTypeOperationShift_C},
	{"kIOBlitTypeOperationMask", &_kIOBlitTypeOperationMask_C},
	{"kIOBlitTypeDestKeyColorNotEqual", &_kIOBlitTypeDestKeyColorNotEqual_C},
	{"kIOBlitTypeDestKeyColorEqual", &_kIOBlitTypeDestKeyColorEqual_C},
	{"kIOBlitTypeSourceKeyColorNotEqual", &_kIOBlitTypeSourceKeyColorNotEqual_C},
	{"kIOBlitTypeSourceKeyColorEqual", &_kIOBlitTypeSourceKeyColorEqual_C},
	{"kIOBlitTypeDestKeyColorModeMask", &_kIOBlitTypeDestKeyColorModeMask_C},
	{"kIOBlitTypeSourceKeyColorModeMask", &_kIOBlitTypeSourceKeyColorModeMask_C},
	{"kIOBlitTypeScale", &_kIOBlitTypeScale_C},
	{"kIOBlitTypeColorSpaceConvert", &_kIOBlitTypeColorSpaceConvert_C},
	{"kIOBlitTypeMonoExpand", &_kIOBlitTypeMonoExpand_C},
	{"kIOBlitTypeHideCursor", &_kIOBlitTypeHideCursor_C},
	{"kIOBlitTypeShowCursor", &_kIOBlitTypeShowCursor_C},
	{"kIOBlitTypeMoveCursor", &_kIOBlitTypeMoveCursor_C},
	{"kIOBlitTypeCopyRegion", &_kIOBlitTypeCopyRegion_C},
	{"kIOBlitTypeScanlines", &_kIOBlitTypeScanlines_C},
	{"kIOBlitTypeLines", &_kIOBlitTypeLines_C},
	{"kIOBlitTypeCopyRects", &_kIOBlitTypeCopyRects_C},
	{"kIOBlitTypeRects", &_kIOBlitTypeRects_C},
	{"kIOBlitTypeVerbMask", &_kIOBlitTypeVerbMask_C},
	{"kIOAccelSurfaceLockInMask", &_kIOAccelSurfaceLockInMask_C},
	{"kIOAccelSurfaceLockInDontCare", &_kIOAccelSurfaceLockInDontCare_C},
	{"kIOAccelSurfaceLockInAccel", &_kIOAccelSurfaceLockInAccel_C},
	{"kIOAccelSurfaceLockInBacking", &_kIOAccelSurfaceLockInBacking_C},
	{"kIOAccelSurfaceFilterLinear", &_kIOAccelSurfaceFilterLinear_C},
	{"kIOAccelSurfaceFilterNone", &_kIOAccelSurfaceFilterNone_C},
	{"kIOAccelSurfaceFilterDefault", &_kIOAccelSurfaceFilterDefault_C},
	{"kIOAccelSurfaceFiltering", &_kIOAccelSurfaceFiltering_C},
	{"kIOAccelSurfaceFixedSource", &_kIOAccelSurfaceFixedSource_C},
	{"kIOAccelSurfaceBeamSyncSwaps", &_kIOAccelSurfaceBeamSyncSwaps_C},
	{"kIOAccelSurfaceStateIdleBit", &_kIOAccelSurfaceStateIdleBit_C},
	{"kIOAccelSurfaceStateNone", &_kIOAccelSurfaceStateNone_C},
	{"kIOAccelSurfaceShapeBlockingBit", &_kIOAccelSurfaceShapeBlockingBit_C},
	{"kIOAccelSurfaceShapeWaitEnabledBit", &_kIOAccelSurfaceShapeWaitEnabledBit_C},
	{"kIOAccelSurfaceShapeAssemblyBit", &_kIOAccelSurfaceShapeAssemblyBit_C},
	{"kIOAccelSurfaceShapeStaleBackingBit", &_kIOAccelSurfaceShapeStaleBackingBit_C},
	{"kIOAccelSurfaceShapeBeamSyncBit", &_kIOAccelSurfaceShapeBeamSyncBit_C},
	{"kIOAccelSurfaceShapeFrameSyncBit", &_kIOAccelSurfaceShapeFrameSyncBit_C},
	{"kIOAccelSurfaceShapeIdentityScaleBit", &_kIOAccelSurfaceShapeIdentityScaleBit_C},
	{"kIOAccelSurfaceShapeNonSimpleBit", &_kIOAccelSurfaceShapeNonSimpleBit_C},
	{"kIOAccelSurfaceShapeNonBlockingBit", &_kIOAccelSurfaceShapeNonBlockingBit_C},
	{"kIOAccelSurfaceShapeNone", &_kIOAccelSurfaceShapeNone_C},
	{"kIOAccelSurfaceModeBeamSync", &_kIOAccelSurfaceModeBeamSync_C},
	{"kIOAccelSurfaceModeWindowedBit", &_kIOAccelSurfaceModeWindowedBit_C},
	{"kIOAccelSurfaceModeStereoBit", &_kIOAccelSurfaceModeStereoBit_C},
	{"kIOAccelSurfaceModeColorDepthBits", &_kIOAccelSurfaceModeColorDepthBits_C},
	{"kIOAccelSurfaceModeColorDepthBGRA32", &_kIOAccelSurfaceModeColorDepthBGRA32_C},
	{"kIOAccelSurfaceModeColorDepthYUV2", &_kIOAccelSurfaceModeColorDepthYUV2_C},
	{"kIOAccelSurfaceModeColorDepthYUV12", &_kIOAccelSurfaceModeColorDepthYUV12_C},
	{"kIOAccelSurfaceModeColorDepthYUV9", &_kIOAccelSurfaceModeColorDepthYUV9_C},
	{"kIOAccelSurfaceModeColorDepthYUV", &_kIOAccelSurfaceModeColorDepthYUV_C},
	{"kIOAccelSurfaceModeColorDepth8888", &_kIOAccelSurfaceModeColorDepth8888_C},
	{"kIOAccelSurfaceModeColorDepth1555", &_kIOAccelSurfaceModeColorDepth1555_C},
	{"kIOAccelNumSurfaceMethods", &_kIOAccelNumSurfaceMethods_C},
	{"kIOAccelSurfaceSetShapeBackingAndLength", &_kIOAccelSurfaceSetShapeBackingAndLength_C},
	{"kIOAccelSurfaceControl", &_kIOAccelSurfaceControl_C},
	{"kIOAccelSurfaceWriteUnlock", &_kIOAccelSurfaceWriteUnlock_C},
	{"kIOAccelSurfaceWriteLock", &_kIOAccelSurfaceWriteLock_C},
	{"kIOAccelSurfaceReadUnlock", &_kIOAccelSurfaceReadUnlock_C},
	{"kIOAccelSurfaceReadLock", &_kIOAccelSurfaceReadLock_C},
	{"kIOAccelSurfaceQueryLock", &_kIOAccelSurfaceQueryLock_C},
	{"kIOAccelSurfaceFlush", &_kIOAccelSurfaceFlush_C},
	{"kIOAccelSurfaceSetShape", &_kIOAccelSurfaceSetShape_C},
	{"kIOAccelSurfaceSetScale", &_kIOAccelSurfaceSetScale_C},
	{"kIOAccelSurfaceSetIDMode", &_kIOAccelSurfaceSetIDMode_C},
	{"kIOAccelSurfaceSetShapeBacking", &_kIOAccelSurfaceSetShapeBacking_C},
	{"kIOAccelSurfaceRead", &_kIOAccelSurfaceRead_C},
	{"kIOAccelSurfaceWriteUnlockOptions", &_kIOAccelSurfaceWriteUnlockOptions_C},
	{"kIOAccelSurfaceWriteLockOptions", &_kIOAccelSurfaceWriteLockOptions_C},
	{"kIOAccelSurfaceGetState", &_kIOAccelSurfaceGetState_C},
	{"kIOAccelSurfaceReadUnlockOptions", &_kIOAccelSurfaceReadUnlockOptions_C},
	{"kIOAccelSurfaceReadLockOptions", &_kIOAccelSurfaceReadLockOptions_C},
	{"kIOAccelNumSurfaceMemoryTypes", &_kIOAccelNumSurfaceMemoryTypes_C},
	{"kIOAccelNumClientTypes", &_kIOAccelNumClientTypes_C},
	{"kIOAccelSurfaceClientType", &_kIOAccelSurfaceClientType_C},
	{"kIOAccelPrivateID", &_kIOAccelPrivateID_C},
	{"kIOAccelKeycolorSurface", &_kIOAccelKeycolorSurface_C},
	{"kIOAccelVolatileSurface", &_kIOAccelVolatileSurface_C},
	{"kIOFBVRAMMemory", &_kIOFBVRAMMemory_C},
	{"kIOFBCursorMemory", &_kIOFBCursorMemory_C},
	{"kIOFBShmemCursorNumFramesShift", &_kIOFBShmemCursorNumFramesShift_C},
	{"kIOFBShmemCursorNumFramesMask", &_kIOFBShmemCursorNumFramesMask_C},
	{"kIOFBCurrentShmemVersion", &_kIOFBCurrentShmemVersion_C},
	{"kIOFBTenPtTwoShmemVersion", &_kIOFBTenPtTwoShmemVersion_C},
	{"kIOFBTenPtOneShmemVersion", &_kIOFBTenPtOneShmemVersion_C},
	{"kIOFBShmemVersionMask", &_kIOFBShmemVersionMask_C},
	{"kIOFBHardwareCursorInVRAM", &_kIOFBHardwareCursorInVRAM_C},
	{"kIOFBHardwareCursorActive", &_kIOFBHardwareCursorActive_C},
	{"kIOFBCursorHWCapable", &_kIOFBCursorHWCapable_C},
	{"kIOFBCursorImageNew", &_kIOFBCursorImageNew_C},
	{"kIOFBMaxCursorDepth", &_kIOFBMaxCursorDepth_C},
	{"kIOFBNumCursorFramesShift", &_kIOFBNumCursorFramesShift_C},
	{"kIOFBNumCursorFrames", &_kIOFBNumCursorFrames_C},
	{"EVLEVEL", &_EVLEVEL_C},
	{"EVMOVE", &_EVMOVE_C},
	{"EVSHOW", &_EVSHOW_C},
	{"EVHIDE", &_EVHIDE_C},
	{"EVNOP", &_EVNOP_C},
	{"NX_RightButton", &_NX_RightButton_C},
	{"NX_LeftButton", &_NX_LeftButton_C},
	{"NX_OneButton", &_NX_OneButton_C},
	{"kDisplaySubPixelShapeElliptical", &_kDisplaySubPixelShapeElliptical_C},
	{"kDisplaySubPixelShapeOval", &_kDisplaySubPixelShapeOval_C},
	{"kDisplaySubPixelShapeRectangular", &_kDisplaySubPixelShapeRectangular_C},
	{"kDisplaySubPixelShapeSquare", &_kDisplaySubPixelShapeSquare_C},
	{"kDisplaySubPixelShapeRound", &_kDisplaySubPixelShapeRound_C},
	{"kDisplaySubPixelShapeUndefined", &_kDisplaySubPixelShapeUndefined_C},
	{"kDisplaySubPixelConfigurationQuad", &_kDisplaySubPixelConfigurationQuad_C},
	{"kDisplaySubPixelConfigurationStripeOffset", &_kDisplaySubPixelConfigurationStripeOffset_C},
	{"kDisplaySubPixelConfigurationStripe", &_kDisplaySubPixelConfigurationStripe_C},
	{"kDisplaySubPixelConfigurationDelta", &_kDisplaySubPixelConfigurationDelta_C},
	{"kDisplaySubPixelConfigurationUndefined", &_kDisplaySubPixelConfigurationUndefined_C},
	{"kDisplaySubPixelLayoutQuadGBR", &_kDisplaySubPixelLayoutQuadGBR_C},
	{"kDisplaySubPixelLayoutQuadGBL", &_kDisplaySubPixelLayoutQuadGBL_C},
	{"kDisplaySubPixelLayoutBGR", &_kDisplaySubPixelLayoutBGR_C},
	{"kDisplaySubPixelLayoutRGB", &_kDisplaySubPixelLayoutRGB_C},
	{"kDisplaySubPixelLayoutUndefined", &_kDisplaySubPixelLayoutUndefined_C},
	{"kDisplayProductIDGeneric", &_kDisplayProductIDGeneric_C},
	{"kDisplayVendorIDUnknown", &_kDisplayVendorIDUnknown_C},
	{"kIOTimingIDVESA_1360x768_60hz", &_kIOTimingIDVESA_1360x768_60hz_C},
	{"kIOTimingIDVESA_848x480_60hz", &_kIOTimingIDVESA_848x480_60hz_C},
	{"kIOTimingIDApple_0x0_0hz_Offline", &_kIOTimingIDApple_0x0_0hz_Offline_C},
	{"kIOTimingIDSony_1920x1200_76hz", &_kIOTimingIDSony_1920x1200_76hz_C},
	{"kIOTimingIDSony_1920x1080_72hz", &_kIOTimingIDSony_1920x1080_72hz_C},
	{"kIOTimingIDSony_1920x1080_60hz", &_kIOTimingIDSony_1920x1080_60hz_C},
	{"kIOTimingIDSony_1600x1024_76hz", &_kIOTimingIDSony_1600x1024_76hz_C},
	{"kIOTimingIDFilmRate_48hz", &_kIOTimingIDFilmRate_48hz_C},
	{"kIOTimingIDSMPTE240M_60hz", &_kIOTimingIDSMPTE240M_60hz_C},
	{"kIOTimingIDVESA_1920x1440_75hz", &_kIOTimingIDVESA_1920x1440_75hz_C},
	{"kIOTimingIDVESA_1920x1440_60hz", &_kIOTimingIDVESA_1920x1440_60hz_C},
	{"kIOTimingIDVESA_1856x1392_75hz", &_kIOTimingIDVESA_1856x1392_75hz_C},
	{"kIOTimingIDVESA_1856x1392_60hz", &_kIOTimingIDVESA_1856x1392_60hz_C},
	{"kIOTimingIDVESA_1792x1344_75hz", &_kIOTimingIDVESA_1792x1344_75hz_C},
	{"kIOTimingIDVESA_1792x1344_60hz", &_kIOTimingIDVESA_1792x1344_60hz_C},
	{"kIOTimingIDVESA_1600x1200_85hz", &_kIOTimingIDVESA_1600x1200_85hz_C},
	{"kIOTimingIDVESA_1600x1200_80hz", &_kIOTimingIDVESA_1600x1200_80hz_C},
	{"kIOTimingIDVESA_1600x1200_75hz", &_kIOTimingIDVESA_1600x1200_75hz_C},
	{"kIOTimingIDVESA_1600x1200_70hz", &_kIOTimingIDVESA_1600x1200_70hz_C},
	{"kIOTimingIDVESA_1600x1200_65hz", &_kIOTimingIDVESA_1600x1200_65hz_C},
	{"kIOTimingIDVESA_1600x1200_60hz", &_kIOTimingIDVESA_1600x1200_60hz_C},
	{"kIOTimingIDVESA_1280x1024_85hz", &_kIOTimingIDVESA_1280x1024_85hz_C},
	{"kIOTimingIDVESA_1280x1024_75hz", &_kIOTimingIDVESA_1280x1024_75hz_C},
	{"kIOTimingIDVESA_1280x1024_60hz", &_kIOTimingIDVESA_1280x1024_60hz_C},
	{"kIOTimingIDVESA_1280x960_85hz", &_kIOTimingIDVESA_1280x960_85hz_C},
	{"kIOTimingIDVESA_1280x960_60hz", &_kIOTimingIDVESA_1280x960_60hz_C},
	{"kIOTimingIDVESA_1280x960_75hz", &_kIOTimingIDVESA_1280x960_75hz_C},
	{"kIOTimingIDApplePAL_FFconv", &_kIOTimingIDApplePAL_FFconv_C},
	{"kIOTimingIDApplePAL_STconv", &_kIOTimingIDApplePAL_STconv_C},
	{"kIOTimingIDApplePAL_FF", &_kIOTimingIDApplePAL_FF_C},
	{"kIOTimingIDApplePAL_ST", &_kIOTimingIDApplePAL_ST_C},
	{"kIOTimingIDAppleNTSC_FFconv", &_kIOTimingIDAppleNTSC_FFconv_C},
	{"kIOTimingIDAppleNTSC_STconv", &_kIOTimingIDAppleNTSC_STconv_C},
	{"kIOTimingIDAppleNTSC_FF", &_kIOTimingIDAppleNTSC_FF_C},
	{"kIOTimingIDAppleNTSC_ST", &_kIOTimingIDAppleNTSC_ST_C},
	{"kIOTimingIDApple_1152x870_75hz", &_kIOTimingIDApple_1152x870_75hz_C},
	{"kIOTimingIDApple_1024x768_75hz", &_kIOTimingIDApple_1024x768_75hz_C},
	{"kIOTimingIDVESA_1024x768_85hz", &_kIOTimingIDVESA_1024x768_85hz_C},
	{"kIOTimingIDVESA_1024x768_75hz", &_kIOTimingIDVESA_1024x768_75hz_C},
	{"kIOTimingIDVESA_1024x768_70hz", &_kIOTimingIDVESA_1024x768_70hz_C},
	{"kIOTimingIDVESA_1024x768_60hz", &_kIOTimingIDVESA_1024x768_60hz_C},
	{"kIOTimingIDVESA_800x600_85hz", &_kIOTimingIDVESA_800x600_85hz_C},
	{"kIOTimingIDVESA_800x600_75hz", &_kIOTimingIDVESA_800x600_75hz_C},
	{"kIOTimingIDVESA_800x600_72hz", &_kIOTimingIDVESA_800x600_72hz_C},
	{"kIOTimingIDVESA_800x600_60hz", &_kIOTimingIDVESA_800x600_60hz_C},
	{"kIOTimingIDVESA_800x600_56hz", &_kIOTimingIDVESA_800x600_56hz_C},
	{"kIOTimingIDApple_832x624_75hz", &_kIOTimingIDApple_832x624_75hz_C},
	{"kIOTimingIDApple_640x818_75hz", &_kIOTimingIDApple_640x818_75hz_C},
	{"kIOTimingIDApple_640x870_75hz", &_kIOTimingIDApple_640x870_75hz_C},
	{"kIOTimingIDGTF_640x480_120hz", &_kIOTimingIDGTF_640x480_120hz_C},
	{"kIOTimingIDVESA_640x480_85hz", &_kIOTimingIDVESA_640x480_85hz_C},
	{"kIOTimingIDVESA_640x480_75hz", &_kIOTimingIDVESA_640x480_75hz_C},
	{"kIOTimingIDVESA_640x480_72hz", &_kIOTimingIDVESA_640x480_72hz_C},
	{"kIOTimingIDVESA_640x480_60hz", &_kIOTimingIDVESA_640x480_60hz_C},
	{"kIOTimingIDApple_640x400_67hz", &_kIOTimingIDApple_640x400_67hz_C},
	{"kIOTimingIDApple_640x480_67hz", &_kIOTimingIDApple_640x480_67hz_C},
	{"kIOTimingIDApple_560x384_60hz", &_kIOTimingIDApple_560x384_60hz_C},
	{"kIOTimingIDApple_512x384_60hz", &_kIOTimingIDApple_512x384_60hz_C},
	{"kIOTimingIDApple_FixedRateLCD", &_kIOTimingIDApple_FixedRateLCD_C},
	{"kIOTimingIDInvalid", &_kIOTimingIDInvalid_C},
	{"kIOFBOnlineInterruptType", &_kIOFBOnlineInterruptType_C},
	{"kIOFBOfflineInterruptType", &_kIOFBOfflineInterruptType_C},
	{"kIOFBChangedInterruptType", &_kIOFBChangedInterruptType_C},
	{"kIOFBConnectInterruptType", &_kIOFBConnectInterruptType_C},
	{"kIOFBFrameInterruptType", &_kIOFBFrameInterruptType_C},
	{"kIOFBHBLInterruptType", &_kIOFBHBLInterruptType_C},
	{"kIOFBVBLInterruptType", &_kIOFBVBLInterruptType_C},
	{"kHardwareCursorDescriptorMinorVersion", &_kHardwareCursorDescriptorMinorVersion_C},
	{"kHardwareCursorDescriptorMajorVersion", &_kHardwareCursorDescriptorMajorVersion_C},
	{"kInvertingEncodedPixel", &_kInvertingEncodedPixel_C},
	{"kInvertingEncodingShift", &_kInvertingEncodingShift_C},
	{"kTransparentEncodedPixel", &_kTransparentEncodedPixel_C},
	{"kTransparentEncodingShift", &_kTransparentEncodingShift_C},
	{"kInvertingEncoding", &_kInvertingEncoding_C},
	{"kTransparentEncoding", &_kTransparentEncoding_C},
	{"kIOFBUserRequestProbe", &_kIOFBUserRequestProbe_C},
	{"kIOFBSharedConnectType", &_kIOFBSharedConnectType_C},
	{"kIOFBServerConnectType", &_kIOFBServerConnectType_C},
	{"kIOSyncOnRed", &_kIOSyncOnRed_C},
	{"kIOSyncOnGreen", &_kIOSyncOnGreen_C},
	{"kIOSyncOnBlue", &_kIOSyncOnBlue_C},
	{"kIOTriStateSyncs", &_kIOTriStateSyncs_C},
	{"kIONoSeparateSyncControl", &_kIONoSeparateSyncControl_C},
	{"kIOCSyncDisable", &_kIOCSyncDisable_C},
	{"kIOVSyncDisable", &_kIOVSyncDisable_C},
	{"kIOHSyncDisable", &_kIOHSyncDisable_C},
	{"kIOConnectionStereoSync", &_kIOConnectionStereoSync_C},
	{"kIOConnectionBuiltIn", &_kIOConnectionBuiltIn_C},
	{"kConnectionVideoBest", &_kConnectionVideoBest_C},
	{"kConnectionOverscan", &_kConnectionOverscan_C},
	{"kConnectionDisplayParameters", &_kConnectionDisplayParameters_C},
	{"kConnectionDisplayParameterCount", &_kConnectionDisplayParameterCount_C},
	{"kConnectionPostWake", &_kConnectionPostWake_C},
	{"kConnectionPower", &_kConnectionPower_C},
	{"kConnectionChanged", &_kConnectionChanged_C},
	{"kConnectionEnable", &_kConnectionEnable_C},
	{"kConnectionSupportsHLDDCSense", &_kConnectionSupportsHLDDCSense_C},
	{"kConnectionSupportsLLDDCSense", &_kConnectionSupportsLLDDCSense_C},
	{"kConnectionSupportsAppleSense", &_kConnectionSupportsAppleSense_C},
	{"kConnectionSyncFlags", &_kConnectionSyncFlags_C},
	{"kConnectionSyncEnable", &_kConnectionSyncEnable_C},
	{"kConnectionFlags", &_kConnectionFlags_C},
	{"kAndConnections", &_kAndConnections_C},
	{"kOrConnections", &_kOrConnections_C},
	{"kIOScaleCanBorderInsetOnly", &_kIOScaleCanBorderInsetOnly_C},
	{"kIOScaleCanRotate", &_kIOScaleCanRotate_C},
	{"kIOScaleCanSupportInset", &_kIOScaleCanSupportInset_C},
	{"kIOScaleCanScaleInterlaced", &_kIOScaleCanScaleInterlaced_C},
	{"kIOScaleCanDownSamplePixels", &_kIOScaleCanDownSamplePixels_C},
	{"kIOScaleCanUpSamplePixels", &_kIOScaleCanUpSamplePixels_C},
	{"kIOScaleStretchOnly", &_kIOScaleStretchOnly_C},
	{"kIOSyncPositivePolarity", &_kIOSyncPositivePolarity_C},
	{"kIOAnalogSignalLevel_0700_0000", &_kIOAnalogSignalLevel_0700_0000_C},
	{"kIOAnalogSignalLevel_1000_0400", &_kIOAnalogSignalLevel_1000_0400_C},
	{"kIOAnalogSignalLevel_0714_0286", &_kIOAnalogSignalLevel_0714_0286_C},
	{"kIOAnalogSignalLevel_0700_0300", &_kIOAnalogSignalLevel_0700_0300_C},
	{"kIOPALTiming", &_kIOPALTiming_C},
	{"kIONTSCTiming", &_kIONTSCTiming_C},
	{"kIOInterlacedCEATiming", &_kIOInterlacedCEATiming_C},
	{"kIOAnalogSetupExpected", &_kIOAnalogSetupExpected_C},
	{"kIODigitalSignal", &_kIODigitalSignal_C},
	{"kIORangeSupportsInterlacedCEATimingWithConfirm", &_kIORangeSupportsInterlacedCEATimingWithConfirm_C},
	{"kIORangeSupportsInterlacedCEATiming", &_kIORangeSupportsInterlacedCEATiming_C},
	{"kIORangeSupportsVSyncSerration", &_kIORangeSupportsVSyncSerration_C},
	{"kIORangeSupportsCompositeSync", &_kIORangeSupportsCompositeSync_C},
	{"kIORangeSupportsSyncOnGreen", &_kIORangeSupportsSyncOnGreen_C},
	{"kIORangeSupportsSeparateSyncs", &_kIORangeSupportsSeparateSyncs_C},
	{"kIORangeSupportsSignal_0700_0000", &_kIORangeSupportsSignal_0700_0000_C},
	{"kIORangeSupportsSignal_1000_0400", &_kIORangeSupportsSignal_1000_0400_C},
	{"kIORangeSupportsSignal_0714_0286", &_kIORangeSupportsSignal_0714_0286_C},
	{"kIORangeSupportsSignal_0700_0300", &_kIORangeSupportsSignal_0700_0300_C},
	{"kIOScaleRotate270", &_kIOScaleRotate270_C},
	{"kIOScaleRotate180", &_kIOScaleRotate180_C},
	{"kIOScaleRotate90", &_kIOScaleRotate90_C},
	{"kIOScaleRotate0", &_kIOScaleRotate0_C},
	{"kIOScaleInvertY", &_kIOScaleInvertY_C},
	{"kIOScaleInvertX", &_kIOScaleInvertX_C},
	{"kIOScaleSwapAxes", &_kIOScaleSwapAxes_C},
	{"kIOScaleRotateFlags", &_kIOScaleRotateFlags_C},
	{"kIOScaleStretchToFit", &_kIOScaleStretchToFit_C},
	{"kIOScalingInfoValid", &_kIOScalingInfoValid_C},
	{"kIODetailedTimingValid", &_kIODetailedTimingValid_C},
	{"kIOMirrorForced", &_kIOMirrorForced_C},
	{"kIOMirrorDefault", &_kIOMirrorDefault_C},
	{"kIOMirrorHWClipped", &_kIOMirrorHWClipped_C},
	{"kIOMirrorIsPrimary", &_kIOMirrorIsPrimary_C},
	{"kIOClamshellStateAttribute", &_kIOClamshellStateAttribute_C},
	{"kIODeferCLUTSetAttribute", &_kIODeferCLUTSetAttribute_C},
	{"kIOVRAMSaveAttribute", &_kIOVRAMSaveAttribute_C},
	{"kIOSystemPowerAttribute", &_kIOSystemPowerAttribute_C},
	{"kIOCursorControlAttribute", &_kIOCursorControlAttribute_C},
	{"kIOCapturedAttribute", &_kIOCapturedAttribute_C},
	{"kIOMirrorDefaultAttribute", &_kIOMirrorDefaultAttribute_C},
	{"kIOMirrorAttribute", &_kIOMirrorAttribute_C},
	{"kIOHardwareCursorAttribute", &_kIOHardwareCursorAttribute_C},
	{"kIOPowerAttribute", &_kIOPowerAttribute_C},
	{"kSetCLUTWithLuminance", &_kSetCLUTWithLuminance_C},
	{"kSetCLUTImmediately", &_kSetCLUTImmediately_C},
	{"kSetCLUTByValue", &_kSetCLUTByValue_C},
	{"kIOFBSystemAperture", &_kIOFBSystemAperture_C},
	{"kFramebufferDisableAltivecAccess", &_kFramebufferDisableAltivecAccess_C},
	{"kFramebufferSupportsGammaCorrection", &_kFramebufferSupportsGammaCorrection_C},
	{"kFramebufferSupportsWritethruCache", &_kFramebufferSupportsWritethruCache_C},
	{"kFramebufferSupportsCopybackCache", &_kFramebufferSupportsCopybackCache_C},
	{"kDisplayModeDefaultFlag", &_kDisplayModeDefaultFlag_C},
	{"kDisplayModeSafeFlag", &_kDisplayModeSafeFlag_C},
	{"kDisplayModeValidFlag", &_kDisplayModeValidFlag_C},
	{"kDisplayModeTelevisionFlag", &_kDisplayModeTelevisionFlag_C},
	{"kDisplayModeNotGraphicsQualityFlag", &_kDisplayModeNotGraphicsQualityFlag_C},
	{"kDisplayModeStretchedFlag", &_kDisplayModeStretchedFlag_C},
	{"kDisplayModeNotPresetFlag", &_kDisplayModeNotPresetFlag_C},
	{"kDisplayModeBuiltInFlag", &_kDisplayModeBuiltInFlag_C},
	{"kDisplayModeSimulscanFlag", &_kDisplayModeSimulscanFlag_C},
	{"kDisplayModeInterlacedFlag", &_kDisplayModeInterlacedFlag_C},
	{"kDisplayModeRequiresPanFlag", &_kDisplayModeRequiresPanFlag_C},
	{"kDisplayModeNotResizeFlag", &_kDisplayModeNotResizeFlag_C},
	{"kDisplayModeNeverShowFlag", &_kDisplayModeNeverShowFlag_C},
	{"kDisplayModeAlwaysShowFlag", &_kDisplayModeAlwaysShowFlag_C},
	{"kDisplayModeSafetyFlags", &_kDisplayModeSafetyFlags_C},
	{"kIOMonoInverseDirectPixels", &_kIOMonoInverseDirectPixels_C},
	{"kIOMonoDirectPixels", &_kIOMonoDirectPixels_C},
	{"kIORGBDirectPixels", &_kIORGBDirectPixels_C},
	{"kIOFixedCLUTPixels", &_kIOFixedCLUTPixels_C},
	{"kIOCLUTPixels", &_kIOCLUTPixels_C},
	{"kIOMaxPixelBits", &_kIOMaxPixelBits_C},
	{"kIODisplayModeIDReservedBase", &_kIODisplayModeIDReservedBase_C},
	{"kIODisplayModeIDBootProgrammable", &_kIODisplayModeIDBootProgrammable_C},
	{"kUSBAddExtraResetTimeMask", &_kUSBAddExtraResetTimeMask_C},
	{"kUSBAddExtraResetTimeBit", &_kUSBAddExtraResetTimeBit_C},
	{"kUSBGangOverCurrentNotificationType", &_kUSBGangOverCurrentNotificationType_C},
	{"kUSBIndividualOverCurrentNotificationType", &_kUSBIndividualOverCurrentNotificationType_C},
	{"kUSBNotEnoughPowerNotificationType", &_kUSBNotEnoughPowerNotificationType_C},
	{"kUSBNoUserNotificationType", &_kUSBNoUserNotificationType_C},
	{"kUSBLowLatencyFrameListBuffer", &_kUSBLowLatencyFrameListBuffer_C},
	{"kUSBLowLatencyReadBuffer", &_kUSBLowLatencyReadBuffer_C},
	{"kUSBLowLatencyWriteBuffer", &_kUSBLowLatencyWriteBuffer_C},
	{"kUSBLowLatencyIsochTransferKey", &_kUSBLowLatencyIsochTransferKey_C},
	{"kUSBHighSpeedMicrosecondsInFrame", &_kUSBHighSpeedMicrosecondsInFrame_C},
	{"kUSBFullSpeedMicrosecondsInFrame", &_kUSBFullSpeedMicrosecondsInFrame_C},
	{"kUSBDeviceSpeedHigh", &_kUSBDeviceSpeedHigh_C},
	{"kUSBDeviceSpeedFull", &_kUSBDeviceSpeedFull_C},
	{"kUSBDeviceSpeedLow", &_kUSBDeviceSpeedLow_C},
	{"kIOUSBVendorIDAppleComputer", &_kIOUSBVendorIDAppleComputer_C},
	{"kIOUSBFindInterfaceDontCare", &_kIOUSBFindInterfaceDontCare_C},
	{"kUSBDefaultControlCompletionTimeoutMS", &_kUSBDefaultControlCompletionTimeoutMS_C},
	{"kUSBDefaultControlNoDataTimeoutMS", &_kUSBDefaultControlNoDataTimeoutMS_C},
	{"kIOUSBAnyProduct", &_kIOUSBAnyProduct_C},
	{"kIOUSBAnyVendor", &_kIOUSBAnyVendor_C},
	{"kIOUSBAnyProtocol", &_kIOUSBAnyProtocol_C},
	{"kIOUSBAnySubClass", &_kIOUSBAnySubClass_C},
	{"kIOUSBAnyClass", &_kIOUSBAnyClass_C},
	{"addPacketShift", &_addPacketShift_C},
	{"kSyncFrame", &_kSyncFrame_C},
	{"kSetInterface", &_kSetInterface_C},
	{"kSetEndpointFeature", &_kSetEndpointFeature_C},
	{"kSetInterfaceFeature", &_kSetInterfaceFeature_C},
	{"kSetDeviceFeature", &_kSetDeviceFeature_C},
	{"kSetDescriptor", &_kSetDescriptor_C},
	{"kSetConfiguration", &_kSetConfiguration_C},
	{"kSetAddress", &_kSetAddress_C},
	{"kGetEndpointStatus", &_kGetEndpointStatus_C},
	{"kGetInterfaceStatus", &_kGetInterfaceStatus_C},
	{"kGetDeviceStatus", &_kGetDeviceStatus_C},
	{"kGetInterface", &_kGetInterface_C},
	{"kGetDescriptor", &_kGetDescriptor_C},
	{"kGetConfiguration", &_kGetConfiguration_C},
	{"kClearEndpointFeature", &_kClearEndpointFeature_C},
	{"kClearInterfaceFeature", &_kClearInterfaceFeature_C},
	{"kClearDeviceFeature", &_kClearDeviceFeature_C},
	{"kUSBMaxHSIsocFrameCount", &_kUSBMaxHSIsocFrameCount_C},
	{"kUSBMaxHSIsocEndpointReqCount", &_kUSBMaxHSIsocEndpointReqCount_C},
	{"kUSBMaxFSIsocEndpointReqCount", &_kUSBMaxFSIsocEndpointReqCount_C},
	{"kUSBRqRecipientMask", &_kUSBRqRecipientMask_C},
	{"kUSBRqTypeMask", &_kUSBRqTypeMask_C},
	{"kUSBRqTypeShift", &_kUSBRqTypeShift_C},
	{"kUSBRqDirnMask", &_kUSBRqDirnMask_C},
	{"kUSBRqDirnShift", &_kUSBRqDirnShift_C},
	{"kUSBNoPipeIdx", &_kUSBNoPipeIdx_C},
	{"kUSBDeviceMask", &_kUSBDeviceMask_C},
	{"kUSBEndPtShift", &_kUSBEndPtShift_C},
	{"kUSBInterfaceIDMask", &_kUSBInterfaceIDMask_C},
	{"kUSBMaxInterfaces", &_kUSBMaxInterfaces_C},
	{"kUSBInterfaceIDShift", &_kUSBInterfaceIDShift_C},
	{"kUSBMaxPipes", &_kUSBMaxPipes_C},
	{"kUSBPipeIDMask", &_kUSBPipeIDMask_C},
	{"kUSBDeviceIDMask", &_kUSBDeviceIDMask_C},
	{"kUSBMaxDevice", &_kUSBMaxDevice_C},
	{"kUSBMaxDevices", &_kUSBMaxDevices_C},
	{"kUSBDeviceIDShift", &_kUSBDeviceIDShift_C},
	{"kUSBEndpointbmAttributesUsageTypeShift", &_kUSBEndpointbmAttributesUsageTypeShift_C},
	{"kUSBEndpointbmAttributesUsageTypeMask", &_kUSBEndpointbmAttributesUsageTypeMask_C},
	{"kUSBEndpointbmAttributesSynchronizationTypeShift", &_kUSBEndpointbmAttributesSynchronizationTypeShift_C},
	{"kUSBEndpointbmAttributesSynchronizationTypeMask", &_kUSBEndpointbmAttributesSynchronizationTypeMask_C},
	{"kUSBEndpointbmAttributesTransferTypeMask", &_kUSBEndpointbmAttributesTransferTypeMask_C},
	{"kUSBEndpointDirectionIn", &_kUSBEndpointDirectionIn_C},
	{"kUSBEndpointDirectionOut", &_kUSBEndpointDirectionOut_C},
	{"kUSBbEndpointDirectionMask", &_kUSBbEndpointDirectionMask_C},
	{"kUSBbEndpointDirectionBit", &_kUSBbEndpointDirectionBit_C},
	{"kUSBbEndpointAddressMask", &_kUSBbEndpointAddressMask_C},
	{"kUSBDFUManifestationTolerantBit", &_kUSBDFUManifestationTolerantBit_C},
	{"kUSBDFUCanUploadBit", &_kUSBDFUCanUploadBit_C},
	{"kUSBDFUCanDownloadBit", &_kUSBDFUCanDownloadBit_C},
	{"kUSBDFUAttributesMask", &_kUSBDFUAttributesMask_C},
	{"KUSBInterfaceAssociationDescriptorProtocol", &_KUSBInterfaceAssociationDescriptorProtocol_C},
	{"kUSBBluetoothProgrammingInterfaceProtocol", &_kUSBBluetoothProgrammingInterfaceProtocol_C},
	{"kUSB2ComplianceDeviceProtocol", &_kUSB2ComplianceDeviceProtocol_C},
	{"kUSBVendorSpecificProtocol", &_kUSBVendorSpecificProtocol_C},
	{"kHIDMouseInterfaceProtocol", &_kHIDMouseInterfaceProtocol_C},
	{"kHIDKeyboardInterfaceProtocol", &_kHIDKeyboardInterfaceProtocol_C},
	{"kHIDNoInterfaceProtocol", &_kHIDNoInterfaceProtocol_C},
	{"kUSBVideoInterfaceCollectionSubClass", &_kUSBVideoInterfaceCollectionSubClass_C},
	{"kUSBVideoStreamingSubClass", &_kUSBVideoStreamingSubClass_C},
	{"kUSBVideoControlSubClass", &_kUSBVideoControlSubClass_C},
	{"kUSBCommonClassSubClass", &_kUSBCommonClassSubClass_C},
	{"kUSBRFControllerSubClass", &_kUSBRFControllerSubClass_C},
	{"kUSBReprogrammableDiagnosticSubClass", &_kUSBReprogrammableDiagnosticSubClass_C},
	{"kUSBATMNetworkingSubClass", &_kUSBATMNetworkingSubClass_C},
	{"kUSBCommEthernetNetworkingSubClass", &_kUSBCommEthernetNetworkingSubClass_C},
	{"kUSBCommCAPISubClass", &_kUSBCommCAPISubClass_C},
	{"kUSBCommMultiChannelSubClass", &_kUSBCommMultiChannelSubClass_C},
	{"kUSBCommTelephoneSubClass", &_kUSBCommTelephoneSubClass_C},
	{"kUSBCommAbstractSubClass", &_kUSBCommAbstractSubClass_C},
	{"kUSBCommDirectLineSubClass", &_kUSBCommDirectLineSubClass_C},
	{"kUSBHIDBootInterfaceSubClass", &_kUSBHIDBootInterfaceSubClass_C},
	{"kUSBMassStorageSCSISubClass", &_kUSBMassStorageSCSISubClass_C},
	{"kUSBMassStorageSFF8070iSubClass", &_kUSBMassStorageSFF8070iSubClass_C},
	{"kUSBMassStorageUFISubClass", &_kUSBMassStorageUFISubClass_C},
	{"kUSBMassStorageQIC157SubClass", &_kUSBMassStorageQIC157SubClass_C},
	{"kUSBMassStorageATAPISubClass", &_kUSBMassStorageATAPISubClass_C},
	{"kUSBMassStorageRBCSubClass", &_kUSBMassStorageRBCSubClass_C},
	{"kUSBTestMeasurementSubClass", &_kUSBTestMeasurementSubClass_C},
	{"kUSBIrDABridgeSubClass", &_kUSBIrDABridgeSubClass_C},
	{"kUSBDFUSubClass", &_kUSBDFUSubClass_C},
	{"kUSBMIDIStreamingSubClass", &_kUSBMIDIStreamingSubClass_C},
	{"kUSBAudioStreamingSubClass", &_kUSBAudioStreamingSubClass_C},
	{"kUSBAudioControlSubClass", &_kUSBAudioControlSubClass_C},
	{"kUSBHubSubClass", &_kUSBHubSubClass_C},
	{"kUSBCompositeSubClass", &_kUSBCompositeSubClass_C},
	{"kUSBDisplayClass", &_kUSBDisplayClass_C},
	{"kUSBVendorSpecificInterfaceClass", &_kUSBVendorSpecificInterfaceClass_C},
	{"kUSBApplicationSpecificInterfaceClass", &_kUSBApplicationSpecificInterfaceClass_C},
	{"kUSBWirelessControllerInterfaceClass", &_kUSBWirelessControllerInterfaceClass_C},
	{"kUSBDiagnosticDeviceInterfaceClass", &_kUSBDiagnosticDeviceInterfaceClass_C},
	{"kUSBVideoInterfaceClass", &_kUSBVideoInterfaceClass_C},
	{"kUSBContentSecurityInterfaceClass", &_kUSBContentSecurityInterfaceClass_C},
	{"kUSBChipSmartCardInterfaceClass", &_kUSBChipSmartCardInterfaceClass_C},
	{"kUSBMassStorageInterfaceClass", &_kUSBMassStorageInterfaceClass_C},
	{"kUSBMassStorageClass", &_kUSBMassStorageClass_C},
	{"kUSBPrintingInterfaceClass", &_kUSBPrintingInterfaceClass_C},
	{"kUSBPrintingClass", &_kUSBPrintingClass_C},
	{"kUSBImageInterfaceClass", &_kUSBImageInterfaceClass_C},
	{"kUSBPhysicalInterfaceClass", &_kUSBPhysicalInterfaceClass_C},
	{"kUSBHIDInterfaceClass", &_kUSBHIDInterfaceClass_C},
	{"kUSBHIDClass", &_kUSBHIDClass_C},
	{"kUSBCommunicationDataInterfaceClass", &_kUSBCommunicationDataInterfaceClass_C},
	{"kUSBCommunicationControlInterfaceClass", &_kUSBCommunicationControlInterfaceClass_C},
	{"kUSBAudioInterfaceClass", &_kUSBAudioInterfaceClass_C},
	{"kUSBAudioClass", &_kUSBAudioClass_C},
	{"kUSBVendorSpecificClass", &_kUSBVendorSpecificClass_C},
	{"kUSBApplicationSpecificClass", &_kUSBApplicationSpecificClass_C},
	{"kUSBMiscellaneousClass", &_kUSBMiscellaneousClass_C},
	{"kUSBWirelessControllerClass", &_kUSBWirelessControllerClass_C},
	{"kUSBDiagnosticClass", &_kUSBDiagnosticClass_C},
	{"kUSBDataClass", &_kUSBDataClass_C},
	{"kUSBHubClass", &_kUSBHubClass_C},
	{"kUSBCommunicationClass", &_kUSBCommunicationClass_C},
	{"kUSBCommClass", &_kUSBCommClass_C},
	{"kUSBCompositeClass", &_kUSBCompositeClass_C},
	{"kUSBScrollLockKey", &_kUSBScrollLockKey_C},
	{"kUSBNumLockKey", &_kUSBNumLockKey_C},
	{"kUSBCapsLockKey", &_kUSBCapsLockKey_C},
	{"kHIDReportProtocolValue", &_kHIDReportProtocolValue_C},
	{"kHIDBootProtocolValue", &_kHIDBootProtocolValue_C},
	{"kHIDRtFeatureReport", &_kHIDRtFeatureReport_C},
	{"kHIDRtOutputReport", &_kHIDRtOutputReport_C},
	{"kHIDRtInputReport", &_kHIDRtInputReport_C},
	{"kHIDRqSetProtocol", &_kHIDRqSetProtocol_C},
	{"kHIDRqSetIdle", &_kHIDRqSetIdle_C},
	{"kHIDRqSetReport", &_kHIDRqSetReport_C},
	{"kHIDRqGetProtocol", &_kHIDRqGetProtocol_C},
	{"kHIDRqGetIdle", &_kHIDRqGetIdle_C},
	{"kHIDRqGetReport", &_kHIDRqGetReport_C},
	{"kUSBRel20", &_kUSBRel20_C},
	{"kUSBRel11", &_kUSBRel11_C},
	{"kUSBRel10", &_kUSBRel10_C},
	{"kUSBAtrRemoteWakeup", &_kUSBAtrRemoteWakeup_C},
	{"kUSBAtrSelfPowered", &_kUSBAtrSelfPowered_C},
	{"kUSBAtrBusPowered", &_kUSBAtrBusPowered_C},
	{"kUSB100mA", &_kUSB100mA_C},
	{"kUSB500mAAvailable", &_kUSB500mAAvailable_C},
	{"kUSB100mAAvailable", &_kUSB100mAAvailable_C},
	{"kUSBFeatureDeviceRemoteWakeup", &_kUSBFeatureDeviceRemoteWakeup_C},
	{"kUSBFeatureEndpointStall", &_kUSBFeatureEndpointStall_C},
	{"kUSBHUBDesc", &_kUSBHUBDesc_C},
	{"kUSBPhysicalDesc", &_kUSBPhysicalDesc_C},
	{"kUSBReportDesc", &_kUSBReportDesc_C},
	{"kUSBHIDDesc", &_kUSBHIDDesc_C},
	{"kUSBInterfaceAssociationDesc", &_kUSBInterfaceAssociationDesc_C},
	{"kUSDebugDesc", &_kUSDebugDesc_C},
	{"kUSBOnTheGoDesc", &_kUSBOnTheGoDesc_C},
	{"kUSBInterfacePowerDesc", &_kUSBInterfacePowerDesc_C},
	{"kUSBOtherSpeedConfDesc", &_kUSBOtherSpeedConfDesc_C},
	{"kUSBDeviceQualifierDesc", &_kUSBDeviceQualifierDesc_C},
	{"kUSBEndpointDesc", &_kUSBEndpointDesc_C},
	{"kUSBInterfaceDesc", &_kUSBInterfaceDesc_C},
	{"kUSBStringDesc", &_kUSBStringDesc_C},
	{"kUSBConfDesc", &_kUSBConfDesc_C},
	{"kUSBDeviceDesc", &_kUSBDeviceDesc_C},
	{"kUSBAnyDesc", &_kUSBAnyDesc_C},
	{"kUSBRqSyncFrame", &_kUSBRqSyncFrame_C},
	{"kUSBRqSetInterface", &_kUSBRqSetInterface_C},
	{"kUSBRqGetInterface", &_kUSBRqGetInterface_C},
	{"kUSBRqSetConfig", &_kUSBRqSetConfig_C},
	{"kUSBRqGetConfig", &_kUSBRqGetConfig_C},
	{"kUSBRqSetDescriptor", &_kUSBRqSetDescriptor_C},
	{"kUSBRqGetDescriptor", &_kUSBRqGetDescriptor_C},
	{"kUSBRqSetAddress", &_kUSBRqSetAddress_C},
	{"kUSBRqReserved2", &_kUSBRqReserved2_C},
	{"kUSBRqSetFeature", &_kUSBRqSetFeature_C},
	{"kUSBRqGetState", &_kUSBRqGetState_C},
	{"kUSBRqClearFeature", &_kUSBRqClearFeature_C},
	{"kUSBRqGetStatus", &_kUSBRqGetStatus_C},
	{"kUSBOther", &_kUSBOther_C},
	{"kUSBEndpoint", &_kUSBEndpoint_C},
	{"kUSBInterface", &_kUSBInterface_C},
	{"kUSBDevice", &_kUSBDevice_C},
	{"kUSBVendor", &_kUSBVendor_C},
	{"kUSBClass", &_kUSBClass_C},
	{"kUSBStandard", &_kUSBStandard_C},
	{"kUSBAnyDirn", &_kUSBAnyDirn_C},
	{"kUSBNone", &_kUSBNone_C},
	{"kUSBIn", &_kUSBIn_C},
	{"kUSBOut", &_kUSBOut_C},
	{"kUSBAnyType", &_kUSBAnyType_C},
	{"kUSBInterrupt", &_kUSBInterrupt_C},
	{"kUSBBulk", &_kUSBBulk_C},
	{"kUSBIsoc", &_kUSBIsoc_C},
	{"kUSBControl", &_kUSBControl_C},
	{"kIORegistryIterateParents", &_kIORegistryIterateParents_C},
	{"kIORegistryIterateRecursively", &_kIORegistryIterateRecursively_C},
	{"kOSAsyncRefSize", &_kOSAsyncRefSize_C},
	{"kOSAsyncRefCount", &_kOSAsyncRefCount_C},
	{"kIOInterestCalloutCount", &_kIOInterestCalloutCount_C},
	{"kIOInterestCalloutServiceIndex", &_kIOInterestCalloutServiceIndex_C},
	{"kIOInterestCalloutRefconIndex", &_kIOInterestCalloutRefconIndex_C},
	{"kIOInterestCalloutFuncIndex", &_kIOInterestCalloutFuncIndex_C},
	{"kIOMatchingCalloutCount", &_kIOMatchingCalloutCount_C},
	{"kIOMatchingCalloutRefconIndex", &_kIOMatchingCalloutRefconIndex_C},
	{"kIOMatchingCalloutFuncIndex", &_kIOMatchingCalloutFuncIndex_C},
	{"kIOAsyncCalloutCount", &_kIOAsyncCalloutCount_C},
	{"kIOAsyncCalloutRefconIndex", &_kIOAsyncCalloutRefconIndex_C},
	{"kIOAsyncCalloutFuncIndex", &_kIOAsyncCalloutFuncIndex_C},
	{"kIOAsyncReservedCount", &_kIOAsyncReservedCount_C},
	{"kIOAsyncReservedIndex", &_kIOAsyncReservedIndex_C},
	{"kMaxAsyncArgs", &_kMaxAsyncArgs_C},
	{"kOSAsyncCompleteMessageID", &_kOSAsyncCompleteMessageID_C},
	{"kOSNotificationMessageID", &_kOSNotificationMessageID_C},
	{"kLastIOKitNotificationType", &_kLastIOKitNotificationType_C},
	{"kIOServiceMessageNotificationType", &_kIOServiceMessageNotificationType_C},
	{"kIOAsyncCompletionNotificationType", &_kIOAsyncCompletionNotificationType_C},
	{"kIOServiceTerminatedNotificationType", &_kIOServiceTerminatedNotificationType_C},
	{"kIOServiceMatchedNotificationType", &_kIOServiceMatchedNotificationType_C},
	{"kIOServicePublishNotificationType", &_kIOServicePublishNotificationType_C},
	{"kFirstIOKitNotificationType", &_kFirstIOKitNotificationType_C},
	{"IO_CopyBack", &_IO_CopyBack_C},
	{"IO_WriteThrough", &_IO_WriteThrough_C},
	{"IO_CacheOff", &_IO_CacheOff_C},
	{"kTickScale", &_kTickScale_C},
	{"kSecondScale", &_kSecondScale_C},
	{"kMillisecondScale", &_kMillisecondScale_C},
	{"kMicrosecondScale", &_kMicrosecondScale_C},
	{"kNanosecondScale", &_kNanosecondScale_C},
	{"kIOMapUnique", &_kIOMapUnique_C},
	{"kIOMapReference", &_kIOMapReference_C},
	{"kIOMapStatic", &_kIOMapStatic_C},
	{"kIOMapReadOnly", &_kIOMapReadOnly_C},
	{"kIOMapUserOptionsMask", &_kIOMapUserOptionsMask_C},
	{"kIOMapWriteCombineCache", &_kIOMapWriteCombineCache_C},
	{"kIOMapCopybackCache", &_kIOMapCopybackCache_C},
	{"kIOMapWriteThruCache", &_kIOMapWriteThruCache_C},
	{"kIOMapInhibitCache", &_kIOMapInhibitCache_C},
	{"kIOMapDefaultCache", &_kIOMapDefaultCache_C},
	{"kIOMapCacheShift", &_kIOMapCacheShift_C},
	{"kIOMapCacheMask", &_kIOMapCacheMask_C},
	{"kIOMapAnywhere", &_kIOMapAnywhere_C},
	{"kIOWriteCombineCache", &_kIOWriteCombineCache_C},
	{"kIOCopybackCache", &_kIOCopybackCache_C},
	{"kIOWriteThruCache", &_kIOWriteThruCache_C},
	{"kIOInhibitCache", &_kIOInhibitCache_C},
	{"kIODefaultCache", &_kIODefaultCache_C},
	{"kIODefaultMemoryType", &_kIODefaultMemoryType_C},
	{"finalStage", &_finalStage_C},
	{"betaStage", &_betaStage_C},
	{"alphaStage", &_alphaStage_C},
	{"developStage", &_developStage_C},
	{"kUnknownType", &_kUnknownType_C},
	{"kVariableLengthArray", &_kVariableLengthArray_C},
	{"kNilOptions", &_kNilOptions_C},
	{"noErr", &_noErr_C},
};

Nat4	VPX_MacOSX_IOKit_Constants_Number = 655;

#pragma export on

Nat4	load_MacOSX_IOKit_Constants(V_Environment environment,char *bundleID);
Nat4	load_MacOSX_IOKit_Constants(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
        V_Dictionary	dictionary = NULL;
		
		dictionary = environment->externalConstantsTable;
		result = add_nodes(dictionary,VPX_MacOSX_IOKit_Constants_Number,VPX_MacOSX_IOKit_Constants);
		
		return result;
}

#pragma export off
