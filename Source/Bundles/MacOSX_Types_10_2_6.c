/*
	
	MacOSX_Types.c
	Copyright 2003 Scott B. Anderson, All Rights Reserved.
	
*/

	VPL_ExtType _2439 = {"2439",kEnumType,4};
	VPL_ExtType _2438 = {"2438",kEnumType,4};
	VPL_ExtType _2437 = {"2437",kEnumType,4};
	VPL_ExtType _2436 = {"2436",kEnumType,4};
	VPL_ExtType _2435 = {"2435",kEnumType,4};
	VPL_ExtType _2434 = {"2434",kEnumType,4};
	VPL_ExtType _2433 = {"2433",kEnumType,4};
	VPL_ExtType _ICD_DisposePropertyPB = {"ICD_DisposePropertyPB",kStructureType,12};
	VPL_ExtType _ICD_NewPropertyPB = {"ICD_NewPropertyPB",kStructureType,32};
	VPL_ExtType _ICD_DisposeObjectPB = {"ICD_DisposeObjectPB",kStructureType,12};
	VPL_ExtType _ICD_NewObjectPB = {"ICD_NewObjectPB",kStructureType,24};
	VPL_ExtType _ICDHeader = {"ICDHeader",kStructureType,8};
	VPL_ExtType _ICACopyObjectPropertyDictionaryPB = {"ICACopyObjectPropertyDictionaryPB",kStructureType,16};
	VPL_ExtType _ICADownloadFilePB = {"ICADownloadFilePB",kStructureType,36};
	VPL_ExtType _ICARegisterEventNotificationPB = {"ICARegisterEventNotificationPB",kStructureType,20};
	VPL_ExtType _ICAObjectSendMessagePB = {"ICAObjectSendMessagePB",kStructureType,36};
	VPL_ExtType _ICAGetDeviceListPB = {"ICAGetDeviceListPB",kStructureType,12};
	VPL_ExtType _ICASetPropertyRefConPB = {"ICASetPropertyRefConPB",kStructureType,16};
	VPL_ExtType _ICAGetPropertyRefConPB = {"ICAGetPropertyRefConPB",kStructureType,16};
	VPL_ExtType _ICAGetRootOfPropertyPB = {"ICAGetRootOfPropertyPB",kStructureType,24};
	VPL_ExtType _ICAGetParentOfPropertyPB = {"ICAGetParentOfPropertyPB",kStructureType,24};
	VPL_ExtType _ICASetPropertyDataPB = {"ICASetPropertyDataPB",kStructureType,28};
	VPL_ExtType _ICAGetPropertyDataPB = {"ICAGetPropertyDataPB",kStructureType,32};
	VPL_ExtType _ICAGetPropertyInfoPB = {"ICAGetPropertyInfoPB",kStructureType,28};
	VPL_ExtType _ICAGetPropertyByTypePB = {"ICAGetPropertyByTypePB",kStructureType,36};
	VPL_ExtType _ICAGetNthPropertyPB = {"ICAGetNthPropertyPB",kStructureType,36};
	VPL_ExtType _ICAGetPropertyCountPB = {"ICAGetPropertyCountPB",kStructureType,16};
	VPL_ExtType _ICASetObjectRefConPB = {"ICASetObjectRefConPB",kStructureType,16};
	VPL_ExtType _ICAGetObjectRefConPB = {"ICAGetObjectRefConPB",kStructureType,16};
	VPL_ExtType _ICAGetRootOfObjectPB = {"ICAGetRootOfObjectPB",kStructureType,24};
	VPL_ExtType _ICAGetParentOfObjectPB = {"ICAGetParentOfObjectPB",kStructureType,24};
	VPL_ExtType _ICAGetObjectInfoPB = {"ICAGetObjectInfoPB",kStructureType,20};
	VPL_ExtType _ICAGetNthChildPB = {"ICAGetNthChildPB",kStructureType,28};
	VPL_ExtType _ICAGetChildCountPB = {"ICAGetChildCountPB",kStructureType,16};
	VPL_ExtType _ICAHeader = {"ICAHeader",kStructureType,8};
	VPL_ExtType _2432 = {"2432",kEnumType,4};
	VPL_ExtType _ICAThumbnail = {"ICAThumbnail",kStructureType,16};
	VPL_ExtType _ICAMessage = {"ICAMessage",kStructureType,20};
	VPL_ExtType _ICAPropertyInfo = {"ICAPropertyInfo",kStructureType,16};
	VPL_ExtType _ICAObjectInfo = {"ICAObjectInfo",kStructureType,8};
	VPL_ExtType _2431 = {"2431",kEnumType,4};
	VPL_ExtType _2430 = {"2430",kEnumType,4};
	VPL_ExtType _2429 = {"2429",kEnumType,4};
	VPL_ExtType _2428 = {"2428",kEnumType,4};
	VPL_ExtType _2427 = {"2427",kEnumType,4};
	VPL_ExtType _2426 = {"2426",kEnumType,4};
	VPL_ExtType _2425 = {"2425",kEnumType,4};
	VPL_ExtType _2424 = {"2424",kEnumType,4};
	VPL_ExtType _2423 = {"2423",kEnumType,4};
	VPL_ExtType _OpaqueICAConnectionID = {"OpaqueICAConnectionID",kStructureType,0};
	VPL_ExtType _OpaqueICAProperty = {"OpaqueICAProperty",kStructureType,0};
	VPL_ExtType _OpaqueICAObject = {"OpaqueICAObject",kStructureType,0};
	VPL_ExtType _2422 = {"2422",kEnumType,4};
	VPL_ExtType _2421 = {"2421",kEnumType,4};
	VPL_ExtType _2420 = {"2420",kEnumType,4};
	VPL_ExtType _URLCallbackInfo = {"URLCallbackInfo",kStructureType,20};
	VPL_ExtType _2419 = {"2419",kEnumType,4};
	VPL_ExtType _2418 = {"2418",kEnumType,4};
	VPL_ExtType _2417 = {"2417",kEnumType,4};
	VPL_ExtType _2416 = {"2416",kEnumType,4};
	VPL_ExtType _OpaqueURLReference = {"OpaqueURLReference",kStructureType,0};
	VPL_ExtType _2415 = {"2415",kEnumType,4};
	VPL_ExtType _2414 = {"2414",kEnumType,4};
	VPL_ExtType _2413 = {"2413",kEnumType,4};
	VPL_ExtType _2412 = {"2412",kEnumType,4};
	VPL_ExtType _2411 = {"2411",kEnumType,4};
	VPL_ExtType _2410 = {"2410",kEnumType,4};
	VPL_ExtType _2409 = {"2409",kEnumType,4};
	VPL_ExtType _2408 = {"2408",kEnumType,4};
	VPL_ExtType _2407 = {"2407",kEnumType,4};
	VPL_ExtType _2406 = {"2406",kEnumType,4};
	VPL_ExtType _2405 = {"2405",kEnumType,4};
	VPL_ExtType _2404 = {"2404",kEnumType,4};
	VPL_ExtType _2403 = {"2403",kEnumType,4};
	VPL_ExtType _2402 = {"2402",kEnumType,4};
	VPL_ExtType _2401 = {"2401",kEnumType,4};
	VPL_ExtType _2400 = {"2400",kEnumType,4};
	VPL_ExtType _2399 = {"2399",kEnumType,4};
	VPL_ExtType _SRCallBackParam = {"SRCallBackParam",kStructureType,8};
	VPL_ExtType _SRCallBackStruct = {"SRCallBackStruct",kStructureType,20};
	VPL_ExtType _OpaqueSRSpeechObject = {"OpaqueSRSpeechObject",kStructureType,0};
	VPL_ExtType _2398 = {"2398",kEnumType,4};
	VPL_ExtType _2397 = {"2397",kEnumType,4};
	VPL_ExtType _2396 = {"2396",kEnumType,4};
	VPL_ExtType _2395 = {"2395",kEnumType,4};
	VPL_ExtType _OpaqueHRReference = {"OpaqueHRReference",kStructureType,0};
	VPL_ExtType _NSLDialogOptions = {"NSLDialogOptions",kStructureType,1032};
	VPL_ExtType _2394 = {"2394",kEnumType,4};
	VPL_ExtType _CalibratorInfo = {"CalibratorInfo",kStructureType,24};
	VPL_ExtType _2393 = {"2393",kEnumType,4};
	VPL_ExtType _NColorPickerInfo = {"NColorPickerInfo",kStructureType,316};
	VPL_ExtType _ColorPickerInfo = {"ColorPickerInfo",kStructureType,316};
	VPL_ExtType _PickerMenuItemInfo = {"PickerMenuItemInfo",kStructureType,12};
	VPL_ExtType _OpaquePicker = {"OpaquePicker",kStructureType,0};
	VPL_ExtType _NPMColor = {"NPMColor",kStructureType,12};
	VPL_ExtType _PMColor = {"PMColor",kStructureType,12};
	VPL_ExtType _CMYColor = {"CMYColor",kStructureType,8};
	VPL_ExtType _HSLColor = {"HSLColor",kStructureType,8};
	VPL_ExtType _HSVColor = {"HSVColor",kStructureType,8};
	VPL_ExtType _2392 = {"2392",kEnumType,4};
	VPL_ExtType _2391 = {"2391",kEnumType,4};
	VPL_ExtType _2390 = {"2390",kEnumType,4};
	VPL_ExtType _2389 = {"2389",kEnumType,4};
	VPL_ExtType _NavDialogCreationOptions = {"NavDialogCreationOptions",kStructureType,68};
	VPL_ExtType _2388 = {"2388",kEnumType,4};
	VPL_ExtType _NavReplyRecord = {"NavReplyRecord",kStructureType,260};
	VPL_ExtType _2387 = {"2387",kEnumType,4};
	VPL_ExtType _NavDialogOptions = {"NavDialogOptions",kStructureType,2052};
	VPL_ExtType _2386 = {"2386",kEnumType,4};
	VPL_ExtType _NavTypeList = {"NavTypeList",kStructureType,12};
	VPL_ExtType _2385 = {"2385",kEnumType,4};
	VPL_ExtType _NavMenuItemSpec = {"NavMenuItemSpec",kStructureType,516};
	VPL_ExtType _2384 = {"2384",kEnumType,4};
	VPL_ExtType _2383 = {"2383",kEnumType,4};
	VPL_ExtType _2382 = {"2382",kEnumType,4};
	VPL_ExtType _2381 = {"2381",kEnumType,4};
	VPL_ExtType _2380 = {"2380",kEnumType,4};
	VPL_ExtType _2379 = {"2379",kEnumType,4};
	VPL_ExtType _2378 = {"2378",kEnumType,4};
	VPL_ExtType _2377 = {"2377",kEnumType,4};
	VPL_ExtType _2376 = {"2376",kEnumType,4};
	VPL_ExtType _NavCBRec = {"NavCBRec",kStructureType,260};
	VPL_ExtType _2375 = {"2375",kEnumType,4};
	VPL_ExtType _2374 = {"2374",kEnumType,4};
	VPL_ExtType ___NavDialog = {"__NavDialog",kStructureType,0};
	VPL_ExtType _NavEventData = {"NavEventData",kStructureType,8};
	VPL_ExtType _NavEventDataInfo = {"NavEventDataInfo",NULL,4};
	VPL_ExtType _2373 = {"2373",kStructureType,264};
	VPL_ExtType _2372 = {"2372",kStructureType,48};
	VPL_ExtType _2371 = {"2371",NULL,264};
	VPL_ExtType _NavFileOrFolderInfo = {"NavFileOrFolderInfo",kStructureType,276};
	VPL_ExtType _2370 = {"2370",kEnumType,4};
	VPL_ExtType _2369 = {"2369",kEnumType,4};
	VPL_ExtType _2368 = {"2368",kEnumType,4};
	VPL_ExtType _2367 = {"2367",kEnumType,4};
	VPL_ExtType _2366 = {"2366",kEnumType,4};
	VPL_ExtType _2365 = {"2365",kEnumType,4};
	VPL_ExtType _2364 = {"2364",kEnumType,4};
	VPL_ExtType _2363 = {"2363",kEnumType,4};
	VPL_ExtType _2362 = {"2362",kEnumType,4};
	VPL_ExtType _2361 = {"2361",kEnumType,4};
	VPL_ExtType _2360 = {"2360",kEnumType,4};
	VPL_ExtType _2359 = {"2359",kEnumType,4};
	VPL_ExtType _2358 = {"2358",kEnumType,4};
	VPL_ExtType _2357 = {"2357",kEnumType,4};
	VPL_ExtType _2356 = {"2356",kEnumType,4};
	VPL_ExtType _2355 = {"2355",kEnumType,4};
	VPL_ExtType _2354 = {"2354",kEnumType,4};
	VPL_ExtType _2353 = {"2353",kEnumType,4};
	VPL_ExtType _2352 = {"2352",kEnumType,4};
	VPL_ExtType _2351 = {"2351",kEnumType,4};
	VPL_ExtType _2350 = {"2350",kEnumType,4};
	VPL_ExtType _2349 = {"2349",kEnumType,4};
	VPL_ExtType _2348 = {"2348",kEnumType,4};
	VPL_ExtType _2347 = {"2347",kEnumType,4};
	VPL_ExtType _2346 = {"2346",kEnumType,4};
	VPL_ExtType _2345 = {"2345",kEnumType,4};
	VPL_ExtType _2344 = {"2344",kEnumType,4};
	VPL_ExtType _2343 = {"2343",kEnumType,4};
	VPL_ExtType _2342 = {"2342",kEnumType,4};
	VPL_ExtType _2341 = {"2341",kEnumType,4};
	VPL_ExtType _2340 = {"2340",kEnumType,4};
	VPL_ExtType _2339 = {"2339",kEnumType,4};
	VPL_ExtType _2338 = {"2338",kEnumType,4};
	VPL_ExtType _2337 = {"2337",kEnumType,4};
	VPL_ExtType _2336 = {"2336",kEnumType,4};
	VPL_ExtType _2335 = {"2335",kEnumType,4};
	VPL_ExtType _2334 = {"2334",kEnumType,4};
	VPL_ExtType _2333 = {"2333",kEnumType,4};
	VPL_ExtType _2332 = {"2332",kEnumType,4};
	VPL_ExtType _2331 = {"2331",kEnumType,4};
	VPL_ExtType _2330 = {"2330",kEnumType,4};
	VPL_ExtType _2329 = {"2329",kEnumType,4};
	VPL_ExtType _2328 = {"2328",kEnumType,4};
	VPL_ExtType _2327 = {"2327",kEnumType,4};
	VPL_ExtType _2326 = {"2326",kEnumType,4};
	VPL_ExtType _2325 = {"2325",kEnumType,4};
	VPL_ExtType _2324 = {"2324",kEnumType,4};
	VPL_ExtType _2323 = {"2323",kEnumType,4};
	VPL_ExtType _2322 = {"2322",kEnumType,4};
	VPL_ExtType _2321 = {"2321",kEnumType,4};
	VPL_ExtType _2320 = {"2320",kEnumType,4};
	VPL_ExtType _2319 = {"2319",kEnumType,4};
	VPL_ExtType _2318 = {"2318",kEnumType,4};
	VPL_ExtType _2317 = {"2317",kEnumType,4};
	VPL_ExtType _2316 = {"2316",kEnumType,4};
	VPL_ExtType _2315 = {"2315",kEnumType,4};
	VPL_ExtType _2314 = {"2314",kEnumType,4};
	VPL_ExtType _2313 = {"2313",kEnumType,4};
	VPL_ExtType _2312 = {"2312",kEnumType,4};
	VPL_ExtType _2311 = {"2311",kEnumType,4};
	VPL_ExtType _2310 = {"2310",kEnumType,4};
	VPL_ExtType _2309 = {"2309",kEnumType,4};
	VPL_ExtType _2308 = {"2308",kEnumType,4};
	VPL_ExtType _2307 = {"2307",kEnumType,4};
	VPL_ExtType _2306 = {"2306",kEnumType,4};
	VPL_ExtType _2305 = {"2305",kEnumType,4};
	VPL_ExtType _2304 = {"2304",kEnumType,4};
	VPL_ExtType _2303 = {"2303",kEnumType,4};
	VPL_ExtType _2302 = {"2302",kEnumType,4};
	VPL_ExtType _2301 = {"2301",kEnumType,4};
	VPL_ExtType _2300 = {"2300",kEnumType,4};
	VPL_ExtType _2299 = {"2299",kEnumType,4};
	VPL_ExtType _2298 = {"2298",kEnumType,4};
	VPL_ExtType _2297 = {"2297",kEnumType,4};
	VPL_ExtType _2296 = {"2296",kEnumType,4};
	VPL_ExtType _2295 = {"2295",kEnumType,4};
	VPL_ExtType _2294 = {"2294",kEnumType,4};
	VPL_ExtType _2293 = {"2293",kEnumType,4};
	VPL_ExtType _2292 = {"2292",kEnumType,4};
	VPL_ExtType _StatementRange = {"StatementRange",kStructureType,8};
	VPL_ExtType _2291 = {"2291",kEnumType,4};
	VPL_ExtType _2290 = {"2290",kEnumType,4};
	VPL_ExtType _2289 = {"2289",kEnumType,4};
	VPL_ExtType _2288 = {"2288",kEnumType,4};
	VPL_ExtType _2287 = {"2287",kEnumType,4};
	VPL_ExtType _2286 = {"2286",kEnumType,4};
	VPL_ExtType _2285 = {"2285",kEnumType,4};
	VPL_ExtType _2284 = {"2284",kEnumType,4};
	VPL_ExtType _2283 = {"2283",kEnumType,4};
	VPL_ExtType _2282 = {"2282",kEnumType,4};
	VPL_ExtType _2281 = {"2281",kEnumType,4};
	VPL_ExtType _2280 = {"2280",kEnumType,4};
	VPL_ExtType _2279 = {"2279",kEnumType,4};
	VPL_ExtType _2278 = {"2278",kEnumType,4};
	VPL_ExtType _2277 = {"2277",kEnumType,4};
	VPL_ExtType _2276 = {"2276",kEnumType,4};
	VPL_ExtType _2275 = {"2275",kEnumType,4};
	VPL_ExtType _2274 = {"2274",kEnumType,4};
	VPL_ExtType _2273 = {"2273",kEnumType,4};
	VPL_ExtType _2272 = {"2272",kEnumType,4};
	VPL_ExtType _2271 = {"2271",kEnumType,4};
	VPL_ExtType _2270 = {"2270",kEnumType,4};
	VPL_ExtType _2269 = {"2269",kEnumType,4};
	VPL_ExtType _2268 = {"2268",kEnumType,4};
	VPL_ExtType _2267 = {"2267",kEnumType,4};
	VPL_ExtType _2266 = {"2266",kEnumType,4};
	VPL_ExtType _2265 = {"2265",kEnumType,4};
	VPL_ExtType _2264 = {"2264",kEnumType,4};
	VPL_ExtType _2263 = {"2263",kEnumType,4};
	VPL_ExtType _2262 = {"2262",kEnumType,4};
	VPL_ExtType _2261 = {"2261",kEnumType,4};
	VPL_ExtType _2260 = {"2260",kEnumType,4};
	VPL_ExtType _2259 = {"2259",kEnumType,4};
	VPL_ExtType _2258 = {"2258",kEnumType,4};
	VPL_ExtType _2257 = {"2257",kEnumType,4};
	VPL_ExtType _2256 = {"2256",kEnumType,4};
	VPL_ExtType _2255 = {"2255",kEnumType,4};
	VPL_ExtType _2254 = {"2254",kEnumType,4};
	VPL_ExtType _2253 = {"2253",kEnumType,4};
	VPL_ExtType _2252 = {"2252",kEnumType,4};
	VPL_ExtType _2251 = {"2251",kEnumType,4};
	VPL_ExtType _2250 = {"2250",kEnumType,4};
	VPL_ExtType _2249 = {"2249",kEnumType,4};
	VPL_ExtType _2248 = {"2248",kEnumType,4};
	VPL_ExtType _2247 = {"2247",kEnumType,4};
	VPL_ExtType _2246 = {"2246",kEnumType,4};
	VPL_ExtType _2245 = {"2245",kEnumType,4};
	VPL_ExtType _2244 = {"2244",kEnumType,4};
	VPL_ExtType _2243 = {"2243",kEnumType,4};
	VPL_ExtType _2242 = {"2242",kEnumType,4};
	VPL_ExtType _2241 = {"2241",kEnumType,4};
	VPL_ExtType _2240 = {"2240",kEnumType,4};
	VPL_ExtType _2239 = {"2239",kEnumType,4};
	VPL_ExtType _2238 = {"2238",kEnumType,4};
	VPL_ExtType _2237 = {"2237",kEnumType,4};
	VPL_ExtType _2236 = {"2236",kEnumType,4};
	VPL_ExtType _2235 = {"2235",kEnumType,4};
	VPL_ExtType _SndInputCmpParam = {"SndInputCmpParam",kStructureType,28};
	VPL_ExtType _2234 = {"2234",kEnumType,4};
	VPL_ExtType _2233 = {"2233",kEnumType,4};
	VPL_ExtType _SPB = {"SPB",kStructureType,40};
	VPL_ExtType _EQSpectrumBandsRecord = {"EQSpectrumBandsRecord",kStructureType,8};
	VPL_ExtType _LevelMeterInfo = {"LevelMeterInfo",kStructureType,4};
	VPL_ExtType _AudioTerminatorAtom = {"AudioTerminatorAtom",kStructureType,8};
	VPL_ExtType _AudioEndianAtom = {"AudioEndianAtom",kStructureType,12};
	VPL_ExtType _AudioFormatAtom = {"AudioFormatAtom",kStructureType,12};
	VPL_ExtType _AudioInfo = {"AudioInfo",kStructureType,12};
	VPL_ExtType _SoundComponentLink = {"SoundComponentLink",kStructureType,28};
	VPL_ExtType _OpaqueSoundSource = {"OpaqueSoundSource",kStructureType,0};
	VPL_ExtType _OpaqueSoundConverter = {"OpaqueSoundConverter",kStructureType,0};
	VPL_ExtType _SoundSlopeAndInterceptRecord = {"SoundSlopeAndInterceptRecord",kStructureType,32};
	VPL_ExtType _CompressionInfo = {"CompressionInfo",kStructureType,20};
	VPL_ExtType _ExtendedSoundParamBlock = {"ExtendedSoundParamBlock",kStructureType,76};
	VPL_ExtType _SoundParamBlock = {"SoundParamBlock",kStructureType,64};
	VPL_ExtType _ExtendedSoundComponentData = {"ExtendedSoundComponentData",kStructureType,40};
	VPL_ExtType _SoundComponentData = {"SoundComponentData",kStructureType,28};
	VPL_ExtType _SoundInfoList = {"SoundInfoList",kStructureType,8};
	VPL_ExtType _AudioSelection = {"AudioSelection",kStructureType,12};
	VPL_ExtType _SCStatus = {"SCStatus",kStructureType,24};
	VPL_ExtType _SMStatus = {"SMStatus",kStructureType,8};
	VPL_ExtType _ExtendedScheduledSoundHeader = {"ExtendedScheduledSoundHeader",kStructureType,112};
	VPL_ExtType _ScheduledSoundHeader = {"ScheduledSoundHeader",kStructureType,100};
	VPL_ExtType _2232 = {"2232",kEnumType,4};
	VPL_ExtType _ConversionBlock = {"ConversionBlock",kStructureType,12};
	VPL_ExtType _SoundHeaderUnion = {"SoundHeaderUnion",NULL,72};
	VPL_ExtType _ExtSoundHeader = {"ExtSoundHeader",kStructureType,72};
	VPL_ExtType _CmpSoundHeader = {"CmpSoundHeader",kStructureType,72};
	VPL_ExtType _SoundHeader = {"SoundHeader",kStructureType,24};
	VPL_ExtType _Snd2ListResource = {"Snd2ListResource",kStructureType,20};
	VPL_ExtType _SndListResource = {"SndListResource",kStructureType,28};
	VPL_ExtType _ModRef = {"ModRef",kStructureType,8};
	VPL_ExtType _LeftOverBlock = {"LeftOverBlock",kStructureType,36};
	VPL_ExtType _StateBlock = {"StateBlock",kStructureType,128};
	VPL_ExtType _SndChannel = {"SndChannel",kStructureType,1060};
	VPL_ExtType _SndCommand = {"SndCommand",kStructureType,8};
	VPL_ExtType _2231 = {"2231",kEnumType,4};
	VPL_ExtType _2230 = {"2230",kEnumType,4};
	VPL_ExtType _2229 = {"2229",kEnumType,4};
	VPL_ExtType _2228 = {"2228",kEnumType,4};
	VPL_ExtType _2227 = {"2227",kEnumType,4};
	VPL_ExtType _2226 = {"2226",kEnumType,4};
	VPL_ExtType _2225 = {"2225",kEnumType,4};
	VPL_ExtType _2224 = {"2224",kEnumType,4};
	VPL_ExtType _2223 = {"2223",kEnumType,4};
	VPL_ExtType _2222 = {"2222",kEnumType,4};
	VPL_ExtType _2221 = {"2221",kEnumType,4};
	VPL_ExtType _2220 = {"2220",kEnumType,4};
	VPL_ExtType _2219 = {"2219",kEnumType,4};
	VPL_ExtType _2218 = {"2218",kEnumType,4};
	VPL_ExtType _2217 = {"2217",kEnumType,4};
	VPL_ExtType _2216 = {"2216",kEnumType,4};
	VPL_ExtType _2215 = {"2215",kEnumType,4};
	VPL_ExtType _2214 = {"2214",kEnumType,4};
	VPL_ExtType _2213 = {"2213",kEnumType,4};
	VPL_ExtType _2212 = {"2212",kEnumType,4};
	VPL_ExtType _2211 = {"2211",kEnumType,4};
	VPL_ExtType _2210 = {"2210",kEnumType,4};
	VPL_ExtType _2209 = {"2209",kEnumType,4};
	VPL_ExtType _2208 = {"2208",kEnumType,4};
	VPL_ExtType _2207 = {"2207",kEnumType,4};
	VPL_ExtType _2206 = {"2206",kEnumType,4};
	VPL_ExtType _2205 = {"2205",kEnumType,4};
	VPL_ExtType _2204 = {"2204",kEnumType,4};
	VPL_ExtType _2203 = {"2203",kEnumType,4};
	VPL_ExtType _2202 = {"2202",kEnumType,4};
	VPL_ExtType _2201 = {"2201",kEnumType,4};
	VPL_ExtType _2200 = {"2200",kEnumType,4};
	VPL_ExtType _2199 = {"2199",kEnumType,4};
	VPL_ExtType _2198 = {"2198",kEnumType,4};
	VPL_ExtType _2197 = {"2197",kEnumType,4};
	VPL_ExtType _2196 = {"2196",kEnumType,4};
	VPL_ExtType _OpaqueIBNibRef = {"OpaqueIBNibRef",kStructureType,0};
	VPL_ExtType _2195 = {"2195",kEnumType,4};
	VPL_ExtType _2194 = {"2194",kEnumType,4};
	VPL_ExtType _2193 = {"2193",kEnumType,4};
	VPL_ExtType _2192 = {"2192",kEnumType,4};
	VPL_ExtType _2191 = {"2191",kEnumType,4};
	VPL_ExtType _ICServices = {"ICServices",kStructureType,264};
	VPL_ExtType _2190 = {"2190",kEnumType,4};
	VPL_ExtType _2189 = {"2189",kEnumType,4};
	VPL_ExtType _ICServiceEntry = {"ICServiceEntry",kStructureType,260};
	VPL_ExtType _2188 = {"2188",kEnumType,4};
	VPL_ExtType _2187 = {"2187",kEnumType,4};
	VPL_ExtType _2186 = {"2186",kEnumType,4};
	VPL_ExtType _ICMapEntry = {"ICMapEntry",kStructureType,1304};
	VPL_ExtType _2185 = {"2185",kEnumType,4};
	VPL_ExtType _ICFileSpec = {"ICFileSpec",kStructureType,116};
	VPL_ExtType _ICAppSpecList = {"ICAppSpecList",kStructureType,72};
	VPL_ExtType _ICAppSpec = {"ICAppSpec",kStructureType,68};
	VPL_ExtType _ICCharTable = {"ICCharTable",kStructureType,512};
	VPL_ExtType _ICFontRecord = {"ICFontRecord",kStructureType,260};
	VPL_ExtType _2184 = {"2184",kEnumType,4};
	VPL_ExtType _2183 = {"2183",kEnumType,4};
	VPL_ExtType _2182 = {"2182",kEnumType,4};
	VPL_ExtType _2181 = {"2181",kEnumType,4};
	VPL_ExtType _2180 = {"2180",kEnumType,4};
	VPL_ExtType _2179 = {"2179",kEnumType,4};
	VPL_ExtType _2178 = {"2178",kEnumType,4};
	VPL_ExtType _2177 = {"2177",kEnumType,4};
	VPL_ExtType _2176 = {"2176",kEnumType,4};
	VPL_ExtType _2175 = {"2175",kEnumType,4};
	VPL_ExtType _ICDirSpec = {"ICDirSpec",kStructureType,8};
	VPL_ExtType _OpaqueICInstance = {"OpaqueICInstance",kStructureType,0};
	VPL_ExtType _2174 = {"2174",kEnumType,4};
	VPL_ExtType _2173 = {"2173",kEnumType,4};
	VPL_ExtType _TypeSelectRecord = {"TypeSelectRecord",kStructureType,72};
	VPL_ExtType _2172 = {"2172",kEnumType,4};
	VPL_ExtType _FileTranslationSpec = {"FileTranslationSpec",kStructureType,48};
	VPL_ExtType _2171 = {"2171",kEnumType,4};
	VPL_ExtType _2170 = {"2170",kEnumType,4};
	VPL_ExtType _2169 = {"2169",kEnumType,4};
	VPL_ExtType _ScrapTranslationList = {"ScrapTranslationList",kStructureType,8};
	VPL_ExtType _ScrapTypeSpec = {"ScrapTypeSpec",kStructureType,8};
	VPL_ExtType _FileTranslationList = {"FileTranslationList",kStructureType,8};
	VPL_ExtType _FileTypeSpec = {"FileTypeSpec",kStructureType,20};
	VPL_ExtType _2168 = {"2168",kEnumType,4};
	VPL_ExtType _2167 = {"2167",kEnumType,4};
	VPL_ExtType _TSMTERec = {"TSMTERec",kStructureType,20};
	VPL_ExtType _2166 = {"2166",kEnumType,4};
	VPL_ExtType _2165 = {"2165",kEnumType,4};
	VPL_ExtType _2164 = {"2164",kEnumType,4};
	VPL_ExtType _2163 = {"2163",kEnumType,4};
	VPL_ExtType _2162 = {"2162",kEnumType,4};
	VPL_ExtType _2161 = {"2161",kEnumType,4};
	VPL_ExtType _DataBrowserListViewColumnDesc = {"DataBrowserListViewColumnDesc",kStructureType,68};
	VPL_ExtType _DataBrowserListViewHeaderDesc = {"DataBrowserListViewHeaderDesc",kStructureType,56};
	VPL_ExtType _2160 = {"2160",kEnumType,4};
	VPL_ExtType _2159 = {"2159",kEnumType,4};
	VPL_ExtType _2158 = {"2158",kEnumType,4};
	VPL_ExtType _2157 = {"2157",kEnumType,4};
	VPL_ExtType _2156 = {"2156",kEnumType,4};
	VPL_ExtType _2155 = {"2155",kStructureType,28};
	VPL_ExtType _2154 = {"2154",NULL,28};
	VPL_ExtType _DataBrowserCustomCallbacks = {"DataBrowserCustomCallbacks",kStructureType,32};
	VPL_ExtType _2153 = {"2153",kEnumType,4};
	VPL_ExtType _2152 = {"2152",kEnumType,4};
	VPL_ExtType _2151 = {"2151",kStructureType,40};
	VPL_ExtType _2150 = {"2150",NULL,40};
	VPL_ExtType _DataBrowserCallbacks = {"DataBrowserCallbacks",kStructureType,44};
	VPL_ExtType _2149 = {"2149",kEnumType,4};
	VPL_ExtType _DataBrowserPropertyDesc = {"DataBrowserPropertyDesc",kStructureType,12};
	VPL_ExtType _2148 = {"2148",kEnumType,4};
	VPL_ExtType _2147 = {"2147",kEnumType,4};
	VPL_ExtType _2146 = {"2146",kEnumType,4};
	VPL_ExtType _2145 = {"2145",kEnumType,4};
	VPL_ExtType _2144 = {"2144",kEnumType,4};
	VPL_ExtType _2143 = {"2143",kEnumType,4};
	VPL_ExtType _2142 = {"2142",kEnumType,4};
	VPL_ExtType _2141 = {"2141",kEnumType,4};
	VPL_ExtType _2140 = {"2140",kEnumType,4};
	VPL_ExtType _2139 = {"2139",kEnumType,4};
	VPL_ExtType _2138 = {"2138",kEnumType,4};
	VPL_ExtType _2137 = {"2137",kEnumType,4};
	VPL_ExtType _2136 = {"2136",kEnumType,4};
	VPL_ExtType _2135 = {"2135",kEnumType,4};
	VPL_ExtType _2134 = {"2134",kEnumType,4};
	VPL_ExtType _2133 = {"2133",kEnumType,4};
	VPL_ExtType _2132 = {"2132",kEnumType,4};
	VPL_ExtType _2131 = {"2131",kEnumType,4};
	VPL_ExtType _2130 = {"2130",kEnumType,4};
	VPL_ExtType _2129 = {"2129",kEnumType,4};
	VPL_ExtType _2128 = {"2128",kEnumType,4};
	VPL_ExtType _2127 = {"2127",kEnumType,4};
	VPL_ExtType _2126 = {"2126",kEnumType,4};
	VPL_ExtType _2125 = {"2125",kEnumType,4};
	VPL_ExtType _2124 = {"2124",kEnumType,4};
	VPL_ExtType _2123 = {"2123",kEnumType,4};
	VPL_ExtType _2122 = {"2122",kEnumType,4};
	VPL_ExtType _2121 = {"2121",kEnumType,4};
	VPL_ExtType _2120 = {"2120",kEnumType,4};
	VPL_ExtType _2119 = {"2119",kEnumType,4};
	VPL_ExtType _2118 = {"2118",kEnumType,4};
	VPL_ExtType _2117 = {"2117",kEnumType,4};
	VPL_ExtType _2116 = {"2116",kEnumType,4};
	VPL_ExtType _2115 = {"2115",kEnumType,4};
	VPL_ExtType _2114 = {"2114",kEnumType,4};
	VPL_ExtType _2113 = {"2113",kEnumType,4};
	VPL_ExtType _2112 = {"2112",kEnumType,4};
	VPL_ExtType _2111 = {"2111",kEnumType,4};
	VPL_ExtType _2110 = {"2110",kEnumType,4};
	VPL_ExtType _2109 = {"2109",kEnumType,4};
	VPL_ExtType _2108 = {"2108",kEnumType,4};
	VPL_ExtType _2107 = {"2107",kEnumType,4};
	VPL_ExtType _2106 = {"2106",kEnumType,4};
	VPL_ExtType _2105 = {"2105",kEnumType,4};
	VPL_ExtType _2104 = {"2104",kEnumType,4};
	VPL_ExtType _2103 = {"2103",kEnumType,4};
	VPL_ExtType _2102 = {"2102",kEnumType,4};
	VPL_ExtType _2101 = {"2101",kEnumType,4};
	VPL_ExtType _2100 = {"2100",kEnumType,4};
	VPL_ExtType _2099 = {"2099",kEnumType,4};
	VPL_ExtType _2098 = {"2098",kEnumType,4};
	VPL_ExtType _2097 = {"2097",kEnumType,4};
	VPL_ExtType _2096 = {"2096",kEnumType,4};
	VPL_ExtType _2095 = {"2095",kEnumType,4};
	VPL_ExtType _2094 = {"2094",kEnumType,4};
	VPL_ExtType _2093 = {"2093",kEnumType,4};
	VPL_ExtType _2092 = {"2092",kEnumType,4};
	VPL_ExtType _2091 = {"2091",kEnumType,4};
	VPL_ExtType _2090 = {"2090",kEnumType,4};
	VPL_ExtType _2089 = {"2089",kEnumType,4};
	VPL_ExtType _2088 = {"2088",kEnumType,4};
	VPL_ExtType _ControlEditTextSelectionRec = {"ControlEditTextSelectionRec",kStructureType,4};
	VPL_ExtType _2087 = {"2087",kEnumType,4};
	VPL_ExtType _2086 = {"2086",kEnumType,4};
	VPL_ExtType _2085 = {"2085",kEnumType,4};
	VPL_ExtType _2084 = {"2084",kEnumType,4};
	VPL_ExtType _2083 = {"2083",kEnumType,4};
	VPL_ExtType _2082 = {"2082",kEnumType,4};
	VPL_ExtType _2081 = {"2081",kEnumType,4};
	VPL_ExtType _2080 = {"2080",kEnumType,4};
	VPL_ExtType _2079 = {"2079",kEnumType,4};
	VPL_ExtType _2078 = {"2078",kEnumType,4};
	VPL_ExtType _2077 = {"2077",kEnumType,4};
	VPL_ExtType _2076 = {"2076",kEnumType,4};
	VPL_ExtType _2075 = {"2075",kEnumType,4};
	VPL_ExtType _2074 = {"2074",kEnumType,4};
	VPL_ExtType _2073 = {"2073",kEnumType,4};
	VPL_ExtType _2072 = {"2072",kEnumType,4};
	VPL_ExtType _2071 = {"2071",kEnumType,4};
	VPL_ExtType _2070 = {"2070",kEnumType,4};
	VPL_ExtType _2069 = {"2069",kEnumType,4};
	VPL_ExtType _2068 = {"2068",kEnumType,4};
	VPL_ExtType _2067 = {"2067",kEnumType,4};
	VPL_ExtType _2066 = {"2066",kEnumType,4};
	VPL_ExtType _2065 = {"2065",kEnumType,4};
	VPL_ExtType _2064 = {"2064",kEnumType,4};
	VPL_ExtType _2063 = {"2063",kEnumType,4};
	VPL_ExtType _2062 = {"2062",kEnumType,4};
	VPL_ExtType _2061 = {"2061",kEnumType,4};
	VPL_ExtType _2060 = {"2060",kEnumType,4};
	VPL_ExtType _2059 = {"2059",kEnumType,4};
	VPL_ExtType _ControlTabInfoRecV1 = {"ControlTabInfoRecV1",kStructureType,8};
	VPL_ExtType _ControlTabInfoRec = {"ControlTabInfoRec",kStructureType,260};
	VPL_ExtType _2058 = {"2058",kEnumType,4};
	VPL_ExtType _2057 = {"2057",kEnumType,4};
	VPL_ExtType _2056 = {"2056",kEnumType,4};
	VPL_ExtType _2055 = {"2055",kEnumType,4};
	VPL_ExtType _2054 = {"2054",kEnumType,4};
	VPL_ExtType _ControlTabEntry = {"ControlTabEntry",kStructureType,12};
	VPL_ExtType _2053 = {"2053",kEnumType,4};
	VPL_ExtType _2052 = {"2052",kEnumType,4};
	VPL_ExtType _2051 = {"2051",kEnumType,4};
	VPL_ExtType _2050 = {"2050",kEnumType,4};
	VPL_ExtType _2049 = {"2049",kEnumType,4};
	VPL_ExtType _2048 = {"2048",kEnumType,4};
	VPL_ExtType _2047 = {"2047",kEnumType,4};
	VPL_ExtType _2046 = {"2046",kEnumType,4};
	VPL_ExtType _2045 = {"2045",kEnumType,4};
	VPL_ExtType _2044 = {"2044",kEnumType,4};
	VPL_ExtType _2043 = {"2043",kEnumType,4};
	VPL_ExtType _2042 = {"2042",kEnumType,4};
	VPL_ExtType _2041 = {"2041",kEnumType,4};
	VPL_ExtType _2040 = {"2040",kEnumType,4};
	VPL_ExtType _2039 = {"2039",kEnumType,4};
	VPL_ExtType _2038 = {"2038",kEnumType,4};
	VPL_ExtType _2037 = {"2037",kEnumType,4};
	VPL_ExtType _2036 = {"2036",kEnumType,4};
	VPL_ExtType _2035 = {"2035",kEnumType,4};
	VPL_ExtType _2034 = {"2034",kEnumType,4};
	VPL_ExtType _2033 = {"2033",kEnumType,4};
	VPL_ExtType _2032 = {"2032",kEnumType,4};
	VPL_ExtType _2031 = {"2031",kEnumType,4};
	VPL_ExtType _2030 = {"2030",kEnumType,4};
	VPL_ExtType _2029 = {"2029",kEnumType,4};
	VPL_ExtType _2028 = {"2028",kEnumType,4};
	VPL_ExtType _2027 = {"2027",kEnumType,4};
	VPL_ExtType _2026 = {"2026",kEnumType,4};
	VPL_ExtType _2025 = {"2025",kEnumType,4};
	VPL_ExtType _2024 = {"2024",kEnumType,4};
	VPL_ExtType _2023 = {"2023",kEnumType,4};
	VPL_ExtType _2022 = {"2022",kEnumType,4};
	VPL_ExtType _2021 = {"2021",kEnumType,4};
	VPL_ExtType _2020 = {"2020",kEnumType,4};
	VPL_ExtType _2019 = {"2019",kEnumType,4};
	VPL_ExtType _2018 = {"2018",kEnumType,4};
	VPL_ExtType _2017 = {"2017",kEnumType,4};
	VPL_ExtType _2016 = {"2016",kEnumType,4};
	VPL_ExtType _2015 = {"2015",kEnumType,4};
	VPL_ExtType _HMHelpContentRec = {"HMHelpContentRec",kStructureType,536};
	VPL_ExtType _2014 = {"2014",NULL,256};
	VPL_ExtType _HMHelpContent = {"HMHelpContent",kStructureType,260};
	VPL_ExtType _2013 = {"2013",kEnumType,4};
	VPL_ExtType _2012 = {"2012",kEnumType,4};
	VPL_ExtType _2011 = {"2011",kEnumType,4};
	VPL_ExtType _2010 = {"2010",kEnumType,4};
	VPL_ExtType _2009 = {"2009",kEnumType,4};
	VPL_ExtType _2008 = {"2008",kEnumType,4};
	VPL_ExtType _2007 = {"2007",kEnumType,4};
	VPL_ExtType _2006 = {"2006",kEnumType,4};
	VPL_ExtType _2005 = {"2005",kEnumType,4};
	VPL_ExtType _2004 = {"2004",kEnumType,4};
	VPL_ExtType _TXNCarbonEventInfo = {"TXNCarbonEventInfo",kStructureType,8};
	VPL_ExtType _2003 = {"2003",kEnumType,4};
	VPL_ExtType _2002 = {"2002",kEnumType,4};
	VPL_ExtType _2001 = {"2001",kEnumType,4};
	VPL_ExtType _2000 = {"2000",kEnumType,4};
	VPL_ExtType _TXNLongRect = {"TXNLongRect",kStructureType,16};
	VPL_ExtType _TXNBackground = {"TXNBackground",kStructureType,12};
	VPL_ExtType _TXNBackgroundData = {"TXNBackgroundData",NULL,8};
	VPL_ExtType _1999 = {"1999",kEnumType,4};
	VPL_ExtType _TXNMatchTextRecord = {"TXNMatchTextRecord",kStructureType,12};
	VPL_ExtType _TXNMacOSPreferredFontDescription = {"TXNMacOSPreferredFontDescription",kStructureType,16};
	VPL_ExtType _TXNTypeAttributes = {"TXNTypeAttributes",kStructureType,12};
	VPL_ExtType _TXNAttributeData = {"TXNAttributeData",NULL,4};
	VPL_ExtType _TXNATSUIVariations = {"TXNATSUIVariations",kStructureType,12};
	VPL_ExtType _TXNATSUIFeatures = {"TXNATSUIFeatures",kStructureType,12};
	VPL_ExtType _1998 = {"1998",kEnumType,4};
	VPL_ExtType _1997 = {"1997",kEnumType,4};
	VPL_ExtType _1996 = {"1996",kEnumType,4};
	VPL_ExtType _1995 = {"1995",kEnumType,4};
	VPL_ExtType _1994 = {"1994",kEnumType,4};
	VPL_ExtType _1993 = {"1993",kEnumType,4};
	VPL_ExtType _1992 = {"1992",kEnumType,4};
	VPL_ExtType _1991 = {"1991",kEnumType,4};
	VPL_ExtType _1990 = {"1990",kEnumType,4};
	VPL_ExtType _1989 = {"1989",kEnumType,4};
	VPL_ExtType _TXNControlData = {"TXNControlData",NULL,4};
	VPL_ExtType _TXNMargins = {"TXNMargins",kStructureType,8};
	VPL_ExtType _1988 = {"1988",kEnumType,4};
	VPL_ExtType _1987 = {"1987",kEnumType,4};
	VPL_ExtType _TXNTab = {"TXNTab",kStructureType,4};
	VPL_ExtType _1986 = {"1986",kEnumType,4};
	VPL_ExtType _1985 = {"1985",kEnumType,4};
	VPL_ExtType _1984 = {"1984",kEnumType,4};
	VPL_ExtType _1983 = {"1983",kEnumType,4};
	VPL_ExtType _1982 = {"1982",kEnumType,4};
	VPL_ExtType _1981 = {"1981",kEnumType,4};
	VPL_ExtType _1980 = {"1980",kEnumType,4};
	VPL_ExtType _1979 = {"1979",kEnumType,4};
	VPL_ExtType _1978 = {"1978",kEnumType,4};
	VPL_ExtType _1977 = {"1977",kEnumType,4};
	VPL_ExtType _1976 = {"1976",kEnumType,4};
	VPL_ExtType _1975 = {"1975",kEnumType,4};
	VPL_ExtType _1974 = {"1974",kEnumType,4};
	VPL_ExtType _1973 = {"1973",kEnumType,4};
	VPL_ExtType _1972 = {"1972",kEnumType,4};
	VPL_ExtType _1971 = {"1971",kEnumType,4};
	VPL_ExtType _1970 = {"1970",kEnumType,4};
	VPL_ExtType _1969 = {"1969",kEnumType,4};
	VPL_ExtType _1968 = {"1968",kEnumType,4};
	VPL_ExtType _1967 = {"1967",kEnumType,4};
	VPL_ExtType _TXNTextBoxOptionsData = {"TXNTextBoxOptionsData",kStructureType,20};
	VPL_ExtType _1966 = {"1966",kEnumType,4};
	VPL_ExtType _1965 = {"1965",kEnumType,4};
	VPL_ExtType _1964 = {"1964",kEnumType,4};
	VPL_ExtType _1963 = {"1963",kEnumType,4};
	VPL_ExtType _1962 = {"1962",kEnumType,4};
	VPL_ExtType _1961 = {"1961",kEnumType,4};
	VPL_ExtType _1960 = {"1960",kEnumType,4};
	VPL_ExtType _1959 = {"1959",kEnumType,4};
	VPL_ExtType _OpaqueTXNFontMenuObject = {"OpaqueTXNFontMenuObject",kStructureType,0};
	VPL_ExtType _OpaqueTXNObject = {"OpaqueTXNObject",kStructureType,0};
	VPL_ExtType _1958 = {"1958",kEnumType,4};
	VPL_ExtType _OpaqueScrapRef = {"OpaqueScrapRef",kStructureType,0};
	VPL_ExtType _ScrapFlavorInfo = {"ScrapFlavorInfo",kStructureType,8};
	VPL_ExtType _1957 = {"1957",kEnumType,4};
	VPL_ExtType _1956 = {"1956",kEnumType,4};
	VPL_ExtType _1955 = {"1955",kEnumType,4};
	VPL_ExtType _1954 = {"1954",kEnumType,4};
	VPL_ExtType _ScriptLanguageSupport = {"ScriptLanguageSupport",kStructureType,8};
	VPL_ExtType _ScriptLanguageRecord = {"ScriptLanguageRecord",kStructureType,4};
	VPL_ExtType _TextServiceList = {"TextServiceList",kStructureType,264};
	VPL_ExtType _TextServiceInfo = {"TextServiceInfo",kStructureType,260};
	VPL_ExtType _OpaqueTSMDocumentID = {"OpaqueTSMDocumentID",kStructureType,0};
	VPL_ExtType _OpaqueTSMContext = {"OpaqueTSMContext",kStructureType,0};
	VPL_ExtType _1953 = {"1953",kEnumType,4};
	VPL_ExtType _1952 = {"1952",kEnumType,4};
	VPL_ExtType _1951 = {"1951",kEnumType,4};
	VPL_ExtType _1950 = {"1950",kEnumType,4};
	VPL_ExtType _1949 = {"1949",kEnumType,4};
	VPL_ExtType _1948 = {"1948",kEnumType,4};
	VPL_ExtType _1947 = {"1947",kEnumType,4};
	VPL_ExtType _1946 = {"1946",kEnumType,4};
	VPL_ExtType _1945 = {"1945",kEnumType,4};
	VPL_ExtType _1944 = {"1944",kEnumType,4};
	VPL_ExtType _1943 = {"1943",kEnumType,4};
	VPL_ExtType _1942 = {"1942",kEnumType,4};
	VPL_ExtType _1941 = {"1941",kEnumType,4};
	VPL_ExtType _1940 = {"1940",kEnumType,4};
	VPL_ExtType _1939 = {"1939",kEnumType,4};
	VPL_ExtType _1938 = {"1938",NULL,4};
	VPL_ExtType _ListDefSpec = {"ListDefSpec",kStructureType,8};
	VPL_ExtType _1937 = {"1937",kEnumType,4};
	VPL_ExtType _StandardIconListCellDataRec = {"StandardIconListCellDataRec",kStructureType,268};
	VPL_ExtType _1936 = {"1936",kEnumType,4};
	VPL_ExtType _1935 = {"1935",kEnumType,4};
	VPL_ExtType _1934 = {"1934",kEnumType,4};
	VPL_ExtType _1933 = {"1933",kEnumType,4};
	VPL_ExtType _1932 = {"1932",kEnumType,4};
	VPL_ExtType _ListRec = {"ListRec",kStructureType,88};
	VPL_ExtType _AlertStdCFStringAlertParamRec = {"AlertStdCFStringAlertParamRec",kStructureType,32};
	VPL_ExtType _1931 = {"1931",kEnumType,4};
	VPL_ExtType _1930 = {"1930",kEnumType,4};
	VPL_ExtType _1929 = {"1929",kEnumType,4};
	VPL_ExtType _AlertStdAlertParamRec = {"AlertStdAlertParamRec",kStructureType,28};
	VPL_ExtType _1928 = {"1928",kEnumType,4};
	VPL_ExtType _1927 = {"1927",kEnumType,4};
	VPL_ExtType _1926 = {"1926",kEnumType,4};
	VPL_ExtType _1925 = {"1925",kEnumType,4};
	VPL_ExtType _1924 = {"1924",kEnumType,4};
	VPL_ExtType _1923 = {"1923",kEnumType,4};
	VPL_ExtType _1922 = {"1922",kEnumType,4};
	VPL_ExtType _AlertTemplate = {"AlertTemplate",kStructureType,12};
	VPL_ExtType _DialogTemplate = {"DialogTemplate",kStructureType,280};
	VPL_ExtType _1921 = {"1921",kEnumType,4};
	VPL_ExtType _1920 = {"1920",kEnumType,4};
	VPL_ExtType _1919 = {"1919",kEnumType,4};
	VPL_ExtType _1918 = {"1918",kEnumType,4};
	VPL_ExtType _1917 = {"1917",kEnumType,4};
	VPL_ExtType _1916 = {"1916",kEnumType,4};
	VPL_ExtType _1915 = {"1915",kEnumType,4};
	VPL_ExtType _1914 = {"1914",kEnumType,4};
	VPL_ExtType _1913 = {"1913",kEnumType,4};
	VPL_ExtType _OpaqueEventHotKeyRef = {"OpaqueEventHotKeyRef",kStructureType,0};
	VPL_ExtType _EventHotKeyID = {"EventHotKeyID",kStructureType,8};
	VPL_ExtType _1912 = {"1912",kEnumType,4};
	VPL_ExtType _OpaqueToolboxObjectClassRef = {"OpaqueToolboxObjectClassRef",kStructureType,0};
	VPL_ExtType _OpaqueEventTargetRef = {"OpaqueEventTargetRef",kStructureType,0};
	VPL_ExtType _OpaqueEventHandlerCallRef = {"OpaqueEventHandlerCallRef",kStructureType,0};
	VPL_ExtType _OpaqueEventHandlerRef = {"OpaqueEventHandlerRef",kStructureType,0};
	VPL_ExtType _1911 = {"1911",kEnumType,4};
	VPL_ExtType _1910 = {"1910",kEnumType,4};
	VPL_ExtType _1909 = {"1909",kEnumType,4};
	VPL_ExtType _1908 = {"1908",kEnumType,4};
	VPL_ExtType _1907 = {"1907",kEnumType,4};
	VPL_ExtType _1906 = {"1906",kEnumType,4};
	VPL_ExtType _1905 = {"1905",kEnumType,4};
	VPL_ExtType _1904 = {"1904",kEnumType,4};
	VPL_ExtType _1903 = {"1903",kEnumType,4};
	VPL_ExtType _1902 = {"1902",kEnumType,4};
	VPL_ExtType _1901 = {"1901",kEnumType,4};
	VPL_ExtType _1900 = {"1900",kEnumType,4};
	VPL_ExtType _1899 = {"1899",kEnumType,4};
	VPL_ExtType _1898 = {"1898",kEnumType,4};
	VPL_ExtType _1897 = {"1897",kEnumType,4};
	VPL_ExtType _1896 = {"1896",kEnumType,4};
	VPL_ExtType _1895 = {"1895",kEnumType,4};
	VPL_ExtType _TabletProximityRec = {"TabletProximityRec",kStructureType,28};
	VPL_ExtType _TabletPointRec = {"TabletPointRec",kStructureType,32};
	VPL_ExtType _1894 = {"1894",kEnumType,4};
	VPL_ExtType _1893 = {"1893",kEnumType,4};
	VPL_ExtType _1892 = {"1892",kEnumType,4};
	VPL_ExtType _1891 = {"1891",kStructureType,8};
	VPL_ExtType _HICommand = {"HICommand",kStructureType,16};
	VPL_ExtType _1890 = {"1890",kEnumType,4};
	VPL_ExtType _1889 = {"1889",kEnumType,4};
	VPL_ExtType _1888 = {"1888",kEnumType,4};
	VPL_ExtType _1887 = {"1887",kEnumType,4};
	VPL_ExtType _1886 = {"1886",kEnumType,4};
	VPL_ExtType _1885 = {"1885",kEnumType,4};
	VPL_ExtType _1884 = {"1884",kEnumType,4};
	VPL_ExtType _1883 = {"1883",kEnumType,4};
	VPL_ExtType _1882 = {"1882",kEnumType,4};
	VPL_ExtType _1881 = {"1881",kEnumType,4};
	VPL_ExtType _1880 = {"1880",kEnumType,4};
	VPL_ExtType _1879 = {"1879",kEnumType,4};
	VPL_ExtType _1878 = {"1878",kEnumType,4};
	VPL_ExtType _1877 = {"1877",kEnumType,4};
	VPL_ExtType _1876 = {"1876",kEnumType,4};
	VPL_ExtType _1875 = {"1875",kEnumType,4};
	VPL_ExtType _1874 = {"1874",kEnumType,4};
	VPL_ExtType _1873 = {"1873",kEnumType,4};
	VPL_ExtType _1872 = {"1872",kEnumType,4};
	VPL_ExtType _1871 = {"1871",kEnumType,4};
	VPL_ExtType _1870 = {"1870",kEnumType,4};
	VPL_ExtType _1869 = {"1869",kEnumType,4};
	VPL_ExtType _HIPoint = {"HIPoint",kStructureType,8};
	VPL_ExtType _1868 = {"1868",kEnumType,4};
	VPL_ExtType _1867 = {"1867",kEnumType,4};
	VPL_ExtType _OpaqueEventLoopTimerRef = {"OpaqueEventLoopTimerRef",kStructureType,0};
	VPL_ExtType _OpaqueEventQueueRef = {"OpaqueEventQueueRef",kStructureType,0};
	VPL_ExtType _1866 = {"1866",kEnumType,4};
	VPL_ExtType _OpaqueEventLoopRef = {"OpaqueEventLoopRef",kStructureType,0};
	VPL_ExtType _1865 = {"1865",kEnumType,4};
	VPL_ExtType _1864 = {"1864",kEnumType,4};
	VPL_ExtType _EventTypeSpec = {"EventTypeSpec",kStructureType,8};
	VPL_ExtType _1863 = {"1863",kEnumType,4};
	VPL_ExtType _1862 = {"1862",kEnumType,4};
	VPL_ExtType _1861 = {"1861",kEnumType,4};
	VPL_ExtType _1860 = {"1860",kEnumType,4};
	VPL_ExtType _1859 = {"1859",kEnumType,4};
	VPL_ExtType _1858 = {"1858",kEnumType,4};
	VPL_ExtType _1857 = {"1857",kEnumType,4};
	VPL_ExtType _1856 = {"1856",kEnumType,4};
	VPL_ExtType _1855 = {"1855",kEnumType,4};
	VPL_ExtType _1854 = {"1854",kEnumType,4};
	VPL_ExtType _1853 = {"1853",kEnumType,4};
	VPL_ExtType _1852 = {"1852",kEnumType,4};
	VPL_ExtType _1851 = {"1851",kEnumType,4};
	VPL_ExtType _OpaqueThemeDrawingState = {"OpaqueThemeDrawingState",kStructureType,0};
	VPL_ExtType _1850 = {"1850",kEnumType,4};
	VPL_ExtType _ThemeWindowMetrics = {"ThemeWindowMetrics",kStructureType,12};
	VPL_ExtType _1849 = {"1849",kEnumType,4};
	VPL_ExtType _1848 = {"1848",kEnumType,4};
	VPL_ExtType _1847 = {"1847",kEnumType,4};
	VPL_ExtType _1846 = {"1846",kEnumType,4};
	VPL_ExtType _ThemeButtonDrawInfo = {"ThemeButtonDrawInfo",kStructureType,8};
	VPL_ExtType _1845 = {"1845",kEnumType,4};
	VPL_ExtType _1844 = {"1844",kEnumType,4};
	VPL_ExtType _1843 = {"1843",kEnumType,4};
	VPL_ExtType _1842 = {"1842",kEnumType,4};
	VPL_ExtType _1841 = {"1841",kEnumType,4};
	VPL_ExtType _1840 = {"1840",kEnumType,4};
	VPL_ExtType _1839 = {"1839",kEnumType,4};
	VPL_ExtType _1838 = {"1838",kEnumType,4};
	VPL_ExtType _1837 = {"1837",kEnumType,4};
	VPL_ExtType _1836 = {"1836",kEnumType,4};
	VPL_ExtType _1835 = {"1835",NULL,8};
	VPL_ExtType _ThemeTrackDrawInfo = {"ThemeTrackDrawInfo",kStructureType,40};
	VPL_ExtType _ProgressTrackInfo = {"ProgressTrackInfo",kStructureType,4};
	VPL_ExtType _SliderTrackInfo = {"SliderTrackInfo",kStructureType,4};
	VPL_ExtType _ScrollBarTrackInfo = {"ScrollBarTrackInfo",kStructureType,8};
	VPL_ExtType _1834 = {"1834",kEnumType,4};
	VPL_ExtType _1833 = {"1833",kEnumType,4};
	VPL_ExtType _1832 = {"1832",kEnumType,4};
	VPL_ExtType _1831 = {"1831",kEnumType,4};
	VPL_ExtType _1830 = {"1830",kEnumType,4};
	VPL_ExtType _1829 = {"1829",kEnumType,4};
	VPL_ExtType _1828 = {"1828",kEnumType,4};
	VPL_ExtType _1827 = {"1827",kEnumType,4};
	VPL_ExtType _1826 = {"1826",kEnumType,4};
	VPL_ExtType _1825 = {"1825",kEnumType,4};
	VPL_ExtType _1824 = {"1824",kEnumType,4};
	VPL_ExtType _1823 = {"1823",kEnumType,4};
	VPL_ExtType _1822 = {"1822",kEnumType,4};
	VPL_ExtType _1821 = {"1821",kEnumType,4};
	VPL_ExtType _1820 = {"1820",kEnumType,4};
	VPL_ExtType _1819 = {"1819",kEnumType,4};
	VPL_ExtType _1818 = {"1818",kEnumType,4};
	VPL_ExtType _1817 = {"1817",kEnumType,4};
	VPL_ExtType _1816 = {"1816",kEnumType,4};
	VPL_ExtType _1815 = {"1815",kEnumType,4};
	VPL_ExtType _1814 = {"1814",kEnumType,4};
	VPL_ExtType _1813 = {"1813",kEnumType,4};
	VPL_ExtType _1812 = {"1812",kEnumType,4};
	VPL_ExtType _1811 = {"1811",kEnumType,4};
	VPL_ExtType _1810 = {"1810",kEnumType,4};
	VPL_ExtType _1809 = {"1809",kEnumType,4};
	VPL_ExtType _1808 = {"1808",kEnumType,4};
	VPL_ExtType _1807 = {"1807",kEnumType,4};
	VPL_ExtType _1806 = {"1806",kEnumType,4};
	VPL_ExtType _1805 = {"1805",kEnumType,4};
	VPL_ExtType _1804 = {"1804",kEnumType,4};
	VPL_ExtType _1803 = {"1803",kEnumType,4};
	VPL_ExtType _1802 = {"1802",kEnumType,4};
	VPL_ExtType _1801 = {"1801",kEnumType,4};
	VPL_ExtType _1800 = {"1800",kEnumType,4};
	VPL_ExtType _1799 = {"1799",kEnumType,4};
	VPL_ExtType _1798 = {"1798",kEnumType,4};
	VPL_ExtType _1797 = {"1797",kEnumType,4};
	VPL_ExtType _1796 = {"1796",kEnumType,4};
	VPL_ExtType _1795 = {"1795",kEnumType,4};
	VPL_ExtType _1794 = {"1794",kEnumType,4};
	VPL_ExtType _1793 = {"1793",kEnumType,4};
	VPL_ExtType _1792 = {"1792",kEnumType,4};
	VPL_ExtType _1791 = {"1791",kEnumType,4};
	VPL_ExtType _1790 = {"1790",kEnumType,4};
	VPL_ExtType _1789 = {"1789",kEnumType,4};
	VPL_ExtType _1788 = {"1788",kEnumType,4};
	VPL_ExtType _1787 = {"1787",kEnumType,4};
	VPL_ExtType _1786 = {"1786",kEnumType,4};
	VPL_ExtType _1785 = {"1785",kEnumType,4};
	VPL_ExtType _OpaqueWindowGroupRef = {"OpaqueWindowGroupRef",kStructureType,0};
	VPL_ExtType _1784 = {"1784",kEnumType,4};
	VPL_ExtType _1783 = {"1783",NULL,4};
	VPL_ExtType _WindowDefSpec = {"WindowDefSpec",kStructureType,8};
	VPL_ExtType _1782 = {"1782",kEnumType,4};
	VPL_ExtType _WStateData = {"WStateData",kStructureType,16};
	VPL_ExtType _1781 = {"1781",kEnumType,4};
	VPL_ExtType _1780 = {"1780",kEnumType,4};
	VPL_ExtType _1779 = {"1779",kStructureType,8};
	VPL_ExtType _1778 = {"1778",kStructureType,4};
	VPL_ExtType _1777 = {"1777",NULL,8};
	VPL_ExtType _BasicWindowDescription = {"BasicWindowDescription",kStructureType,44};
	VPL_ExtType _1776 = {"1776",kEnumType,4};
	VPL_ExtType _1775 = {"1775",kEnumType,4};
	VPL_ExtType _WinCTab = {"WinCTab",kStructureType,68};
	VPL_ExtType _1774 = {"1774",kEnumType,4};
	VPL_ExtType _1773 = {"1773",kEnumType,4};
	VPL_ExtType _1772 = {"1772",kEnumType,4};
	VPL_ExtType _1771 = {"1771",kEnumType,4};
	VPL_ExtType _1770 = {"1770",kEnumType,4};
	VPL_ExtType _1769 = {"1769",kEnumType,4};
	VPL_ExtType _1768 = {"1768",kEnumType,4};
	VPL_ExtType _1767 = {"1767",kEnumType,4};
	VPL_ExtType _1766 = {"1766",kEnumType,4};
	VPL_ExtType _1765 = {"1765",kEnumType,4};
	VPL_ExtType _1764 = {"1764",kEnumType,4};
	VPL_ExtType _1763 = {"1763",kEnumType,4};
	VPL_ExtType _1762 = {"1762",kEnumType,4};
	VPL_ExtType _1761 = {"1761",kEnumType,4};
	VPL_ExtType _1760 = {"1760",kEnumType,4};
	VPL_ExtType _1759 = {"1759",kEnumType,4};
	VPL_ExtType _GetGrowImageRegionRec = {"GetGrowImageRegionRec",kStructureType,12};
	VPL_ExtType _MeasureWindowTitleRec = {"MeasureWindowTitleRec",kStructureType,8};
	VPL_ExtType _SetupWindowProxyDragImageRec = {"SetupWindowProxyDragImageRec",kStructureType,12};
	VPL_ExtType _GetWindowRegionRec = {"GetWindowRegionRec",kStructureType,8};
	VPL_ExtType _1758 = {"1758",kEnumType,4};
	VPL_ExtType _1757 = {"1757",kEnumType,4};
	VPL_ExtType _1756 = {"1756",kEnumType,4};
	VPL_ExtType _1755 = {"1755",kEnumType,4};
	VPL_ExtType _1754 = {"1754",kEnumType,4};
	VPL_ExtType _1753 = {"1753",kEnumType,4};
	VPL_ExtType _1752 = {"1752",kEnumType,4};
	VPL_ExtType _1751 = {"1751",kEnumType,4};
	VPL_ExtType _1750 = {"1750",kEnumType,4};
	VPL_ExtType _1749 = {"1749",kEnumType,4};
	VPL_ExtType _1748 = {"1748",kEnumType,4};
	VPL_ExtType _1747 = {"1747",kEnumType,4};
	VPL_ExtType _1746 = {"1746",kEnumType,4};
	VPL_ExtType _1745 = {"1745",kEnumType,4};
	VPL_ExtType _1744 = {"1744",kEnumType,4};
	VPL_ExtType _1743 = {"1743",kEnumType,4};
	VPL_ExtType _1742 = {"1742",kEnumType,4};
	VPL_ExtType _1741 = {"1741",kEnumType,4};
	VPL_ExtType _1740 = {"1740",kEnumType,4};
	VPL_ExtType _1739 = {"1739",kEnumType,4};
	VPL_ExtType _ControlKind = {"ControlKind",kStructureType,8};
	VPL_ExtType _ControlID = {"ControlID",kStructureType,8};
	VPL_ExtType _1738 = {"1738",NULL,4};
	VPL_ExtType _ControlDefSpec = {"ControlDefSpec",kStructureType,8};
	VPL_ExtType _1737 = {"1737",kEnumType,4};
	VPL_ExtType _1736 = {"1736",kEnumType,4};
	VPL_ExtType _1735 = {"1735",kEnumType,4};
	VPL_ExtType _ControlClickActivationRec = {"ControlClickActivationRec",kStructureType,12};
	VPL_ExtType _ControlContextualMenuClickRec = {"ControlContextualMenuClickRec",kStructureType,8};
	VPL_ExtType _ControlSetCursorRec = {"ControlSetCursorRec",kStructureType,8};
	VPL_ExtType _ControlGetRegionRec = {"ControlGetRegionRec",kStructureType,8};
	VPL_ExtType _ControlApplyTextColorRec = {"ControlApplyTextColorRec",kStructureType,4};
	VPL_ExtType _ControlBackgroundRec = {"ControlBackgroundRec",kStructureType,4};
	VPL_ExtType _ControlCalcSizeRec = {"ControlCalcSizeRec",kStructureType,8};
	VPL_ExtType _ControlDataAccessRec = {"ControlDataAccessRec",kStructureType,16};
	VPL_ExtType _ControlKeyDownRec = {"ControlKeyDownRec",kStructureType,8};
	VPL_ExtType _ControlTrackingRec = {"ControlTrackingRec",kStructureType,12};
	VPL_ExtType _1734 = {"1734",kEnumType,4};
	VPL_ExtType _IndicatorDragConstraint = {"IndicatorDragConstraint",kStructureType,20};
	VPL_ExtType _1733 = {"1733",kEnumType,4};
	VPL_ExtType _1732 = {"1732",kEnumType,4};
	VPL_ExtType _1731 = {"1731",kEnumType,4};
	VPL_ExtType _1730 = {"1730",kEnumType,4};
	VPL_ExtType _1729 = {"1729",kEnumType,4};
	VPL_ExtType _1728 = {"1728",kEnumType,4};
	VPL_ExtType _1727 = {"1727",kEnumType,4};
	VPL_ExtType _ControlFontStyleRec = {"ControlFontStyleRec",kStructureType,28};
	VPL_ExtType _1726 = {"1726",kEnumType,4};
	VPL_ExtType _1725 = {"1725",kEnumType,4};
	VPL_ExtType _1724 = {"1724",kEnumType,4};
	VPL_ExtType _1723 = {"1723",kEnumType,4};
	VPL_ExtType _1722 = {"1722",kEnumType,4};
	VPL_ExtType _1721 = {"1721",NULL,4};
	VPL_ExtType _ControlButtonContentInfo = {"ControlButtonContentInfo",kStructureType,8};
	VPL_ExtType _1720 = {"1720",kEnumType,4};
	VPL_ExtType _1719 = {"1719",kEnumType,4};
	VPL_ExtType _1718 = {"1718",kEnumType,4};
	VPL_ExtType _1717 = {"1717",kEnumType,4};
	VPL_ExtType _1716 = {"1716",kEnumType,4};
	VPL_ExtType _1715 = {"1715",kEnumType,4};
	VPL_ExtType _1714 = {"1714",kEnumType,4};
	VPL_ExtType _1713 = {"1713",kEnumType,4};
	VPL_ExtType _CtlCTab = {"CtlCTab",kStructureType,56};
	VPL_ExtType _1712 = {"1712",kEnumType,4};
	VPL_ExtType _OpaqueControlRef = {"OpaqueControlRef",kStructureType,0};
	VPL_ExtType _ControlTemplate = {"ControlTemplate",kStructureType,280};
	VPL_ExtType _1711 = {"1711",kEnumType,4};
	VPL_ExtType _IconFamilyResource = {"IconFamilyResource",kStructureType,20};
	VPL_ExtType _IconFamilyElement = {"IconFamilyElement",kStructureType,12};
	VPL_ExtType _1710 = {"1710",kEnumType,4};
	VPL_ExtType _1709 = {"1709",kEnumType,4};
	VPL_ExtType _1708 = {"1708",kEnumType,4};
	VPL_ExtType _1707 = {"1707",kEnumType,4};
	VPL_ExtType _1706 = {"1706",kEnumType,4};
	VPL_ExtType _1705 = {"1705",kEnumType,4};
	VPL_ExtType _1704 = {"1704",kEnumType,4};
	VPL_ExtType _1703 = {"1703",kEnumType,4};
	VPL_ExtType _1702 = {"1702",kEnumType,4};
	VPL_ExtType _1701 = {"1701",kEnumType,4};
	VPL_ExtType _1700 = {"1700",kEnumType,4};
	VPL_ExtType _1699 = {"1699",kEnumType,4};
	VPL_ExtType _1698 = {"1698",kEnumType,4};
	VPL_ExtType _1697 = {"1697",kEnumType,4};
	VPL_ExtType _1696 = {"1696",kEnumType,4};
	VPL_ExtType _1695 = {"1695",kEnumType,4};
	VPL_ExtType _OpaqueIconRef = {"OpaqueIconRef",kStructureType,0};
	VPL_ExtType _CIcon = {"CIcon",kStructureType,92};
	VPL_ExtType _1694 = {"1694",kEnumType,4};
	VPL_ExtType _1693 = {"1693",kEnumType,4};
	VPL_ExtType _1692 = {"1692",kEnumType,4};
	VPL_ExtType _1691 = {"1691",kEnumType,4};
	VPL_ExtType _1690 = {"1690",kEnumType,4};
	VPL_ExtType _1689 = {"1689",kEnumType,4};
	VPL_ExtType _1688 = {"1688",kEnumType,4};
	VPL_ExtType _1687 = {"1687",kEnumType,4};
	VPL_ExtType _1686 = {"1686",kEnumType,4};
	VPL_ExtType _1685 = {"1685",kEnumType,4};
	VPL_ExtType _1684 = {"1684",kEnumType,4};
	VPL_ExtType _1683 = {"1683",kEnumType,4};
	VPL_ExtType _1682 = {"1682",kEnumType,4};
	VPL_ExtType _1681 = {"1681",kEnumType,4};
	VPL_ExtType _1680 = {"1680",kEnumType,4};
	VPL_ExtType _1679 = {"1679",kEnumType,4};
	VPL_ExtType _1678 = {"1678",kEnumType,4};
	VPL_ExtType _PromiseHFSFlavor = {"PromiseHFSFlavor",kStructureType,16};
	VPL_ExtType _HFSFlavor = {"HFSFlavor",kStructureType,84};
	VPL_ExtType _1677 = {"1677",kEnumType,4};
	VPL_ExtType _1676 = {"1676",kEnumType,4};
	VPL_ExtType _1675 = {"1675",kEnumType,4};
	VPL_ExtType _1674 = {"1674",kEnumType,4};
	VPL_ExtType _1673 = {"1673",kEnumType,4};
	VPL_ExtType _1672 = {"1672",kEnumType,4};
	VPL_ExtType _1671 = {"1671",kEnumType,4};
	VPL_ExtType _1670 = {"1670",kEnumType,4};
	VPL_ExtType _1669 = {"1669",kEnumType,4};
	VPL_ExtType _1668 = {"1668",kEnumType,4};
	VPL_ExtType _1667 = {"1667",kEnumType,4};
	VPL_ExtType _1666 = {"1666",kEnumType,4};
	VPL_ExtType _1665 = {"1665",kEnumType,4};
	VPL_ExtType _1664 = {"1664",kEnumType,4};
	VPL_ExtType _OpaqueDragRef = {"OpaqueDragRef",kStructureType,0};
	VPL_ExtType _NMRec = {"NMRec",kStructureType,36};
	VPL_ExtType _1663 = {"1663",NULL,256};
	VPL_ExtType _HMMessageRecord = {"HMMessageRecord",kStructureType,260};
	VPL_ExtType _HMStringResType = {"HMStringResType",kStructureType,4};
	VPL_ExtType _1662 = {"1662",kEnumType,4};
	VPL_ExtType _1661 = {"1661",kEnumType,4};
	VPL_ExtType _1660 = {"1660",kEnumType,4};
	VPL_ExtType _1659 = {"1659",kEnumType,4};
	VPL_ExtType _1658 = {"1658",kEnumType,4};
	VPL_ExtType _1657 = {"1657",kEnumType,4};
	VPL_ExtType _1656 = {"1656",kEnumType,4};
	VPL_ExtType _1655 = {"1655",kEnumType,4};
	VPL_ExtType _1654 = {"1654",kEnumType,4};
	VPL_ExtType _1653 = {"1653",kEnumType,4};
	VPL_ExtType _1652 = {"1652",kEnumType,4};
	VPL_ExtType _1651 = {"1651",kEnumType,4};
	VPL_ExtType _TextStyle = {"TextStyle",kStructureType,16};
	VPL_ExtType _TEStyleRec = {"TEStyleRec",kStructureType,32024};
	VPL_ExtType _NullStRec = {"NullStRec",kStructureType,8};
	VPL_ExtType _StScrpRec = {"StScrpRec",kStructureType,38428};
	VPL_ExtType _ScrpSTElement = {"ScrpSTElement",kStructureType,24};
	VPL_ExtType _LHElement = {"LHElement",kStructureType,4};
	VPL_ExtType _STElement = {"STElement",kStructureType,20};
	VPL_ExtType _StyleRun = {"StyleRun",kStructureType,4};
	VPL_ExtType _1650 = {"1650",kEnumType,4};
	VPL_ExtType _1649 = {"1649",kEnumType,4};
	VPL_ExtType _1648 = {"1648",kEnumType,4};
	VPL_ExtType _1647 = {"1647",kEnumType,4};
	VPL_ExtType _1646 = {"1646",kEnumType,4};
	VPL_ExtType _1645 = {"1645",kEnumType,4};
	VPL_ExtType _1644 = {"1644",kEnumType,4};
	VPL_ExtType _1643 = {"1643",kEnumType,4};
	VPL_ExtType _1642 = {"1642",kEnumType,4};
	VPL_ExtType _1641 = {"1641",kEnumType,4};
	VPL_ExtType _TERec = {"TERec",kStructureType,32104};
	VPL_ExtType _ContextualMenuInterfaceStruct = {"ContextualMenuInterfaceStruct",kStructureType,28};
	VPL_ExtType _1640 = {"1640",kEnumType,4};
	VPL_ExtType _1639 = {"1639",kEnumType,4};
	VPL_ExtType _1638 = {"1638",kEnumType,4};
	VPL_ExtType _1637 = {"1637",kEnumType,4};
	VPL_ExtType _1636 = {"1636",kEnumType,4};
	VPL_ExtType _1635 = {"1635",NULL,4};
	VPL_ExtType _MenuDefSpec = {"MenuDefSpec",kStructureType,8};
	VPL_ExtType _OpaqueMenuLayoutRef = {"OpaqueMenuLayoutRef",kStructureType,0};
	VPL_ExtType _1634 = {"1634",kEnumType,4};
	VPL_ExtType _1633 = {"1633",kEnumType,4};
	VPL_ExtType _MenuItemDataRec = {"MenuItemDataRec",kStructureType,80};
	VPL_ExtType _1632 = {"1632",kEnumType,4};
	VPL_ExtType _1631 = {"1631",kEnumType,4};
	VPL_ExtType _MDEFDrawItemsData = {"MDEFDrawItemsData",kStructureType,12};
	VPL_ExtType _MDEFFindItemData = {"MDEFFindItemData",kStructureType,28};
	VPL_ExtType _MDEFDrawData = {"MDEFDrawData",kStructureType,28};
	VPL_ExtType _MDEFHiliteItemData = {"MDEFHiliteItemData",kStructureType,8};
	VPL_ExtType _MenuTrackingData = {"MenuTrackingData",kStructureType,24};
	VPL_ExtType _MenuCRsrc = {"MenuCRsrc",kStructureType,44};
	VPL_ExtType _MCEntry = {"MCEntry",kStructureType,40};
	VPL_ExtType _OpaqueMenuHandle = {"OpaqueMenuHandle",kStructureType,0};
	VPL_ExtType _1630 = {"1630",kEnumType,4};
	VPL_ExtType _1629 = {"1629",kEnumType,4};
	VPL_ExtType _1628 = {"1628",kEnumType,4};
	VPL_ExtType _1627 = {"1627",kEnumType,4};
	VPL_ExtType _1626 = {"1626",kEnumType,4};
	VPL_ExtType _1625 = {"1625",kEnumType,4};
	VPL_ExtType _1624 = {"1624",kEnumType,4};
	VPL_ExtType _1623 = {"1623",kEnumType,4};
	VPL_ExtType _1622 = {"1622",kEnumType,4};
	VPL_ExtType _1621 = {"1621",kEnumType,4};
	VPL_ExtType _1620 = {"1620",kEnumType,4};
	VPL_ExtType _1619 = {"1619",kEnumType,4};
	VPL_ExtType _1618 = {"1618",kEnumType,4};
	VPL_ExtType _1617 = {"1617",kEnumType,4};
	VPL_ExtType _1616 = {"1616",kEnumType,4};
	VPL_ExtType _1615 = {"1615",kEnumType,4};
	VPL_ExtType _1614 = {"1614",kEnumType,4};
	VPL_ExtType _SizeResourceRec = {"SizeResourceRec",kStructureType,12};
	VPL_ExtType _ProcessInfoExtendedRec = {"ProcessInfoExtendedRec",kStructureType,68};
	VPL_ExtType _ProcessInfoRec = {"ProcessInfoRec",kStructureType,60};
	VPL_ExtType _1613 = {"1613",kEnumType,4};
	VPL_ExtType _1612 = {"1612",kEnumType,4};
	VPL_ExtType _LaunchParamBlockRec = {"LaunchParamBlockRec",kStructureType,44};
	VPL_ExtType _AppParameters = {"AppParameters",kStructureType,28};
	VPL_ExtType _1611 = {"1611",kEnumType,4};
	VPL_ExtType _1610 = {"1610",kEnumType,4};
	VPL_ExtType _ProcessSerialNumber = {"ProcessSerialNumber",kStructureType,8};
	VPL_ExtType _OpaqueEventRef = {"OpaqueEventRef",kStructureType,0};
	VPL_ExtType _EvQEl = {"EvQEl",kStructureType,24};
	VPL_ExtType _1609 = {"1609",kEnumType,4};
	VPL_ExtType _EventRecord = {"EventRecord",kStructureType,20};
	VPL_ExtType _1608 = {"1608",kEnumType,4};
	VPL_ExtType _1607 = {"1607",kEnumType,4};
	VPL_ExtType _1606 = {"1606",kEnumType,4};
	VPL_ExtType _1605 = {"1605",kEnumType,4};
	VPL_ExtType _1604 = {"1604",kEnumType,4};
	VPL_ExtType _1603 = {"1603",kEnumType,4};
	VPL_ExtType _1602 = {"1602",kEnumType,4};
	VPL_ExtType _1601 = {"1601",kEnumType,4};
	VPL_ExtType _1600 = {"1600",kEnumType,4};
	VPL_ExtType _LSLaunchURLSpec = {"LSLaunchURLSpec",kStructureType,20};
	VPL_ExtType _LSLaunchFSRefSpec = {"LSLaunchFSRefSpec",kStructureType,24};
	VPL_ExtType _1599 = {"1599",kEnumType,4};
	VPL_ExtType _1598 = {"1598",kEnumType,4};
	VPL_ExtType _LSItemInfoRecord = {"LSItemInfoRecord",kStructureType,24};
	VPL_ExtType _1597 = {"1597",kEnumType,4};
	VPL_ExtType _1596 = {"1596",kEnumType,4};
	VPL_ExtType _1595 = {"1595",kEnumType,4};
	VPL_ExtType _1594 = {"1594",kEnumType,4};
	VPL_ExtType _1593 = {"1593",kEnumType,4};
	VPL_ExtType _1592 = {"1592",kEnumType,4};
	VPL_ExtType _1591 = {"1591",kEnumType,4};
	VPL_ExtType _1590 = {"1590",kEnumType,4};
	VPL_ExtType _1589 = {"1589",kEnumType,4};
	VPL_ExtType _DelimiterInfo = {"DelimiterInfo",kStructureType,4};
	VPL_ExtType _SpeechXtndData = {"SpeechXtndData",kStructureType,8};
	VPL_ExtType _PhonemeDescriptor = {"PhonemeDescriptor",kStructureType,60};
	VPL_ExtType _PhonemeInfo = {"PhonemeInfo",kStructureType,56};
	VPL_ExtType _SpeechVersionInfo = {"SpeechVersionInfo",kStructureType,20};
	VPL_ExtType _SpeechErrorInfo = {"SpeechErrorInfo",kStructureType,16};
	VPL_ExtType _SpeechStatusInfo = {"SpeechStatusInfo",kStructureType,12};
	VPL_ExtType _VoiceFileInfo = {"VoiceFileInfo",kStructureType,76};
	VPL_ExtType _VoiceDescription = {"VoiceDescription",kStructureType,364};
	VPL_ExtType _1588 = {"1588",kEnumType,4};
	VPL_ExtType _VoiceSpec = {"VoiceSpec",kStructureType,8};
	VPL_ExtType _SpeechChannelRecord = {"SpeechChannelRecord",kStructureType,4};
	VPL_ExtType _1587 = {"1587",kEnumType,4};
	VPL_ExtType _1586 = {"1586",kEnumType,4};
	VPL_ExtType _1585 = {"1585",kEnumType,4};
	VPL_ExtType _1584 = {"1584",kEnumType,4};
	VPL_ExtType _1583 = {"1583",kEnumType,4};
	VPL_ExtType _1582 = {"1582",kEnumType,4};
	VPL_ExtType _1581 = {"1581",kEnumType,4};
	VPL_ExtType _1580 = {"1580",kEnumType,4};
	VPL_ExtType _1579 = {"1579",kEnumType,4};
	VPL_ExtType _HomographDicInfoRec = {"HomographDicInfoRec",kStructureType,8};
	VPL_ExtType _1578 = {"1578",kEnumType,4};
	VPL_ExtType _1577 = {"1577",kEnumType,4};
	VPL_ExtType _1576 = {"1576",kEnumType,4};
	VPL_ExtType _1575 = {"1575",kEnumType,4};
	VPL_ExtType _1574 = {"1574",kEnumType,4};
	VPL_ExtType _1573 = {"1573",kEnumType,4};
	VPL_ExtType _1572 = {"1572",kEnumType,4};
	VPL_ExtType _MorphemeTextRange = {"MorphemeTextRange",kStructureType,8};
	VPL_ExtType _1571 = {"1571",kEnumType,4};
	VPL_ExtType _1570 = {"1570",kEnumType,4};
	VPL_ExtType _1569 = {"1569",kEnumType,4};
	VPL_ExtType _1568 = {"1568",kEnumType,4};
	VPL_ExtType _1567 = {"1567",kEnumType,4};
	VPL_ExtType _LAMorphemesArray = {"LAMorphemesArray",kStructureType,32};
	VPL_ExtType _LAMorphemeRec = {"LAMorphemeRec",kStructureType,20};
	VPL_ExtType _OpaqueLAContextRef = {"OpaqueLAContextRef",kStructureType,0};
	VPL_ExtType _OpaqueLAEnvironmentRef = {"OpaqueLAEnvironmentRef",kStructureType,0};
	VPL_ExtType _DictionaryAttributeTable = {"DictionaryAttributeTable",kStructureType,4};
	VPL_ExtType _DictionaryInformation = {"DictionaryInformation",kStructureType,88};
	VPL_ExtType _1566 = {"1566",kEnumType,4};
	VPL_ExtType _1565 = {"1565",kEnumType,4};
	VPL_ExtType _1564 = {"1564",kEnumType,4};
	VPL_ExtType _1563 = {"1563",kEnumType,4};
	VPL_ExtType _1562 = {"1562",kEnumType,4};
	VPL_ExtType _1561 = {"1561",kEnumType,4};
	VPL_ExtType _DCMDictionaryHeader = {"DCMDictionaryHeader",kStructureType,76};
	VPL_ExtType _OpaqueDCMFoundRecordIterator = {"OpaqueDCMFoundRecordIterator",kStructureType,0};
	VPL_ExtType _OpaqueDCMObjectIterator = {"OpaqueDCMObjectIterator",kStructureType,0};
	VPL_ExtType _OpaqueDCMObjectRef = {"OpaqueDCMObjectRef",kStructureType,0};
	VPL_ExtType _OpaqueDCMObjectID = {"OpaqueDCMObjectID",kStructureType,0};
	VPL_ExtType _1560 = {"1560",kEnumType,4};
	VPL_ExtType _1559 = {"1559",kEnumType,4};
	VPL_ExtType _1558 = {"1558",kEnumType,4};
	VPL_ExtType _1557 = {"1557",kEnumType,4};
	VPL_ExtType _1556 = {"1556",kEnumType,4};
	VPL_ExtType _1555 = {"1555",kEnumType,4};
	VPL_ExtType _1554 = {"1554",kEnumType,4};
	VPL_ExtType _1553 = {"1553",kEnumType,4};
	VPL_ExtType _1552 = {"1552",kEnumType,4};
	VPL_ExtType _1551 = {"1551",kEnumType,4};
	VPL_ExtType _1550 = {"1550",kEnumType,4};
	VPL_ExtType _PMLanguageInfo = {"PMLanguageInfo",kStructureType,100};
	VPL_ExtType _PMResolution = {"PMResolution",kStructureType,16};
	VPL_ExtType _PMRect = {"PMRect",kStructureType,32};
	VPL_ExtType _1549 = {"1549",kEnumType,4};
	VPL_ExtType _1548 = {"1548",kEnumType,4};
	VPL_ExtType _1547 = {"1547",kEnumType,4};
	VPL_ExtType _1546 = {"1546",kEnumType,4};
	VPL_ExtType _1545 = {"1545",kEnumType,4};
	VPL_ExtType _1544 = {"1544",kEnumType,4};
	VPL_ExtType _1543 = {"1543",kEnumType,4};
	VPL_ExtType _1542 = {"1542",kEnumType,4};
	VPL_ExtType _1541 = {"1541",kEnumType,4};
	VPL_ExtType _1540 = {"1540",kEnumType,4};
	VPL_ExtType _1539 = {"1539",kEnumType,4};
	VPL_ExtType _1538 = {"1538",kEnumType,4};
	VPL_ExtType _1537 = {"1537",kEnumType,4};
	VPL_ExtType _1536 = {"1536",kEnumType,4};
	VPL_ExtType _1535 = {"1535",kEnumType,4};
	VPL_ExtType _1534 = {"1534",kEnumType,4};
	VPL_ExtType _1533 = {"1533",kEnumType,4};
	VPL_ExtType _1532 = {"1532",kEnumType,4};
	VPL_ExtType _1531 = {"1531",kEnumType,4};
	VPL_ExtType _1530 = {"1530",kEnumType,4};
	VPL_ExtType _OpaquePMPrinter = {"OpaquePMPrinter",kStructureType,0};
	VPL_ExtType _OpaquePMPrintSession = {"OpaquePMPrintSession",kStructureType,0};
	VPL_ExtType _OpaquePMPrintContext = {"OpaquePMPrintContext",kStructureType,0};
	VPL_ExtType _OpaquePMPageFormat = {"OpaquePMPageFormat",kStructureType,0};
	VPL_ExtType _OpaquePMPrintSettings = {"OpaquePMPrintSettings",kStructureType,0};
	VPL_ExtType _OpaquePMDialog = {"OpaquePMDialog",kStructureType,0};
	VPL_ExtType _OpaqueFBCSearchSession = {"OpaqueFBCSearchSession",kStructureType,0};
	VPL_ExtType _1529 = {"1529",kEnumType,4};
	VPL_ExtType _1528 = {"1528",kEnumType,4};
	VPL_ExtType _OpaqueFNSFontProfile = {"OpaqueFNSFontProfile",kStructureType,0};
	VPL_ExtType _1527 = {"1527",kEnumType,4};
	VPL_ExtType _OpaqueFNSFontReference = {"OpaqueFNSFontReference",kStructureType,0};
	VPL_ExtType _FNSSysInfo = {"FNSSysInfo",kStructureType,28};
	VPL_ExtType _1526 = {"1526",kEnumType,4};
	VPL_ExtType _1525 = {"1525",kEnumType,4};
	VPL_ExtType _DMProfileListEntryRec = {"DMProfileListEntryRec",kStructureType,16};
	VPL_ExtType _DisplayListEntryRec = {"DisplayListEntryRec",kStructureType,32};
	VPL_ExtType _1524 = {"1524",kEnumType,4};
	VPL_ExtType _1523 = {"1523",kEnumType,4};
	VPL_ExtType _DMMakeAndModelRec = {"DMMakeAndModelRec",kStructureType,32};
	VPL_ExtType _DependentNotifyRec = {"DependentNotifyRec",kStructureType,32};
	VPL_ExtType _DMDisplayModeListEntryRec = {"DMDisplayModeListEntryRec",kStructureType,32};
	VPL_ExtType _DMDepthInfoBlockRec = {"DMDepthInfoBlockRec",kStructureType,20};
	VPL_ExtType _DMDepthInfoRec = {"DMDepthInfoRec",kStructureType,20};
	VPL_ExtType _AVLocationRec = {"AVLocationRec",kStructureType,4};
	VPL_ExtType _DMComponentListEntryRec = {"DMComponentListEntryRec",kStructureType,68};
	VPL_ExtType _DMDisplayTimingInfoRec = {"DMDisplayTimingInfoRec",kStructureType,80};
	VPL_ExtType _1522 = {"1522",kEnumType,4};
	VPL_ExtType _1521 = {"1521",kEnumType,4};
	VPL_ExtType _1520 = {"1520",kEnumType,4};
	VPL_ExtType _1519 = {"1519",kEnumType,4};
	VPL_ExtType _1518 = {"1518",kEnumType,4};
	VPL_ExtType _1517 = {"1517",kEnumType,4};
	VPL_ExtType _1516 = {"1516",kEnumType,4};
	VPL_ExtType _1515 = {"1515",kEnumType,4};
	VPL_ExtType _1514 = {"1514",kEnumType,4};
	VPL_ExtType _1513 = {"1513",kEnumType,4};
	VPL_ExtType _1512 = {"1512",kEnumType,4};
	VPL_ExtType _1511 = {"1511",kEnumType,4};
	VPL_ExtType _1510 = {"1510",kEnumType,4};
	VPL_ExtType _1509 = {"1509",kEnumType,4};
	VPL_ExtType _1508 = {"1508",kEnumType,4};
	VPL_ExtType _1507 = {"1507",kEnumType,4};
	VPL_ExtType _1506 = {"1506",kEnumType,4};
	VPL_ExtType _1505 = {"1505",kEnumType,4};
	VPL_ExtType _1504 = {"1504",kEnumType,4};
	VPL_ExtType _1503 = {"1503",kEnumType,4};
	VPL_ExtType _VDCommunicationInfoRec = {"VDCommunicationInfoRec",kStructureType,48};
	VPL_ExtType _VDCommunicationRec = {"VDCommunicationRec",kStructureType,64};
	VPL_ExtType _1502 = {"1502",kEnumType,4};
	VPL_ExtType _VDDetailedTimingRec = {"VDDetailedTimingRec",kStructureType,148};
	VPL_ExtType _1501 = {"1501",kEnumType,4};
	VPL_ExtType _1500 = {"1500",kEnumType,4};
	VPL_ExtType _1499 = {"1499",kEnumType,4};
	VPL_ExtType _1498 = {"1498",kEnumType,4};
	VPL_ExtType _1497 = {"1497",kEnumType,4};
	VPL_ExtType _1496 = {"1496",kEnumType,4};
	VPL_ExtType _1495 = {"1495",kEnumType,4};
	VPL_ExtType _VDDisplayTimingRangeRec = {"VDDisplayTimingRangeRec",kStructureType,232};
	VPL_ExtType _1494 = {"1494",kEnumType,4};
	VPL_ExtType _VDDDCBlockRec = {"VDDDCBlockRec",kStructureType,144};
	VPL_ExtType _VDPrivateSelectorRec = {"VDPrivateSelectorRec",kStructureType,20};
	VPL_ExtType _VDPrivateSelectorDataRec = {"VDPrivateSelectorDataRec",kStructureType,16};
	VPL_ExtType _VDPowerStateRec = {"VDPowerStateRec",kStructureType,16};
	VPL_ExtType _VDConvolutionInfoRec = {"VDConvolutionInfoRec",kStructureType,20};
	VPL_ExtType _VDHardwareCursorDrawStateRec = {"VDHardwareCursorDrawStateRec",kStructureType,24};
	VPL_ExtType _VDSupportsHardwareCursorRec = {"VDSupportsHardwareCursorRec",kStructureType,12};
	VPL_ExtType _VDDrawHardwareCursorRec = {"VDDrawHardwareCursorRec",kStructureType,20};
	VPL_ExtType _VDSetHardwareCursorRec = {"VDSetHardwareCursorRec",kStructureType,12};
	VPL_ExtType _VDRetrieveGammaRec = {"VDRetrieveGammaRec",kStructureType,8};
	VPL_ExtType _VDGetGammaListRec = {"VDGetGammaListRec",kStructureType,16};
	VPL_ExtType _VDGammaInfoRec = {"VDGammaInfoRec",kStructureType,16};
	VPL_ExtType _VDVideoParametersInfoRec = {"VDVideoParametersInfoRec",kStructureType,24};
	VPL_ExtType _VDResolutionInfoRec = {"VDResolutionInfoRec",kStructureType,32};
	VPL_ExtType _1493 = {"1493",kEnumType,4};
	VPL_ExtType _1492 = {"1492",kEnumType,4};
	VPL_ExtType _1491 = {"1491",kEnumType,4};
	VPL_ExtType _1490 = {"1490",kEnumType,4};
	VPL_ExtType _1489 = {"1489",kEnumType,4};
	VPL_ExtType _1488 = {"1488",kEnumType,4};
	VPL_ExtType _1487 = {"1487",kEnumType,4};
	VPL_ExtType _VDSyncInfoRec = {"VDSyncInfoRec",kStructureType,4};
	VPL_ExtType _VDDefMode = {"VDDefMode",kStructureType,4};
	VPL_ExtType _VDSettings = {"VDSettings",kStructureType,40};
	VPL_ExtType _VDSizeInfo = {"VDSizeInfo",kStructureType,8};
	VPL_ExtType _VDPageInfo = {"VDPageInfo",kStructureType,16};
	VPL_ExtType _1486 = {"1486",kEnumType,4};
	VPL_ExtType _1485 = {"1485",kEnumType,4};
	VPL_ExtType _1484 = {"1484",kEnumType,4};
	VPL_ExtType _1483 = {"1483",kEnumType,4};
	VPL_ExtType _VDMultiConnectInfoRec = {"VDMultiConnectInfoRec",kStructureType,20};
	VPL_ExtType _VDDisplayConnectInfoRec = {"VDDisplayConnectInfoRec",kStructureType,16};
	VPL_ExtType _VDTimingInfoRec = {"VDTimingInfoRec",kStructureType,20};
	VPL_ExtType _VDSwitchInfoRec = {"VDSwitchInfoRec",kStructureType,20};
	VPL_ExtType _VDBaseAddressInfoRec = {"VDBaseAddressInfoRec",kStructureType,16};
	VPL_ExtType _VDGammaRecord = {"VDGammaRecord",kStructureType,4};
	VPL_ExtType _VDSetEntryRecord = {"VDSetEntryRecord",kStructureType,8};
	VPL_ExtType _VDFlagRecord = {"VDFlagRecord",kStructureType,4};
	VPL_ExtType _VDGrayRecord = {"VDGrayRecord",kStructureType,4};
	VPL_ExtType _VDEntryRecord = {"VDEntryRecord",kStructureType,4};
	VPL_ExtType _VPBlock = {"VPBlock",kStructureType,44};
	VPL_ExtType _1482 = {"1482",kEnumType,4};
	VPL_ExtType _1481 = {"1481",kEnumType,4};
	VPL_ExtType _1480 = {"1480",kEnumType,4};
	VPL_ExtType _1479 = {"1479",kEnumType,4};
	VPL_ExtType _1478 = {"1478",kEnumType,4};
	VPL_ExtType _1477 = {"1477",kEnumType,4};
	VPL_ExtType _1476 = {"1476",kEnumType,4};
	VPL_ExtType _1475 = {"1475",kEnumType,4};
	VPL_ExtType _1474 = {"1474",kEnumType,4};
	VPL_ExtType _1473 = {"1473",kEnumType,4};
	VPL_ExtType _1472 = {"1472",kEnumType,4};
	VPL_ExtType _1471 = {"1471",kEnumType,4};
	VPL_ExtType _1470 = {"1470",kEnumType,4};
	VPL_ExtType _1469 = {"1469",kEnumType,4};
	VPL_ExtType _1468 = {"1468",kEnumType,4};
	VPL_ExtType _1467 = {"1467",kEnumType,4};
	VPL_ExtType _1466 = {"1466",kEnumType,4};
	VPL_ExtType _1465 = {"1465",kEnumType,4};
	VPL_ExtType _1464 = {"1464",kEnumType,4};
	VPL_ExtType _1463 = {"1463",kEnumType,4};
	VPL_ExtType _1462 = {"1462",kEnumType,4};
	VPL_ExtType _1461 = {"1461",kEnumType,4};
	VPL_ExtType _1460 = {"1460",kEnumType,4};
	VPL_ExtType _1459 = {"1459",kEnumType,4};
	VPL_ExtType _1458 = {"1458",kEnumType,4};
	VPL_ExtType _1457 = {"1457",kEnumType,4};
	VPL_ExtType _ATSUUnhighlightData = {"ATSUUnhighlightData",kStructureType,20};
	VPL_ExtType _ATSUBackgroundData = {"ATSUBackgroundData",NULL,16};
	VPL_ExtType _ATSUBackgroundColor = {"ATSUBackgroundColor",kStructureType,16};
	VPL_ExtType _1456 = {"1456",kEnumType,4};
	VPL_ExtType _1455 = {"1455",kEnumType,4};
	VPL_ExtType _ATSUGlyphInfoArray = {"ATSUGlyphInfoArray",kStructureType,36};
	VPL_ExtType _ATSUGlyphInfo = {"ATSUGlyphInfo",kStructureType,28};
	VPL_ExtType _1454 = {"1454",kEnumType,4};
	VPL_ExtType _1453 = {"1453",kEnumType,4};
	VPL_ExtType _1452 = {"1452",kEnumType,4};
	VPL_ExtType _1451 = {"1451",kEnumType,4};
	VPL_ExtType _1450 = {"1450",kEnumType,4};
	VPL_ExtType _ATSUCaret = {"ATSUCaret",kStructureType,16};
	VPL_ExtType _ATSUAttributeInfo = {"ATSUAttributeInfo",kStructureType,8};
	VPL_ExtType _1449 = {"1449",kEnumType,4};
	VPL_ExtType _OpaqueATSUFontFallbacks = {"OpaqueATSUFontFallbacks",kStructureType,0};
	VPL_ExtType _OpaqueATSUStyle = {"OpaqueATSUStyle",kStructureType,0};
	VPL_ExtType _OpaqueATSUTextLayout = {"OpaqueATSUTextLayout",kStructureType,0};
	VPL_ExtType _PictInfo = {"PictInfo",kStructureType,108};
	VPL_ExtType _FontSpec = {"FontSpec",kStructureType,28};
	VPL_ExtType _CommentSpec = {"CommentSpec",kStructureType,4};
	VPL_ExtType _1448 = {"1448",kEnumType,4};
	VPL_ExtType _1447 = {"1447",kEnumType,4};
	VPL_ExtType _1446 = {"1446",kEnumType,4};
	VPL_ExtType _Palette = {"Palette",kStructureType,36};
	VPL_ExtType _ColorInfo = {"ColorInfo",kStructureType,20};
	VPL_ExtType _1445 = {"1445",kEnumType,4};
	VPL_ExtType _FontRec = {"FontRec",kStructureType,28};
	VPL_ExtType _FamRec = {"FamRec",kStructureType,52};
	VPL_ExtType _WidthTable = {"WidthTable",kStructureType,1076};
	VPL_ExtType _KernTable = {"KernTable",kStructureType,4};
	VPL_ExtType _KernEntry = {"KernEntry",kStructureType,4};
	VPL_ExtType _KernPair = {"KernPair",kStructureType,4};
	VPL_ExtType _NameTable = {"NameTable",kStructureType,260};
	VPL_ExtType _StyleTable = {"StyleTable",kStructureType,60};
	VPL_ExtType _FontAssoc = {"FontAssoc",kStructureType,4};
	VPL_ExtType _AsscEntry = {"AsscEntry",kStructureType,8};
	VPL_ExtType _WidTable = {"WidTable",kStructureType,4};
	VPL_ExtType _WidEntry = {"WidEntry",kStructureType,4};
	VPL_ExtType _1444 = {"1444",kEnumType,4};
	VPL_ExtType _1443 = {"1443",kEnumType,4};
	VPL_ExtType _FMetricRec = {"FMetricRec",kStructureType,20};
	VPL_ExtType _FMOutput = {"FMOutput",kStructureType,28};
	VPL_ExtType _FMInput = {"FMInput",kStructureType,16};
	VPL_ExtType _1442 = {"1442",kEnumType,4};
	VPL_ExtType _1441 = {"1441",kEnumType,4};
	VPL_ExtType _1440 = {"1440",kEnumType,4};
	VPL_ExtType _1439 = {"1439",kEnumType,4};
	VPL_ExtType _1438 = {"1438",kEnumType,4};
	VPL_ExtType _1437 = {"1437",kEnumType,4};
	VPL_ExtType _1436 = {"1436",kEnumType,4};
	VPL_ExtType _1435 = {"1435",kEnumType,4};
	VPL_ExtType _1434 = {"1434",kEnumType,4};
	VPL_ExtType _1433 = {"1433",kEnumType,4};
	VPL_ExtType _OpaqueQDRegionBitsRef = {"OpaqueQDRegionBitsRef",kStructureType,0};
	VPL_ExtType _1432 = {"1432",kEnumType,4};
	VPL_ExtType _CursorInfo = {"CursorInfo",kStructureType,28};
	VPL_ExtType _1431 = {"1431",kEnumType,4};
	VPL_ExtType _1430 = {"1430",kEnumType,4};
	VPL_ExtType _1429 = {"1429",kEnumType,4};
	VPL_ExtType _1428 = {"1428",kEnumType,4};
	VPL_ExtType _CustomXFerRec = {"CustomXFerRec",kStructureType,40};
	VPL_ExtType _1427 = {"1427",kEnumType,4};
	VPL_ExtType _1426 = {"1426",kEnumType,4};
	VPL_ExtType _1425 = {"1425",kEnumType,4};
	VPL_ExtType _1424 = {"1424",kEnumType,4};
	VPL_ExtType _CursorImageRec = {"CursorImageRec",kStructureType,12};
	VPL_ExtType _1423 = {"1423",kEnumType,4};
	VPL_ExtType _OpenCPicParams = {"OpenCPicParams",kStructureType,24};
	VPL_ExtType _ReqListRec = {"ReqListRec",kStructureType,4};
	VPL_ExtType _CQDProcs = {"CQDProcs",kStructureType,80};
	VPL_ExtType _GrafVars = {"GrafVars",kStructureType,32};
	VPL_ExtType _GDevice = {"GDevice",kStructureType,68};
	VPL_ExtType _CProcRec = {"CProcRec",kStructureType,8};
	VPL_ExtType _SProcRec = {"SProcRec",kStructureType,8};
	VPL_ExtType _ITab = {"ITab",kStructureType,8};
	VPL_ExtType _GammaTbl = {"GammaTbl",kStructureType,16};
	VPL_ExtType _CCrsr = {"CCrsr",kStructureType,100};
	VPL_ExtType _PixPat = {"PixPat",kStructureType,32};
	VPL_ExtType _PixMap = {"PixMap",kStructureType,52};
	VPL_ExtType _1422 = {"1422",kEnumType,4};
	VPL_ExtType _1421 = {"1421",kEnumType,4};
	VPL_ExtType _MatchRec = {"MatchRec",kStructureType,12};
	VPL_ExtType _xColorSpec = {"xColorSpec",kStructureType,16};
	VPL_ExtType _ColorTable = {"ColorTable",kStructureType,20};
	VPL_ExtType _ColorSpec = {"ColorSpec",kStructureType,12};
	VPL_ExtType _RGBColor = {"RGBColor",kStructureType,8};
	VPL_ExtType _1420 = {"1420",kEnumType,4};
	VPL_ExtType _OpaqueGrafPtr = {"OpaqueGrafPtr",kStructureType,0};
	VPL_ExtType _OpaqueDialogPtr = {"OpaqueDialogPtr",kStructureType,0};
	VPL_ExtType _OpaqueWindowPtr = {"OpaqueWindowPtr",kStructureType,0};
	VPL_ExtType _QDProcs = {"QDProcs",kStructureType,52};
	VPL_ExtType _MacPolygon = {"MacPolygon",kStructureType,16};
	VPL_ExtType _Picture = {"Picture",kStructureType,12};
	VPL_ExtType _OpaqueRgnHandle = {"OpaqueRgnHandle",kStructureType,0};
	VPL_ExtType _PenState = {"PenState",kStructureType,20};
	VPL_ExtType _Cursor = {"Cursor",kStructureType,68};
	VPL_ExtType _BitMap = {"BitMap",kStructureType,16};
	VPL_ExtType _PrinterScalingStatus = {"PrinterScalingStatus",kStructureType,4};
	VPL_ExtType _PrinterFontStatus = {"PrinterFontStatus",kStructureType,8};
	VPL_ExtType _1419 = {"1419",kEnumType,4};
	VPL_ExtType _1418 = {"1418",kEnumType,4};
	VPL_ExtType _1417 = {"1417",kEnumType,4};
	VPL_ExtType _Pattern = {"Pattern",kStructureType,8};
	VPL_ExtType _1416 = {"1416",kEnumType,4};
	VPL_ExtType _1415 = {"1415",kEnumType,4};
	VPL_ExtType _1414 = {"1414",kEnumType,4};
	VPL_ExtType _1413 = {"1413",kEnumType,4};
	VPL_ExtType _1412 = {"1412",kEnumType,4};
	VPL_ExtType _1411 = {"1411",kEnumType,4};
	VPL_ExtType _1410 = {"1410",kEnumType,4};
	VPL_ExtType _1409 = {"1409",kEnumType,4};
	VPL_ExtType _1408 = {"1408",kEnumType,4};
	VPL_ExtType _1407 = {"1407",kEnumType,4};
	VPL_ExtType _FontInfo = {"FontInfo",kStructureType,8};
	VPL_ExtType _1406 = {"1406",kEnumType,4};
	VPL_ExtType _1405 = {"1405",kEnumType,4};
	VPL_ExtType _1404 = {"1404",kEnumType,4};
	VPL_ExtType _1403 = {"1403",kEnumType,4};
	VPL_ExtType _1402 = {"1402",kEnumType,4};
	VPL_ExtType _1401 = {"1401",kEnumType,4};
	VPL_ExtType _1400 = {"1400",kEnumType,4};
	VPL_ExtType _1399 = {"1399",kEnumType,4};
	VPL_ExtType _1398 = {"1398",NULL,32};
	VPL_ExtType _1397 = {"1397",kStructureType,32};
	VPL_ExtType _1396 = {"1396",kStructureType,24};
	VPL_ExtType _1395 = {"1395",kStructureType,20};
	VPL_ExtType _1394 = {"1394",kStructureType,8};
	VPL_ExtType _1393 = {"1393",kStructureType,12};
	VPL_ExtType _1392 = {"1392",kStructureType,8};
	VPL_ExtType _1391 = {"1391",kStructureType,28};
	VPL_ExtType _1390 = {"1390",kStructureType,24};
	VPL_ExtType _1389 = {"1389",kStructureType,4};
	VPL_ExtType _1388 = {"1388",NULL,12};
	VPL_ExtType _1387 = {"1387",kStructureType,12};
	VPL_ExtType _1386 = {"1386",kStructureType,12};
	VPL_ExtType _1385 = {"1385",kStructureType,12};
	VPL_ExtType _1384 = {"1384",kStructureType,12};
	VPL_ExtType _OpaqueAEStreamRef = {"OpaqueAEStreamRef",kStructureType,0};
	VPL_ExtType _AEBuildError = {"AEBuildError",kStructureType,8};
	VPL_ExtType _1383 = {"1383",kEnumType,4};
	VPL_ExtType _1382 = {"1382",kEnumType,4};
	VPL_ExtType _1381 = {"1381",kEnumType,4};
	VPL_ExtType _1380 = {"1380",kEnumType,4};
	VPL_ExtType _TScriptingSizeResource = {"TScriptingSizeResource",kStructureType,28};
	VPL_ExtType _1379 = {"1379",kEnumType,4};
	VPL_ExtType _1378 = {"1378",kEnumType,4};
	VPL_ExtType _1377 = {"1377",kEnumType,4};
	VPL_ExtType _1376 = {"1376",kEnumType,4};
	VPL_ExtType _1375 = {"1375",kEnumType,4};
	VPL_ExtType _1374 = {"1374",kEnumType,4};
	VPL_ExtType _1373 = {"1373",kEnumType,4};
	VPL_ExtType _1372 = {"1372",kEnumType,4};
	VPL_ExtType _1371 = {"1371",kEnumType,4};
	VPL_ExtType _1370 = {"1370",kEnumType,4};
	VPL_ExtType _1369 = {"1369",kEnumType,4};
	VPL_ExtType _1368 = {"1368",kEnumType,4};
	VPL_ExtType _1367 = {"1367",kEnumType,4};
	VPL_ExtType _1366 = {"1366",kEnumType,4};
	VPL_ExtType _1365 = {"1365",kEnumType,4};
	VPL_ExtType _IntlText = {"IntlText",kStructureType,8};
	VPL_ExtType _WritingCode = {"WritingCode",kStructureType,4};
	VPL_ExtType _OffsetArray = {"OffsetArray",kStructureType,8};
	VPL_ExtType _TextRangeArray = {"TextRangeArray",kStructureType,16};
	VPL_ExtType _TextRange = {"TextRange",kStructureType,12};
	VPL_ExtType _1364 = {"1364",kEnumType,4};
	VPL_ExtType _1363 = {"1363",kEnumType,4};
	VPL_ExtType _1362 = {"1362",kEnumType,4};
	VPL_ExtType _1361 = {"1361",kEnumType,4};
	VPL_ExtType _1360 = {"1360",kEnumType,4};
	VPL_ExtType _1359 = {"1359",kEnumType,4};
	VPL_ExtType _1358 = {"1358",kEnumType,4};
	VPL_ExtType _1357 = {"1357",kEnumType,4};
	VPL_ExtType _1356 = {"1356",kEnumType,4};
	VPL_ExtType _1355 = {"1355",kEnumType,4};
	VPL_ExtType _1354 = {"1354",kEnumType,4};
	VPL_ExtType _1353 = {"1353",kEnumType,4};
	VPL_ExtType _1352 = {"1352",kEnumType,4};
	VPL_ExtType _1351 = {"1351",kEnumType,4};
	VPL_ExtType _1350 = {"1350",kEnumType,4};
	VPL_ExtType _1349 = {"1349",kEnumType,4};
	VPL_ExtType _1348 = {"1348",kEnumType,4};
	VPL_ExtType _1347 = {"1347",kEnumType,4};
	VPL_ExtType _1346 = {"1346",kEnumType,4};
	VPL_ExtType _1345 = {"1345",kEnumType,4};
	VPL_ExtType _1344 = {"1344",kEnumType,4};
	VPL_ExtType _1343 = {"1343",kEnumType,4};
	VPL_ExtType _1342 = {"1342",kEnumType,4};
	VPL_ExtType _1341 = {"1341",kEnumType,4};
	VPL_ExtType _1340 = {"1340",kEnumType,4};
	VPL_ExtType _1339 = {"1339",kEnumType,4};
	VPL_ExtType _1338 = {"1338",kEnumType,4};
	VPL_ExtType _1337 = {"1337",kEnumType,4};
	VPL_ExtType _1336 = {"1336",kEnumType,4};
	VPL_ExtType _1335 = {"1335",kEnumType,4};
	VPL_ExtType _1334 = {"1334",kEnumType,4};
	VPL_ExtType _1333 = {"1333",kEnumType,4};
	VPL_ExtType _1332 = {"1332",kEnumType,4};
	VPL_ExtType _1331 = {"1331",kEnumType,4};
	VPL_ExtType _1330 = {"1330",kEnumType,4};
	VPL_ExtType _ccntTokenRecord = {"ccntTokenRecord",kStructureType,12};
	VPL_ExtType _1329 = {"1329",kEnumType,4};
	VPL_ExtType _1328 = {"1328",kEnumType,4};
	VPL_ExtType _1327 = {"1327",kEnumType,4};
	VPL_ExtType _1326 = {"1326",kEnumType,4};
	VPL_ExtType _1325 = {"1325",kEnumType,4};
	VPL_ExtType _1324 = {"1324",kEnumType,4};
	VPL_ExtType _1323 = {"1323",kEnumType,4};
	VPL_ExtType _1322 = {"1322",kEnumType,4};
	VPL_ExtType _1321 = {"1321",kEnumType,4};
	VPL_ExtType _1320 = {"1320",kEnumType,4};
	VPL_ExtType _1319 = {"1319",kEnumType,4};
	VPL_ExtType _1318 = {"1318",kEnumType,4};
	VPL_ExtType _1317 = {"1317",kEnumType,4};
	VPL_ExtType _AEArrayData = {"AEArrayData",NULL,12};
	VPL_ExtType _1316 = {"1316",kEnumType,4};
	VPL_ExtType _1315 = {"1315",kEnumType,4};
	VPL_ExtType _AEKeyDesc = {"AEKeyDesc",kStructureType,12};
	VPL_ExtType _AEDesc = {"AEDesc",kStructureType,8};
	VPL_ExtType _OpaqueAEDataStorageType = {"OpaqueAEDataStorageType",kStructureType,0};
	VPL_ExtType _1314 = {"1314",kEnumType,4};
	VPL_ExtType _1313 = {"1313",kEnumType,4};
	VPL_ExtType _1312 = {"1312",kEnumType,4};
	VPL_ExtType _1311 = {"1311",kEnumType,4};
	VPL_ExtType _1310 = {"1310",kEnumType,4};
	VPL_ExtType _1309 = {"1309",kEnumType,4};
	VPL_ExtType _1308 = {"1308",kEnumType,4};
	VPL_ExtType _1307 = {"1307",kEnumType,4};
	VPL_ExtType _1306 = {"1306",kEnumType,4};
	VPL_ExtType _1305 = {"1305",kEnumType,4};
	VPL_ExtType _1304 = {"1304",kEnumType,4};
	VPL_ExtType _1303 = {"1303",kEnumType,4};
	VPL_ExtType _1302 = {"1302",kEnumType,4};
	VPL_ExtType _1301 = {"1301",kEnumType,4};
	VPL_ExtType _1300 = {"1300",kEnumType,4};
	VPL_ExtType _1299 = {"1299",kEnumType,4};
	VPL_ExtType _CMDeviceProfileArray = {"CMDeviceProfileArray",kStructureType,280};
	VPL_ExtType _NCMDeviceProfileInfo = {"NCMDeviceProfileInfo",kStructureType,284};
	VPL_ExtType _CMDeviceProfileInfo = {"CMDeviceProfileInfo",kStructureType,276};
	VPL_ExtType _CMDeviceInfo = {"CMDeviceInfo",kStructureType,40};
	VPL_ExtType _CMDeviceScope = {"CMDeviceScope",kStructureType,8};
	VPL_ExtType _1298 = {"1298",kEnumType,4};
	VPL_ExtType _1297 = {"1297",kEnumType,4};
	VPL_ExtType _1296 = {"1296",kEnumType,4};
	VPL_ExtType _1295 = {"1295",kEnumType,4};
	VPL_ExtType _1294 = {"1294",kEnumType,4};
	VPL_ExtType _1293 = {"1293",kEnumType,4};
	VPL_ExtType _1292 = {"1292",kEnumType,4};
	VPL_ExtType _CMProfileIterateData = {"CMProfileIterateData",kStructureType,672};
	VPL_ExtType _1291 = {"1291",kEnumType,4};
	VPL_ExtType _1290 = {"1290",kEnumType,4};
	VPL_ExtType _CMProfileLocation = {"CMProfileLocation",kStructureType,260};
	VPL_ExtType _CMProfLoc = {"CMProfLoc",NULL,256};
	VPL_ExtType _CMBufferLocation = {"CMBufferLocation",kStructureType,8};
	VPL_ExtType _CMPathLocation = {"CMPathLocation",kStructureType,256};
	VPL_ExtType _CMProcedureLocation = {"CMProcedureLocation",kStructureType,8};
	VPL_ExtType _CMPtrLocation = {"CMPtrLocation",kStructureType,4};
	VPL_ExtType _CMHandleLocation = {"CMHandleLocation",kStructureType,4};
	VPL_ExtType _CMFileLocation = {"CMFileLocation",kStructureType,72};
	VPL_ExtType _1289 = {"1289",kEnumType,4};
	VPL_ExtType _1288 = {"1288",kEnumType,4};
	VPL_ExtType _1287 = {"1287",kEnumType,4};
	VPL_ExtType _CMBitmap = {"CMBitmap",kStructureType,32};
	VPL_ExtType _1286 = {"1286",kEnumType,4};
	VPL_ExtType _1285 = {"1285",kEnumType,4};
	VPL_ExtType _1284 = {"1284",kEnumType,4};
	VPL_ExtType _1283 = {"1283",kEnumType,4};
	VPL_ExtType _1282 = {"1282",kEnumType,4};
	VPL_ExtType _CMProfileIdentifier = {"CMProfileIdentifier",kStructureType,148};
	VPL_ExtType _CMCWInfoRecord = {"CMCWInfoRecord",kStructureType,20};
	VPL_ExtType _CMMInfoRecord = {"CMMInfoRecord",kStructureType,8};
	VPL_ExtType _CMMInfo = {"CMMInfo",kStructureType,888};
	VPL_ExtType _CMSearchRecord = {"CMSearchRecord",kStructureType,44};
	VPL_ExtType _CMProfileSearchRecord = {"CMProfileSearchRecord",kStructureType,80};
	VPL_ExtType _CMColor = {"CMColor",NULL,8};
	VPL_ExtType _CMNamedColor = {"CMNamedColor",kStructureType,4};
	VPL_ExtType _CMMultichannel8Color = {"CMMultichannel8Color",kStructureType,8};
	VPL_ExtType _CMMultichannel7Color = {"CMMultichannel7Color",kStructureType,8};
	VPL_ExtType _CMMultichannel6Color = {"CMMultichannel6Color",kStructureType,8};
	VPL_ExtType _CMMultichannel5Color = {"CMMultichannel5Color",kStructureType,8};
	VPL_ExtType _CMGrayColor = {"CMGrayColor",kStructureType,4};
	VPL_ExtType _CMYxyColor = {"CMYxyColor",kStructureType,8};
	VPL_ExtType _CMLuvColor = {"CMLuvColor",kStructureType,8};
	VPL_ExtType _CMLabColor = {"CMLabColor",kStructureType,8};
	VPL_ExtType _CMHSVColor = {"CMHSVColor",kStructureType,8};
	VPL_ExtType _CMHLSColor = {"CMHLSColor",kStructureType,8};
	VPL_ExtType _CMCMYColor = {"CMCMYColor",kStructureType,8};
	VPL_ExtType _CMCMYKColor = {"CMCMYKColor",kStructureType,8};
	VPL_ExtType _CMRGBColor = {"CMRGBColor",kStructureType,8};
	VPL_ExtType _1281 = {"1281",kEnumType,4};
	VPL_ExtType _NCMConcatProfileSet = {"NCMConcatProfileSet",kStructureType,28};
	VPL_ExtType _NCMConcatProfileSpec = {"NCMConcatProfileSpec",kStructureType,12};
	VPL_ExtType _CMConcatProfileSet = {"CMConcatProfileSet",kStructureType,8};
	VPL_ExtType _CMAppleProfileHeader = {"CMAppleProfileHeader",NULL,128};
	VPL_ExtType _1280 = {"1280",kEnumType,4};
	VPL_ExtType _1279 = {"1279",kEnumType,4};
	VPL_ExtType _1278 = {"1278",kEnumType,4};
	VPL_ExtType _1277 = {"1277",kEnumType,4};
	VPL_ExtType _1276 = {"1276",kEnumType,4};
	VPL_ExtType _1275 = {"1275",kEnumType,4};
	VPL_ExtType _1274 = {"1274",kEnumType,4};
	VPL_ExtType _1273 = {"1273",kEnumType,4};
	VPL_ExtType _1272 = {"1272",kEnumType,4};
	VPL_ExtType _1271 = {"1271",kEnumType,4};
	VPL_ExtType _1270 = {"1270",kEnumType,4};
	VPL_ExtType _CMProfile = {"CMProfile",kStructureType,208};
	VPL_ExtType _CMProfileResponse = {"CMProfileResponse",kStructureType,20};
	VPL_ExtType _CMProfileChromaticities = {"CMProfileChromaticities",kStructureType,48};
	VPL_ExtType _CMHeader = {"CMHeader",kStructureType,68};
	VPL_ExtType _1269 = {"1269",kEnumType,4};
	VPL_ExtType _1268 = {"1268",kEnumType,4};
	VPL_ExtType _CMIString = {"CMIString",kStructureType,68};
	VPL_ExtType _1267 = {"1267",kEnumType,4};
	VPL_ExtType _1266 = {"1266",kEnumType,4};
	VPL_ExtType _CMMultiLocalizedUniCodeType = {"CMMultiLocalizedUniCodeType",kStructureType,16};
	VPL_ExtType _CMMultiLocalizedUniCodeEntryRec = {"CMMultiLocalizedUniCodeEntryRec",kStructureType,12};
	VPL_ExtType _CMMakeAndModelType = {"CMMakeAndModelType",kStructureType,40};
	VPL_ExtType _CMMakeAndModel = {"CMMakeAndModel",kStructureType,32};
	VPL_ExtType _CMVideoCardGammaType = {"CMVideoCardGammaType",kStructureType,48};
	VPL_ExtType _1265 = {"1265",NULL,36};
	VPL_ExtType _CMVideoCardGamma = {"CMVideoCardGamma",kStructureType,40};
	VPL_ExtType _CMVideoCardGammaFormula = {"CMVideoCardGammaFormula",kStructureType,36};
	VPL_ExtType _CMVideoCardGammaTable = {"CMVideoCardGammaTable",kStructureType,8};
	VPL_ExtType _1264 = {"1264",kEnumType,4};
	VPL_ExtType _CMPS2CRDVMSizeType = {"CMPS2CRDVMSizeType",kStructureType,20};
	VPL_ExtType _CMIntentCRDVMSize = {"CMIntentCRDVMSize",kStructureType,8};
	VPL_ExtType _CMUcrBgType = {"CMUcrBgType",kStructureType,16};
	VPL_ExtType _CMProfileSequenceDescType = {"CMProfileSequenceDescType",kStructureType,16};
	VPL_ExtType _CMXYZType = {"CMXYZType",kStructureType,20};
	VPL_ExtType _CMViewingConditionsType = {"CMViewingConditionsType",kStructureType,36};
	VPL_ExtType _CMUInt64ArrayType = {"CMUInt64ArrayType",kStructureType,12};
	VPL_ExtType _CMUInt32ArrayType = {"CMUInt32ArrayType",kStructureType,12};
	VPL_ExtType _CMUInt16ArrayType = {"CMUInt16ArrayType",kStructureType,12};
	VPL_ExtType _CMUInt8ArrayType = {"CMUInt8ArrayType",kStructureType,12};
	VPL_ExtType _CMU16Fixed16ArrayType = {"CMU16Fixed16ArrayType",kStructureType,12};
	VPL_ExtType _CMS15Fixed16ArrayType = {"CMS15Fixed16ArrayType",kStructureType,12};
	VPL_ExtType _CMSignatureType = {"CMSignatureType",kStructureType,12};
	VPL_ExtType _CMScreeningType = {"CMScreeningType",kStructureType,28};
	VPL_ExtType _CMScreeningChannelRec = {"CMScreeningChannelRec",kStructureType,12};
	VPL_ExtType _CMUnicodeTextType = {"CMUnicodeTextType",kStructureType,12};
	VPL_ExtType _CMTextType = {"CMTextType",kStructureType,12};
	VPL_ExtType _CMTextDescriptionType = {"CMTextDescriptionType",kStructureType,16};
	VPL_ExtType _CMParametricCurveType = {"CMParametricCurveType",kStructureType,16};
	VPL_ExtType _CMNativeDisplayInfoType = {"CMNativeDisplayInfoType",kStructureType,64};
	VPL_ExtType _CMNativeDisplayInfo = {"CMNativeDisplayInfo",kStructureType,56};
	VPL_ExtType _CMNamedColor2Type = {"CMNamedColor2Type",kStructureType,88};
	VPL_ExtType _CMNamedColor2EntryType = {"CMNamedColor2EntryType",kStructureType,40};
	VPL_ExtType _CMNamedColorType = {"CMNamedColorType",kStructureType,20};
	VPL_ExtType _CMMeasurementType = {"CMMeasurementType",kStructureType,36};
	VPL_ExtType _CMMultiFunctCLUTType = {"CMMultiFunctCLUTType",kStructureType,24};
	VPL_ExtType _CMMultiFunctLutType = {"CMMultiFunctLutType",kStructureType,36};
	VPL_ExtType _CMLut8Type = {"CMLut8Type",kStructureType,52};
	VPL_ExtType _CMLut16Type = {"CMLut16Type",kStructureType,56};
	VPL_ExtType _CMDateTimeType = {"CMDateTimeType",kStructureType,20};
	VPL_ExtType _CMDataType = {"CMDataType",kStructureType,16};
	VPL_ExtType _CMCurveType = {"CMCurveType",kStructureType,16};
	VPL_ExtType _CMAdaptationMatrixType = {"CMAdaptationMatrixType",kStructureType,44};
	VPL_ExtType _CM2Profile = {"CM2Profile",kStructureType,148};
	VPL_ExtType _CMTagElemTable = {"CMTagElemTable",kStructureType,16};
	VPL_ExtType _CMTagRecord = {"CMTagRecord",kStructureType,12};
	VPL_ExtType _CM4Header = {"CM4Header",kStructureType,128};
	VPL_ExtType _CM2Header = {"CM2Header",kStructureType,128};
	VPL_ExtType _CMXYZColor = {"CMXYZColor",kStructureType,8};
	VPL_ExtType _CMFixedXYZColor = {"CMFixedXYZColor",kStructureType,12};
	VPL_ExtType _CMFixedXYColor = {"CMFixedXYColor",kStructureType,8};
	VPL_ExtType _CMDateTime = {"CMDateTime",kStructureType,12};
	VPL_ExtType _1263 = {"1263",kEnumType,4};
	VPL_ExtType _1262 = {"1262",kEnumType,4};
	VPL_ExtType _1261 = {"1261",kEnumType,4};
	VPL_ExtType _1260 = {"1260",kEnumType,4};
	VPL_ExtType _1259 = {"1259",kEnumType,4};
	VPL_ExtType _1258 = {"1258",kEnumType,4};
	VPL_ExtType _1257 = {"1257",kEnumType,4};
	VPL_ExtType _1256 = {"1256",kEnumType,4};
	VPL_ExtType _1255 = {"1255",kEnumType,4};
	VPL_ExtType _1254 = {"1254",kEnumType,4};
	VPL_ExtType _1253 = {"1253",kEnumType,4};
	VPL_ExtType _1252 = {"1252",kEnumType,4};
	VPL_ExtType _1251 = {"1251",kEnumType,4};
	VPL_ExtType _1250 = {"1250",kEnumType,4};
	VPL_ExtType _1249 = {"1249",kEnumType,4};
	VPL_ExtType _1248 = {"1248",kEnumType,4};
	VPL_ExtType _1247 = {"1247",kEnumType,4};
	VPL_ExtType _1246 = {"1246",kEnumType,4};
	VPL_ExtType _1245 = {"1245",kEnumType,4};
	VPL_ExtType _1244 = {"1244",kEnumType,4};
	VPL_ExtType _1243 = {"1243",kEnumType,4};
	VPL_ExtType _1242 = {"1242",kEnumType,4};
	VPL_ExtType _1241 = {"1241",kEnumType,4};
	VPL_ExtType _1240 = {"1240",kEnumType,4};
	VPL_ExtType _1239 = {"1239",kEnumType,4};
	VPL_ExtType _1238 = {"1238",kEnumType,4};
	VPL_ExtType _1237 = {"1237",kEnumType,4};
	VPL_ExtType _OpaqueCMWorldRef = {"OpaqueCMWorldRef",kStructureType,0};
	VPL_ExtType _OpaqueCMMatchRef = {"OpaqueCMMatchRef",kStructureType,0};
	VPL_ExtType _OpaqueCMProfileSearchRef = {"OpaqueCMProfileSearchRef",kStructureType,0};
	VPL_ExtType _OpaqueCMProfileRef = {"OpaqueCMProfileRef",kStructureType,0};
	VPL_ExtType __CGCommonWindowLevelKey = {"_CGCommonWindowLevelKey",kEnumType,4};
	VPL_ExtType _1236 = {"1236",kEnumType,4};
	VPL_ExtType _1235 = {"1235",kEnumType,4};
	VPL_ExtType __CGDeviceByteColor = {"_CGDeviceByteColor",kStructureType,4};
	VPL_ExtType __CGDeviceColor = {"_CGDeviceColor",kStructureType,12};
	VPL_ExtType __CGDirectPaletteRef = {"_CGDirectPaletteRef",kStructureType,0};
	VPL_ExtType __CGDirectDisplayID = {"_CGDirectDisplayID",kStructureType,0};
	VPL_ExtType __CGError = {"_CGError",kEnumType,4};
	VPL_ExtType _CGDataConsumerCallbacks = {"CGDataConsumerCallbacks",kStructureType,8};
	VPL_ExtType _CGDataConsumer = {"CGDataConsumer",kStructureType,0};
	VPL_ExtType _CGInterpolationQuality = {"CGInterpolationQuality",kEnumType,4};
	VPL_ExtType _CGTextEncoding = {"CGTextEncoding",kEnumType,4};
	VPL_ExtType _CGTextDrawingMode = {"CGTextDrawingMode",kEnumType,4};
	VPL_ExtType _CGPathDrawingMode = {"CGPathDrawingMode",kEnumType,4};
	VPL_ExtType _CGLineCap = {"CGLineCap",kEnumType,4};
	VPL_ExtType _CGLineJoin = {"CGLineJoin",kEnumType,4};
	VPL_ExtType _CGPDFDocument = {"CGPDFDocument",kStructureType,0};
	VPL_ExtType _CGPatternCallbacks = {"CGPatternCallbacks",kStructureType,12};
	VPL_ExtType _CGPatternTiling = {"CGPatternTiling",kEnumType,4};
	VPL_ExtType _CGPattern = {"CGPattern",kStructureType,0};
	VPL_ExtType _CGImageAlphaInfo = {"CGImageAlphaInfo",kEnumType,4};
	VPL_ExtType _CGImage = {"CGImage",kStructureType,0};
	VPL_ExtType _1234 = {"1234",kEnumType,4};
	VPL_ExtType _CGFont = {"CGFont",kStructureType,0};
	VPL_ExtType _CGColorRenderingIntent = {"CGColorRenderingIntent",kEnumType,4};
	VPL_ExtType _CGDataProviderDirectAccessCallbacks = {"CGDataProviderDirectAccessCallbacks",kStructureType,16};
	VPL_ExtType _CGDataProviderCallbacks = {"CGDataProviderCallbacks",kStructureType,16};
	VPL_ExtType _CGDataProvider = {"CGDataProvider",kStructureType,0};
	VPL_ExtType _CGColorSpace = {"CGColorSpace",kStructureType,0};
	VPL_ExtType _CGContext = {"CGContext",kStructureType,0};
	VPL_ExtType _CGRectEdge = {"CGRectEdge",kEnumType,4};
	VPL_ExtType _CGRect = {"CGRect",kStructureType,16};
	VPL_ExtType _CGSize = {"CGSize",kStructureType,8};
	VPL_ExtType _CGPoint = {"CGPoint",kStructureType,8};
	VPL_ExtType _CGAffineTransform = {"CGAffineTransform",kStructureType,24};
	VPL_ExtType _scalerStreamData = {"scalerStreamData",kStructureType,12};
	VPL_ExtType _1233 = {"1233",kStructureType,8};
	VPL_ExtType _1232 = {"1232",kStructureType,12};
	VPL_ExtType _1231 = {"1231",NULL,12};
	VPL_ExtType _scalerStream = {"scalerStream",kStructureType,40};
	VPL_ExtType _scalerPrerequisiteItem = {"scalerPrerequisiteItem",kStructureType,12};
	VPL_ExtType _1230 = {"1230",kEnumType,4};
	VPL_ExtType _1229 = {"1229",kEnumType,4};
	VPL_ExtType _1228 = {"1228",kEnumType,4};
	VPL_ExtType _1227 = {"1227",NULL,4};
	VPL_ExtType _ATSFontFilter = {"ATSFontFilter",kStructureType,12};
	VPL_ExtType _ATSFontFilterSelector = {"ATSFontFilterSelector",kEnumType,4};
	VPL_ExtType _1226 = {"1226",kEnumType,4};
	VPL_ExtType _ATSFontIterator_ = {"ATSFontIterator_",kStructureType,0};
	VPL_ExtType _ATSFontFamilyIterator_ = {"ATSFontFamilyIterator_",kStructureType,0};
	VPL_ExtType _1225 = {"1225",kEnumType,4};
	VPL_ExtType _1224 = {"1224",kEnumType,4};
	VPL_ExtType _1223 = {"1223",kEnumType,4};
	VPL_ExtType _1222 = {"1222",kEnumType,4};
	VPL_ExtType _1221 = {"1221",kEnumType,4};
	VPL_ExtType _1220 = {"1220",kEnumType,4};
	VPL_ExtType _FontVariation = {"FontVariation",kStructureType,8};
	VPL_ExtType _1219 = {"1219",kEnumType,4};
	VPL_ExtType _1218 = {"1218",kEnumType,4};
	VPL_ExtType _sfntFeatureHeader = {"sfntFeatureHeader",kStructureType,32};
	VPL_ExtType _sfntFontRunFeature = {"sfntFontRunFeature",kStructureType,4};
	VPL_ExtType _sfntFontFeatureSetting = {"sfntFontFeatureSetting",kStructureType,4};
	VPL_ExtType _sfntFeatureName = {"sfntFeatureName",kStructureType,12};
	VPL_ExtType _1217 = {"1217",kEnumType,4};
	VPL_ExtType _1216 = {"1216",kEnumType,4};
	VPL_ExtType _sfntDescriptorHeader = {"sfntDescriptorHeader",kStructureType,16};
	VPL_ExtType _sfntFontDescriptor = {"sfntFontDescriptor",kStructureType,8};
	VPL_ExtType _1215 = {"1215",kEnumType,4};
	VPL_ExtType _1214 = {"1214",kEnumType,4};
	VPL_ExtType _sfntVariationHeader = {"sfntVariationHeader",kStructureType,44};
	VPL_ExtType _1213 = {"1213",kEnumType,4};
	VPL_ExtType _sfntInstance = {"sfntInstance",kStructureType,8};
	VPL_ExtType _1212 = {"1212",kEnumType,4};
	VPL_ExtType _sfntVariationAxis = {"sfntVariationAxis",kStructureType,20};
	VPL_ExtType _1211 = {"1211",kEnumType,4};
	VPL_ExtType _1210 = {"1210",kEnumType,4};
	VPL_ExtType _sfntNameHeader = {"sfntNameHeader",kStructureType,20};
	VPL_ExtType _1209 = {"1209",kEnumType,4};
	VPL_ExtType _sfntNameRecord = {"sfntNameRecord",kStructureType,12};
	VPL_ExtType _1208 = {"1208",kEnumType,4};
	VPL_ExtType _1207 = {"1207",kEnumType,4};
	VPL_ExtType _1206 = {"1206",kEnumType,4};
	VPL_ExtType _1205 = {"1205",kEnumType,4};
	VPL_ExtType _sfntCMapHeader = {"sfntCMapHeader",kStructureType,12};
	VPL_ExtType _1204 = {"1204",kEnumType,4};
	VPL_ExtType _sfntCMapEncoding = {"sfntCMapEncoding",kStructureType,8};
	VPL_ExtType _1203 = {"1203",kEnumType,4};
	VPL_ExtType _sfntCMapSubHeader = {"sfntCMapSubHeader",kStructureType,8};
	VPL_ExtType _1202 = {"1202",kEnumType,4};
	VPL_ExtType _1201 = {"1201",kEnumType,4};
	VPL_ExtType _1200 = {"1200",kEnumType,4};
	VPL_ExtType _1199 = {"1199",kEnumType,4};
	VPL_ExtType _1198 = {"1198",kEnumType,4};
	VPL_ExtType _1197 = {"1197",kEnumType,4};
	VPL_ExtType _1196 = {"1196",kEnumType,4};
	VPL_ExtType _1195 = {"1195",kEnumType,4};
	VPL_ExtType _1194 = {"1194",kEnumType,4};
	VPL_ExtType _sfntDirectory = {"sfntDirectory",kStructureType,28};
	VPL_ExtType _sfntDirectoryEntry = {"sfntDirectoryEntry",kStructureType,16};
	VPL_ExtType _ATSJustWidthDeltaEntryOverride = {"ATSJustWidthDeltaEntryOverride",kStructureType,20};
	VPL_ExtType _ATSTrapezoid = {"ATSTrapezoid",kStructureType,32};
	VPL_ExtType _1193 = {"1193",kEnumType,4};
	VPL_ExtType _1192 = {"1192",kEnumType,4};
	VPL_ExtType _1191 = {"1191",kEnumType,4};
	VPL_ExtType _1190 = {"1190",kEnumType,4};
	VPL_ExtType _ATSGlyphScreenMetrics = {"ATSGlyphScreenMetrics",kStructureType,40};
	VPL_ExtType _ATSGlyphIdealMetrics = {"ATSGlyphIdealMetrics",kStructureType,24};
	VPL_ExtType _ATSUCurvePaths = {"ATSUCurvePaths",kStructureType,20};
	VPL_ExtType _ATSUCurvePath = {"ATSUCurvePath",kStructureType,16};
	VPL_ExtType _1189 = {"1189",kEnumType,4};
	VPL_ExtType _1188 = {"1188",kEnumType,4};
	VPL_ExtType _ATSFontMetrics = {"ATSFontMetrics",kStructureType,60};
	VPL_ExtType _1187 = {"1187",kEnumType,4};
	VPL_ExtType _1186 = {"1186",NULL,72};
	VPL_ExtType _FMFilter = {"FMFilter",kStructureType,80};
	VPL_ExtType _FMFontDirectoryFilter = {"FMFontDirectoryFilter",kStructureType,12};
	VPL_ExtType _1185 = {"1185",kEnumType,4};
	VPL_ExtType _1184 = {"1184",kEnumType,4};
	VPL_ExtType _1183 = {"1183",kEnumType,4};
	VPL_ExtType _1182 = {"1182",kEnumType,4};
	VPL_ExtType _FMFontFamilyInstanceIterator = {"FMFontFamilyInstanceIterator",kStructureType,64};
	VPL_ExtType _FMFontIterator = {"FMFontIterator",kStructureType,64};
	VPL_ExtType _FMFontFamilyIterator = {"FMFontFamilyIterator",kStructureType,64};
	VPL_ExtType _FMFontFamilyInstance = {"FMFontFamilyInstance",kStructureType,4};
	VPL_ExtType _BslnTable = {"BslnTable",kStructureType,100};
	VPL_ExtType _BslnFormatUnion = {"BslnFormatUnion",NULL,92};
	VPL_ExtType _BslnFormat3Part = {"BslnFormat3Part",kStructureType,92};
	VPL_ExtType _BslnFormat2Part = {"BslnFormat2Part",kStructureType,68};
	VPL_ExtType _BslnFormat1Part = {"BslnFormat1Part",kStructureType,88};
	VPL_ExtType _BslnFormat0Part = {"BslnFormat0Part",kStructureType,64};
	VPL_ExtType _1181 = {"1181",kEnumType,4};
	VPL_ExtType _1180 = {"1180",kEnumType,4};
	VPL_ExtType _KernSubtableHeader = {"KernSubtableHeader",kStructureType,20};
	VPL_ExtType _KernVersion0SubtableHeader = {"KernVersion0SubtableHeader",kStructureType,20};
	VPL_ExtType _KernFormatSpecificHeader = {"KernFormatSpecificHeader",NULL,12};
	VPL_ExtType _KernIndexArrayHeader = {"KernIndexArrayHeader",kStructureType,12};
	VPL_ExtType _KernSimpleArrayHeader = {"KernSimpleArrayHeader",kStructureType,12};
	VPL_ExtType _KernOffsetTable = {"KernOffsetTable",kStructureType,8};
	VPL_ExtType _KernStateEntry = {"KernStateEntry",kStructureType,4};
	VPL_ExtType _KernStateHeader = {"KernStateHeader",kStructureType,12};
	VPL_ExtType _KernOrderedListHeader = {"KernOrderedListHeader",kStructureType,12};
	VPL_ExtType _KernOrderedListEntry = {"KernOrderedListEntry",kStructureType,8};
	VPL_ExtType _KernKerningPair = {"KernKerningPair",kStructureType,4};
	VPL_ExtType _KernTableHeader = {"KernTableHeader",kStructureType,12};
	VPL_ExtType _KernVersion0Header = {"KernVersion0Header",kStructureType,8};
	VPL_ExtType _1179 = {"1179",kEnumType,4};
	VPL_ExtType _1178 = {"1178",kEnumType,4};
	VPL_ExtType _1177 = {"1177",kEnumType,4};
	VPL_ExtType _TrakTable = {"TrakTable",kStructureType,12};
	VPL_ExtType _TrakTableData = {"TrakTableData",kStructureType,16};
	VPL_ExtType _TrakTableEntry = {"TrakTableEntry",kStructureType,8};
	VPL_ExtType _1176 = {"1176",kEnumType,4};
	VPL_ExtType _PropLookupSingle = {"PropLookupSingle",kStructureType,4};
	VPL_ExtType _PropLookupSegment = {"PropLookupSegment",kStructureType,8};
	VPL_ExtType _PropTable = {"PropTable",kStructureType,32};
	VPL_ExtType _1175 = {"1175",kEnumType,4};
	VPL_ExtType _1174 = {"1174",kEnumType,4};
	VPL_ExtType _MorxTable = {"MorxTable",kStructureType,36};
	VPL_ExtType _MorxChain = {"MorxChain",kStructureType,28};
	VPL_ExtType _MorxSubtable = {"MorxSubtable",kStructureType,40};
	VPL_ExtType _MorxSpecificSubtable = {"MorxSpecificSubtable",NULL,28};
	VPL_ExtType _MorxInsertionSubtable = {"MorxInsertionSubtable",kStructureType,20};
	VPL_ExtType _MorxLigatureSubtable = {"MorxLigatureSubtable",kStructureType,28};
	VPL_ExtType _MorxContextualSubtable = {"MorxContextualSubtable",kStructureType,20};
	VPL_ExtType _MorxRearrangementSubtable = {"MorxRearrangementSubtable",kStructureType,16};
	VPL_ExtType _1173 = {"1173",kEnumType,4};
	VPL_ExtType _MortTable = {"MortTable",kStructureType,32};
	VPL_ExtType _MortChain = {"MortChain",kStructureType,24};
	VPL_ExtType _MortFeatureEntry = {"MortFeatureEntry",kStructureType,12};
	VPL_ExtType _MortSubtable = {"MortSubtable",kStructureType,32};
	VPL_ExtType _MortSpecificSubtable = {"MortSpecificSubtable",NULL,24};
	VPL_ExtType _MortInsertionSubtable = {"MortInsertionSubtable",kStructureType,8};
	VPL_ExtType _MortSwashSubtable = {"MortSwashSubtable",kStructureType,24};
	VPL_ExtType _MortLigatureSubtable = {"MortLigatureSubtable",kStructureType,16};
	VPL_ExtType _MortContextualSubtable = {"MortContextualSubtable",kStructureType,12};
	VPL_ExtType _MortRearrangementSubtable = {"MortRearrangementSubtable",kStructureType,8};
	VPL_ExtType _1172 = {"1172",kEnumType,4};
	VPL_ExtType _OpbdTable = {"OpbdTable",kStructureType,32};
	VPL_ExtType _OpbdSideValues = {"OpbdSideValues",kStructureType,8};
	VPL_ExtType _1171 = {"1171",kEnumType,4};
	VPL_ExtType _JustTable = {"JustTable",kStructureType,12};
	VPL_ExtType _JustDirectionTable = {"JustDirectionTable",kStructureType,32};
	VPL_ExtType _JustPostcompTable = {"JustPostcompTable",kStructureType,24};
	VPL_ExtType _JustWidthDeltaGroup = {"JustWidthDeltaGroup",kStructureType,28};
	VPL_ExtType _JustWidthDeltaEntry = {"JustWidthDeltaEntry",kStructureType,24};
	VPL_ExtType _JustPCAction = {"JustPCAction",kStructureType,16};
	VPL_ExtType _JustPCActionSubrecord = {"JustPCActionSubrecord",kStructureType,12};
	VPL_ExtType _JustPCGlyphRepeatAddAction = {"JustPCGlyphRepeatAddAction",kStructureType,4};
	VPL_ExtType _JustPCDuctilityAction = {"JustPCDuctilityAction",kStructureType,16};
	VPL_ExtType _JustPCConditionalAddAction = {"JustPCConditionalAddAction",kStructureType,8};
	VPL_ExtType _JustPCDecompositionAction = {"JustPCDecompositionAction",kStructureType,16};
	VPL_ExtType _1170 = {"1170",kEnumType,4};
	VPL_ExtType _1169 = {"1169",kEnumType,4};
	VPL_ExtType _1168 = {"1168",kEnumType,4};
	VPL_ExtType _LcarCaretTable = {"LcarCaretTable",kStructureType,32};
	VPL_ExtType _LcarCaretClassEntry = {"LcarCaretClassEntry",kStructureType,4};
	VPL_ExtType _1167 = {"1167",kEnumType,4};
	VPL_ExtType _STXEntryTwo = {"STXEntryTwo",kStructureType,8};
	VPL_ExtType _STXEntryOne = {"STXEntryOne",kStructureType,8};
	VPL_ExtType _STXEntryZero = {"STXEntryZero",kStructureType,4};
	VPL_ExtType _STXHeader = {"STXHeader",kStructureType,16};
	VPL_ExtType _1166 = {"1166",kEnumType,4};
	VPL_ExtType _STEntryTwo = {"STEntryTwo",kStructureType,8};
	VPL_ExtType _STEntryOne = {"STEntryOne",kStructureType,8};
	VPL_ExtType _STEntryZero = {"STEntryZero",kStructureType,4};
	VPL_ExtType _STClassTable = {"STClassTable",kStructureType,8};
	VPL_ExtType _STHeader = {"STHeader",kStructureType,8};
	VPL_ExtType _1165 = {"1165",kEnumType,4};
	VPL_ExtType _SFNTLookupTable = {"SFNTLookupTable",kStructureType,24};
	VPL_ExtType _SFNTLookupFormatSpecificHeader = {"SFNTLookupFormatSpecificHeader",NULL,20};
	VPL_ExtType _SFNTLookupSingleHeader = {"SFNTLookupSingleHeader",kStructureType,16};
	VPL_ExtType _SFNTLookupSingle = {"SFNTLookupSingle",kStructureType,4};
	VPL_ExtType _SFNTLookupSegmentHeader = {"SFNTLookupSegmentHeader",kStructureType,20};
	VPL_ExtType _SFNTLookupSegment = {"SFNTLookupSegment",kStructureType,8};
	VPL_ExtType _SFNTLookupTrimmedArrayHeader = {"SFNTLookupTrimmedArrayHeader",kStructureType,8};
	VPL_ExtType _SFNTLookupArrayHeader = {"SFNTLookupArrayHeader",kStructureType,4};
	VPL_ExtType _SFNTLookupBinarySearchHeader = {"SFNTLookupBinarySearchHeader",kStructureType,12};
	VPL_ExtType _1164 = {"1164",kEnumType,4};
	VPL_ExtType _1163 = {"1163",kEnumType,4};
	VPL_ExtType _1162 = {"1162",kEnumType,4};
	VPL_ExtType _1161 = {"1161",kEnumType,4};
	VPL_ExtType _1160 = {"1160",kEnumType,4};
	VPL_ExtType _1159 = {"1159",kEnumType,4};
	VPL_ExtType _1158 = {"1158",kEnumType,4};
	VPL_ExtType _1157 = {"1157",kEnumType,4};
	VPL_ExtType _1156 = {"1156",kEnumType,4};
	VPL_ExtType _1155 = {"1155",kEnumType,4};
	VPL_ExtType _1154 = {"1154",kEnumType,4};
	VPL_ExtType _1153 = {"1153",kEnumType,4};
	VPL_ExtType _1152 = {"1152",kEnumType,4};
	VPL_ExtType _1151 = {"1151",kEnumType,4};
	VPL_ExtType _1150 = {"1150",kEnumType,4};
	VPL_ExtType _1149 = {"1149",kEnumType,4};
	VPL_ExtType _1148 = {"1148",kEnumType,4};
	VPL_ExtType _1147 = {"1147",kEnumType,4};
	VPL_ExtType _1146 = {"1146",kEnumType,4};
	VPL_ExtType _1145 = {"1145",kEnumType,4};
	VPL_ExtType _1144 = {"1144",kEnumType,4};
	VPL_ExtType _1143 = {"1143",kEnumType,4};
	VPL_ExtType _1142 = {"1142",kEnumType,4};
	VPL_ExtType _1141 = {"1141",kEnumType,4};
	VPL_ExtType _1140 = {"1140",kEnumType,4};
	VPL_ExtType _1139 = {"1139",kEnumType,4};
	VPL_ExtType _1138 = {"1138",kEnumType,4};
	VPL_ExtType _1137 = {"1137",kEnumType,4};
	VPL_ExtType _1136 = {"1136",kEnumType,4};
	VPL_ExtType _1135 = {"1135",kEnumType,4};
	VPL_ExtType _1134 = {"1134",kEnumType,4};
	VPL_ExtType _1133 = {"1133",kEnumType,4};
	VPL_ExtType _1132 = {"1132",kEnumType,4};
	VPL_ExtType _1131 = {"1131",kEnumType,4};
	VPL_ExtType _1130 = {"1130",kEnumType,4};
	VPL_ExtType ___CFHTTPMessage = {"__CFHTTPMessage",kStructureType,0};
	VPL_ExtType _1129 = {"1129",kEnumType,4};
	VPL_ExtType _1128 = {"1128",kEnumType,4};
	VPL_ExtType _1127 = {"1127",kEnumType,4};
	VPL_ExtType _1126 = {"1126",kEnumType,4};
	VPL_ExtType _1125 = {"1125",kEnumType,4};
	VPL_ExtType _1124 = {"1124",kEnumType,4};
	VPL_ExtType _1123 = {"1123",kEnumType,4};
	VPL_ExtType _1122 = {"1122",kEnumType,4};
	VPL_ExtType _1121 = {"1121",kEnumType,4};
	VPL_ExtType _1120 = {"1120",kEnumType,4};
	VPL_ExtType _1119 = {"1119",kEnumType,4};
	VPL_ExtType _KCCallbackInfo = {"KCCallbackInfo",kStructureType,36};
	VPL_ExtType _1118 = {"1118",kEnumType,4};
	VPL_ExtType _1117 = {"1117",kEnumType,4};
	VPL_ExtType _SecKeychainAttributeList = {"SecKeychainAttributeList",kStructureType,8};
	VPL_ExtType _SecKeychainAttribute = {"SecKeychainAttribute",kStructureType,12};
	VPL_ExtType _OpaqueSecKeychainSearchRef = {"OpaqueSecKeychainSearchRef",kStructureType,0};
	VPL_ExtType _OpaqueSecKeychainItemRef = {"OpaqueSecKeychainItemRef",kStructureType,0};
	VPL_ExtType _OpaqueSecKeychainRef = {"OpaqueSecKeychainRef",kStructureType,0};
	VPL_ExtType _NSLPluginData = {"NSLPluginData",kStructureType,24};
	VPL_ExtType _NSLServicesListHeader = {"NSLServicesListHeader",kStructureType,8};
	VPL_ExtType _NSLTypedData = {"NSLTypedData",kStructureType,8};
	VPL_ExtType _NSLPluginAsyncInfo = {"NSLPluginAsyncInfo",kStructureType,56};
	VPL_ExtType _NSLClientAsyncInfo = {"NSLClientAsyncInfo",kStructureType,60};
	VPL_ExtType _1116 = {"1116",kEnumType,4};
	VPL_ExtType _1115 = {"1115",kEnumType,4};
	VPL_ExtType _1114 = {"1114",kEnumType,4};
	VPL_ExtType _NSLError = {"NSLError",kStructureType,8};
	VPL_ExtType _1113 = {"1113",kEnumType,4};
	VPL_ExtType _1112 = {"1112",kEnumType,4};
	VPL_ExtType _1111 = {"1111",kEnumType,4};
	VPL_ExtType _1110 = {"1110",kEnumType,4};
	VPL_ExtType _1109 = {"1109",kEnumType,4};
	VPL_ExtType _1108 = {"1108",kEnumType,4};
	VPL_ExtType _Partition = {"Partition",kStructureType,512};
	VPL_ExtType _1107 = {"1107",kEnumType,4};
	VPL_ExtType _DDMap = {"DDMap",kStructureType,8};
	VPL_ExtType _Block0 = {"Block0",kStructureType,516};
	VPL_ExtType _1106 = {"1106",kEnumType,4};
	VPL_ExtType _1105 = {"1105",kEnumType,4};
	VPL_ExtType _1104 = {"1104",kEnumType,4};
	VPL_ExtType _1103 = {"1103",kEnumType,4};
	VPL_ExtType _1102 = {"1102",kEnumType,4};
	VPL_ExtType _1101 = {"1101",kEnumType,4};
	VPL_ExtType _1100 = {"1100",kEnumType,4};
	VPL_ExtType _1099 = {"1099",kEnumType,4};
	VPL_ExtType _1098 = {"1098",kEnumType,4};
	VPL_ExtType _1097 = {"1097",kEnumType,4};
	VPL_ExtType _1096 = {"1096",kEnumType,4};
	VPL_ExtType _1095 = {"1095",kEnumType,4};
	VPL_ExtType _1094 = {"1094",kEnumType,4};
	VPL_ExtType _1093 = {"1093",kEnumType,4};
	VPL_ExtType _1092 = {"1092",kEnumType,4};
	VPL_ExtType _1091 = {"1091",kEnumType,4};
	VPL_ExtType _1090 = {"1090",kEnumType,4};
	VPL_ExtType _1089 = {"1089",kEnumType,4};
	VPL_ExtType _1088 = {"1088",kEnumType,4};
	VPL_ExtType _1087 = {"1087",kEnumType,4};
	VPL_ExtType _1086 = {"1086",kEnumType,4};
	VPL_ExtType _1085 = {"1085",kEnumType,4};
	VPL_ExtType _SCSILoadDriverPB = {"SCSILoadDriverPB",kStructureType,40};
	VPL_ExtType _SCSIDriverPB = {"SCSIDriverPB",kStructureType,44};
	VPL_ExtType _SCSIGetVirtualIDInfoPB = {"SCSIGetVirtualIDInfoPB",kStructureType,40};
	VPL_ExtType _SCSIReleaseQPB = {"SCSIReleaseQPB",kStructureType,36};
	VPL_ExtType _SCSIResetDevicePB = {"SCSIResetDevicePB",kStructureType,36};
	VPL_ExtType _SCSIResetBusPB = {"SCSIResetBusPB",kStructureType,36};
	VPL_ExtType _SCSITerminateIOPB = {"SCSITerminateIOPB",kStructureType,40};
	VPL_ExtType _SCSIAbortCommandPB = {"SCSIAbortCommandPB",kStructureType,40};
	VPL_ExtType _SCSIBusInquiryPB = {"SCSIBusInquiryPB",kStructureType,172};
	VPL_ExtType _SCSI_IO = {"SCSI_IO",kStructureType,176};
	VPL_ExtType _SCSI_PB = {"SCSI_PB",kStructureType,36};
	VPL_ExtType _SCSIHdr = {"SCSIHdr",kStructureType,36};
	VPL_ExtType _SGRecord = {"SGRecord",kStructureType,8};
	VPL_ExtType _CDB = {"CDB",NULL,16};
	VPL_ExtType _DeviceIdentATA = {"DeviceIdentATA",kStructureType,4};
	VPL_ExtType _1084 = {"1084",kEnumType,4};
	VPL_ExtType _DeviceIdent = {"DeviceIdent",kStructureType,4};
	VPL_ExtType _1083 = {"1083",kEnumType,4};
	VPL_ExtType _1082 = {"1082",kEnumType,4};
	VPL_ExtType _1081 = {"1081",kEnumType,4};
	VPL_ExtType _1080 = {"1080",kEnumType,4};
	VPL_ExtType _SCSIInstr = {"SCSIInstr",kStructureType,12};
	VPL_ExtType _1079 = {"1079",kEnumType,4};
	VPL_ExtType _PowerSourceParamBlock = {"PowerSourceParamBlock",kStructureType,112};
	VPL_ExtType _1078 = {"1078",kEnumType,4};
	VPL_ExtType _1077 = {"1077",kEnumType,4};
	VPL_ExtType _1076 = {"1076",kEnumType,4};
	VPL_ExtType _1075 = {"1075",kEnumType,4};
	VPL_ExtType _1074 = {"1074",kEnumType,4};
	VPL_ExtType _1073 = {"1073",kEnumType,4};
	VPL_ExtType _StartupTime = {"StartupTime",kStructureType,8};
	VPL_ExtType _WakeupTime = {"WakeupTime",kStructureType,8};
	VPL_ExtType _BatteryTimeRec = {"BatteryTimeRec",kStructureType,16};
	VPL_ExtType _PMgrQueueElement = {"PMgrQueueElement",kStructureType,20};
	VPL_ExtType _HDQueueElement = {"HDQueueElement",kStructureType,16};
	VPL_ExtType _SleepQRec = {"SleepQRec",kStructureType,16};
	VPL_ExtType _BatteryInfo = {"BatteryInfo",kStructureType,4};
	VPL_ExtType _ActivityInfo = {"ActivityInfo",kStructureType,8};
	VPL_ExtType _1072 = {"1072",kEnumType,4};
	VPL_ExtType _1071 = {"1071",kEnumType,4};
	VPL_ExtType _1070 = {"1070",kEnumType,4};
	VPL_ExtType _1069 = {"1069",kEnumType,4};
	VPL_ExtType _1068 = {"1068",kEnumType,4};
	VPL_ExtType _1067 = {"1067",kEnumType,4};
	VPL_ExtType _1066 = {"1066",kEnumType,4};
	VPL_ExtType _1065 = {"1065",kEnumType,4};
	VPL_ExtType _1064 = {"1064",kEnumType,4};
	VPL_ExtType _1063 = {"1063",kEnumType,4};
	VPL_ExtType _1062 = {"1062",kEnumType,4};
	VPL_ExtType _1061 = {"1061",kEnumType,4};
	VPL_ExtType _1060 = {"1060",kEnumType,4};
	VPL_ExtType _PowerSummary = {"PowerSummary",kStructureType,56};
	VPL_ExtType _DevicePowerInfo = {"DevicePowerInfo",kStructureType,32};
	VPL_ExtType _1059 = {"1059",kEnumType,4};
	VPL_ExtType _1058 = {"1058",kEnumType,4};
	VPL_ExtType _1057 = {"1057",kEnumType,4};
	VPL_ExtType _1056 = {"1056",kEnumType,4};
	VPL_ExtType _1055 = {"1055",kEnumType,4};
	VPL_ExtType _1054 = {"1054",kEnumType,4};
	VPL_ExtType _1053 = {"1053",kEnumType,4};
	VPL_ExtType _1052 = {"1052",kEnumType,4};
	VPL_ExtType _1051 = {"1051",kEnumType,4};
	VPL_ExtType _1050 = {"1050",kEnumType,4};
	VPL_ExtType _1049 = {"1049",kEnumType,4};
	VPL_ExtType _1048 = {"1048",kEnumType,4};
	VPL_ExtType _OTGate = {"OTGate",kStructureType,20};
	VPL_ExtType _OTHashList = {"OTHashList",kStructureType,12};
	VPL_ExtType _OTClientList = {"OTClientList",kStructureType,8};
	VPL_ExtType _OTPortCloseStruct = {"OTPortCloseStruct",kStructureType,12};
	VPL_ExtType _1047 = {"1047",kEnumType,4};
	VPL_ExtType _1046 = {"1046",kEnumType,4};
	VPL_ExtType _trace_ids = {"trace_ids",kStructureType,8};
	VPL_ExtType _1045 = {"1045",kEnumType,4};
	VPL_ExtType _log_ctl = {"log_ctl",kStructureType,20};
	VPL_ExtType _strioctl = {"strioctl",kStructureType,16};
	VPL_ExtType _strrecvfd = {"strrecvfd",kStructureType,16};
	VPL_ExtType _strpmsg = {"strpmsg",kStructureType,32};
	VPL_ExtType _strpeek = {"strpeek",kStructureType,28};
	VPL_ExtType _str_list = {"str_list",kStructureType,8};
	VPL_ExtType _str_mlist = {"str_mlist",kStructureType,32};
	VPL_ExtType _strfdinsert = {"strfdinsert",kStructureType,36};
	VPL_ExtType _1044 = {"1044",kEnumType,4};
	VPL_ExtType _1043 = {"1043",kEnumType,4};
	VPL_ExtType _1042 = {"1042",kEnumType,4};
	VPL_ExtType _1041 = {"1041",kEnumType,4};
	VPL_ExtType _1040 = {"1040",kEnumType,4};
	VPL_ExtType _1039 = {"1039",kEnumType,4};
	VPL_ExtType _bandinfo = {"bandinfo",kStructureType,8};
	VPL_ExtType _1038 = {"1038",kEnumType,4};
	VPL_ExtType _1037 = {"1037",kEnumType,4};
	VPL_ExtType _1036 = {"1036",kEnumType,4};
	VPL_ExtType _1035 = {"1035",kEnumType,4};
	VPL_ExtType _1034 = {"1034",kEnumType,4};
	VPL_ExtType _1033 = {"1033",kEnumType,4};
	VPL_ExtType _1032 = {"1032",kEnumType,4};
	VPL_ExtType _1031 = {"1031",kEnumType,4};
	VPL_ExtType _1030 = {"1030",kEnumType,4};
	VPL_ExtType _LCPEcho = {"LCPEcho",kStructureType,8};
	VPL_ExtType _CCMiscInfo = {"CCMiscInfo",kStructureType,24};
	VPL_ExtType _PPPMRULimits = {"PPPMRULimits",kStructureType,12};
	VPL_ExtType _1029 = {"1029",kEnumType,4};
	VPL_ExtType _1028 = {"1028",kEnumType,4};
	VPL_ExtType _1027 = {"1027",kEnumType,4};
	VPL_ExtType _1026 = {"1026",kEnumType,4};
	VPL_ExtType _1025 = {"1025",kEnumType,4};
	VPL_ExtType _1024 = {"1024",kEnumType,4};
	VPL_ExtType _1023 = {"1023",kEnumType,4};
	VPL_ExtType _1022 = {"1022",kEnumType,4};
	VPL_ExtType _1021 = {"1021",kEnumType,4};
	VPL_ExtType _1020 = {"1020",kEnumType,4};
	VPL_ExtType _1019 = {"1019",kEnumType,4};
	VPL_ExtType _1018 = {"1018",kEnumType,4};
	VPL_ExtType _1017 = {"1017",kEnumType,4};
	VPL_ExtType _1016 = {"1016",kEnumType,4};
	VPL_ExtType _1015 = {"1015",kEnumType,4};
	VPL_ExtType _1014 = {"1014",kEnumType,4};
	VPL_ExtType _1013 = {"1013",kEnumType,4};
	VPL_ExtType _1012 = {"1012",kEnumType,4};
	VPL_ExtType _1011 = {"1011",kEnumType,4};
	VPL_ExtType _1010 = {"1010",kEnumType,4};
	VPL_ExtType _1009 = {"1009",kEnumType,4};
	VPL_ExtType _1008 = {"1008",kEnumType,4};
	VPL_ExtType _1007 = {"1007",kEnumType,4};
	VPL_ExtType _OTISDNAddress = {"OTISDNAddress",kStructureType,44};
	VPL_ExtType _1006 = {"1006",kEnumType,4};
	VPL_ExtType _1005 = {"1005",kEnumType,4};
	VPL_ExtType _1004 = {"1004",kEnumType,4};
	VPL_ExtType _1003 = {"1003",kEnumType,4};
	VPL_ExtType _1002 = {"1002",kEnumType,4};
	VPL_ExtType _1001 = {"1001",kEnumType,4};
	VPL_ExtType _1000 = {"1000",kEnumType,4};
	VPL_ExtType _999 = {"999",kEnumType,4};
	VPL_ExtType _998 = {"998",kEnumType,4};
	VPL_ExtType _997 = {"997",kEnumType,4};
	VPL_ExtType _996 = {"996",kEnumType,4};
	VPL_ExtType _995 = {"995",kEnumType,4};
	VPL_ExtType _994 = {"994",kEnumType,4};
	VPL_ExtType _993 = {"993",kEnumType,4};
	VPL_ExtType _992 = {"992",kEnumType,4};
	VPL_ExtType _991 = {"991",kEnumType,4};
	VPL_ExtType _T8022FullPacketHeader = {"T8022FullPacketHeader",kStructureType,24};
	VPL_ExtType _T8022SNAPHeader = {"T8022SNAPHeader",kStructureType,8};
	VPL_ExtType _T8022Header = {"T8022Header",kStructureType,4};
	VPL_ExtType _EnetPacketHeader = {"EnetPacketHeader",kStructureType,16};
	VPL_ExtType _990 = {"990",kEnumType,4};
	VPL_ExtType _989 = {"989",kEnumType,4};
	VPL_ExtType _988 = {"988",kEnumType,4};
	VPL_ExtType _T8022Address = {"T8022Address",kStructureType,16};
	VPL_ExtType _987 = {"987",kEnumType,4};
	VPL_ExtType _986 = {"986",kEnumType,4};
	VPL_ExtType _985 = {"985",kEnumType,4};
	VPL_ExtType _984 = {"984",kEnumType,4};
	VPL_ExtType _983 = {"983",kEnumType,4};
	VPL_ExtType _982 = {"982",kEnumType,4};
	VPL_ExtType _981 = {"981",kEnumType,4};
	VPL_ExtType _980 = {"980",kEnumType,4};
	VPL_ExtType _979 = {"979",kEnumType,4};
	VPL_ExtType _978 = {"978",kEnumType,4};
	VPL_ExtType _AppleTalkInfo = {"AppleTalkInfo",kStructureType,24};
	VPL_ExtType _DDPNBPAddress = {"DDPNBPAddress",kStructureType,116};
	VPL_ExtType _NBPAddress = {"NBPAddress",kStructureType,108};
	VPL_ExtType _DDPAddress = {"DDPAddress",kStructureType,8};
	VPL_ExtType _NBPEntity = {"NBPEntity",kStructureType,100};
	VPL_ExtType _977 = {"977",kEnumType,4};
	VPL_ExtType _976 = {"976",kEnumType,4};
	VPL_ExtType _975 = {"975",kEnumType,4};
	VPL_ExtType _974 = {"974",kEnumType,4};
	VPL_ExtType _973 = {"973",kEnumType,4};
	VPL_ExtType _972 = {"972",kEnumType,4};
	VPL_ExtType _971 = {"971",kEnumType,4};
	VPL_ExtType _970 = {"970",kEnumType,4};
	VPL_ExtType _969 = {"969",kEnumType,4};
	VPL_ExtType _968 = {"968",kEnumType,4};
	VPL_ExtType _967 = {"967",kEnumType,4};
	VPL_ExtType _966 = {"966",kEnumType,4};
	VPL_ExtType _965 = {"965",kEnumType,4};
	VPL_ExtType _InetDHCPOption = {"InetDHCPOption",kStructureType,4};
	VPL_ExtType _964 = {"964",kEnumType,4};
	VPL_ExtType _InetInterfaceInfo = {"InetInterfaceInfo",kStructureType,552};
	VPL_ExtType _963 = {"963",kEnumType,4};
	VPL_ExtType _962 = {"962",kEnumType,4};
	VPL_ExtType _DNSAddress = {"DNSAddress",kStructureType,260};
	VPL_ExtType _DNSQueryInfo = {"DNSQueryInfo",kStructureType,272};
	VPL_ExtType _InetMailExchange = {"InetMailExchange",kStructureType,260};
	VPL_ExtType _InetSysInfo = {"InetSysInfo",kStructureType,64};
	VPL_ExtType _InetHostInfo = {"InetHostInfo",kStructureType,296};
	VPL_ExtType _961 = {"961",kEnumType,4};
	VPL_ExtType _InetAddress = {"InetAddress",kStructureType,16};
	VPL_ExtType _960 = {"960",kEnumType,4};
	VPL_ExtType _TIPAddMulticast = {"TIPAddMulticast",kStructureType,8};
	VPL_ExtType _959 = {"959",kEnumType,4};
	VPL_ExtType _958 = {"958",kEnumType,4};
	VPL_ExtType _957 = {"957",kEnumType,4};
	VPL_ExtType _956 = {"956",kEnumType,4};
	VPL_ExtType _955 = {"955",kEnumType,4};
	VPL_ExtType _954 = {"954",kEnumType,4};
	VPL_ExtType _953 = {"953",kEnumType,4};
	VPL_ExtType _952 = {"952",kEnumType,4};
	VPL_ExtType _951 = {"951",kEnumType,4};
	VPL_ExtType _950 = {"950",kEnumType,4};
	VPL_ExtType _OTList = {"OTList",kStructureType,4};
	VPL_ExtType _OTLIFO = {"OTLIFO",kStructureType,4};
	VPL_ExtType _OTLink = {"OTLink",kStructureType,4};
	VPL_ExtType _949 = {"949",kEnumType,4};
	VPL_ExtType _OpaqueOTClientContextPtr = {"OpaqueOTClientContextPtr",kStructureType,0};
	VPL_ExtType _TLookupBuffer = {"TLookupBuffer",kStructureType,8};
	VPL_ExtType _TLookupReply = {"TLookupReply",kStructureType,16};
	VPL_ExtType _TLookupRequest = {"TLookupRequest",kStructureType,36};
	VPL_ExtType _TRegisterReply = {"TRegisterReply",kStructureType,16};
	VPL_ExtType _TRegisterRequest = {"TRegisterRequest",kStructureType,28};
	VPL_ExtType _TUnitReply = {"TUnitReply",kStructureType,28};
	VPL_ExtType _TUnitRequest = {"TUnitRequest",kStructureType,40};
	VPL_ExtType _TReply = {"TReply",kStructureType,28};
	VPL_ExtType _TRequest = {"TRequest",kStructureType,28};
	VPL_ExtType _TOptMgmt = {"TOptMgmt",kStructureType,16};
	VPL_ExtType _TUDErr = {"TUDErr",kStructureType,28};
	VPL_ExtType _TUnitData = {"TUnitData",kStructureType,36};
	VPL_ExtType _TCall = {"TCall",kStructureType,40};
	VPL_ExtType _TDiscon = {"TDiscon",kStructureType,20};
	VPL_ExtType _TBind = {"TBind",kStructureType,16};
	VPL_ExtType _948 = {"948",kEnumType,4};
	VPL_ExtType _OTBufferInfo = {"OTBufferInfo",kStructureType,12};
	VPL_ExtType _947 = {"947",kEnumType,4};
	VPL_ExtType _OTBuffer = {"OTBuffer",kStructureType,28};
	VPL_ExtType _946 = {"946",kEnumType,4};
	VPL_ExtType _OTData = {"OTData",kStructureType,12};
	VPL_ExtType _strbuf = {"strbuf",kStructureType,12};
	VPL_ExtType _TNetbuf = {"TNetbuf",kStructureType,12};
	VPL_ExtType _OTPortRecord = {"OTPortRecord",kStructureType,296};
	VPL_ExtType _945 = {"945",kEnumType,4};
	VPL_ExtType _944 = {"944",kEnumType,4};
	VPL_ExtType _943 = {"943",kEnumType,4};
	VPL_ExtType _942 = {"942",kEnumType,4};
	VPL_ExtType _941 = {"941",kEnumType,4};
	VPL_ExtType _940 = {"940",kEnumType,4};
	VPL_ExtType _939 = {"939",kEnumType,4};
	VPL_ExtType _TEndpointInfo = {"TEndpointInfo",kStructureType,32};
	VPL_ExtType _938 = {"938",kEnumType,4};
	VPL_ExtType _937 = {"937",kEnumType,4};
	VPL_ExtType _936 = {"936",kEnumType,4};
	VPL_ExtType _t_linger = {"t_linger",kStructureType,8};
	VPL_ExtType _t_kpalive = {"t_kpalive",kStructureType,8};
	VPL_ExtType _935 = {"935",kEnumType,4};
	VPL_ExtType _TOption = {"TOption",kStructureType,20};
	VPL_ExtType _TOptionHeader = {"TOptionHeader",kStructureType,16};
	VPL_ExtType _OTConfiguration = {"OTConfiguration",kStructureType,0};
	VPL_ExtType _934 = {"934",kEnumType,4};
	VPL_ExtType _933 = {"933",kEnumType,4};
	VPL_ExtType _932 = {"932",kEnumType,4};
	VPL_ExtType _OTScriptInfo = {"OTScriptInfo",kStructureType,12};
	VPL_ExtType _931 = {"931",kEnumType,4};
	VPL_ExtType _930 = {"930",kEnumType,4};
	VPL_ExtType _929 = {"929",kEnumType,4};
	VPL_ExtType _928 = {"928",kEnumType,4};
	VPL_ExtType _927 = {"927",kEnumType,4};
	VPL_ExtType _926 = {"926",kEnumType,4};
	VPL_ExtType _925 = {"925",kEnumType,4};
	VPL_ExtType _924 = {"924",kEnumType,4};
	VPL_ExtType _923 = {"923",kEnumType,4};
	VPL_ExtType _922 = {"922",kEnumType,4};
	VPL_ExtType _921 = {"921",kEnumType,4};
	VPL_ExtType _920 = {"920",kEnumType,4};
	VPL_ExtType _OTAddress = {"OTAddress",kStructureType,4};
	VPL_ExtType _919 = {"919",kEnumType,4};
	VPL_ExtType _918 = {"918",kEnumType,4};
	VPL_ExtType _917 = {"917",kEnumType,4};
	VPL_ExtType _916 = {"916",kEnumType,4};
	VPL_ExtType _TECPluginDispatchTable = {"TECPluginDispatchTable",kStructureType,84};
	VPL_ExtType _915 = {"915",kEnumType,4};
	VPL_ExtType _TECSnifferContextRec = {"TECSnifferContextRec",kStructureType,64};
	VPL_ExtType _TECConverterContextRec = {"TECConverterContextRec",kStructureType,96};
	VPL_ExtType _TECPluginStateRec = {"TECPluginStateRec",kStructureType,20};
	VPL_ExtType _TECBufferContextRec = {"TECBufferContextRec",kStructureType,32};
	VPL_ExtType _TextChunk = {"TextChunk",kStructureType,12};
	VPL_ExtType _CommentsChunk = {"CommentsChunk",kStructureType,24};
	VPL_ExtType _Comment = {"Comment",kStructureType,12};
	VPL_ExtType _ApplicationSpecificChunk = {"ApplicationSpecificChunk",kStructureType,16};
	VPL_ExtType _AudioRecordingChunk = {"AudioRecordingChunk",kStructureType,32};
	VPL_ExtType _MIDIDataChunk = {"MIDIDataChunk",kStructureType,12};
	VPL_ExtType _InstrumentChunk = {"InstrumentChunk",kStructureType,32};
	VPL_ExtType _AIFFLoop = {"AIFFLoop",kStructureType,8};
	VPL_ExtType _MarkerChunk = {"MarkerChunk",kStructureType,276};
	VPL_ExtType _Marker = {"Marker",kStructureType,264};
	VPL_ExtType _SoundDataChunk = {"SoundDataChunk",kStructureType,16};
	VPL_ExtType _ExtCommonChunk = {"ExtCommonChunk",kStructureType,40};
	VPL_ExtType _CommonChunk = {"CommonChunk",kStructureType,32};
	VPL_ExtType _FormatVersionChunk = {"FormatVersionChunk",kStructureType,12};
	VPL_ExtType _ContainerChunk = {"ContainerChunk",kStructureType,12};
	VPL_ExtType _ChunkHeader = {"ChunkHeader",kStructureType,8};
	VPL_ExtType _914 = {"914",kEnumType,4};
	VPL_ExtType _913 = {"913",kEnumType,4};
	VPL_ExtType _912 = {"912",kEnumType,4};
	VPL_ExtType _911 = {"911",kEnumType,4};
	VPL_ExtType _910 = {"910",kEnumType,4};
	VPL_ExtType _BTHeaderRec = {"BTHeaderRec",kStructureType,112};
	VPL_ExtType _909 = {"909",kEnumType,4};
	VPL_ExtType _BTNodeDescriptor = {"BTNodeDescriptor",kStructureType,16};
	VPL_ExtType _HFSPlusVolumeHeader = {"HFSPlusVolumeHeader",kStructureType,488};
	VPL_ExtType _HFSMasterDirectoryBlock = {"HFSMasterDirectoryBlock",kStructureType,176};
	VPL_ExtType _908 = {"908",kEnumType,4};
	VPL_ExtType _907 = {"907",kEnumType,4};
	VPL_ExtType _906 = {"906",kEnumType,4};
	VPL_ExtType _HFSPlusAttrRecord = {"HFSPlusAttrRecord",NULL,84};
	VPL_ExtType _HFSPlusAttrExtents = {"HFSPlusAttrExtents",kStructureType,72};
	VPL_ExtType _HFSPlusAttrForkData = {"HFSPlusAttrForkData",kStructureType,84};
	VPL_ExtType _HFSPlusAttrInlineData = {"HFSPlusAttrInlineData",kStructureType,16};
	VPL_ExtType _905 = {"905",kEnumType,4};
	VPL_ExtType _HFSPlusCatalogThread = {"HFSPlusCatalogThread",kStructureType,520};
	VPL_ExtType _HFSCatalogThread = {"HFSCatalogThread",kStructureType,48};
	VPL_ExtType _HFSPlusCatalogFile = {"HFSPlusCatalogFile",kStructureType,244};
	VPL_ExtType _HFSCatalogFile = {"HFSCatalogFile",kStructureType,112};
	VPL_ExtType _HFSPlusCatalogFolder = {"HFSPlusCatalogFolder",kStructureType,92};
	VPL_ExtType _HFSCatalogFolder = {"HFSCatalogFolder",kStructureType,76};
	VPL_ExtType _904 = {"904",kEnumType,4};
	VPL_ExtType _903 = {"903",kEnumType,4};
	VPL_ExtType _HFSPlusCatalogKey = {"HFSPlusCatalogKey",kStructureType,520};
	VPL_ExtType _HFSCatalogKey = {"HFSCatalogKey",kStructureType,40};
	VPL_ExtType _902 = {"902",kEnumType,4};
	VPL_ExtType _HFSPlusPermissions = {"HFSPlusPermissions",kStructureType,16};
	VPL_ExtType _HFSPlusForkData = {"HFSPlusForkData",kStructureType,76};
	VPL_ExtType _HFSPlusExtentDescriptor = {"HFSPlusExtentDescriptor",kStructureType,8};
	VPL_ExtType _HFSExtentDescriptor = {"HFSExtentDescriptor",kStructureType,4};
	VPL_ExtType _901 = {"901",kEnumType,4};
	VPL_ExtType _HFSPlusExtentKey = {"HFSPlusExtentKey",kStructureType,12};
	VPL_ExtType _HFSExtentKey = {"HFSExtentKey",kStructureType,12};
	VPL_ExtType _900 = {"900",kEnumType,4};
	VPL_ExtType _899 = {"899",kEnumType,4};
	VPL_ExtType _XLibExportedSymbol = {"XLibExportedSymbol",kStructureType,8};
	VPL_ExtType _898 = {"898",kEnumType,4};
	VPL_ExtType _XLibContainerHeader = {"XLibContainerHeader",kStructureType,80};
	VPL_ExtType _897 = {"897",kEnumType,4};
	VPL_ExtType _896 = {"896",kEnumType,4};
	VPL_ExtType _895 = {"895",kEnumType,4};
	VPL_ExtType _894 = {"894",kEnumType,4};
	VPL_ExtType _893 = {"893",kEnumType,4};
	VPL_ExtType _892 = {"892",kEnumType,4};
	VPL_ExtType _891 = {"891",kEnumType,4};
	VPL_ExtType _890 = {"890",kEnumType,4};
	VPL_ExtType _889 = {"889",kEnumType,4};
	VPL_ExtType _888 = {"888",kEnumType,4};
	VPL_ExtType _887 = {"887",kEnumType,4};
	VPL_ExtType _886 = {"886",kEnumType,4};
	VPL_ExtType _PEFLoaderRelocationHeader = {"PEFLoaderRelocationHeader",kStructureType,12};
	VPL_ExtType _885 = {"885",kEnumType,4};
	VPL_ExtType _884 = {"884",kEnumType,4};
	VPL_ExtType _PEFExportedSymbol = {"PEFExportedSymbol",kStructureType,12};
	VPL_ExtType _883 = {"883",kEnumType,4};
	VPL_ExtType _882 = {"882",NULL,4};
	VPL_ExtType _PEFExportedSymbolKey = {"PEFExportedSymbolKey",kStructureType,4};
	VPL_ExtType _PEFSplitHashWord = {"PEFSplitHashWord",kStructureType,4};
	VPL_ExtType _881 = {"881",kEnumType,4};
	VPL_ExtType _PEFExportedSymbolHashSlot = {"PEFExportedSymbolHashSlot",kStructureType,4};
	VPL_ExtType _880 = {"880",kEnumType,4};
	VPL_ExtType _879 = {"879",kEnumType,4};
	VPL_ExtType _PEFImportedSymbol = {"PEFImportedSymbol",kStructureType,4};
	VPL_ExtType _878 = {"878",kEnumType,4};
	VPL_ExtType _PEFImportedLibrary = {"PEFImportedLibrary",kStructureType,24};
	VPL_ExtType _PEFLoaderInfoHeader = {"PEFLoaderInfoHeader",kStructureType,56};
	VPL_ExtType _877 = {"877",kEnumType,4};
	VPL_ExtType _876 = {"876",kEnumType,4};
	VPL_ExtType _875 = {"875",kEnumType,4};
	VPL_ExtType _874 = {"874",kEnumType,4};
	VPL_ExtType _PEFSectionHeader = {"PEFSectionHeader",kStructureType,28};
	VPL_ExtType _873 = {"873",kEnumType,4};
	VPL_ExtType _872 = {"872",kEnumType,4};
	VPL_ExtType _PEFContainerHeader = {"PEFContainerHeader",kStructureType,40};
	VPL_ExtType _AVLTreeStruct = {"AVLTreeStruct",kStructureType,36};
	VPL_ExtType _871 = {"871",kEnumType,4};
	VPL_ExtType _870 = {"870",kEnumType,4};
	VPL_ExtType _869 = {"869",kEnumType,4};
	VPL_ExtType _868 = {"868",kEnumType,4};
	VPL_ExtType _MPAddressSpaceInfo = {"MPAddressSpaceInfo",kStructureType,80};
	VPL_ExtType _MPNotificationInfo = {"MPNotificationInfo",kStructureType,40};
	VPL_ExtType _MPCriticalRegionInfo = {"MPCriticalRegionInfo",kStructureType,28};
	VPL_ExtType _MPEventInfo = {"MPEventInfo",kStructureType,24};
	VPL_ExtType _MPSemaphoreInfo = {"MPSemaphoreInfo",kStructureType,28};
	VPL_ExtType _MPQueueInfo = {"MPQueueInfo",kStructureType,40};
	VPL_ExtType _867 = {"867",kEnumType,4};
	VPL_ExtType _TMTask = {"TMTask",kStructureType,24};
	VPL_ExtType _866 = {"866",kEnumType,4};
	VPL_ExtType _MultiUserGestalt = {"MultiUserGestalt",kStructureType,252};
	VPL_ExtType _865 = {"865",kEnumType,4};
	VPL_ExtType _864 = {"864",kEnumType,4};
	VPL_ExtType _863 = {"863",kEnumType,4};
	VPL_ExtType _862 = {"862",kEnumType,4};
	VPL_ExtType _861 = {"861",kEnumType,4};
	VPL_ExtType _FindFolderUserRedirectionGlobals = {"FindFolderUserRedirectionGlobals",kStructureType,56};
	VPL_ExtType _860 = {"860",kEnumType,4};
	VPL_ExtType _FolderRouting = {"FolderRouting",kStructureType,20};
	VPL_ExtType _FolderDesc = {"FolderDesc",kStructureType,96};
	VPL_ExtType _859 = {"859",kEnumType,4};
	VPL_ExtType _858 = {"858",kEnumType,4};
	VPL_ExtType _857 = {"857",kEnumType,4};
	VPL_ExtType _856 = {"856",kEnumType,4};
	VPL_ExtType _855 = {"855",kEnumType,4};
	VPL_ExtType _854 = {"854",kEnumType,4};
	VPL_ExtType _853 = {"853",kEnumType,4};
	VPL_ExtType _852 = {"852",kEnumType,4};
	VPL_ExtType _851 = {"851",kEnumType,4};
	VPL_ExtType _850 = {"850",kEnumType,4};
	VPL_ExtType _849 = {"849",kEnumType,4};
	VPL_ExtType _848 = {"848",kEnumType,4};
	VPL_ExtType _847 = {"847",kEnumType,4};
	VPL_ExtType _846 = {"846",kEnumType,4};
	VPL_ExtType _845 = {"845",kEnumType,4};
	VPL_ExtType _SchedulerInfoRec = {"SchedulerInfoRec",kStructureType,16};
	VPL_ExtType _844 = {"844",kEnumType,4};
	VPL_ExtType _843 = {"843",kEnumType,4};
	VPL_ExtType _842 = {"842",kEnumType,4};
	VPL_ExtType _841 = {"841",kEnumType,4};
	VPL_ExtType _840 = {"840",kEnumType,4};
	VPL_ExtType _839 = {"839",kEnumType,4};
	VPL_ExtType _838 = {"838",kEnumType,4};
	VPL_ExtType _837 = {"837",kEnumType,4};
	VPL_ExtType _836 = {"836",kEnumType,4};
	VPL_ExtType _835 = {"835",kEnumType,4};
	VPL_ExtType _834 = {"834",kEnumType,4};
	VPL_ExtType _833 = {"833",kEnumType,4};
	VPL_ExtType _832 = {"832",kEnumType,4};
	VPL_ExtType _UnicodeMapping = {"UnicodeMapping",kStructureType,12};
	VPL_ExtType _831 = {"831",kEnumType,4};
	VPL_ExtType _OpaqueUnicodeToTextRunInfo = {"OpaqueUnicodeToTextRunInfo",kStructureType,0};
	VPL_ExtType _OpaqueUnicodeToTextInfo = {"OpaqueUnicodeToTextInfo",kStructureType,0};
	VPL_ExtType _OpaqueTextToUnicodeInfo = {"OpaqueTextToUnicodeInfo",kStructureType,0};
	VPL_ExtType _TECConversionInfo = {"TECConversionInfo",kStructureType,12};
	VPL_ExtType _OpaqueTECSnifferObjectRef = {"OpaqueTECSnifferObjectRef",kStructureType,0};
	VPL_ExtType _OpaqueTECObjectRef = {"OpaqueTECObjectRef",kStructureType,0};
	VPL_ExtType _830 = {"830",kEnumType,4};
	VPL_ExtType _829 = {"829",kEnumType,4};
	VPL_ExtType _828 = {"828",kEnumType,4};
	VPL_ExtType _decform = {"decform",kStructureType,4};
	VPL_ExtType _827 = {"827",kStructureType,40};
	VPL_ExtType _decimal = {"decimal",kStructureType,44};
	VPL_ExtType _826 = {"826",kEnumType,4};
	VPL_ExtType _825 = {"825",kEnumType,4};
	VPL_ExtType _824 = {"824",kEnumType,4};
	VPL_ExtType _823 = {"823",kEnumType,4};
	VPL_ExtType _OpaqueTextBreakLocatorRef = {"OpaqueTextBreakLocatorRef",kStructureType,0};
	VPL_ExtType _822 = {"822",kEnumType,4};
	VPL_ExtType _821 = {"821",kEnumType,4};
	VPL_ExtType _820 = {"820",kEnumType,4};
	VPL_ExtType _819 = {"819",kEnumType,4};
	VPL_ExtType _818 = {"818",kEnumType,4};
	VPL_ExtType _817 = {"817",kEnumType,4};
	VPL_ExtType _OpaqueCollatorRef = {"OpaqueCollatorRef",kStructureType,0};
	VPL_ExtType _816 = {"816",kEnumType,4};
	VPL_ExtType _815 = {"815",kEnumType,4};
	VPL_ExtType _814 = {"814",kEnumType,4};
	VPL_ExtType _813 = {"813",kEnumType,4};
	VPL_ExtType _812 = {"812",kEnumType,4};
	VPL_ExtType _UCKeySequenceDataIndex = {"UCKeySequenceDataIndex",kStructureType,8};
	VPL_ExtType _UCKeyStateTerminators = {"UCKeyStateTerminators",kStructureType,8};
	VPL_ExtType _UCKeyStateRecordsIndex = {"UCKeyStateRecordsIndex",kStructureType,8};
	VPL_ExtType _UCKeyToCharTableIndex = {"UCKeyToCharTableIndex",kStructureType,12};
	VPL_ExtType _UCKeyModifiersToTableNum = {"UCKeyModifiersToTableNum",kStructureType,12};
	VPL_ExtType _UCKeyLayoutFeatureInfo = {"UCKeyLayoutFeatureInfo",kStructureType,8};
	VPL_ExtType _UCKeyboardLayout = {"UCKeyboardLayout",kStructureType,40};
	VPL_ExtType _UCKeyboardTypeHeader = {"UCKeyboardTypeHeader",kStructureType,28};
	VPL_ExtType _UCKeyStateEntryRange = {"UCKeyStateEntryRange",kStructureType,8};
	VPL_ExtType _UCKeyStateEntryTerminal = {"UCKeyStateEntryTerminal",kStructureType,4};
	VPL_ExtType _811 = {"811",kEnumType,4};
	VPL_ExtType _UCKeyStateRecord = {"UCKeyStateRecord",kStructureType,12};
	VPL_ExtType _810 = {"810",kEnumType,4};
	VPL_ExtType _NBreakTable = {"NBreakTable",kStructureType,276};
	VPL_ExtType _BreakTable = {"BreakTable",kStructureType,260};
	VPL_ExtType _ScriptRunStatus = {"ScriptRunStatus",kStructureType,4};
	VPL_ExtType _809 = {"809",kEnumType,4};
	VPL_ExtType _808 = {"808",kEnumType,4};
	VPL_ExtType _FVector = {"FVector",kStructureType,4};
	VPL_ExtType _807 = {"807",kEnumType,4};
	VPL_ExtType _806 = {"806",kEnumType,4};
	VPL_ExtType _805 = {"805",kEnumType,4};
	VPL_ExtType _NumFormatString = {"NumFormatString",kStructureType,256};
	VPL_ExtType _804 = {"804",kEnumType,4};
	VPL_ExtType _803 = {"803",kEnumType,4};
	VPL_ExtType _802 = {"802",kEnumType,4};
	VPL_ExtType _801 = {"801",kEnumType,4};
	VPL_ExtType _InterruptSetMember = {"InterruptSetMember",kStructureType,8};
	VPL_ExtType _OpaqueInterruptSetID = {"OpaqueInterruptSetID",kStructureType,0};
	VPL_ExtType _800 = {"800",kEnumType,4};
	VPL_ExtType _799 = {"799",kEnumType,4};
	VPL_ExtType _PageInformation = {"PageInformation",kStructureType,12};
	VPL_ExtType _798 = {"798",kEnumType,4};
	VPL_ExtType _797 = {"797",kEnumType,4};
	VPL_ExtType _796 = {"796",kEnumType,4};
	VPL_ExtType _795 = {"795",kEnumType,4};
	VPL_ExtType _794 = {"794",NULL,8};
	VPL_ExtType _IOPreparationTable = {"IOPreparationTable",kStructureType,48};
	VPL_ExtType _MultipleAddressRange = {"MultipleAddressRange",kStructureType,8};
	VPL_ExtType _AddressRange = {"AddressRange",kStructureType,8};
	VPL_ExtType _793 = {"793",kEnumType,4};
	VPL_ExtType _792 = {"792",kEnumType,4};
	VPL_ExtType _791 = {"791",kEnumType,4};
	VPL_ExtType _PhysicalAddressRange = {"PhysicalAddressRange",kStructureType,8};
	VPL_ExtType _LogicalAddressRange = {"LogicalAddressRange",kStructureType,8};
	VPL_ExtType _790 = {"790",kEnumType,4};
	VPL_ExtType _OpaqueTimerID = {"OpaqueTimerID",kStructureType,0};
	VPL_ExtType _OpaqueTaskID = {"OpaqueTaskID",kStructureType,0};
	VPL_ExtType _OpaqueSoftwareInterruptID = {"OpaqueSoftwareInterruptID",kStructureType,0};
	VPL_ExtType _OpaqueIOPreparationID = {"OpaqueIOPreparationID",kStructureType,0};
	VPL_ExtType _ExceptionInformationPowerPC = {"ExceptionInformationPowerPC",kStructureType,24};
	VPL_ExtType _ExceptionInfo = {"ExceptionInfo",NULL,4};
	VPL_ExtType _789 = {"789",kEnumType,4};
	VPL_ExtType _MemoryExceptionInformation = {"MemoryExceptionInformation",kStructureType,16};
	VPL_ExtType _788 = {"788",kEnumType,4};
	VPL_ExtType _VectorInformationPowerPC = {"VectorInformationPowerPC",kStructureType,532};
	VPL_ExtType _Vector128 = {"Vector128",NULL,16};
	VPL_ExtType _FPUInformationPowerPC = {"FPUInformationPowerPC",kStructureType,264};
	VPL_ExtType _RegisterInformationPowerPC = {"RegisterInformationPowerPC",kStructureType,256};
	VPL_ExtType _MachineInformationPowerPC = {"MachineInformationPowerPC",kStructureType,64};
	VPL_ExtType _OpaqueAreaID = {"OpaqueAreaID",kStructureType,0};
	VPL_ExtType _OpaqueIOCommandID = {"OpaqueIOCommandID",kStructureType,0};
	VPL_ExtType _787 = {"787",kEnumType,4};
	VPL_ExtType _786 = {"786",kEnumType,4};
	VPL_ExtType _DRVRHeader = {"DRVRHeader",kStructureType,20};
	VPL_ExtType _785 = {"785",kEnumType,4};
	VPL_ExtType _784 = {"784",kEnumType,4};
	VPL_ExtType _783 = {"783",kEnumType,4};
	VPL_ExtType _782 = {"782",kEnumType,4};
	VPL_ExtType _781 = {"781",kEnumType,4};
	VPL_ExtType _780 = {"780",kEnumType,4};
	VPL_ExtType _779 = {"779",kEnumType,4};
	VPL_ExtType _778 = {"778",kEnumType,4};
	VPL_ExtType _777 = {"777",kEnumType,4};
	VPL_ExtType _OpaqueRegPropertyIter = {"OpaqueRegPropertyIter",kStructureType,0};
	VPL_ExtType _OpaqueRegEntryIter = {"OpaqueRegEntryIter",kStructureType,0};
	VPL_ExtType _776 = {"776",kEnumType,4};
	VPL_ExtType _775 = {"775",kEnumType,4};
	VPL_ExtType _774 = {"774",kEnumType,4};
	VPL_ExtType _773 = {"773",kEnumType,4};
	VPL_ExtType _772 = {"772",kEnumType,4};
	VPL_ExtType _771 = {"771",kEnumType,4};
	VPL_ExtType _770 = {"770",kEnumType,4};
	VPL_ExtType _769 = {"769",kEnumType,4};
	VPL_ExtType _768 = {"768",kEnumType,4};
	VPL_ExtType _RegEntryID = {"RegEntryID",kStructureType,16};
	VPL_ExtType _OpaqueDeviceNodePtr = {"OpaqueDeviceNodePtr",kStructureType,0};
	VPL_ExtType _767 = {"767",kEnumType,4};
	VPL_ExtType _766 = {"766",kEnumType,4};
	VPL_ExtType _765 = {"765",kEnumType,4};
	VPL_ExtType _764 = {"764",kEnumType,4};
	VPL_ExtType _763 = {"763",kEnumType,4};
	VPL_ExtType _LocaleAndVariant = {"LocaleAndVariant",kStructureType,8};
	VPL_ExtType _762 = {"762",kEnumType,4};
	VPL_ExtType _OpaqueLocaleRef = {"OpaqueLocaleRef",kStructureType,0};
	VPL_ExtType _AliasRecord = {"AliasRecord",kStructureType,8};
	VPL_ExtType _761 = {"761",kEnumType,4};
	VPL_ExtType _760 = {"760",kEnumType,4};
	VPL_ExtType _759 = {"759",kEnumType,4};
	VPL_ExtType _758 = {"758",kEnumType,4};
	VPL_ExtType _757 = {"757",kEnumType,4};
	VPL_ExtType _CFragSystem7InitBlock = {"CFragSystem7InitBlock",kStructureType,36};
	VPL_ExtType _756 = {"756",NULL,12};
	VPL_ExtType _CFragSystem7Locator = {"CFragSystem7Locator",kStructureType,16};
	VPL_ExtType _CFragCFBundleLocator = {"CFragCFBundleLocator",kStructureType,12};
	VPL_ExtType _CFragSystem7SegmentedLocator = {"CFragSystem7SegmentedLocator",kStructureType,12};
	VPL_ExtType _CFragSystem7DiskFlatLocator = {"CFragSystem7DiskFlatLocator",kStructureType,12};
	VPL_ExtType _CFragSystem7MemoryLocator = {"CFragSystem7MemoryLocator",kStructureType,12};
	VPL_ExtType _755 = {"755",kEnumType,4};
	VPL_ExtType _754 = {"754",kEnumType,4};
	VPL_ExtType _753 = {"753",kEnumType,4};
	VPL_ExtType _OpaqueCFragContainerID = {"OpaqueCFragContainerID",kStructureType,0};
	VPL_ExtType _OpaqueCFragClosureID = {"OpaqueCFragClosureID",kStructureType,0};
	VPL_ExtType _OpaqueCFragConnectionID = {"OpaqueCFragConnectionID",kStructureType,0};
	VPL_ExtType _752 = {"752",kEnumType,4};
	VPL_ExtType _CFragResource = {"CFragResource",kStructureType,96};
	VPL_ExtType _751 = {"751",kEnumType,4};
	VPL_ExtType _CFragResourceSearchExtension = {"CFragResourceSearchExtension",kStructureType,12};
	VPL_ExtType _CFragResourceExtensionHeader = {"CFragResourceExtensionHeader",kStructureType,4};
	VPL_ExtType _CFragResourceMember = {"CFragResourceMember",kStructureType,64};
	VPL_ExtType _750 = {"750",kEnumType,4};
	VPL_ExtType _CFragWhere2Union = {"CFragWhere2Union",NULL,4};
	VPL_ExtType _CFragWhere1Union = {"CFragWhere1Union",NULL,4};
	VPL_ExtType _749 = {"749",kEnumType,4};
	VPL_ExtType _CFragUsage2Union = {"CFragUsage2Union",NULL,4};
	VPL_ExtType _CFragUsage1Union = {"CFragUsage1Union",NULL,4};
	VPL_ExtType _748 = {"748",kEnumType,4};
	VPL_ExtType _747 = {"747",kEnumType,4};
	VPL_ExtType _746 = {"746",kEnumType,4};
	VPL_ExtType _745 = {"745",kEnumType,4};
	VPL_ExtType _744 = {"744",kEnumType,4};
	VPL_ExtType _743 = {"743",kEnumType,4};
	VPL_ExtType _742 = {"742",kEnumType,4};
	VPL_ExtType _741 = {"741",kEnumType,4};
	VPL_ExtType _740 = {"740",kEnumType,4};
	VPL_ExtType _739 = {"739",kEnumType,4};
	VPL_ExtType _MPTaskInfo = {"MPTaskInfo",kStructureType,84};
	VPL_ExtType _MPTaskInfoVersion2 = {"MPTaskInfoVersion2",kStructureType,64};
	VPL_ExtType _738 = {"738",kEnumType,4};
	VPL_ExtType _737 = {"737",kEnumType,4};
	VPL_ExtType _736 = {"736",kEnumType,4};
	VPL_ExtType _735 = {"735",kEnumType,4};
	VPL_ExtType _734 = {"734",kEnumType,4};
	VPL_ExtType _733 = {"733",kEnumType,4};
	VPL_ExtType _732 = {"732",kEnumType,4};
	VPL_ExtType _731 = {"731",kEnumType,4};
	VPL_ExtType _730 = {"730",kEnumType,4};
	VPL_ExtType _729 = {"729",kEnumType,4};
	VPL_ExtType _728 = {"728",kEnumType,4};
	VPL_ExtType _727 = {"727",kEnumType,4};
	VPL_ExtType _OpaqueMPOpaqueID = {"OpaqueMPOpaqueID",kStructureType,0};
	VPL_ExtType _OpaqueMPConsoleID = {"OpaqueMPConsoleID",kStructureType,0};
	VPL_ExtType _OpaqueMPAreaID = {"OpaqueMPAreaID",kStructureType,0};
	VPL_ExtType _OpaqueMPCpuID = {"OpaqueMPCpuID",kStructureType,0};
	VPL_ExtType _OpaqueMPCoherenceID = {"OpaqueMPCoherenceID",kStructureType,0};
	VPL_ExtType _OpaqueMPNotificationID = {"OpaqueMPNotificationID",kStructureType,0};
	VPL_ExtType _OpaqueMPAddressSpaceID = {"OpaqueMPAddressSpaceID",kStructureType,0};
	VPL_ExtType _OpaqueMPEventID = {"OpaqueMPEventID",kStructureType,0};
	VPL_ExtType _OpaqueMPTimerID = {"OpaqueMPTimerID",kStructureType,0};
	VPL_ExtType _OpaqueMPCriticalRegionID = {"OpaqueMPCriticalRegionID",kStructureType,0};
	VPL_ExtType _OpaqueMPSemaphoreID = {"OpaqueMPSemaphoreID",kStructureType,0};
	VPL_ExtType _OpaqueMPQueueID = {"OpaqueMPQueueID",kStructureType,0};
	VPL_ExtType _OpaqueMPTaskID = {"OpaqueMPTaskID",kStructureType,0};
	VPL_ExtType _OpaqueMPProcessID = {"OpaqueMPProcessID",kStructureType,0};
	VPL_ExtType _726 = {"726",kEnumType,4};
	VPL_ExtType _725 = {"725",kEnumType,4};
	VPL_ExtType _724 = {"724",kEnumType,4};
	VPL_ExtType _723 = {"723",kEnumType,4};
	VPL_ExtType _722 = {"722",kEnumType,4};
	VPL_ExtType _721 = {"721",kEnumType,4};
	VPL_ExtType _ComponentMPWorkFunctionHeaderRecord = {"ComponentMPWorkFunctionHeaderRecord",kStructureType,16};
	VPL_ExtType _720 = {"720",kEnumType,4};
	VPL_ExtType _719 = {"719",kEnumType,4};
	VPL_ExtType _718 = {"718",kEnumType,4};
	VPL_ExtType _717 = {"717",kEnumType,4};
	VPL_ExtType _RegisteredComponentInstanceRecord = {"RegisteredComponentInstanceRecord",kStructureType,4};
	VPL_ExtType _RegisteredComponentRecord = {"RegisteredComponentRecord",kStructureType,4};
	VPL_ExtType _ComponentInstanceRecord = {"ComponentInstanceRecord",kStructureType,4};
	VPL_ExtType _ComponentRecord = {"ComponentRecord",kStructureType,4};
	VPL_ExtType _ComponentParameters = {"ComponentParameters",kStructureType,8};
	VPL_ExtType _ComponentAliasResource = {"ComponentAliasResource",kStructureType,72};
	VPL_ExtType _ExtComponentResource = {"ExtComponentResource",kStructureType,84};
	VPL_ExtType _ComponentPlatformInfoArray = {"ComponentPlatformInfoArray",kStructureType,20};
	VPL_ExtType _ComponentResourceExtension = {"ComponentResourceExtension",kStructureType,12};
	VPL_ExtType _ComponentPlatformInfo = {"ComponentPlatformInfo",kStructureType,16};
	VPL_ExtType _ComponentResource = {"ComponentResource",kStructureType,52};
	VPL_ExtType _ResourceSpec = {"ResourceSpec",kStructureType,8};
	VPL_ExtType _ComponentDescription = {"ComponentDescription",kStructureType,20};
	VPL_ExtType _716 = {"716",kEnumType,4};
	VPL_ExtType _715 = {"715",kEnumType,4};
	VPL_ExtType _714 = {"714",kEnumType,4};
	VPL_ExtType _713 = {"713",kEnumType,4};
	VPL_ExtType _712 = {"712",kEnumType,4};
	VPL_ExtType _711 = {"711",kEnumType,4};
	VPL_ExtType _710 = {"710",kEnumType,4};
	VPL_ExtType _709 = {"709",kEnumType,4};
	VPL_ExtType _708 = {"708",kEnumType,4};
	VPL_ExtType _707 = {"707",kEnumType,4};
	VPL_ExtType _706 = {"706",kEnumType,4};
	VPL_ExtType _705 = {"705",kEnumType,4};
	VPL_ExtType _704 = {"704",kEnumType,4};
	VPL_ExtType _703 = {"703",kEnumType,4};
	VPL_ExtType _702 = {"702",kEnumType,4};
	VPL_ExtType _701 = {"701",kEnumType,4};
	VPL_ExtType _700 = {"700",kEnumType,4};
	VPL_ExtType _699 = {"699",kEnumType,4};
	VPL_ExtType _698 = {"698",kEnumType,4};
	VPL_ExtType _697 = {"697",kEnumType,4};
	VPL_ExtType _696 = {"696",kEnumType,4};
	VPL_ExtType _695 = {"695",kEnumType,4};
	VPL_ExtType _694 = {"694",kEnumType,4};
	VPL_ExtType _693 = {"693",kEnumType,4};
	VPL_ExtType _692 = {"692",kEnumType,4};
	VPL_ExtType _691 = {"691",kEnumType,4};
	VPL_ExtType _690 = {"690",kEnumType,4};
	VPL_ExtType _689 = {"689",kEnumType,4};
	VPL_ExtType _688 = {"688",kEnumType,4};
	VPL_ExtType _687 = {"687",kEnumType,4};
	VPL_ExtType _686 = {"686",kEnumType,4};
	VPL_ExtType _685 = {"685",kEnumType,4};
	VPL_ExtType _684 = {"684",kEnumType,4};
	VPL_ExtType _683 = {"683",kEnumType,4};
	VPL_ExtType _682 = {"682",kEnumType,4};
	VPL_ExtType _681 = {"681",kEnumType,4};
	VPL_ExtType _680 = {"680",kEnumType,4};
	VPL_ExtType _679 = {"679",kEnumType,4};
	VPL_ExtType _678 = {"678",kEnumType,4};
	VPL_ExtType _677 = {"677",kEnumType,4};
	VPL_ExtType _676 = {"676",kEnumType,4};
	VPL_ExtType _675 = {"675",kEnumType,4};
	VPL_ExtType _674 = {"674",kEnumType,4};
	VPL_ExtType _673 = {"673",kEnumType,4};
	VPL_ExtType _672 = {"672",kEnumType,4};
	VPL_ExtType _671 = {"671",kEnumType,4};
	VPL_ExtType _670 = {"670",kEnumType,4};
	VPL_ExtType _669 = {"669",kEnumType,4};
	VPL_ExtType _668 = {"668",kEnumType,4};
	VPL_ExtType _667 = {"667",kEnumType,4};
	VPL_ExtType _666 = {"666",kEnumType,4};
	VPL_ExtType _665 = {"665",kEnumType,4};
	VPL_ExtType _664 = {"664",kEnumType,4};
	VPL_ExtType _663 = {"663",kEnumType,4};
	VPL_ExtType _662 = {"662",kEnumType,4};
	VPL_ExtType _661 = {"661",kEnumType,4};
	VPL_ExtType _660 = {"660",kEnumType,4};
	VPL_ExtType _659 = {"659",kEnumType,4};
	VPL_ExtType _658 = {"658",kEnumType,4};
	VPL_ExtType _657 = {"657",kEnumType,4};
	VPL_ExtType _656 = {"656",kEnumType,4};
	VPL_ExtType _655 = {"655",kEnumType,4};
	VPL_ExtType _654 = {"654",kEnumType,4};
	VPL_ExtType _653 = {"653",kEnumType,4};
	VPL_ExtType _652 = {"652",kEnumType,4};
	VPL_ExtType _651 = {"651",kEnumType,4};
	VPL_ExtType _650 = {"650",kEnumType,4};
	VPL_ExtType _649 = {"649",kEnumType,4};
	VPL_ExtType _648 = {"648",kEnumType,4};
	VPL_ExtType _647 = {"647",kEnumType,4};
	VPL_ExtType _646 = {"646",kEnumType,4};
	VPL_ExtType _645 = {"645",kEnumType,4};
	VPL_ExtType _644 = {"644",kEnumType,4};
	VPL_ExtType _643 = {"643",kEnumType,4};
	VPL_ExtType _642 = {"642",kEnumType,4};
	VPL_ExtType _641 = {"641",kEnumType,4};
	VPL_ExtType _640 = {"640",kEnumType,4};
	VPL_ExtType _639 = {"639",kEnumType,4};
	VPL_ExtType _638 = {"638",kEnumType,4};
	VPL_ExtType _637 = {"637",kEnumType,4};
	VPL_ExtType _636 = {"636",kEnumType,4};
	VPL_ExtType _635 = {"635",kEnumType,4};
	VPL_ExtType _634 = {"634",kEnumType,4};
	VPL_ExtType _633 = {"633",kEnumType,4};
	VPL_ExtType _632 = {"632",kEnumType,4};
	VPL_ExtType _631 = {"631",kEnumType,4};
	VPL_ExtType _630 = {"630",kEnumType,4};
	VPL_ExtType _629 = {"629",kEnumType,4};
	VPL_ExtType _628 = {"628",kEnumType,4};
	VPL_ExtType _627 = {"627",kEnumType,4};
	VPL_ExtType _626 = {"626",kEnumType,4};
	VPL_ExtType _625 = {"625",kEnumType,4};
	VPL_ExtType _624 = {"624",kEnumType,4};
	VPL_ExtType _623 = {"623",kEnumType,4};
	VPL_ExtType _622 = {"622",kEnumType,4};
	VPL_ExtType _621 = {"621",kEnumType,4};
	VPL_ExtType _620 = {"620",kEnumType,4};
	VPL_ExtType _619 = {"619",kEnumType,4};
	VPL_ExtType _618 = {"618",kEnumType,4};
	VPL_ExtType _617 = {"617",kEnumType,4};
	VPL_ExtType _616 = {"616",kEnumType,4};
	VPL_ExtType _615 = {"615",kEnumType,4};
	VPL_ExtType _614 = {"614",kEnumType,4};
	VPL_ExtType _613 = {"613",kEnumType,4};
	VPL_ExtType _612 = {"612",kEnumType,4};
	VPL_ExtType _611 = {"611",kEnumType,4};
	VPL_ExtType _610 = {"610",kEnumType,4};
	VPL_ExtType _609 = {"609",kEnumType,4};
	VPL_ExtType _608 = {"608",kEnumType,4};
	VPL_ExtType _607 = {"607",kEnumType,4};
	VPL_ExtType _606 = {"606",kEnumType,4};
	VPL_ExtType _605 = {"605",kEnumType,4};
	VPL_ExtType _604 = {"604",kEnumType,4};
	VPL_ExtType _603 = {"603",kEnumType,4};
	VPL_ExtType _602 = {"602",kEnumType,4};
	VPL_ExtType _601 = {"601",kEnumType,4};
	VPL_ExtType _600 = {"600",kEnumType,4};
	VPL_ExtType _599 = {"599",kEnumType,4};
	VPL_ExtType _598 = {"598",kEnumType,4};
	VPL_ExtType _597 = {"597",kEnumType,4};
	VPL_ExtType _596 = {"596",kEnumType,4};
	VPL_ExtType _595 = {"595",kEnumType,4};
	VPL_ExtType _594 = {"594",kEnumType,4};
	VPL_ExtType _593 = {"593",kEnumType,4};
	VPL_ExtType _592 = {"592",kEnumType,4};
	VPL_ExtType _591 = {"591",kEnumType,4};
	VPL_ExtType _590 = {"590",kEnumType,4};
	VPL_ExtType _589 = {"589",kEnumType,4};
	VPL_ExtType _588 = {"588",kEnumType,4};
	VPL_ExtType _587 = {"587",kEnumType,4};
	VPL_ExtType _586 = {"586",kEnumType,4};
	VPL_ExtType _585 = {"585",kEnumType,4};
	VPL_ExtType _584 = {"584",kEnumType,4};
	VPL_ExtType _583 = {"583",kEnumType,4};
	VPL_ExtType _582 = {"582",kEnumType,4};
	VPL_ExtType _581 = {"581",kEnumType,4};
	VPL_ExtType _580 = {"580",kEnumType,4};
	VPL_ExtType _579 = {"579",kEnumType,4};
	VPL_ExtType _578 = {"578",kEnumType,4};
	VPL_ExtType _577 = {"577",kEnumType,4};
	VPL_ExtType _576 = {"576",kEnumType,4};
	VPL_ExtType _575 = {"575",kEnumType,4};
	VPL_ExtType _574 = {"574",kEnumType,4};
	VPL_ExtType _573 = {"573",kEnumType,4};
	VPL_ExtType _572 = {"572",kEnumType,4};
	VPL_ExtType _571 = {"571",kEnumType,4};
	VPL_ExtType _570 = {"570",kEnumType,4};
	VPL_ExtType _569 = {"569",kEnumType,4};
	VPL_ExtType _568 = {"568",kEnumType,4};
	VPL_ExtType _567 = {"567",kEnumType,4};
	VPL_ExtType _566 = {"566",kEnumType,4};
	VPL_ExtType _565 = {"565",kEnumType,4};
	VPL_ExtType _564 = {"564",kEnumType,4};
	VPL_ExtType _563 = {"563",kEnumType,4};
	VPL_ExtType _562 = {"562",kEnumType,4};
	VPL_ExtType _561 = {"561",kEnumType,4};
	VPL_ExtType _560 = {"560",kEnumType,4};
	VPL_ExtType _559 = {"559",kEnumType,4};
	VPL_ExtType _558 = {"558",kEnumType,4};
	VPL_ExtType _557 = {"557",kEnumType,4};
	VPL_ExtType _556 = {"556",kEnumType,4};
	VPL_ExtType _555 = {"555",kEnumType,4};
	VPL_ExtType _554 = {"554",kEnumType,4};
	VPL_ExtType _553 = {"553",kEnumType,4};
	VPL_ExtType _552 = {"552",kEnumType,4};
	VPL_ExtType _551 = {"551",kEnumType,4};
	VPL_ExtType _550 = {"550",kEnumType,4};
	VPL_ExtType _549 = {"549",kEnumType,4};
	VPL_ExtType _548 = {"548",kEnumType,4};
	VPL_ExtType _547 = {"547",kEnumType,4};
	VPL_ExtType _546 = {"546",kEnumType,4};
	VPL_ExtType _545 = {"545",kEnumType,4};
	VPL_ExtType _544 = {"544",kEnumType,4};
	VPL_ExtType _543 = {"543",kEnumType,4};
	VPL_ExtType _542 = {"542",kEnumType,4};
	VPL_ExtType _541 = {"541",kEnumType,4};
	VPL_ExtType _540 = {"540",kEnumType,4};
	VPL_ExtType _539 = {"539",kEnumType,4};
	VPL_ExtType _538 = {"538",kEnumType,4};
	VPL_ExtType _537 = {"537",kEnumType,4};
	VPL_ExtType _536 = {"536",kEnumType,4};
	VPL_ExtType _535 = {"535",kEnumType,4};
	VPL_ExtType _534 = {"534",kEnumType,4};
	VPL_ExtType _533 = {"533",kEnumType,4};
	VPL_ExtType _532 = {"532",kEnumType,4};
	VPL_ExtType _531 = {"531",kEnumType,4};
	VPL_ExtType _530 = {"530",kEnumType,4};
	VPL_ExtType _529 = {"529",kEnumType,4};
	VPL_ExtType _528 = {"528",kEnumType,4};
	VPL_ExtType _527 = {"527",kEnumType,4};
	VPL_ExtType _526 = {"526",kEnumType,4};
	VPL_ExtType _525 = {"525",kEnumType,4};
	VPL_ExtType _524 = {"524",kEnumType,4};
	VPL_ExtType _523 = {"523",kEnumType,4};
	VPL_ExtType _522 = {"522",kEnumType,4};
	VPL_ExtType _521 = {"521",kEnumType,4};
	VPL_ExtType _520 = {"520",kEnumType,4};
	VPL_ExtType _519 = {"519",kEnumType,4};
	VPL_ExtType _518 = {"518",kEnumType,4};
	VPL_ExtType _517 = {"517",kEnumType,4};
	VPL_ExtType _516 = {"516",kEnumType,4};
	VPL_ExtType _515 = {"515",kEnumType,4};
	VPL_ExtType _514 = {"514",kEnumType,4};
	VPL_ExtType _513 = {"513",kEnumType,4};
	VPL_ExtType _512 = {"512",kEnumType,4};
	VPL_ExtType _511 = {"511",kEnumType,4};
	VPL_ExtType _510 = {"510",kEnumType,4};
	VPL_ExtType _509 = {"509",kEnumType,4};
	VPL_ExtType _508 = {"508",kEnumType,4};
	VPL_ExtType _507 = {"507",kEnumType,4};
	VPL_ExtType _506 = {"506",kEnumType,4};
	VPL_ExtType _OpaqueCollection = {"OpaqueCollection",kStructureType,0};
	VPL_ExtType _505 = {"505",kEnumType,4};
	VPL_ExtType _504 = {"504",kEnumType,4};
	VPL_ExtType _503 = {"503",kEnumType,4};
	VPL_ExtType _502 = {"502",kEnumType,4};
	VPL_ExtType _TokenBlock = {"TokenBlock",kStructureType,100};
	VPL_ExtType _TokenRec = {"TokenRec",kStructureType,16};
	VPL_ExtType _501 = {"501",kEnumType,4};
	VPL_ExtType _500 = {"500",kEnumType,4};
	VPL_ExtType _499 = {"499",kEnumType,4};
	VPL_ExtType _498 = {"498",kEnumType,4};
	VPL_ExtType _497 = {"497",kEnumType,4};
	VPL_ExtType _496 = {"496",kEnumType,4};
	VPL_ExtType _495 = {"495",kEnumType,4};
	VPL_ExtType _494 = {"494",kEnumType,4};
	VPL_ExtType _493 = {"493",kEnumType,4};
	VPL_ExtType _492 = {"492",kEnumType,4};
	VPL_ExtType _491 = {"491",kEnumType,4};
	VPL_ExtType _490 = {"490",kEnumType,4};
	VPL_ExtType _489 = {"489",kEnumType,4};
	VPL_ExtType _488 = {"488",kEnumType,4};
	VPL_ExtType _487 = {"487",kEnumType,4};
	VPL_ExtType _486 = {"486",kEnumType,4};
	VPL_ExtType _485 = {"485",kEnumType,4};
	VPL_ExtType _484 = {"484",kEnumType,4};
	VPL_ExtType _483 = {"483",kEnumType,4};
	VPL_ExtType _482 = {"482",kEnumType,4};
	VPL_ExtType _481 = {"481",kEnumType,4};
	VPL_ExtType _480 = {"480",kEnumType,4};
	VPL_ExtType _479 = {"479",kEnumType,4};
	VPL_ExtType _478 = {"478",kEnumType,4};
	VPL_ExtType _477 = {"477",kEnumType,4};
	VPL_ExtType _476 = {"476",kEnumType,4};
	VPL_ExtType _475 = {"475",kEnumType,4};
	VPL_ExtType _474 = {"474",kEnumType,4};
	VPL_ExtType _473 = {"473",kEnumType,4};
	VPL_ExtType _472 = {"472",kEnumType,4};
	VPL_ExtType _471 = {"471",kEnumType,4};
	VPL_ExtType _470 = {"470",kEnumType,4};
	VPL_ExtType _469 = {"469",kEnumType,4};
	VPL_ExtType _468 = {"468",kEnumType,4};
	VPL_ExtType _467 = {"467",kEnumType,4};
	VPL_ExtType _466 = {"466",kEnumType,4};
	VPL_ExtType _465 = {"465",kEnumType,4};
	VPL_ExtType _464 = {"464",kEnumType,4};
	VPL_ExtType _463 = {"463",kEnumType,4};
	VPL_ExtType _462 = {"462",kEnumType,4};
	VPL_ExtType _461 = {"461",kEnumType,4};
	VPL_ExtType _460 = {"460",kEnumType,4};
	VPL_ExtType _459 = {"459",kEnumType,4};
	VPL_ExtType _458 = {"458",kEnumType,4};
	VPL_ExtType _457 = {"457",kEnumType,4};
	VPL_ExtType _456 = {"456",kEnumType,4};
	VPL_ExtType _455 = {"455",kEnumType,4};
	VPL_ExtType _454 = {"454",kEnumType,4};
	VPL_ExtType _453 = {"453",kEnumType,4};
	VPL_ExtType _452 = {"452",kEnumType,4};
	VPL_ExtType _451 = {"451",kEnumType,4};
	VPL_ExtType _ItlbExtRecord = {"ItlbExtRecord",kStructureType,52};
	VPL_ExtType _ItlbRecord = {"ItlbRecord",kStructureType,20};
	VPL_ExtType _ItlcRecord = {"ItlcRecord",kStructureType,48};
	VPL_ExtType _RuleBasedTrslRecord = {"RuleBasedTrslRecord",kStructureType,12};
	VPL_ExtType _Itl5Record = {"Itl5Record",kStructureType,28};
	VPL_ExtType _TableDirectoryRecord = {"TableDirectoryRecord",kStructureType,16};
	VPL_ExtType _NItl4Rec = {"NItl4Rec",kStructureType,72};
	VPL_ExtType _Itl4Rec = {"Itl4Rec",kStructureType,56};
	VPL_ExtType _NumberParts = {"NumberParts",kStructureType,324};
	VPL_ExtType _WideCharArr = {"WideCharArr",kStructureType,44};
	VPL_ExtType _WideChar = {"WideChar",NULL,4};
	VPL_ExtType _UntokenTable = {"UntokenTable",kStructureType,516};
	VPL_ExtType _Itl1ExtRec = {"Itl1ExtRec",kStructureType,384};
	VPL_ExtType _Intl1Rec = {"Intl1Rec",kStructureType,332};
	VPL_ExtType _Intl0Rec = {"Intl0Rec",kStructureType,32};
	VPL_ExtType _OffPair = {"OffPair",kStructureType,4};
	VPL_ExtType _450 = {"450",kEnumType,4};
	VPL_ExtType _449 = {"449",kEnumType,4};
	VPL_ExtType _448 = {"448",kEnumType,4};
	VPL_ExtType _447 = {"447",kEnumType,4};
	VPL_ExtType _446 = {"446",kEnumType,4};
	VPL_ExtType _445 = {"445",kEnumType,4};
	VPL_ExtType _444 = {"444",kEnumType,4};
	VPL_ExtType _440 = {"440",kEnumType,4};
	VPL_ExtType _439 = {"439",kEnumType,4};
	VPL_ExtType _438 = {"438",kEnumType,4};
	VPL_ExtType ___CFUserNotification = {"__CFUserNotification",kStructureType,0};
	VPL_ExtType _437 = {"437",kEnumType,4};
	VPL_ExtType ___CFWriteStream = {"__CFWriteStream",kStructureType,0};
	VPL_ExtType ___CFReadStream = {"__CFReadStream",kStructureType,0};
	VPL_ExtType _436 = {"436",kStructureType,20};
	VPL_ExtType _435 = {"435",kEnumType,4};
	VPL_ExtType _434 = {"434",kStructureType,8};
	VPL_ExtType _433 = {"433",kEnumType,4};
	VPL_ExtType _432 = {"432",kEnumType,4};
	VPL_ExtType _431 = {"431",kEnumType,4};
	VPL_ExtType ___CFNotificationCenter = {"__CFNotificationCenter",kStructureType,0};
	VPL_ExtType _430 = {"430",kStructureType,20};
	VPL_ExtType _429 = {"429",kEnumType,4};
	VPL_ExtType _428 = {"428",kStructureType,16};
	VPL_ExtType _427 = {"427",kEnumType,4};
	VPL_ExtType ___CFSocket = {"__CFSocket",kStructureType,0};
	VPL_ExtType _426 = {"426",kStructureType,20};
	VPL_ExtType _425 = {"425",kEnumType,4};
	VPL_ExtType ___CFMessagePort = {"__CFMessagePort",kStructureType,0};
	VPL_ExtType _424 = {"424",kStructureType,20};
	VPL_ExtType ___CFMachPort = {"__CFMachPort",kStructureType,0};
	VPL_ExtType _423 = {"423",kStructureType,20};
	VPL_ExtType _422 = {"422",kStructureType,20};
	VPL_ExtType _421 = {"421",kStructureType,36};
	VPL_ExtType _420 = {"420",kStructureType,40};
	VPL_ExtType _419 = {"419",kEnumType,4};
	VPL_ExtType _418 = {"418",kEnumType,4};
	VPL_ExtType ___CFRunLoopTimer = {"__CFRunLoopTimer",kStructureType,0};
	VPL_ExtType ___CFRunLoopObserver = {"__CFRunLoopObserver",kStructureType,0};
	VPL_ExtType ___CFRunLoopSource = {"__CFRunLoopSource",kStructureType,0};
	VPL_ExtType ___CFRunLoop = {"__CFRunLoop",kStructureType,0};
	VPL_ExtType _mach_port_qos = {"mach_port_qos",kStructureType,8};
	VPL_ExtType _mach_port_limits = {"mach_port_limits",kStructureType,4};
	VPL_ExtType _mach_port_status = {"mach_port_status",kStructureType,40};
	VPL_ExtType _417 = {"417",kEnumType,4};
	VPL_ExtType ___CFPlugInInstance = {"__CFPlugInInstance",kStructureType,0};
	VPL_ExtType _416 = {"416",kStructureType,16};
	VPL_ExtType ___CFUUID = {"__CFUUID",kStructureType,0};
	VPL_ExtType _403 = {"403",kStructureType,4};
	VPL_ExtType _402 = {"402",kStructureType,4};
	VPL_ExtType ___CFByteOrder = {"__CFByteOrder",kEnumType,4};
	VPL_ExtType ___CFBundle = {"__CFBundle",kStructureType,0};
	VPL_ExtType ___CFBitVector = {"__CFBitVector",kStructureType,0};
	VPL_ExtType ___CFBinaryHeap = {"__CFBinaryHeap",kStructureType,0};
	VPL_ExtType _378 = {"378",kStructureType,20};
	VPL_ExtType _377 = {"377",kStructureType,20};
	VPL_ExtType _tm = {"tm",kStructureType,44};
	VPL_ExtType _376 = {"376",kStructureType,8};
	VPL_ExtType _375 = {"375",kStructureType,8};
	VPL_ExtType ___sFILE = {"__sFILE",kStructureType,84};
	VPL_ExtType ___sbuf = {"__sbuf",kStructureType,8};
	VPL_ExtType _sigstack = {"sigstack",kStructureType,8};
	VPL_ExtType _sigvec = {"sigvec",kStructureType,12};
	VPL_ExtType _sigaltstack = {"sigaltstack",kStructureType,12};
	VPL_ExtType _sigaction = {"sigaction",kStructureType,12};
	VPL_ExtType _370 = {"370",kStructureType,8};
	VPL_ExtType __opaque_pthread_cond_t = {"_opaque_pthread_cond_t",kStructureType,28};
	VPL_ExtType __opaque_pthread_condattr_t = {"_opaque_pthread_condattr_t",kStructureType,8};
	VPL_ExtType __opaque_pthread_mutex_t = {"_opaque_pthread_mutex_t",kStructureType,44};
	VPL_ExtType __opaque_pthread_mutexattr_t = {"_opaque_pthread_mutexattr_t",kStructureType,12};
	VPL_ExtType __opaque_pthread_attr_t = {"_opaque_pthread_attr_t",kStructureType,40};
	VPL_ExtType __opaque_pthread_t = {"_opaque_pthread_t",kStructureType,604};
	VPL_ExtType __pthread_handler_rec = {"_pthread_handler_rec",kStructureType,12};
	VPL_ExtType _fd_set = {"fd_set",kStructureType,128};
	VPL_ExtType __jmp_buf = {"_jmp_buf",kStructureType,540};
	VPL_ExtType _sigcontext = {"sigcontext",kStructureType,24};
	VPL_ExtType _369 = {"369",kEnumType,4};
	VPL_ExtType _exception = {"exception",kStructureType,32};
	VPL_ExtType _fdversion = {"fdversion",kEnumType,4};
	VPL_ExtType _lconv = {"lconv",kStructureType,48};
	VPL_ExtType _345 = {"345",kStructureType,3156};
	VPL_ExtType _344 = {"344",kStructureType,8};
	VPL_ExtType _343 = {"343",kStructureType,16};
	VPL_ExtType _342 = {"342",kStructureType,20};
	VPL_ExtType _341 = {"341",kStructureType,24};
	VPL_ExtType _340 = {"340",kEnumType,4};
	VPL_ExtType _339 = {"339",kEnumType,4};
	VPL_ExtType ___CFXMLParser = {"__CFXMLParser",kStructureType,0};
	VPL_ExtType _338 = {"338",kStructureType,4};
	VPL_ExtType _337 = {"337",kStructureType,20};
	VPL_ExtType _336 = {"336",kEnumType,4};
	VPL_ExtType _335 = {"335",kStructureType,8};
	VPL_ExtType _334 = {"334",kStructureType,12};
	VPL_ExtType _333 = {"333",kStructureType,4};
	VPL_ExtType _332 = {"332",kStructureType,8};
	VPL_ExtType _331 = {"331",kStructureType,8};
	VPL_ExtType _330 = {"330",kStructureType,8};
	VPL_ExtType _329 = {"329",kStructureType,8};
	VPL_ExtType _328 = {"328",kStructureType,4};
	VPL_ExtType _327 = {"327",kStructureType,12};
	VPL_ExtType _326 = {"326",kEnumType,4};
	VPL_ExtType ___CFXMLNode = {"__CFXMLNode",kStructureType,0};
	VPL_ExtType _325 = {"325",kEnumType,4};
	VPL_ExtType ___CFURL = {"__CFURL",kStructureType,0};
	VPL_ExtType _324 = {"324",kEnumType,4};
	VPL_ExtType _323 = {"323",kEnumType,4};
	VPL_ExtType _OpaqueFNSubscriptionRef = {"OpaqueFNSubscriptionRef",kStructureType,0};
	VPL_ExtType _322 = {"322",kEnumType,4};
	VPL_ExtType _FSVolumeInfoParam = {"FSVolumeInfoParam",kStructureType,48};
	VPL_ExtType _FSVolumeInfo = {"FSVolumeInfo",kStructureType,136};
	VPL_ExtType _321 = {"321",kEnumType,4};
	VPL_ExtType _320 = {"320",kEnumType,4};
	VPL_ExtType _FSForkCBInfoParam = {"FSForkCBInfoParam",kStructureType,40};
	VPL_ExtType _FSForkInfo = {"FSForkInfo",kStructureType,32};
	VPL_ExtType _FSForkIOParam = {"FSForkIOParam",kStructureType,92};
	VPL_ExtType _319 = {"319",kEnumType,4};
	VPL_ExtType _FSCatalogBulkParam = {"FSCatalogBulkParam",kStructureType,64};
	VPL_ExtType _FSSearchParams = {"FSSearchParams",kStructureType,24};
	VPL_ExtType _318 = {"318",kEnumType,4};
	VPL_ExtType _317 = {"317",kEnumType,4};
	VPL_ExtType _OpaqueFSIterator = {"OpaqueFSIterator",kStructureType,0};
	VPL_ExtType _FSRefParam = {"FSRefParam",kStructureType,76};
	VPL_ExtType _FSCatalogInfo = {"FSCatalogInfo",kStructureType,148};
	VPL_ExtType _316 = {"316",kEnumType,4};
	VPL_ExtType _315 = {"315",kEnumType,4};
	VPL_ExtType _314 = {"314",kEnumType,4};
	VPL_ExtType _FSPermissionInfo = {"FSPermissionInfo",kStructureType,16};
	VPL_ExtType _FSRef = {"FSRef",kStructureType,80};
	VPL_ExtType _313 = {"313",kEnumType,4};
	VPL_ExtType _DrvQEl = {"DrvQEl",kStructureType,16};
	VPL_ExtType _VCB = {"VCB",kStructureType,196};
	VPL_ExtType _FCBPBRec = {"FCBPBRec",kStructureType,68};
	VPL_ExtType _WDPBRec = {"WDPBRec",kStructureType,56};
	VPL_ExtType _CMovePBRec = {"CMovePBRec",kStructureType,56};
	VPL_ExtType _HParamBlockRec = {"HParamBlockRec",NULL,136};
	VPL_ExtType _CSParam = {"CSParam",kStructureType,80};
	VPL_ExtType _ForeignPrivParam = {"ForeignPrivParam",kStructureType,72};
	VPL_ExtType _FIDParam = {"FIDParam",kStructureType,64};
	VPL_ExtType _WDParam = {"WDParam",kStructureType,56};
	VPL_ExtType _CopyParam = {"CopyParam",kStructureType,56};
	VPL_ExtType _ObjParam = {"ObjParam",kStructureType,40};
	VPL_ExtType _AccessParam = {"AccessParam",kStructureType,56};
	VPL_ExtType _XVolumeParam = {"XVolumeParam",kStructureType,144};
	VPL_ExtType _XIOParam = {"XIOParam",kStructureType,60};
	VPL_ExtType _HVolumeParam = {"HVolumeParam",kStructureType,136};
	VPL_ExtType _HFileParam = {"HFileParam",kStructureType,92};
	VPL_ExtType _HIOParam = {"HIOParam",kStructureType,56};
	VPL_ExtType _DTPBRec = {"DTPBRec",kStructureType,108};
	VPL_ExtType _AFPAlternateAddress = {"AFPAlternateAddress",kStructureType,4};
	VPL_ExtType _AFPTagData = {"AFPTagData",kStructureType,4};
	VPL_ExtType _312 = {"312",kEnumType,4};
	VPL_ExtType _311 = {"311",kEnumType,4};
	VPL_ExtType _310 = {"310",kEnumType,4};
	VPL_ExtType _AFPXVolMountInfo = {"AFPXVolMountInfo",kStructureType,208};
	VPL_ExtType _AFPVolMountInfo = {"AFPVolMountInfo",kStructureType,172};
	VPL_ExtType _309 = {"309",kEnumType,4};
	VPL_ExtType _VolumeMountInfoHeader = {"VolumeMountInfoHeader",kStructureType,12};
	VPL_ExtType _VolMountInfoHeader = {"VolMountInfoHeader",kStructureType,8};
	VPL_ExtType _308 = {"308",kEnumType,4};
	VPL_ExtType _FSSpec = {"FSSpec",kStructureType,72};
	VPL_ExtType _CatPositionRec = {"CatPositionRec",kStructureType,16};
	VPL_ExtType _XCInfoPBRec = {"XCInfoPBRec",kStructureType,56};
	VPL_ExtType _CInfoPBRec = {"CInfoPBRec",NULL,120};
	VPL_ExtType _DirInfo = {"DirInfo",kStructureType,112};
	VPL_ExtType _HFileInfo = {"HFileInfo",kStructureType,120};
	VPL_ExtType _MultiDevParam = {"MultiDevParam",kStructureType,44};
	VPL_ExtType _SlotDevParam = {"SlotDevParam",kStructureType,40};
	VPL_ExtType _CntrlParam = {"CntrlParam",kStructureType,52};
	VPL_ExtType _VolumeParam = {"VolumeParam",kStructureType,76};
	VPL_ExtType _FileParam = {"FileParam",kStructureType,92};
	VPL_ExtType _IOParam = {"IOParam",kStructureType,56};
	VPL_ExtType _ParamBlockRec = {"ParamBlockRec",NULL,92};
	VPL_ExtType _GetVolParmsInfoBuffer = {"GetVolParmsInfoBuffer",kStructureType,32};
	VPL_ExtType _307 = {"307",kEnumType,4};
	VPL_ExtType _306 = {"306",kEnumType,4};
	VPL_ExtType _305 = {"305",kEnumType,4};
	VPL_ExtType _304 = {"304",kEnumType,4};
	VPL_ExtType _303 = {"303",kEnumType,4};
	VPL_ExtType _302 = {"302",kEnumType,4};
	VPL_ExtType _301 = {"301",kEnumType,4};
	VPL_ExtType _300 = {"300",kEnumType,4};
	VPL_ExtType _299 = {"299",kEnumType,4};
	VPL_ExtType _298 = {"298",kEnumType,4};
	VPL_ExtType _297 = {"297",kEnumType,4};
	VPL_ExtType _296 = {"296",kEnumType,4};
	VPL_ExtType _295 = {"295",kEnumType,4};
	VPL_ExtType _294 = {"294",kEnumType,4};
	VPL_ExtType _293 = {"293",kEnumType,4};
	VPL_ExtType _292 = {"292",kEnumType,4};
	VPL_ExtType _291 = {"291",kEnumType,4};
	VPL_ExtType _290 = {"290",kEnumType,4};
	VPL_ExtType _289 = {"289",kEnumType,4};
	VPL_ExtType _288 = {"288",kEnumType,4};
	VPL_ExtType _287 = {"287",kEnumType,4};
	VPL_ExtType _286 = {"286",kEnumType,4};
	VPL_ExtType _285 = {"285",kEnumType,4};
	VPL_ExtType _284 = {"284",kEnumType,4};
	VPL_ExtType _283 = {"283",kEnumType,4};
	VPL_ExtType _HFSUniStr255 = {"HFSUniStr255",kStructureType,512};
	VPL_ExtType _DXInfo = {"DXInfo",kStructureType,16};
	VPL_ExtType _DInfo = {"DInfo",kStructureType,20};
	VPL_ExtType _FXInfo = {"FXInfo",kStructureType,16};
	VPL_ExtType _FInfo = {"FInfo",kStructureType,20};
	VPL_ExtType _ExtendedFolderInfo = {"ExtendedFolderInfo",kStructureType,16};
	VPL_ExtType _ExtendedFileInfo = {"ExtendedFileInfo",kStructureType,16};
	VPL_ExtType _FolderInfo = {"FolderInfo",kStructureType,20};
	VPL_ExtType _FileInfo = {"FileInfo",kStructureType,20};
	VPL_ExtType _282 = {"282",kEnumType,4};
	VPL_ExtType _281 = {"281",kEnumType,4};
	VPL_ExtType _280 = {"280",kEnumType,4};
	VPL_ExtType _279 = {"279",kEnumType,4};
	VPL_ExtType _278 = {"278",kEnumType,4};
	VPL_ExtType _277 = {"277",kEnumType,4};
	VPL_ExtType _276 = {"276",kEnumType,4};
	VPL_ExtType _275 = {"275",kEnumType,4};
	VPL_ExtType _274 = {"274",kEnumType,4};
	VPL_ExtType _RoutingResourceEntry = {"RoutingResourceEntry",kStructureType,20};
	VPL_ExtType _273 = {"273",kEnumType,4};
	VPL_ExtType _CustomBadgeResource = {"CustomBadgeResource",kStructureType,28};
	VPL_ExtType _272 = {"272",kEnumType,4};
	VPL_ExtType _271 = {"271",kEnumType,4};
	VPL_ExtType _270 = {"270",kEnumType,4};
	VPL_ExtType _269 = {"269",kEnumType,4};
	VPL_ExtType _LocalDateTime = {"LocalDateTime",kStructureType,12};
	VPL_ExtType _UTCDateTime = {"UTCDateTime",kStructureType,12};
	VPL_ExtType _268 = {"268",kEnumType,4};
	VPL_ExtType _267 = {"267",kEnumType,4};
	VPL_ExtType _266 = {"266",kEnumType,4};
	VPL_ExtType _265 = {"265",kEnumType,4};
	VPL_ExtType _264 = {"264",kEnumType,4};
	VPL_ExtType _263 = {"263",kEnumType,4};
	VPL_ExtType _262 = {"262",kEnumType,4};
	VPL_ExtType _261 = {"261",kEnumType,4};
	VPL_ExtType _260 = {"260",kEnumType,4};
	VPL_ExtType _259 = {"259",kEnumType,4};
	VPL_ExtType _258 = {"258",kEnumType,4};
	VPL_ExtType _257 = {"257",kEnumType,4};
	VPL_ExtType _256 = {"256",kEnumType,4};
	VPL_ExtType _255 = {"255",kEnumType,4};
	VPL_ExtType _254 = {"254",kEnumType,4};
	VPL_ExtType _253 = {"253",kEnumType,4};
	VPL_ExtType _252 = {"252",kEnumType,4};
	VPL_ExtType _251 = {"251",kEnumType,4};
	VPL_ExtType _250 = {"250",kEnumType,4};
	VPL_ExtType _249 = {"249",kEnumType,4};
	VPL_ExtType _248 = {"248",kEnumType,4};
	VPL_ExtType _247 = {"247",kEnumType,4};
	VPL_ExtType _246 = {"246",kEnumType,4};
	VPL_ExtType _245 = {"245",kEnumType,4};
	VPL_ExtType _244 = {"244",kEnumType,4};
	VPL_ExtType _243 = {"243",kEnumType,4};
	VPL_ExtType _242 = {"242",kEnumType,4};
	VPL_ExtType _241 = {"241",kEnumType,4};
	VPL_ExtType _240 = {"240",kEnumType,4};
	VPL_ExtType _239 = {"239",kEnumType,4};
	VPL_ExtType _238 = {"238",kEnumType,4};
	VPL_ExtType _237 = {"237",kEnumType,4};
	VPL_ExtType _236 = {"236",kEnumType,4};
	VPL_ExtType _235 = {"235",kEnumType,4};
	VPL_ExtType _234 = {"234",kEnumType,4};
	VPL_ExtType _233 = {"233",kEnumType,4};
	VPL_ExtType _232 = {"232",kEnumType,4};
	VPL_ExtType _231 = {"231",kEnumType,4};
	VPL_ExtType _230 = {"230",kEnumType,4};
	VPL_ExtType _229 = {"229",kEnumType,4};
	VPL_ExtType _228 = {"228",kEnumType,4};
	VPL_ExtType _227 = {"227",kEnumType,4};
	VPL_ExtType _226 = {"226",kEnumType,4};
	VPL_ExtType _225 = {"225",kEnumType,4};
	VPL_ExtType _224 = {"224",kEnumType,4};
	VPL_ExtType _223 = {"223",kEnumType,4};
	VPL_ExtType _222 = {"222",kEnumType,4};
	VPL_ExtType _221 = {"221",kEnumType,4};
	VPL_ExtType _220 = {"220",kEnumType,4};
	VPL_ExtType _219 = {"219",kEnumType,4};
	VPL_ExtType _218 = {"218",kEnumType,4};
	VPL_ExtType _217 = {"217",kEnumType,4};
	VPL_ExtType _216 = {"216",kEnumType,4};
	VPL_ExtType _215 = {"215",kEnumType,4};
	VPL_ExtType _214 = {"214",kEnumType,4};
	VPL_ExtType _213 = {"213",kEnumType,4};
	VPL_ExtType _212 = {"212",kEnumType,4};
	VPL_ExtType _211 = {"211",kEnumType,4};
	VPL_ExtType _210 = {"210",kEnumType,4};
	VPL_ExtType _209 = {"209",kEnumType,4};
	VPL_ExtType _208 = {"208",kEnumType,4};
	VPL_ExtType _207 = {"207",kEnumType,4};
	VPL_ExtType _206 = {"206",kEnumType,4};
	VPL_ExtType _205 = {"205",kEnumType,4};
	VPL_ExtType _204 = {"204",kEnumType,4};
	VPL_ExtType _203 = {"203",kEnumType,4};
	VPL_ExtType _202 = {"202",kEnumType,4};
	VPL_ExtType _201 = {"201",kEnumType,4};
	VPL_ExtType _200 = {"200",kEnumType,4};
	VPL_ExtType _199 = {"199",kEnumType,4};
	VPL_ExtType _198 = {"198",kEnumType,4};
	VPL_ExtType _197 = {"197",kEnumType,4};
	VPL_ExtType _196 = {"196",kEnumType,4};
	VPL_ExtType _195 = {"195",kEnumType,4};
	VPL_ExtType _194 = {"194",kEnumType,4};
	VPL_ExtType _193 = {"193",kEnumType,4};
	VPL_ExtType _192 = {"192",kEnumType,4};
	VPL_ExtType _191 = {"191",kEnumType,4};
	VPL_ExtType _190 = {"190",kEnumType,4};
	VPL_ExtType _189 = {"189",kEnumType,4};
	VPL_ExtType _188 = {"188",kEnumType,4};
	VPL_ExtType _187 = {"187",kEnumType,4};
	VPL_ExtType _186 = {"186",kEnumType,4};
	VPL_ExtType _185 = {"185",kEnumType,4};
	VPL_ExtType _184 = {"184",kEnumType,4};
	VPL_ExtType _183 = {"183",kEnumType,4};
	VPL_ExtType _182 = {"182",kEnumType,4};
	VPL_ExtType _181 = {"181",kEnumType,4};
	VPL_ExtType _180 = {"180",kEnumType,4};
	VPL_ExtType _179 = {"179",kEnumType,4};
	VPL_ExtType _178 = {"178",kEnumType,4};
	VPL_ExtType _177 = {"177",kEnumType,4};
	VPL_ExtType _176 = {"176",kEnumType,4};
	VPL_ExtType _175 = {"175",kEnumType,4};
	VPL_ExtType _174 = {"174",kEnumType,4};
	VPL_ExtType _173 = {"173",kEnumType,4};
	VPL_ExtType _172 = {"172",kEnumType,4};
	VPL_ExtType _171 = {"171",kEnumType,4};
	VPL_ExtType _170 = {"170",kEnumType,4};
	VPL_ExtType _169 = {"169",kEnumType,4};
	VPL_ExtType _168 = {"168",kEnumType,4};
	VPL_ExtType _167 = {"167",kEnumType,4};
	VPL_ExtType _166 = {"166",kEnumType,4};
	VPL_ExtType _165 = {"165",kEnumType,4};
	VPL_ExtType _164 = {"164",kEnumType,4};
	VPL_ExtType _163 = {"163",kEnumType,4};
	VPL_ExtType _162 = {"162",kEnumType,4};
	VPL_ExtType _161 = {"161",kEnumType,4};
	VPL_ExtType _160 = {"160",kEnumType,4};
	VPL_ExtType _159 = {"159",kEnumType,4};
	VPL_ExtType _158 = {"158",kEnumType,4};
	VPL_ExtType _157 = {"157",kEnumType,4};
	VPL_ExtType _156 = {"156",kEnumType,4};
	VPL_ExtType _155 = {"155",kEnumType,4};
	VPL_ExtType _154 = {"154",kEnumType,4};
	VPL_ExtType _153 = {"153",kEnumType,4};
	VPL_ExtType _152 = {"152",kEnumType,4};
	VPL_ExtType _151 = {"151",kEnumType,4};
	VPL_ExtType _150 = {"150",kEnumType,4};
	VPL_ExtType _149 = {"149",kEnumType,4};
	VPL_ExtType _148 = {"148",kEnumType,4};
	VPL_ExtType _147 = {"147",kEnumType,4};
	VPL_ExtType _146 = {"146",kEnumType,4};
	VPL_ExtType _145 = {"145",kEnumType,4};
	VPL_ExtType _144 = {"144",kEnumType,4};
	VPL_ExtType _143 = {"143",kEnumType,4};
	VPL_ExtType _142 = {"142",kEnumType,4};
	VPL_ExtType _141 = {"141",kEnumType,4};
	VPL_ExtType _140 = {"140",kEnumType,4};
	VPL_ExtType _139 = {"139",kEnumType,4};
	VPL_ExtType _138 = {"138",kEnumType,4};
	VPL_ExtType _137 = {"137",kEnumType,4};
	VPL_ExtType _136 = {"136",kEnumType,4};
	VPL_ExtType _135 = {"135",kEnumType,4};
	VPL_ExtType _134 = {"134",kEnumType,4};
	VPL_ExtType _133 = {"133",kEnumType,4};
	VPL_ExtType _132 = {"132",kEnumType,4};
	VPL_ExtType _131 = {"131",kEnumType,4};
	VPL_ExtType _130 = {"130",kEnumType,4};
	VPL_ExtType _129 = {"129",kEnumType,4};
	VPL_ExtType _128 = {"128",kEnumType,4};
	VPL_ExtType _127 = {"127",kEnumType,4};
	VPL_ExtType _126 = {"126",kEnumType,4};
	VPL_ExtType _125 = {"125",kEnumType,4};
	VPL_ExtType _124 = {"124",kEnumType,4};
	VPL_ExtType _123 = {"123",kEnumType,4};
	VPL_ExtType _122 = {"122",kEnumType,4};
	VPL_ExtType _TECInfo = {"TECInfo",kStructureType,84};
	VPL_ExtType _121 = {"121",kEnumType,4};
	VPL_ExtType _ScriptCodeRun = {"ScriptCodeRun",kStructureType,8};
	VPL_ExtType _TextEncodingRun = {"TextEncodingRun",kStructureType,8};
	VPL_ExtType _120 = {"120",kEnumType,4};
	VPL_ExtType _119 = {"119",kEnumType,4};
	VPL_ExtType _118 = {"118",kEnumType,4};
	VPL_ExtType _117 = {"117",kEnumType,4};
	VPL_ExtType _116 = {"116",kEnumType,4};
	VPL_ExtType _115 = {"115",kEnumType,4};
	VPL_ExtType _114 = {"114",kEnumType,4};
	VPL_ExtType _113 = {"113",kEnumType,4};
	VPL_ExtType _112 = {"112",kEnumType,4};
	VPL_ExtType _111 = {"111",kEnumType,4};
	VPL_ExtType _110 = {"110",kEnumType,4};
	VPL_ExtType _109 = {"109",kEnumType,4};
	VPL_ExtType _108 = {"108",kEnumType,4};
	VPL_ExtType _107 = {"107",kEnumType,4};
	VPL_ExtType _106 = {"106",kEnumType,4};
	VPL_ExtType _105 = {"105",kEnumType,4};
	VPL_ExtType _104 = {"104",kEnumType,4};
	VPL_ExtType _103 = {"103",kEnumType,4};
	VPL_ExtType _102 = {"102",kEnumType,4};
	VPL_ExtType _101 = {"101",kEnumType,4};
	VPL_ExtType _100 = {"100",kEnumType,4};
	VPL_ExtType _99 = {"99",kEnumType,4};
	VPL_ExtType _98 = {"98",kEnumType,4};
	VPL_ExtType _97 = {"97",kEnumType,4};
	VPL_ExtType _96 = {"96",kEnumType,4};
	VPL_ExtType _95 = {"95",kEnumType,4};
	VPL_ExtType _94 = {"94",kEnumType,4};
	VPL_ExtType _93 = {"93",kEnumType,4};
	VPL_ExtType _92 = {"92",kEnumType,4};
	VPL_ExtType _91 = {"91",kEnumType,4};
	VPL_ExtType _90 = {"90",kEnumType,4};
	VPL_ExtType _89 = {"89",kEnumType,4};
	VPL_ExtType _88 = {"88",kEnumType,4};
	VPL_ExtType _87 = {"87",kEnumType,4};
	VPL_ExtType _86 = {"86",kEnumType,4};
	VPL_ExtType _85 = {"85",kEnumType,4};
	VPL_ExtType _SysEnvRec = {"SysEnvRec",kStructureType,16};
	VPL_ExtType _84 = {"84",kEnumType,4};
	VPL_ExtType _83 = {"83",NULL,4};
	VPL_ExtType _MachineLocation = {"MachineLocation",kStructureType,12};
	VPL_ExtType _DeferredTask = {"DeferredTask",kStructureType,20};
	VPL_ExtType _QHdr = {"QHdr",kStructureType,12};
	VPL_ExtType _QElem = {"QElem",kStructureType,8};
	VPL_ExtType _SysParmType = {"SysParmType",kStructureType,20};
	VPL_ExtType _82 = {"82",kEnumType,4};
	VPL_ExtType _81 = {"81",kEnumType,4};
	VPL_ExtType _80 = {"80",kEnumType,4};
	VPL_ExtType _79 = {"79",kEnumType,4};
	VPL_ExtType _TogglePB = {"TogglePB",kStructureType,28};
	VPL_ExtType _78 = {"78",kStructureType,20};
	VPL_ExtType _77 = {"77",kStructureType,28};
	VPL_ExtType _LongDateRec = {"LongDateRec",NULL,28};
	VPL_ExtType _76 = {"76",kStructureType,8};
	VPL_ExtType _LongDateCvt = {"LongDateCvt",NULL,8};
	VPL_ExtType _DateTimeRec = {"DateTimeRec",kStructureType,16};
	VPL_ExtType _DateCacheRecord = {"DateCacheRecord",kStructureType,512};
	VPL_ExtType _75 = {"75",kEnumType,4};
	VPL_ExtType _74 = {"74",kEnumType,4};
	VPL_ExtType _73 = {"73",kEnumType,4};
	VPL_ExtType _72 = {"72",kEnumType,4};
	VPL_ExtType _71 = {"71",kEnumType,4};
	VPL_ExtType _70 = {"70",kEnumType,4};
	VPL_ExtType _VolumeVirtualMemoryInfo = {"VolumeVirtualMemoryInfo",kStructureType,12};
	VPL_ExtType _69 = {"69",kEnumType,4};
	VPL_ExtType _LogicalToPhysicalTable = {"LogicalToPhysicalTable",kStructureType,72};
	VPL_ExtType _MemoryBlock = {"MemoryBlock",kStructureType,8};
	VPL_ExtType _Zone = {"Zone",kStructureType,56};
	VPL_ExtType _68 = {"68",kEnumType,4};
	VPL_ExtType _67 = {"67",kEnumType,4};
	VPL_ExtType _66 = {"66",kEnumType,4};
	VPL_ExtType _65 = {"65",kEnumType,4};
	VPL_ExtType _64 = {"64",kEnumType,4};
	VPL_ExtType _63 = {"63",kEnumType,4};
	VPL_ExtType _62 = {"62",kEnumType,4};
	VPL_ExtType _61 = {"61",kEnumType,4};
	VPL_ExtType _60 = {"60",kEnumType,4};
	VPL_ExtType _MixedModeStateRecord = {"MixedModeStateRecord",kStructureType,16};
	VPL_ExtType _RoutineDescriptor = {"RoutineDescriptor",kStructureType,32};
	VPL_ExtType _59 = {"59",kEnumType,4};
	VPL_ExtType _RoutineRecord = {"RoutineRecord",kStructureType,20};
	VPL_ExtType _58 = {"58",kEnumType,4};
	VPL_ExtType _57 = {"57",kEnumType,4};
	VPL_ExtType _56 = {"56",kEnumType,4};
	VPL_ExtType _55 = {"55",kEnumType,4};
	VPL_ExtType _54 = {"54",kEnumType,4};
	VPL_ExtType _53 = {"53",kEnumType,4};
	VPL_ExtType _52 = {"52",kEnumType,4};
	VPL_ExtType _51 = {"51",kEnumType,4};
	VPL_ExtType _50 = {"50",kEnumType,4};
	VPL_ExtType _49 = {"49",kEnumType,4};
	VPL_ExtType _48 = {"48",kEnumType,4};
	VPL_ExtType _47 = {"47",kEnumType,4};
	VPL_ExtType _46 = {"46",kEnumType,4};
	VPL_ExtType _45 = {"45",kEnumType,4};
	VPL_ExtType _44 = {"44",kEnumType,4};
	VPL_ExtType _43 = {"43",kEnumType,4};
	VPL_ExtType ___CFTree = {"__CFTree",kStructureType,0};
	VPL_ExtType _42 = {"42",kStructureType,20};
	VPL_ExtType ___CFSet = {"__CFSet",kStructureType,0};
	VPL_ExtType _41 = {"41",kStructureType,24};
	VPL_ExtType _40 = {"40",kEnumType,4};
	VPL_ExtType ___CFNumber = {"__CFNumber",kStructureType,0};
	VPL_ExtType _39 = {"39",kEnumType,4};
	VPL_ExtType ___CFBoolean = {"__CFBoolean",kStructureType,0};
	VPL_ExtType _38 = {"38",kEnumType,4};
	VPL_ExtType _37 = {"37",kStructureType,32};
	VPL_ExtType _36 = {"36",kStructureType,16};
	VPL_ExtType ___CFTimeZone = {"__CFTimeZone",kStructureType,0};
	VPL_ExtType ___CFDate = {"__CFDate",kStructureType,0};
	VPL_ExtType _35 = {"35",kEnumType,4};
	VPL_ExtType ___CFCharacterSet = {"__CFCharacterSet",kStructureType,0};
	VPL_ExtType _20 = {"20",kStructureType,152};
	VPL_ExtType _19 = {"19",kEnumType,4};
	VPL_ExtType _18 = {"18",kEnumType,4};
	VPL_ExtType ___CFDictionary = {"__CFDictionary",kStructureType,0};
	VPL_ExtType _17 = {"17",kStructureType,20};
	VPL_ExtType _16 = {"16",kStructureType,24};
	VPL_ExtType ___CFData = {"__CFData",kStructureType,0};
	VPL_ExtType ___CFBag = {"__CFBag",kStructureType,0};
	VPL_ExtType _15 = {"15",kStructureType,24};
	VPL_ExtType ___CFArray = {"__CFArray",kStructureType,0};
	VPL_ExtType _14 = {"14",kStructureType,20};
	VPL_ExtType _13 = {"13",kStructureType,36};
	VPL_ExtType ___CFAllocator = {"__CFAllocator",kStructureType,0};
	VPL_ExtType _9 = {"9",kStructureType,8};
	VPL_ExtType _8 = {"8",kEnumType,4};
	VPL_ExtType _7 = {"7",kEnumType,4};
	VPL_ExtType ___CFString = {"__CFString",kStructureType,0};
	VPL_ExtType _VersRec = {"VersRec",kStructureType,520};
	VPL_ExtType _NumVersionVariant = {"NumVersionVariant",NULL,4};
	VPL_ExtType _6 = {"6",kEnumType,4};
	VPL_ExtType _NumVersion = {"NumVersion",kStructureType,4};
	VPL_ExtType _TimeRecord = {"TimeRecord",kStructureType,16};
	VPL_ExtType _TimeBaseRecord = {"TimeBaseRecord",kStructureType,0};
	VPL_ExtType _5 = {"5",kEnumType,4};
	VPL_ExtType _FixedRect = {"FixedRect",kStructureType,16};
	VPL_ExtType _FixedPoint = {"FixedPoint",kStructureType,8};
	VPL_ExtType _Rect = {"Rect",kStructureType,8};
	VPL_ExtType _Point = {"Point",kStructureType,4};
	VPL_ExtType _4 = {"4",kEnumType,4};
	VPL_ExtType _3 = {"3",kEnumType,4};
	VPL_ExtType _2 = {"2",kEnumType,4};
	VPL_ExtType _1 = {"1",kEnumType,4};
	VPL_ExtType _Float32Point = {"Float32Point",kStructureType,8};
	VPL_ExtType _Float96 = {"Float96",kStructureType,12};
	VPL_ExtType _Float80 = {"Float80",kStructureType,12};
	VPL_ExtType _UnsignedWide = {"UnsignedWide",kStructureType,8};
	VPL_ExtType _wide = {"wide",kStructureType,8};
	VPL_ExtType _T_44_ = {"T*",NULL,0};
	VPL_ExtType _void = {"void",NULL,0};
	VPL_ExtType _unsigned_32_long_32_long = {"unsigned long long",kUnsignedType,4};
	VPL_ExtType _unsigned_32_int = {"unsigned int",kUnsignedType,4};
	VPL_ExtType _unsigned_32_short = {"unsigned short",kUnsignedType,2};
	VPL_ExtType _unsigned_32_long = {"unsigned long",kUnsignedType,4};
	VPL_ExtType _unsigned_32_char = {"unsigned char",kUnsignedType,1};
	VPL_ExtType _signed_32_char = {"signed char",kIntType,1};
	VPL_ExtType _short = {"short",kIntType,2};
	VPL_ExtType _long_32_long_32_int = {"long long int",kIntType,4};
	VPL_ExtType _long_32_int = {"long int",kIntType,4};
	VPL_ExtType _long_32_double = {"long double",kFloatType,8};
	VPL_ExtType _int = {"int",kIntType,4};
	VPL_ExtType _float = {"float",kFloatType,4};
	VPL_ExtType _double = {"double",kFloatType,8};
	VPL_ExtType _char = {"char",kIntType,1};

VPL_DictionaryNode VPX_MacOSX_Types[] =	{
	{"2439",&_2439},
	{"2438",&_2438},
	{"2437",&_2437},
	{"2436",&_2436},
	{"2435",&_2435},
	{"2434",&_2434},
	{"2433",&_2433},
	{"ICD_DisposePropertyPB",&_ICD_DisposePropertyPB},
	{"ICD_NewPropertyPB",&_ICD_NewPropertyPB},
	{"ICD_DisposeObjectPB",&_ICD_DisposeObjectPB},
	{"ICD_NewObjectPB",&_ICD_NewObjectPB},
	{"ICDHeader",&_ICDHeader},
	{"ICACopyObjectPropertyDictionaryPB",&_ICACopyObjectPropertyDictionaryPB},
	{"ICADownloadFilePB",&_ICADownloadFilePB},
	{"ICARegisterEventNotificationPB",&_ICARegisterEventNotificationPB},
	{"ICAObjectSendMessagePB",&_ICAObjectSendMessagePB},
	{"ICAGetDeviceListPB",&_ICAGetDeviceListPB},
	{"ICASetPropertyRefConPB",&_ICASetPropertyRefConPB},
	{"ICAGetPropertyRefConPB",&_ICAGetPropertyRefConPB},
	{"ICAGetRootOfPropertyPB",&_ICAGetRootOfPropertyPB},
	{"ICAGetParentOfPropertyPB",&_ICAGetParentOfPropertyPB},
	{"ICASetPropertyDataPB",&_ICASetPropertyDataPB},
	{"ICAGetPropertyDataPB",&_ICAGetPropertyDataPB},
	{"ICAGetPropertyInfoPB",&_ICAGetPropertyInfoPB},
	{"ICAGetPropertyByTypePB",&_ICAGetPropertyByTypePB},
	{"ICAGetNthPropertyPB",&_ICAGetNthPropertyPB},
	{"ICAGetPropertyCountPB",&_ICAGetPropertyCountPB},
	{"ICASetObjectRefConPB",&_ICASetObjectRefConPB},
	{"ICAGetObjectRefConPB",&_ICAGetObjectRefConPB},
	{"ICAGetRootOfObjectPB",&_ICAGetRootOfObjectPB},
	{"ICAGetParentOfObjectPB",&_ICAGetParentOfObjectPB},
	{"ICAGetObjectInfoPB",&_ICAGetObjectInfoPB},
	{"ICAGetNthChildPB",&_ICAGetNthChildPB},
	{"ICAGetChildCountPB",&_ICAGetChildCountPB},
	{"ICAHeader",&_ICAHeader},
	{"2432",&_2432},
	{"ICAThumbnail",&_ICAThumbnail},
	{"ICAMessage",&_ICAMessage},
	{"ICAPropertyInfo",&_ICAPropertyInfo},
	{"ICAObjectInfo",&_ICAObjectInfo},
	{"2431",&_2431},
	{"2430",&_2430},
	{"2429",&_2429},
	{"2428",&_2428},
	{"2427",&_2427},
	{"2426",&_2426},
	{"2425",&_2425},
	{"2424",&_2424},
	{"2423",&_2423},
	{"OpaqueICAConnectionID",&_OpaqueICAConnectionID},
	{"OpaqueICAProperty",&_OpaqueICAProperty},
	{"OpaqueICAObject",&_OpaqueICAObject},
	{"2422",&_2422},
	{"2421",&_2421},
	{"2420",&_2420},
	{"URLCallbackInfo",&_URLCallbackInfo},
	{"2419",&_2419},
	{"2418",&_2418},
	{"2417",&_2417},
	{"2416",&_2416},
	{"OpaqueURLReference",&_OpaqueURLReference},
	{"2415",&_2415},
	{"2414",&_2414},
	{"2413",&_2413},
	{"2412",&_2412},
	{"2411",&_2411},
	{"2410",&_2410},
	{"2409",&_2409},
	{"2408",&_2408},
	{"2407",&_2407},
	{"2406",&_2406},
	{"2405",&_2405},
	{"2404",&_2404},
	{"2403",&_2403},
	{"2402",&_2402},
	{"2401",&_2401},
	{"2400",&_2400},
	{"2399",&_2399},
	{"SRCallBackParam",&_SRCallBackParam},
	{"SRCallBackStruct",&_SRCallBackStruct},
	{"OpaqueSRSpeechObject",&_OpaqueSRSpeechObject},
	{"2398",&_2398},
	{"2397",&_2397},
	{"2396",&_2396},
	{"2395",&_2395},
	{"OpaqueHRReference",&_OpaqueHRReference},
	{"NSLDialogOptions",&_NSLDialogOptions},
	{"2394",&_2394},
	{"CalibratorInfo",&_CalibratorInfo},
	{"2393",&_2393},
	{"NColorPickerInfo",&_NColorPickerInfo},
	{"ColorPickerInfo",&_ColorPickerInfo},
	{"PickerMenuItemInfo",&_PickerMenuItemInfo},
	{"OpaquePicker",&_OpaquePicker},
	{"NPMColor",&_NPMColor},
	{"PMColor",&_PMColor},
	{"CMYColor",&_CMYColor},
	{"HSLColor",&_HSLColor},
	{"HSVColor",&_HSVColor},
	{"2392",&_2392},
	{"2391",&_2391},
	{"2390",&_2390},
	{"2389",&_2389},
	{"NavDialogCreationOptions",&_NavDialogCreationOptions},
	{"2388",&_2388},
	{"NavReplyRecord",&_NavReplyRecord},
	{"2387",&_2387},
	{"NavDialogOptions",&_NavDialogOptions},
	{"2386",&_2386},
	{"NavTypeList",&_NavTypeList},
	{"2385",&_2385},
	{"NavMenuItemSpec",&_NavMenuItemSpec},
	{"2384",&_2384},
	{"2383",&_2383},
	{"2382",&_2382},
	{"2381",&_2381},
	{"2380",&_2380},
	{"2379",&_2379},
	{"2378",&_2378},
	{"2377",&_2377},
	{"2376",&_2376},
	{"NavCBRec",&_NavCBRec},
	{"2375",&_2375},
	{"2374",&_2374},
	{"__NavDialog",&___NavDialog},
	{"NavEventData",&_NavEventData},
	{"NavEventDataInfo",&_NavEventDataInfo},
	{"2373",&_2373},
	{"2372",&_2372},
	{"2371",&_2371},
	{"NavFileOrFolderInfo",&_NavFileOrFolderInfo},
	{"2370",&_2370},
	{"2369",&_2369},
	{"2368",&_2368},
	{"2367",&_2367},
	{"2366",&_2366},
	{"2365",&_2365},
	{"2364",&_2364},
	{"2363",&_2363},
	{"2362",&_2362},
	{"2361",&_2361},
	{"2360",&_2360},
	{"2359",&_2359},
	{"2358",&_2358},
	{"2357",&_2357},
	{"2356",&_2356},
	{"2355",&_2355},
	{"2354",&_2354},
	{"2353",&_2353},
	{"2352",&_2352},
	{"2351",&_2351},
	{"2350",&_2350},
	{"2349",&_2349},
	{"2348",&_2348},
	{"2347",&_2347},
	{"2346",&_2346},
	{"2345",&_2345},
	{"2344",&_2344},
	{"2343",&_2343},
	{"2342",&_2342},
	{"2341",&_2341},
	{"2340",&_2340},
	{"2339",&_2339},
	{"2338",&_2338},
	{"2337",&_2337},
	{"2336",&_2336},
	{"2335",&_2335},
	{"2334",&_2334},
	{"2333",&_2333},
	{"2332",&_2332},
	{"2331",&_2331},
	{"2330",&_2330},
	{"2329",&_2329},
	{"2328",&_2328},
	{"2327",&_2327},
	{"2326",&_2326},
	{"2325",&_2325},
	{"2324",&_2324},
	{"2323",&_2323},
	{"2322",&_2322},
	{"2321",&_2321},
	{"2320",&_2320},
	{"2319",&_2319},
	{"2318",&_2318},
	{"2317",&_2317},
	{"2316",&_2316},
	{"2315",&_2315},
	{"2314",&_2314},
	{"2313",&_2313},
	{"2312",&_2312},
	{"2311",&_2311},
	{"2310",&_2310},
	{"2309",&_2309},
	{"2308",&_2308},
	{"2307",&_2307},
	{"2306",&_2306},
	{"2305",&_2305},
	{"2304",&_2304},
	{"2303",&_2303},
	{"2302",&_2302},
	{"2301",&_2301},
	{"2300",&_2300},
	{"2299",&_2299},
	{"2298",&_2298},
	{"2297",&_2297},
	{"2296",&_2296},
	{"2295",&_2295},
	{"2294",&_2294},
	{"2293",&_2293},
	{"2292",&_2292},
	{"StatementRange",&_StatementRange},
	{"2291",&_2291},
	{"2290",&_2290},
	{"2289",&_2289},
	{"2288",&_2288},
	{"2287",&_2287},
	{"2286",&_2286},
	{"2285",&_2285},
	{"2284",&_2284},
	{"2283",&_2283},
	{"2282",&_2282},
	{"2281",&_2281},
	{"2280",&_2280},
	{"2279",&_2279},
	{"2278",&_2278},
	{"2277",&_2277},
	{"2276",&_2276},
	{"2275",&_2275},
	{"2274",&_2274},
	{"2273",&_2273},
	{"2272",&_2272},
	{"2271",&_2271},
	{"2270",&_2270},
	{"2269",&_2269},
	{"2268",&_2268},
	{"2267",&_2267},
	{"2266",&_2266},
	{"2265",&_2265},
	{"2264",&_2264},
	{"2263",&_2263},
	{"2262",&_2262},
	{"2261",&_2261},
	{"2260",&_2260},
	{"2259",&_2259},
	{"2258",&_2258},
	{"2257",&_2257},
	{"2256",&_2256},
	{"2255",&_2255},
	{"2254",&_2254},
	{"2253",&_2253},
	{"2252",&_2252},
	{"2251",&_2251},
	{"2250",&_2250},
	{"2249",&_2249},
	{"2248",&_2248},
	{"2247",&_2247},
	{"2246",&_2246},
	{"2245",&_2245},
	{"2244",&_2244},
	{"2243",&_2243},
	{"2242",&_2242},
	{"2241",&_2241},
	{"2240",&_2240},
	{"2239",&_2239},
	{"2238",&_2238},
	{"2237",&_2237},
	{"2236",&_2236},
	{"2235",&_2235},
	{"SndInputCmpParam",&_SndInputCmpParam},
	{"2234",&_2234},
	{"2233",&_2233},
	{"SPB",&_SPB},
	{"EQSpectrumBandsRecord",&_EQSpectrumBandsRecord},
	{"LevelMeterInfo",&_LevelMeterInfo},
	{"AudioTerminatorAtom",&_AudioTerminatorAtom},
	{"AudioEndianAtom",&_AudioEndianAtom},
	{"AudioFormatAtom",&_AudioFormatAtom},
	{"AudioInfo",&_AudioInfo},
	{"SoundComponentLink",&_SoundComponentLink},
	{"OpaqueSoundSource",&_OpaqueSoundSource},
	{"OpaqueSoundConverter",&_OpaqueSoundConverter},
	{"SoundSlopeAndInterceptRecord",&_SoundSlopeAndInterceptRecord},
	{"CompressionInfo",&_CompressionInfo},
	{"ExtendedSoundParamBlock",&_ExtendedSoundParamBlock},
	{"SoundParamBlock",&_SoundParamBlock},
	{"ExtendedSoundComponentData",&_ExtendedSoundComponentData},
	{"SoundComponentData",&_SoundComponentData},
	{"SoundInfoList",&_SoundInfoList},
	{"AudioSelection",&_AudioSelection},
	{"SCStatus",&_SCStatus},
	{"SMStatus",&_SMStatus},
	{"ExtendedScheduledSoundHeader",&_ExtendedScheduledSoundHeader},
	{"ScheduledSoundHeader",&_ScheduledSoundHeader},
	{"2232",&_2232},
	{"ConversionBlock",&_ConversionBlock},
	{"SoundHeaderUnion",&_SoundHeaderUnion},
	{"ExtSoundHeader",&_ExtSoundHeader},
	{"CmpSoundHeader",&_CmpSoundHeader},
	{"SoundHeader",&_SoundHeader},
	{"Snd2ListResource",&_Snd2ListResource},
	{"SndListResource",&_SndListResource},
	{"ModRef",&_ModRef},
	{"LeftOverBlock",&_LeftOverBlock},
	{"StateBlock",&_StateBlock},
	{"SndChannel",&_SndChannel},
	{"SndCommand",&_SndCommand},
	{"2231",&_2231},
	{"2230",&_2230},
	{"2229",&_2229},
	{"2228",&_2228},
	{"2227",&_2227},
	{"2226",&_2226},
	{"2225",&_2225},
	{"2224",&_2224},
	{"2223",&_2223},
	{"2222",&_2222},
	{"2221",&_2221},
	{"2220",&_2220},
	{"2219",&_2219},
	{"2218",&_2218},
	{"2217",&_2217},
	{"2216",&_2216},
	{"2215",&_2215},
	{"2214",&_2214},
	{"2213",&_2213},
	{"2212",&_2212},
	{"2211",&_2211},
	{"2210",&_2210},
	{"2209",&_2209},
	{"2208",&_2208},
	{"2207",&_2207},
	{"2206",&_2206},
	{"2205",&_2205},
	{"2204",&_2204},
	{"2203",&_2203},
	{"2202",&_2202},
	{"2201",&_2201},
	{"2200",&_2200},
	{"2199",&_2199},
	{"2198",&_2198},
	{"2197",&_2197},
	{"2196",&_2196},
	{"OpaqueIBNibRef",&_OpaqueIBNibRef},
	{"2195",&_2195},
	{"2194",&_2194},
	{"2193",&_2193},
	{"2192",&_2192},
	{"2191",&_2191},
	{"ICServices",&_ICServices},
	{"2190",&_2190},
	{"2189",&_2189},
	{"ICServiceEntry",&_ICServiceEntry},
	{"2188",&_2188},
	{"2187",&_2187},
	{"2186",&_2186},
	{"ICMapEntry",&_ICMapEntry},
	{"2185",&_2185},
	{"ICFileSpec",&_ICFileSpec},
	{"ICAppSpecList",&_ICAppSpecList},
	{"ICAppSpec",&_ICAppSpec},
	{"ICCharTable",&_ICCharTable},
	{"ICFontRecord",&_ICFontRecord},
	{"2184",&_2184},
	{"2183",&_2183},
	{"2182",&_2182},
	{"2181",&_2181},
	{"2180",&_2180},
	{"2179",&_2179},
	{"2178",&_2178},
	{"2177",&_2177},
	{"2176",&_2176},
	{"2175",&_2175},
	{"ICDirSpec",&_ICDirSpec},
	{"OpaqueICInstance",&_OpaqueICInstance},
	{"2174",&_2174},
	{"2173",&_2173},
	{"TypeSelectRecord",&_TypeSelectRecord},
	{"2172",&_2172},
	{"FileTranslationSpec",&_FileTranslationSpec},
	{"2171",&_2171},
	{"2170",&_2170},
	{"2169",&_2169},
	{"ScrapTranslationList",&_ScrapTranslationList},
	{"ScrapTypeSpec",&_ScrapTypeSpec},
	{"FileTranslationList",&_FileTranslationList},
	{"FileTypeSpec",&_FileTypeSpec},
	{"2168",&_2168},
	{"2167",&_2167},
	{"TSMTERec",&_TSMTERec},
	{"2166",&_2166},
	{"2165",&_2165},
	{"2164",&_2164},
	{"2163",&_2163},
	{"2162",&_2162},
	{"2161",&_2161},
	{"DataBrowserListViewColumnDesc",&_DataBrowserListViewColumnDesc},
	{"DataBrowserListViewHeaderDesc",&_DataBrowserListViewHeaderDesc},
	{"2160",&_2160},
	{"2159",&_2159},
	{"2158",&_2158},
	{"2157",&_2157},
	{"2156",&_2156},
	{"2155",&_2155},
	{"2154",&_2154},
	{"DataBrowserCustomCallbacks",&_DataBrowserCustomCallbacks},
	{"2153",&_2153},
	{"2152",&_2152},
	{"2151",&_2151},
	{"2150",&_2150},
	{"DataBrowserCallbacks",&_DataBrowserCallbacks},
	{"2149",&_2149},
	{"DataBrowserPropertyDesc",&_DataBrowserPropertyDesc},
	{"2148",&_2148},
	{"2147",&_2147},
	{"2146",&_2146},
	{"2145",&_2145},
	{"2144",&_2144},
	{"2143",&_2143},
	{"2142",&_2142},
	{"2141",&_2141},
	{"2140",&_2140},
	{"2139",&_2139},
	{"2138",&_2138},
	{"2137",&_2137},
	{"2136",&_2136},
	{"2135",&_2135},
	{"2134",&_2134},
	{"2133",&_2133},
	{"2132",&_2132},
	{"2131",&_2131},
	{"2130",&_2130},
	{"2129",&_2129},
	{"2128",&_2128},
	{"2127",&_2127},
	{"2126",&_2126},
	{"2125",&_2125},
	{"2124",&_2124},
	{"2123",&_2123},
	{"2122",&_2122},
	{"2121",&_2121},
	{"2120",&_2120},
	{"2119",&_2119},
	{"2118",&_2118},
	{"2117",&_2117},
	{"2116",&_2116},
	{"2115",&_2115},
	{"2114",&_2114},
	{"2113",&_2113},
	{"2112",&_2112},
	{"2111",&_2111},
	{"2110",&_2110},
	{"2109",&_2109},
	{"2108",&_2108},
	{"2107",&_2107},
	{"2106",&_2106},
	{"2105",&_2105},
	{"2104",&_2104},
	{"2103",&_2103},
	{"2102",&_2102},
	{"2101",&_2101},
	{"2100",&_2100},
	{"2099",&_2099},
	{"2098",&_2098},
	{"2097",&_2097},
	{"2096",&_2096},
	{"2095",&_2095},
	{"2094",&_2094},
	{"2093",&_2093},
	{"2092",&_2092},
	{"2091",&_2091},
	{"2090",&_2090},
	{"2089",&_2089},
	{"2088",&_2088},
	{"ControlEditTextSelectionRec",&_ControlEditTextSelectionRec},
	{"2087",&_2087},
	{"2086",&_2086},
	{"2085",&_2085},
	{"2084",&_2084},
	{"2083",&_2083},
	{"2082",&_2082},
	{"2081",&_2081},
	{"2080",&_2080},
	{"2079",&_2079},
	{"2078",&_2078},
	{"2077",&_2077},
	{"2076",&_2076},
	{"2075",&_2075},
	{"2074",&_2074},
	{"2073",&_2073},
	{"2072",&_2072},
	{"2071",&_2071},
	{"2070",&_2070},
	{"2069",&_2069},
	{"2068",&_2068},
	{"2067",&_2067},
	{"2066",&_2066},
	{"2065",&_2065},
	{"2064",&_2064},
	{"2063",&_2063},
	{"2062",&_2062},
	{"2061",&_2061},
	{"2060",&_2060},
	{"2059",&_2059},
	{"ControlTabInfoRecV1",&_ControlTabInfoRecV1},
	{"ControlTabInfoRec",&_ControlTabInfoRec},
	{"2058",&_2058},
	{"2057",&_2057},
	{"2056",&_2056},
	{"2055",&_2055},
	{"2054",&_2054},
	{"ControlTabEntry",&_ControlTabEntry},
	{"2053",&_2053},
	{"2052",&_2052},
	{"2051",&_2051},
	{"2050",&_2050},
	{"2049",&_2049},
	{"2048",&_2048},
	{"2047",&_2047},
	{"2046",&_2046},
	{"2045",&_2045},
	{"2044",&_2044},
	{"2043",&_2043},
	{"2042",&_2042},
	{"2041",&_2041},
	{"2040",&_2040},
	{"2039",&_2039},
	{"2038",&_2038},
	{"2037",&_2037},
	{"2036",&_2036},
	{"2035",&_2035},
	{"2034",&_2034},
	{"2033",&_2033},
	{"2032",&_2032},
	{"2031",&_2031},
	{"2030",&_2030},
	{"2029",&_2029},
	{"2028",&_2028},
	{"2027",&_2027},
	{"2026",&_2026},
	{"2025",&_2025},
	{"2024",&_2024},
	{"2023",&_2023},
	{"2022",&_2022},
	{"2021",&_2021},
	{"2020",&_2020},
	{"2019",&_2019},
	{"2018",&_2018},
	{"2017",&_2017},
	{"2016",&_2016},
	{"2015",&_2015},
	{"HMHelpContentRec",&_HMHelpContentRec},
	{"2014",&_2014},
	{"HMHelpContent",&_HMHelpContent},
	{"2013",&_2013},
	{"2012",&_2012},
	{"2011",&_2011},
	{"2010",&_2010},
	{"2009",&_2009},
	{"2008",&_2008},
	{"2007",&_2007},
	{"2006",&_2006},
	{"2005",&_2005},
	{"2004",&_2004},
	{"TXNCarbonEventInfo",&_TXNCarbonEventInfo},
	{"2003",&_2003},
	{"2002",&_2002},
	{"2001",&_2001},
	{"2000",&_2000},
	{"TXNLongRect",&_TXNLongRect},
	{"TXNBackground",&_TXNBackground},
	{"TXNBackgroundData",&_TXNBackgroundData},
	{"1999",&_1999},
	{"TXNMatchTextRecord",&_TXNMatchTextRecord},
	{"TXNMacOSPreferredFontDescription",&_TXNMacOSPreferredFontDescription},
	{"TXNTypeAttributes",&_TXNTypeAttributes},
	{"TXNAttributeData",&_TXNAttributeData},
	{"TXNATSUIVariations",&_TXNATSUIVariations},
	{"TXNATSUIFeatures",&_TXNATSUIFeatures},
	{"1998",&_1998},
	{"1997",&_1997},
	{"1996",&_1996},
	{"1995",&_1995},
	{"1994",&_1994},
	{"1993",&_1993},
	{"1992",&_1992},
	{"1991",&_1991},
	{"1990",&_1990},
	{"1989",&_1989},
	{"TXNControlData",&_TXNControlData},
	{"TXNMargins",&_TXNMargins},
	{"1988",&_1988},
	{"1987",&_1987},
	{"TXNTab",&_TXNTab},
	{"1986",&_1986},
	{"1985",&_1985},
	{"1984",&_1984},
	{"1983",&_1983},
	{"1982",&_1982},
	{"1981",&_1981},
	{"1980",&_1980},
	{"1979",&_1979},
	{"1978",&_1978},
	{"1977",&_1977},
	{"1976",&_1976},
	{"1975",&_1975},
	{"1974",&_1974},
	{"1973",&_1973},
	{"1972",&_1972},
	{"1971",&_1971},
	{"1970",&_1970},
	{"1969",&_1969},
	{"1968",&_1968},
	{"1967",&_1967},
	{"TXNTextBoxOptionsData",&_TXNTextBoxOptionsData},
	{"1966",&_1966},
	{"1965",&_1965},
	{"1964",&_1964},
	{"1963",&_1963},
	{"1962",&_1962},
	{"1961",&_1961},
	{"1960",&_1960},
	{"1959",&_1959},
	{"OpaqueTXNFontMenuObject",&_OpaqueTXNFontMenuObject},
	{"OpaqueTXNObject",&_OpaqueTXNObject},
	{"1958",&_1958},
	{"OpaqueScrapRef",&_OpaqueScrapRef},
	{"ScrapFlavorInfo",&_ScrapFlavorInfo},
	{"1957",&_1957},
	{"1956",&_1956},
	{"1955",&_1955},
	{"1954",&_1954},
	{"ScriptLanguageSupport",&_ScriptLanguageSupport},
	{"ScriptLanguageRecord",&_ScriptLanguageRecord},
	{"TextServiceList",&_TextServiceList},
	{"TextServiceInfo",&_TextServiceInfo},
	{"OpaqueTSMDocumentID",&_OpaqueTSMDocumentID},
	{"OpaqueTSMContext",&_OpaqueTSMContext},
	{"1953",&_1953},
	{"1952",&_1952},
	{"1951",&_1951},
	{"1950",&_1950},
	{"1949",&_1949},
	{"1948",&_1948},
	{"1947",&_1947},
	{"1946",&_1946},
	{"1945",&_1945},
	{"1944",&_1944},
	{"1943",&_1943},
	{"1942",&_1942},
	{"1941",&_1941},
	{"1940",&_1940},
	{"1939",&_1939},
	{"1938",&_1938},
	{"ListDefSpec",&_ListDefSpec},
	{"1937",&_1937},
	{"StandardIconListCellDataRec",&_StandardIconListCellDataRec},
	{"1936",&_1936},
	{"1935",&_1935},
	{"1934",&_1934},
	{"1933",&_1933},
	{"1932",&_1932},
	{"ListRec",&_ListRec},
	{"AlertStdCFStringAlertParamRec",&_AlertStdCFStringAlertParamRec},
	{"1931",&_1931},
	{"1930",&_1930},
	{"1929",&_1929},
	{"AlertStdAlertParamRec",&_AlertStdAlertParamRec},
	{"1928",&_1928},
	{"1927",&_1927},
	{"1926",&_1926},
	{"1925",&_1925},
	{"1924",&_1924},
	{"1923",&_1923},
	{"1922",&_1922},
	{"AlertTemplate",&_AlertTemplate},
	{"DialogTemplate",&_DialogTemplate},
	{"1921",&_1921},
	{"1920",&_1920},
	{"1919",&_1919},
	{"1918",&_1918},
	{"1917",&_1917},
	{"1916",&_1916},
	{"1915",&_1915},
	{"1914",&_1914},
	{"1913",&_1913},
	{"OpaqueEventHotKeyRef",&_OpaqueEventHotKeyRef},
	{"EventHotKeyID",&_EventHotKeyID},
	{"1912",&_1912},
	{"OpaqueToolboxObjectClassRef",&_OpaqueToolboxObjectClassRef},
	{"OpaqueEventTargetRef",&_OpaqueEventTargetRef},
	{"OpaqueEventHandlerCallRef",&_OpaqueEventHandlerCallRef},
	{"OpaqueEventHandlerRef",&_OpaqueEventHandlerRef},
	{"1911",&_1911},
	{"1910",&_1910},
	{"1909",&_1909},
	{"1908",&_1908},
	{"1907",&_1907},
	{"1906",&_1906},
	{"1905",&_1905},
	{"1904",&_1904},
	{"1903",&_1903},
	{"1902",&_1902},
	{"1901",&_1901},
	{"1900",&_1900},
	{"1899",&_1899},
	{"1898",&_1898},
	{"1897",&_1897},
	{"1896",&_1896},
	{"1895",&_1895},
	{"TabletProximityRec",&_TabletProximityRec},
	{"TabletPointRec",&_TabletPointRec},
	{"1894",&_1894},
	{"1893",&_1893},
	{"1892",&_1892},
	{"1891",&_1891},
	{"HICommand",&_HICommand},
	{"1890",&_1890},
	{"1889",&_1889},
	{"1888",&_1888},
	{"1887",&_1887},
	{"1886",&_1886},
	{"1885",&_1885},
	{"1884",&_1884},
	{"1883",&_1883},
	{"1882",&_1882},
	{"1881",&_1881},
	{"1880",&_1880},
	{"1879",&_1879},
	{"1878",&_1878},
	{"1877",&_1877},
	{"1876",&_1876},
	{"1875",&_1875},
	{"1874",&_1874},
	{"1873",&_1873},
	{"1872",&_1872},
	{"1871",&_1871},
	{"1870",&_1870},
	{"1869",&_1869},
	{"HIPoint",&_HIPoint},
	{"1868",&_1868},
	{"1867",&_1867},
	{"OpaqueEventLoopTimerRef",&_OpaqueEventLoopTimerRef},
	{"OpaqueEventQueueRef",&_OpaqueEventQueueRef},
	{"1866",&_1866},
	{"OpaqueEventLoopRef",&_OpaqueEventLoopRef},
	{"1865",&_1865},
	{"1864",&_1864},
	{"EventTypeSpec",&_EventTypeSpec},
	{"1863",&_1863},
	{"1862",&_1862},
	{"1861",&_1861},
	{"1860",&_1860},
	{"1859",&_1859},
	{"1858",&_1858},
	{"1857",&_1857},
	{"1856",&_1856},
	{"1855",&_1855},
	{"1854",&_1854},
	{"1853",&_1853},
	{"1852",&_1852},
	{"1851",&_1851},
	{"OpaqueThemeDrawingState",&_OpaqueThemeDrawingState},
	{"1850",&_1850},
	{"ThemeWindowMetrics",&_ThemeWindowMetrics},
	{"1849",&_1849},
	{"1848",&_1848},
	{"1847",&_1847},
	{"1846",&_1846},
	{"ThemeButtonDrawInfo",&_ThemeButtonDrawInfo},
	{"1845",&_1845},
	{"1844",&_1844},
	{"1843",&_1843},
	{"1842",&_1842},
	{"1841",&_1841},
	{"1840",&_1840},
	{"1839",&_1839},
	{"1838",&_1838},
	{"1837",&_1837},
	{"1836",&_1836},
	{"1835",&_1835},
	{"ThemeTrackDrawInfo",&_ThemeTrackDrawInfo},
	{"ProgressTrackInfo",&_ProgressTrackInfo},
	{"SliderTrackInfo",&_SliderTrackInfo},
	{"ScrollBarTrackInfo",&_ScrollBarTrackInfo},
	{"1834",&_1834},
	{"1833",&_1833},
	{"1832",&_1832},
	{"1831",&_1831},
	{"1830",&_1830},
	{"1829",&_1829},
	{"1828",&_1828},
	{"1827",&_1827},
	{"1826",&_1826},
	{"1825",&_1825},
	{"1824",&_1824},
	{"1823",&_1823},
	{"1822",&_1822},
	{"1821",&_1821},
	{"1820",&_1820},
	{"1819",&_1819},
	{"1818",&_1818},
	{"1817",&_1817},
	{"1816",&_1816},
	{"1815",&_1815},
	{"1814",&_1814},
	{"1813",&_1813},
	{"1812",&_1812},
	{"1811",&_1811},
	{"1810",&_1810},
	{"1809",&_1809},
	{"1808",&_1808},
	{"1807",&_1807},
	{"1806",&_1806},
	{"1805",&_1805},
	{"1804",&_1804},
	{"1803",&_1803},
	{"1802",&_1802},
	{"1801",&_1801},
	{"1800",&_1800},
	{"1799",&_1799},
	{"1798",&_1798},
	{"1797",&_1797},
	{"1796",&_1796},
	{"1795",&_1795},
	{"1794",&_1794},
	{"1793",&_1793},
	{"1792",&_1792},
	{"1791",&_1791},
	{"1790",&_1790},
	{"1789",&_1789},
	{"1788",&_1788},
	{"1787",&_1787},
	{"1786",&_1786},
	{"1785",&_1785},
	{"OpaqueWindowGroupRef",&_OpaqueWindowGroupRef},
	{"1784",&_1784},
	{"1783",&_1783},
	{"WindowDefSpec",&_WindowDefSpec},
	{"1782",&_1782},
	{"WStateData",&_WStateData},
	{"1781",&_1781},
	{"1780",&_1780},
	{"1779",&_1779},
	{"1778",&_1778},
	{"1777",&_1777},
	{"BasicWindowDescription",&_BasicWindowDescription},
	{"1776",&_1776},
	{"1775",&_1775},
	{"WinCTab",&_WinCTab},
	{"1774",&_1774},
	{"1773",&_1773},
	{"1772",&_1772},
	{"1771",&_1771},
	{"1770",&_1770},
	{"1769",&_1769},
	{"1768",&_1768},
	{"1767",&_1767},
	{"1766",&_1766},
	{"1765",&_1765},
	{"1764",&_1764},
	{"1763",&_1763},
	{"1762",&_1762},
	{"1761",&_1761},
	{"1760",&_1760},
	{"1759",&_1759},
	{"GetGrowImageRegionRec",&_GetGrowImageRegionRec},
	{"MeasureWindowTitleRec",&_MeasureWindowTitleRec},
	{"SetupWindowProxyDragImageRec",&_SetupWindowProxyDragImageRec},
	{"GetWindowRegionRec",&_GetWindowRegionRec},
	{"1758",&_1758},
	{"1757",&_1757},
	{"1756",&_1756},
	{"1755",&_1755},
	{"1754",&_1754},
	{"1753",&_1753},
	{"1752",&_1752},
	{"1751",&_1751},
	{"1750",&_1750},
	{"1749",&_1749},
	{"1748",&_1748},
	{"1747",&_1747},
	{"1746",&_1746},
	{"1745",&_1745},
	{"1744",&_1744},
	{"1743",&_1743},
	{"1742",&_1742},
	{"1741",&_1741},
	{"1740",&_1740},
	{"1739",&_1739},
	{"ControlKind",&_ControlKind},
	{"ControlID",&_ControlID},
	{"1738",&_1738},
	{"ControlDefSpec",&_ControlDefSpec},
	{"1737",&_1737},
	{"1736",&_1736},
	{"1735",&_1735},
	{"ControlClickActivationRec",&_ControlClickActivationRec},
	{"ControlContextualMenuClickRec",&_ControlContextualMenuClickRec},
	{"ControlSetCursorRec",&_ControlSetCursorRec},
	{"ControlGetRegionRec",&_ControlGetRegionRec},
	{"ControlApplyTextColorRec",&_ControlApplyTextColorRec},
	{"ControlBackgroundRec",&_ControlBackgroundRec},
	{"ControlCalcSizeRec",&_ControlCalcSizeRec},
	{"ControlDataAccessRec",&_ControlDataAccessRec},
	{"ControlKeyDownRec",&_ControlKeyDownRec},
	{"ControlTrackingRec",&_ControlTrackingRec},
	{"1734",&_1734},
	{"IndicatorDragConstraint",&_IndicatorDragConstraint},
	{"1733",&_1733},
	{"1732",&_1732},
	{"1731",&_1731},
	{"1730",&_1730},
	{"1729",&_1729},
	{"1728",&_1728},
	{"1727",&_1727},
	{"ControlFontStyleRec",&_ControlFontStyleRec},
	{"1726",&_1726},
	{"1725",&_1725},
	{"1724",&_1724},
	{"1723",&_1723},
	{"1722",&_1722},
	{"1721",&_1721},
	{"ControlButtonContentInfo",&_ControlButtonContentInfo},
	{"1720",&_1720},
	{"1719",&_1719},
	{"1718",&_1718},
	{"1717",&_1717},
	{"1716",&_1716},
	{"1715",&_1715},
	{"1714",&_1714},
	{"1713",&_1713},
	{"CtlCTab",&_CtlCTab},
	{"1712",&_1712},
	{"OpaqueControlRef",&_OpaqueControlRef},
	{"ControlTemplate",&_ControlTemplate},
	{"1711",&_1711},
	{"IconFamilyResource",&_IconFamilyResource},
	{"IconFamilyElement",&_IconFamilyElement},
	{"1710",&_1710},
	{"1709",&_1709},
	{"1708",&_1708},
	{"1707",&_1707},
	{"1706",&_1706},
	{"1705",&_1705},
	{"1704",&_1704},
	{"1703",&_1703},
	{"1702",&_1702},
	{"1701",&_1701},
	{"1700",&_1700},
	{"1699",&_1699},
	{"1698",&_1698},
	{"1697",&_1697},
	{"1696",&_1696},
	{"1695",&_1695},
	{"OpaqueIconRef",&_OpaqueIconRef},
	{"CIcon",&_CIcon},
	{"1694",&_1694},
	{"1693",&_1693},
	{"1692",&_1692},
	{"1691",&_1691},
	{"1690",&_1690},
	{"1689",&_1689},
	{"1688",&_1688},
	{"1687",&_1687},
	{"1686",&_1686},
	{"1685",&_1685},
	{"1684",&_1684},
	{"1683",&_1683},
	{"1682",&_1682},
	{"1681",&_1681},
	{"1680",&_1680},
	{"1679",&_1679},
	{"1678",&_1678},
	{"PromiseHFSFlavor",&_PromiseHFSFlavor},
	{"HFSFlavor",&_HFSFlavor},
	{"1677",&_1677},
	{"1676",&_1676},
	{"1675",&_1675},
	{"1674",&_1674},
	{"1673",&_1673},
	{"1672",&_1672},
	{"1671",&_1671},
	{"1670",&_1670},
	{"1669",&_1669},
	{"1668",&_1668},
	{"1667",&_1667},
	{"1666",&_1666},
	{"1665",&_1665},
	{"1664",&_1664},
	{"OpaqueDragRef",&_OpaqueDragRef},
	{"NMRec",&_NMRec},
	{"1663",&_1663},
	{"HMMessageRecord",&_HMMessageRecord},
	{"HMStringResType",&_HMStringResType},
	{"1662",&_1662},
	{"1661",&_1661},
	{"1660",&_1660},
	{"1659",&_1659},
	{"1658",&_1658},
	{"1657",&_1657},
	{"1656",&_1656},
	{"1655",&_1655},
	{"1654",&_1654},
	{"1653",&_1653},
	{"1652",&_1652},
	{"1651",&_1651},
	{"TextStyle",&_TextStyle},
	{"TEStyleRec",&_TEStyleRec},
	{"NullStRec",&_NullStRec},
	{"StScrpRec",&_StScrpRec},
	{"ScrpSTElement",&_ScrpSTElement},
	{"LHElement",&_LHElement},
	{"STElement",&_STElement},
	{"StyleRun",&_StyleRun},
	{"1650",&_1650},
	{"1649",&_1649},
	{"1648",&_1648},
	{"1647",&_1647},
	{"1646",&_1646},
	{"1645",&_1645},
	{"1644",&_1644},
	{"1643",&_1643},
	{"1642",&_1642},
	{"1641",&_1641},
	{"TERec",&_TERec},
	{"ContextualMenuInterfaceStruct",&_ContextualMenuInterfaceStruct},
	{"1640",&_1640},
	{"1639",&_1639},
	{"1638",&_1638},
	{"1637",&_1637},
	{"1636",&_1636},
	{"1635",&_1635},
	{"MenuDefSpec",&_MenuDefSpec},
	{"OpaqueMenuLayoutRef",&_OpaqueMenuLayoutRef},
	{"1634",&_1634},
	{"1633",&_1633},
	{"MenuItemDataRec",&_MenuItemDataRec},
	{"1632",&_1632},
	{"1631",&_1631},
	{"MDEFDrawItemsData",&_MDEFDrawItemsData},
	{"MDEFFindItemData",&_MDEFFindItemData},
	{"MDEFDrawData",&_MDEFDrawData},
	{"MDEFHiliteItemData",&_MDEFHiliteItemData},
	{"MenuTrackingData",&_MenuTrackingData},
	{"MenuCRsrc",&_MenuCRsrc},
	{"MCEntry",&_MCEntry},
	{"OpaqueMenuHandle",&_OpaqueMenuHandle},
	{"1630",&_1630},
	{"1629",&_1629},
	{"1628",&_1628},
	{"1627",&_1627},
	{"1626",&_1626},
	{"1625",&_1625},
	{"1624",&_1624},
	{"1623",&_1623},
	{"1622",&_1622},
	{"1621",&_1621},
	{"1620",&_1620},
	{"1619",&_1619},
	{"1618",&_1618},
	{"1617",&_1617},
	{"1616",&_1616},
	{"1615",&_1615},
	{"1614",&_1614},
	{"SizeResourceRec",&_SizeResourceRec},
	{"ProcessInfoExtendedRec",&_ProcessInfoExtendedRec},
	{"ProcessInfoRec",&_ProcessInfoRec},
	{"1613",&_1613},
	{"1612",&_1612},
	{"LaunchParamBlockRec",&_LaunchParamBlockRec},
	{"AppParameters",&_AppParameters},
	{"1611",&_1611},
	{"1610",&_1610},
	{"ProcessSerialNumber",&_ProcessSerialNumber},
	{"OpaqueEventRef",&_OpaqueEventRef},
	{"EvQEl",&_EvQEl},
	{"1609",&_1609},
	{"EventRecord",&_EventRecord},
	{"1608",&_1608},
	{"1607",&_1607},
	{"1606",&_1606},
	{"1605",&_1605},
	{"1604",&_1604},
	{"1603",&_1603},
	{"1602",&_1602},
	{"1601",&_1601},
	{"1600",&_1600},
	{"LSLaunchURLSpec",&_LSLaunchURLSpec},
	{"LSLaunchFSRefSpec",&_LSLaunchFSRefSpec},
	{"1599",&_1599},
	{"1598",&_1598},
	{"LSItemInfoRecord",&_LSItemInfoRecord},
	{"1597",&_1597},
	{"1596",&_1596},
	{"1595",&_1595},
	{"1594",&_1594},
	{"1593",&_1593},
	{"1592",&_1592},
	{"1591",&_1591},
	{"1590",&_1590},
	{"1589",&_1589},
	{"DelimiterInfo",&_DelimiterInfo},
	{"SpeechXtndData",&_SpeechXtndData},
	{"PhonemeDescriptor",&_PhonemeDescriptor},
	{"PhonemeInfo",&_PhonemeInfo},
	{"SpeechVersionInfo",&_SpeechVersionInfo},
	{"SpeechErrorInfo",&_SpeechErrorInfo},
	{"SpeechStatusInfo",&_SpeechStatusInfo},
	{"VoiceFileInfo",&_VoiceFileInfo},
	{"VoiceDescription",&_VoiceDescription},
	{"1588",&_1588},
	{"VoiceSpec",&_VoiceSpec},
	{"SpeechChannelRecord",&_SpeechChannelRecord},
	{"1587",&_1587},
	{"1586",&_1586},
	{"1585",&_1585},
	{"1584",&_1584},
	{"1583",&_1583},
	{"1582",&_1582},
	{"1581",&_1581},
	{"1580",&_1580},
	{"1579",&_1579},
	{"HomographDicInfoRec",&_HomographDicInfoRec},
	{"1578",&_1578},
	{"1577",&_1577},
	{"1576",&_1576},
	{"1575",&_1575},
	{"1574",&_1574},
	{"1573",&_1573},
	{"1572",&_1572},
	{"MorphemeTextRange",&_MorphemeTextRange},
	{"1571",&_1571},
	{"1570",&_1570},
	{"1569",&_1569},
	{"1568",&_1568},
	{"1567",&_1567},
	{"LAMorphemesArray",&_LAMorphemesArray},
	{"LAMorphemeRec",&_LAMorphemeRec},
	{"OpaqueLAContextRef",&_OpaqueLAContextRef},
	{"OpaqueLAEnvironmentRef",&_OpaqueLAEnvironmentRef},
	{"DictionaryAttributeTable",&_DictionaryAttributeTable},
	{"DictionaryInformation",&_DictionaryInformation},
	{"1566",&_1566},
	{"1565",&_1565},
	{"1564",&_1564},
	{"1563",&_1563},
	{"1562",&_1562},
	{"1561",&_1561},
	{"DCMDictionaryHeader",&_DCMDictionaryHeader},
	{"OpaqueDCMFoundRecordIterator",&_OpaqueDCMFoundRecordIterator},
	{"OpaqueDCMObjectIterator",&_OpaqueDCMObjectIterator},
	{"OpaqueDCMObjectRef",&_OpaqueDCMObjectRef},
	{"OpaqueDCMObjectID",&_OpaqueDCMObjectID},
	{"1560",&_1560},
	{"1559",&_1559},
	{"1558",&_1558},
	{"1557",&_1557},
	{"1556",&_1556},
	{"1555",&_1555},
	{"1554",&_1554},
	{"1553",&_1553},
	{"1552",&_1552},
	{"1551",&_1551},
	{"1550",&_1550},
	{"PMLanguageInfo",&_PMLanguageInfo},
	{"PMResolution",&_PMResolution},
	{"PMRect",&_PMRect},
	{"1549",&_1549},
	{"1548",&_1548},
	{"1547",&_1547},
	{"1546",&_1546},
	{"1545",&_1545},
	{"1544",&_1544},
	{"1543",&_1543},
	{"1542",&_1542},
	{"1541",&_1541},
	{"1540",&_1540},
	{"1539",&_1539},
	{"1538",&_1538},
	{"1537",&_1537},
	{"1536",&_1536},
	{"1535",&_1535},
	{"1534",&_1534},
	{"1533",&_1533},
	{"1532",&_1532},
	{"1531",&_1531},
	{"1530",&_1530},
	{"OpaquePMPrinter",&_OpaquePMPrinter},
	{"OpaquePMPrintSession",&_OpaquePMPrintSession},
	{"OpaquePMPrintContext",&_OpaquePMPrintContext},
	{"OpaquePMPageFormat",&_OpaquePMPageFormat},
	{"OpaquePMPrintSettings",&_OpaquePMPrintSettings},
	{"OpaquePMDialog",&_OpaquePMDialog},
	{"OpaqueFBCSearchSession",&_OpaqueFBCSearchSession},
	{"1529",&_1529},
	{"1528",&_1528},
	{"OpaqueFNSFontProfile",&_OpaqueFNSFontProfile},
	{"1527",&_1527},
	{"OpaqueFNSFontReference",&_OpaqueFNSFontReference},
	{"FNSSysInfo",&_FNSSysInfo},
	{"1526",&_1526},
	{"1525",&_1525},
	{"DMProfileListEntryRec",&_DMProfileListEntryRec},
	{"DisplayListEntryRec",&_DisplayListEntryRec},
	{"1524",&_1524},
	{"1523",&_1523},
	{"DMMakeAndModelRec",&_DMMakeAndModelRec},
	{"DependentNotifyRec",&_DependentNotifyRec},
	{"DMDisplayModeListEntryRec",&_DMDisplayModeListEntryRec},
	{"DMDepthInfoBlockRec",&_DMDepthInfoBlockRec},
	{"DMDepthInfoRec",&_DMDepthInfoRec},
	{"AVLocationRec",&_AVLocationRec},
	{"DMComponentListEntryRec",&_DMComponentListEntryRec},
	{"DMDisplayTimingInfoRec",&_DMDisplayTimingInfoRec},
	{"1522",&_1522},
	{"1521",&_1521},
	{"1520",&_1520},
	{"1519",&_1519},
	{"1518",&_1518},
	{"1517",&_1517},
	{"1516",&_1516},
	{"1515",&_1515},
	{"1514",&_1514},
	{"1513",&_1513},
	{"1512",&_1512},
	{"1511",&_1511},
	{"1510",&_1510},
	{"1509",&_1509},
	{"1508",&_1508},
	{"1507",&_1507},
	{"1506",&_1506},
	{"1505",&_1505},
	{"1504",&_1504},
	{"1503",&_1503},
	{"VDCommunicationInfoRec",&_VDCommunicationInfoRec},
	{"VDCommunicationRec",&_VDCommunicationRec},
	{"1502",&_1502},
	{"VDDetailedTimingRec",&_VDDetailedTimingRec},
	{"1501",&_1501},
	{"1500",&_1500},
	{"1499",&_1499},
	{"1498",&_1498},
	{"1497",&_1497},
	{"1496",&_1496},
	{"1495",&_1495},
	{"VDDisplayTimingRangeRec",&_VDDisplayTimingRangeRec},
	{"1494",&_1494},
	{"VDDDCBlockRec",&_VDDDCBlockRec},
	{"VDPrivateSelectorRec",&_VDPrivateSelectorRec},
	{"VDPrivateSelectorDataRec",&_VDPrivateSelectorDataRec},
	{"VDPowerStateRec",&_VDPowerStateRec},
	{"VDConvolutionInfoRec",&_VDConvolutionInfoRec},
	{"VDHardwareCursorDrawStateRec",&_VDHardwareCursorDrawStateRec},
	{"VDSupportsHardwareCursorRec",&_VDSupportsHardwareCursorRec},
	{"VDDrawHardwareCursorRec",&_VDDrawHardwareCursorRec},
	{"VDSetHardwareCursorRec",&_VDSetHardwareCursorRec},
	{"VDRetrieveGammaRec",&_VDRetrieveGammaRec},
	{"VDGetGammaListRec",&_VDGetGammaListRec},
	{"VDGammaInfoRec",&_VDGammaInfoRec},
	{"VDVideoParametersInfoRec",&_VDVideoParametersInfoRec},
	{"VDResolutionInfoRec",&_VDResolutionInfoRec},
	{"1493",&_1493},
	{"1492",&_1492},
	{"1491",&_1491},
	{"1490",&_1490},
	{"1489",&_1489},
	{"1488",&_1488},
	{"1487",&_1487},
	{"VDSyncInfoRec",&_VDSyncInfoRec},
	{"VDDefMode",&_VDDefMode},
	{"VDSettings",&_VDSettings},
	{"VDSizeInfo",&_VDSizeInfo},
	{"VDPageInfo",&_VDPageInfo},
	{"1486",&_1486},
	{"1485",&_1485},
	{"1484",&_1484},
	{"1483",&_1483},
	{"VDMultiConnectInfoRec",&_VDMultiConnectInfoRec},
	{"VDDisplayConnectInfoRec",&_VDDisplayConnectInfoRec},
	{"VDTimingInfoRec",&_VDTimingInfoRec},
	{"VDSwitchInfoRec",&_VDSwitchInfoRec},
	{"VDBaseAddressInfoRec",&_VDBaseAddressInfoRec},
	{"VDGammaRecord",&_VDGammaRecord},
	{"VDSetEntryRecord",&_VDSetEntryRecord},
	{"VDFlagRecord",&_VDFlagRecord},
	{"VDGrayRecord",&_VDGrayRecord},
	{"VDEntryRecord",&_VDEntryRecord},
	{"VPBlock",&_VPBlock},
	{"1482",&_1482},
	{"1481",&_1481},
	{"1480",&_1480},
	{"1479",&_1479},
	{"1478",&_1478},
	{"1477",&_1477},
	{"1476",&_1476},
	{"1475",&_1475},
	{"1474",&_1474},
	{"1473",&_1473},
	{"1472",&_1472},
	{"1471",&_1471},
	{"1470",&_1470},
	{"1469",&_1469},
	{"1468",&_1468},
	{"1467",&_1467},
	{"1466",&_1466},
	{"1465",&_1465},
	{"1464",&_1464},
	{"1463",&_1463},
	{"1462",&_1462},
	{"1461",&_1461},
	{"1460",&_1460},
	{"1459",&_1459},
	{"1458",&_1458},
	{"1457",&_1457},
	{"ATSUUnhighlightData",&_ATSUUnhighlightData},
	{"ATSUBackgroundData",&_ATSUBackgroundData},
	{"ATSUBackgroundColor",&_ATSUBackgroundColor},
	{"1456",&_1456},
	{"1455",&_1455},
	{"ATSUGlyphInfoArray",&_ATSUGlyphInfoArray},
	{"ATSUGlyphInfo",&_ATSUGlyphInfo},
	{"1454",&_1454},
	{"1453",&_1453},
	{"1452",&_1452},
	{"1451",&_1451},
	{"1450",&_1450},
	{"ATSUCaret",&_ATSUCaret},
	{"ATSUAttributeInfo",&_ATSUAttributeInfo},
	{"1449",&_1449},
	{"OpaqueATSUFontFallbacks",&_OpaqueATSUFontFallbacks},
	{"OpaqueATSUStyle",&_OpaqueATSUStyle},
	{"OpaqueATSUTextLayout",&_OpaqueATSUTextLayout},
	{"PictInfo",&_PictInfo},
	{"FontSpec",&_FontSpec},
	{"CommentSpec",&_CommentSpec},
	{"1448",&_1448},
	{"1447",&_1447},
	{"1446",&_1446},
	{"Palette",&_Palette},
	{"ColorInfo",&_ColorInfo},
	{"1445",&_1445},
	{"FontRec",&_FontRec},
	{"FamRec",&_FamRec},
	{"WidthTable",&_WidthTable},
	{"KernTable",&_KernTable},
	{"KernEntry",&_KernEntry},
	{"KernPair",&_KernPair},
	{"NameTable",&_NameTable},
	{"StyleTable",&_StyleTable},
	{"FontAssoc",&_FontAssoc},
	{"AsscEntry",&_AsscEntry},
	{"WidTable",&_WidTable},
	{"WidEntry",&_WidEntry},
	{"1444",&_1444},
	{"1443",&_1443},
	{"FMetricRec",&_FMetricRec},
	{"FMOutput",&_FMOutput},
	{"FMInput",&_FMInput},
	{"1442",&_1442},
	{"1441",&_1441},
	{"1440",&_1440},
	{"1439",&_1439},
	{"1438",&_1438},
	{"1437",&_1437},
	{"1436",&_1436},
	{"1435",&_1435},
	{"1434",&_1434},
	{"1433",&_1433},
	{"OpaqueQDRegionBitsRef",&_OpaqueQDRegionBitsRef},
	{"1432",&_1432},
	{"CursorInfo",&_CursorInfo},
	{"1431",&_1431},
	{"1430",&_1430},
	{"1429",&_1429},
	{"1428",&_1428},
	{"CustomXFerRec",&_CustomXFerRec},
	{"1427",&_1427},
	{"1426",&_1426},
	{"1425",&_1425},
	{"1424",&_1424},
	{"CursorImageRec",&_CursorImageRec},
	{"1423",&_1423},
	{"OpenCPicParams",&_OpenCPicParams},
	{"ReqListRec",&_ReqListRec},
	{"CQDProcs",&_CQDProcs},
	{"GrafVars",&_GrafVars},
	{"GDevice",&_GDevice},
	{"CProcRec",&_CProcRec},
	{"SProcRec",&_SProcRec},
	{"ITab",&_ITab},
	{"GammaTbl",&_GammaTbl},
	{"CCrsr",&_CCrsr},
	{"PixPat",&_PixPat},
	{"PixMap",&_PixMap},
	{"1422",&_1422},
	{"1421",&_1421},
	{"MatchRec",&_MatchRec},
	{"xColorSpec",&_xColorSpec},
	{"ColorTable",&_ColorTable},
	{"ColorSpec",&_ColorSpec},
	{"RGBColor",&_RGBColor},
	{"1420",&_1420},
	{"OpaqueGrafPtr",&_OpaqueGrafPtr},
	{"OpaqueDialogPtr",&_OpaqueDialogPtr},
	{"OpaqueWindowPtr",&_OpaqueWindowPtr},
	{"QDProcs",&_QDProcs},
	{"MacPolygon",&_MacPolygon},
	{"Picture",&_Picture},
	{"OpaqueRgnHandle",&_OpaqueRgnHandle},
	{"PenState",&_PenState},
	{"Cursor",&_Cursor},
	{"BitMap",&_BitMap},
	{"PrinterScalingStatus",&_PrinterScalingStatus},
	{"PrinterFontStatus",&_PrinterFontStatus},
	{"1419",&_1419},
	{"1418",&_1418},
	{"1417",&_1417},
	{"Pattern",&_Pattern},
	{"1416",&_1416},
	{"1415",&_1415},
	{"1414",&_1414},
	{"1413",&_1413},
	{"1412",&_1412},
	{"1411",&_1411},
	{"1410",&_1410},
	{"1409",&_1409},
	{"1408",&_1408},
	{"1407",&_1407},
	{"FontInfo",&_FontInfo},
	{"1406",&_1406},
	{"1405",&_1405},
	{"1404",&_1404},
	{"1403",&_1403},
	{"1402",&_1402},
	{"1401",&_1401},
	{"1400",&_1400},
	{"1399",&_1399},
	{"1398",&_1398},
	{"1397",&_1397},
	{"1396",&_1396},
	{"1395",&_1395},
	{"1394",&_1394},
	{"1393",&_1393},
	{"1392",&_1392},
	{"1391",&_1391},
	{"1390",&_1390},
	{"1389",&_1389},
	{"1388",&_1388},
	{"1387",&_1387},
	{"1386",&_1386},
	{"1385",&_1385},
	{"1384",&_1384},
	{"OpaqueAEStreamRef",&_OpaqueAEStreamRef},
	{"AEBuildError",&_AEBuildError},
	{"1383",&_1383},
	{"1382",&_1382},
	{"1381",&_1381},
	{"1380",&_1380},
	{"TScriptingSizeResource",&_TScriptingSizeResource},
	{"1379",&_1379},
	{"1378",&_1378},
	{"1377",&_1377},
	{"1376",&_1376},
	{"1375",&_1375},
	{"1374",&_1374},
	{"1373",&_1373},
	{"1372",&_1372},
	{"1371",&_1371},
	{"1370",&_1370},
	{"1369",&_1369},
	{"1368",&_1368},
	{"1367",&_1367},
	{"1366",&_1366},
	{"1365",&_1365},
	{"IntlText",&_IntlText},
	{"WritingCode",&_WritingCode},
	{"OffsetArray",&_OffsetArray},
	{"TextRangeArray",&_TextRangeArray},
	{"TextRange",&_TextRange},
	{"1364",&_1364},
	{"1363",&_1363},
	{"1362",&_1362},
	{"1361",&_1361},
	{"1360",&_1360},
	{"1359",&_1359},
	{"1358",&_1358},
	{"1357",&_1357},
	{"1356",&_1356},
	{"1355",&_1355},
	{"1354",&_1354},
	{"1353",&_1353},
	{"1352",&_1352},
	{"1351",&_1351},
	{"1350",&_1350},
	{"1349",&_1349},
	{"1348",&_1348},
	{"1347",&_1347},
	{"1346",&_1346},
	{"1345",&_1345},
	{"1344",&_1344},
	{"1343",&_1343},
	{"1342",&_1342},
	{"1341",&_1341},
	{"1340",&_1340},
	{"1339",&_1339},
	{"1338",&_1338},
	{"1337",&_1337},
	{"1336",&_1336},
	{"1335",&_1335},
	{"1334",&_1334},
	{"1333",&_1333},
	{"1332",&_1332},
	{"1331",&_1331},
	{"1330",&_1330},
	{"ccntTokenRecord",&_ccntTokenRecord},
	{"1329",&_1329},
	{"1328",&_1328},
	{"1327",&_1327},
	{"1326",&_1326},
	{"1325",&_1325},
	{"1324",&_1324},
	{"1323",&_1323},
	{"1322",&_1322},
	{"1321",&_1321},
	{"1320",&_1320},
	{"1319",&_1319},
	{"1318",&_1318},
	{"1317",&_1317},
	{"AEArrayData",&_AEArrayData},
	{"1316",&_1316},
	{"1315",&_1315},
	{"AEKeyDesc",&_AEKeyDesc},
	{"AEDesc",&_AEDesc},
	{"OpaqueAEDataStorageType",&_OpaqueAEDataStorageType},
	{"1314",&_1314},
	{"1313",&_1313},
	{"1312",&_1312},
	{"1311",&_1311},
	{"1310",&_1310},
	{"1309",&_1309},
	{"1308",&_1308},
	{"1307",&_1307},
	{"1306",&_1306},
	{"1305",&_1305},
	{"1304",&_1304},
	{"1303",&_1303},
	{"1302",&_1302},
	{"1301",&_1301},
	{"1300",&_1300},
	{"1299",&_1299},
	{"CMDeviceProfileArray",&_CMDeviceProfileArray},
	{"NCMDeviceProfileInfo",&_NCMDeviceProfileInfo},
	{"CMDeviceProfileInfo",&_CMDeviceProfileInfo},
	{"CMDeviceInfo",&_CMDeviceInfo},
	{"CMDeviceScope",&_CMDeviceScope},
	{"1298",&_1298},
	{"1297",&_1297},
	{"1296",&_1296},
	{"1295",&_1295},
	{"1294",&_1294},
	{"1293",&_1293},
	{"1292",&_1292},
	{"CMProfileIterateData",&_CMProfileIterateData},
	{"1291",&_1291},
	{"1290",&_1290},
	{"CMProfileLocation",&_CMProfileLocation},
	{"CMProfLoc",&_CMProfLoc},
	{"CMBufferLocation",&_CMBufferLocation},
	{"CMPathLocation",&_CMPathLocation},
	{"CMProcedureLocation",&_CMProcedureLocation},
	{"CMPtrLocation",&_CMPtrLocation},
	{"CMHandleLocation",&_CMHandleLocation},
	{"CMFileLocation",&_CMFileLocation},
	{"1289",&_1289},
	{"1288",&_1288},
	{"1287",&_1287},
	{"CMBitmap",&_CMBitmap},
	{"1286",&_1286},
	{"1285",&_1285},
	{"1284",&_1284},
	{"1283",&_1283},
	{"1282",&_1282},
	{"CMProfileIdentifier",&_CMProfileIdentifier},
	{"CMCWInfoRecord",&_CMCWInfoRecord},
	{"CMMInfoRecord",&_CMMInfoRecord},
	{"CMMInfo",&_CMMInfo},
	{"CMSearchRecord",&_CMSearchRecord},
	{"CMProfileSearchRecord",&_CMProfileSearchRecord},
	{"CMColor",&_CMColor},
	{"CMNamedColor",&_CMNamedColor},
	{"CMMultichannel8Color",&_CMMultichannel8Color},
	{"CMMultichannel7Color",&_CMMultichannel7Color},
	{"CMMultichannel6Color",&_CMMultichannel6Color},
	{"CMMultichannel5Color",&_CMMultichannel5Color},
	{"CMGrayColor",&_CMGrayColor},
	{"CMYxyColor",&_CMYxyColor},
	{"CMLuvColor",&_CMLuvColor},
	{"CMLabColor",&_CMLabColor},
	{"CMHSVColor",&_CMHSVColor},
	{"CMHLSColor",&_CMHLSColor},
	{"CMCMYColor",&_CMCMYColor},
	{"CMCMYKColor",&_CMCMYKColor},
	{"CMRGBColor",&_CMRGBColor},
	{"1281",&_1281},
	{"NCMConcatProfileSet",&_NCMConcatProfileSet},
	{"NCMConcatProfileSpec",&_NCMConcatProfileSpec},
	{"CMConcatProfileSet",&_CMConcatProfileSet},
	{"CMAppleProfileHeader",&_CMAppleProfileHeader},
	{"1280",&_1280},
	{"1279",&_1279},
	{"1278",&_1278},
	{"1277",&_1277},
	{"1276",&_1276},
	{"1275",&_1275},
	{"1274",&_1274},
	{"1273",&_1273},
	{"1272",&_1272},
	{"1271",&_1271},
	{"1270",&_1270},
	{"CMProfile",&_CMProfile},
	{"CMProfileResponse",&_CMProfileResponse},
	{"CMProfileChromaticities",&_CMProfileChromaticities},
	{"CMHeader",&_CMHeader},
	{"1269",&_1269},
	{"1268",&_1268},
	{"CMIString",&_CMIString},
	{"1267",&_1267},
	{"1266",&_1266},
	{"CMMultiLocalizedUniCodeType",&_CMMultiLocalizedUniCodeType},
	{"CMMultiLocalizedUniCodeEntryRec",&_CMMultiLocalizedUniCodeEntryRec},
	{"CMMakeAndModelType",&_CMMakeAndModelType},
	{"CMMakeAndModel",&_CMMakeAndModel},
	{"CMVideoCardGammaType",&_CMVideoCardGammaType},
	{"1265",&_1265},
	{"CMVideoCardGamma",&_CMVideoCardGamma},
	{"CMVideoCardGammaFormula",&_CMVideoCardGammaFormula},
	{"CMVideoCardGammaTable",&_CMVideoCardGammaTable},
	{"1264",&_1264},
	{"CMPS2CRDVMSizeType",&_CMPS2CRDVMSizeType},
	{"CMIntentCRDVMSize",&_CMIntentCRDVMSize},
	{"CMUcrBgType",&_CMUcrBgType},
	{"CMProfileSequenceDescType",&_CMProfileSequenceDescType},
	{"CMXYZType",&_CMXYZType},
	{"CMViewingConditionsType",&_CMViewingConditionsType},
	{"CMUInt64ArrayType",&_CMUInt64ArrayType},
	{"CMUInt32ArrayType",&_CMUInt32ArrayType},
	{"CMUInt16ArrayType",&_CMUInt16ArrayType},
	{"CMUInt8ArrayType",&_CMUInt8ArrayType},
	{"CMU16Fixed16ArrayType",&_CMU16Fixed16ArrayType},
	{"CMS15Fixed16ArrayType",&_CMS15Fixed16ArrayType},
	{"CMSignatureType",&_CMSignatureType},
	{"CMScreeningType",&_CMScreeningType},
	{"CMScreeningChannelRec",&_CMScreeningChannelRec},
	{"CMUnicodeTextType",&_CMUnicodeTextType},
	{"CMTextType",&_CMTextType},
	{"CMTextDescriptionType",&_CMTextDescriptionType},
	{"CMParametricCurveType",&_CMParametricCurveType},
	{"CMNativeDisplayInfoType",&_CMNativeDisplayInfoType},
	{"CMNativeDisplayInfo",&_CMNativeDisplayInfo},
	{"CMNamedColor2Type",&_CMNamedColor2Type},
	{"CMNamedColor2EntryType",&_CMNamedColor2EntryType},
	{"CMNamedColorType",&_CMNamedColorType},
	{"CMMeasurementType",&_CMMeasurementType},
	{"CMMultiFunctCLUTType",&_CMMultiFunctCLUTType},
	{"CMMultiFunctLutType",&_CMMultiFunctLutType},
	{"CMLut8Type",&_CMLut8Type},
	{"CMLut16Type",&_CMLut16Type},
	{"CMDateTimeType",&_CMDateTimeType},
	{"CMDataType",&_CMDataType},
	{"CMCurveType",&_CMCurveType},
	{"CMAdaptationMatrixType",&_CMAdaptationMatrixType},
	{"CM2Profile",&_CM2Profile},
	{"CMTagElemTable",&_CMTagElemTable},
	{"CMTagRecord",&_CMTagRecord},
	{"CM4Header",&_CM4Header},
	{"CM2Header",&_CM2Header},
	{"CMXYZColor",&_CMXYZColor},
	{"CMFixedXYZColor",&_CMFixedXYZColor},
	{"CMFixedXYColor",&_CMFixedXYColor},
	{"CMDateTime",&_CMDateTime},
	{"1263",&_1263},
	{"1262",&_1262},
	{"1261",&_1261},
	{"1260",&_1260},
	{"1259",&_1259},
	{"1258",&_1258},
	{"1257",&_1257},
	{"1256",&_1256},
	{"1255",&_1255},
	{"1254",&_1254},
	{"1253",&_1253},
	{"1252",&_1252},
	{"1251",&_1251},
	{"1250",&_1250},
	{"1249",&_1249},
	{"1248",&_1248},
	{"1247",&_1247},
	{"1246",&_1246},
	{"1245",&_1245},
	{"1244",&_1244},
	{"1243",&_1243},
	{"1242",&_1242},
	{"1241",&_1241},
	{"1240",&_1240},
	{"1239",&_1239},
	{"1238",&_1238},
	{"1237",&_1237},
	{"OpaqueCMWorldRef",&_OpaqueCMWorldRef},
	{"OpaqueCMMatchRef",&_OpaqueCMMatchRef},
	{"OpaqueCMProfileSearchRef",&_OpaqueCMProfileSearchRef},
	{"OpaqueCMProfileRef",&_OpaqueCMProfileRef},
	{"_CGCommonWindowLevelKey",&__CGCommonWindowLevelKey},
	{"1236",&_1236},
	{"1235",&_1235},
	{"_CGDeviceByteColor",&__CGDeviceByteColor},
	{"_CGDeviceColor",&__CGDeviceColor},
	{"_CGDirectPaletteRef",&__CGDirectPaletteRef},
	{"_CGDirectDisplayID",&__CGDirectDisplayID},
	{"_CGError",&__CGError},
	{"CGDataConsumerCallbacks",&_CGDataConsumerCallbacks},
	{"CGDataConsumer",&_CGDataConsumer},
	{"CGInterpolationQuality",&_CGInterpolationQuality},
	{"CGTextEncoding",&_CGTextEncoding},
	{"CGTextDrawingMode",&_CGTextDrawingMode},
	{"CGPathDrawingMode",&_CGPathDrawingMode},
	{"CGLineCap",&_CGLineCap},
	{"CGLineJoin",&_CGLineJoin},
	{"CGPDFDocument",&_CGPDFDocument},
	{"CGPatternCallbacks",&_CGPatternCallbacks},
	{"CGPatternTiling",&_CGPatternTiling},
	{"CGPattern",&_CGPattern},
	{"CGImageAlphaInfo",&_CGImageAlphaInfo},
	{"CGImage",&_CGImage},
	{"1234",&_1234},
	{"CGFont",&_CGFont},
	{"CGColorRenderingIntent",&_CGColorRenderingIntent},
	{"CGDataProviderDirectAccessCallbacks",&_CGDataProviderDirectAccessCallbacks},
	{"CGDataProviderCallbacks",&_CGDataProviderCallbacks},
	{"CGDataProvider",&_CGDataProvider},
	{"CGColorSpace",&_CGColorSpace},
	{"CGContext",&_CGContext},
	{"CGRectEdge",&_CGRectEdge},
	{"CGRect",&_CGRect},
	{"CGSize",&_CGSize},
	{"CGPoint",&_CGPoint},
	{"CGAffineTransform",&_CGAffineTransform},
	{"scalerStreamData",&_scalerStreamData},
	{"1233",&_1233},
	{"1232",&_1232},
	{"1231",&_1231},
	{"scalerStream",&_scalerStream},
	{"scalerPrerequisiteItem",&_scalerPrerequisiteItem},
	{"1230",&_1230},
	{"1229",&_1229},
	{"1228",&_1228},
	{"1227",&_1227},
	{"ATSFontFilter",&_ATSFontFilter},
	{"ATSFontFilterSelector",&_ATSFontFilterSelector},
	{"1226",&_1226},
	{"ATSFontIterator_",&_ATSFontIterator_},
	{"ATSFontFamilyIterator_",&_ATSFontFamilyIterator_},
	{"1225",&_1225},
	{"1224",&_1224},
	{"1223",&_1223},
	{"1222",&_1222},
	{"1221",&_1221},
	{"1220",&_1220},
	{"FontVariation",&_FontVariation},
	{"1219",&_1219},
	{"1218",&_1218},
	{"sfntFeatureHeader",&_sfntFeatureHeader},
	{"sfntFontRunFeature",&_sfntFontRunFeature},
	{"sfntFontFeatureSetting",&_sfntFontFeatureSetting},
	{"sfntFeatureName",&_sfntFeatureName},
	{"1217",&_1217},
	{"1216",&_1216},
	{"sfntDescriptorHeader",&_sfntDescriptorHeader},
	{"sfntFontDescriptor",&_sfntFontDescriptor},
	{"1215",&_1215},
	{"1214",&_1214},
	{"sfntVariationHeader",&_sfntVariationHeader},
	{"1213",&_1213},
	{"sfntInstance",&_sfntInstance},
	{"1212",&_1212},
	{"sfntVariationAxis",&_sfntVariationAxis},
	{"1211",&_1211},
	{"1210",&_1210},
	{"sfntNameHeader",&_sfntNameHeader},
	{"1209",&_1209},
	{"sfntNameRecord",&_sfntNameRecord},
	{"1208",&_1208},
	{"1207",&_1207},
	{"1206",&_1206},
	{"1205",&_1205},
	{"sfntCMapHeader",&_sfntCMapHeader},
	{"1204",&_1204},
	{"sfntCMapEncoding",&_sfntCMapEncoding},
	{"1203",&_1203},
	{"sfntCMapSubHeader",&_sfntCMapSubHeader},
	{"1202",&_1202},
	{"1201",&_1201},
	{"1200",&_1200},
	{"1199",&_1199},
	{"1198",&_1198},
	{"1197",&_1197},
	{"1196",&_1196},
	{"1195",&_1195},
	{"1194",&_1194},
	{"sfntDirectory",&_sfntDirectory},
	{"sfntDirectoryEntry",&_sfntDirectoryEntry},
	{"ATSJustWidthDeltaEntryOverride",&_ATSJustWidthDeltaEntryOverride},
	{"ATSTrapezoid",&_ATSTrapezoid},
	{"1193",&_1193},
	{"1192",&_1192},
	{"1191",&_1191},
	{"1190",&_1190},
	{"ATSGlyphScreenMetrics",&_ATSGlyphScreenMetrics},
	{"ATSGlyphIdealMetrics",&_ATSGlyphIdealMetrics},
	{"ATSUCurvePaths",&_ATSUCurvePaths},
	{"ATSUCurvePath",&_ATSUCurvePath},
	{"1189",&_1189},
	{"1188",&_1188},
	{"ATSFontMetrics",&_ATSFontMetrics},
	{"1187",&_1187},
	{"1186",&_1186},
	{"FMFilter",&_FMFilter},
	{"FMFontDirectoryFilter",&_FMFontDirectoryFilter},
	{"1185",&_1185},
	{"1184",&_1184},
	{"1183",&_1183},
	{"1182",&_1182},
	{"FMFontFamilyInstanceIterator",&_FMFontFamilyInstanceIterator},
	{"FMFontIterator",&_FMFontIterator},
	{"FMFontFamilyIterator",&_FMFontFamilyIterator},
	{"FMFontFamilyInstance",&_FMFontFamilyInstance},
	{"BslnTable",&_BslnTable},
	{"BslnFormatUnion",&_BslnFormatUnion},
	{"BslnFormat3Part",&_BslnFormat3Part},
	{"BslnFormat2Part",&_BslnFormat2Part},
	{"BslnFormat1Part",&_BslnFormat1Part},
	{"BslnFormat0Part",&_BslnFormat0Part},
	{"1181",&_1181},
	{"1180",&_1180},
	{"KernSubtableHeader",&_KernSubtableHeader},
	{"KernVersion0SubtableHeader",&_KernVersion0SubtableHeader},
	{"KernFormatSpecificHeader",&_KernFormatSpecificHeader},
	{"KernIndexArrayHeader",&_KernIndexArrayHeader},
	{"KernSimpleArrayHeader",&_KernSimpleArrayHeader},
	{"KernOffsetTable",&_KernOffsetTable},
	{"KernStateEntry",&_KernStateEntry},
	{"KernStateHeader",&_KernStateHeader},
	{"KernOrderedListHeader",&_KernOrderedListHeader},
	{"KernOrderedListEntry",&_KernOrderedListEntry},
	{"KernKerningPair",&_KernKerningPair},
	{"KernTableHeader",&_KernTableHeader},
	{"KernVersion0Header",&_KernVersion0Header},
	{"1179",&_1179},
	{"1178",&_1178},
	{"1177",&_1177},
	{"TrakTable",&_TrakTable},
	{"TrakTableData",&_TrakTableData},
	{"TrakTableEntry",&_TrakTableEntry},
	{"1176",&_1176},
	{"PropLookupSingle",&_PropLookupSingle},
	{"PropLookupSegment",&_PropLookupSegment},
	{"PropTable",&_PropTable},
	{"1175",&_1175},
	{"1174",&_1174},
	{"MorxTable",&_MorxTable},
	{"MorxChain",&_MorxChain},
	{"MorxSubtable",&_MorxSubtable},
	{"MorxSpecificSubtable",&_MorxSpecificSubtable},
	{"MorxInsertionSubtable",&_MorxInsertionSubtable},
	{"MorxLigatureSubtable",&_MorxLigatureSubtable},
	{"MorxContextualSubtable",&_MorxContextualSubtable},
	{"MorxRearrangementSubtable",&_MorxRearrangementSubtable},
	{"1173",&_1173},
	{"MortTable",&_MortTable},
	{"MortChain",&_MortChain},
	{"MortFeatureEntry",&_MortFeatureEntry},
	{"MortSubtable",&_MortSubtable},
	{"MortSpecificSubtable",&_MortSpecificSubtable},
	{"MortInsertionSubtable",&_MortInsertionSubtable},
	{"MortSwashSubtable",&_MortSwashSubtable},
	{"MortLigatureSubtable",&_MortLigatureSubtable},
	{"MortContextualSubtable",&_MortContextualSubtable},
	{"MortRearrangementSubtable",&_MortRearrangementSubtable},
	{"1172",&_1172},
	{"OpbdTable",&_OpbdTable},
	{"OpbdSideValues",&_OpbdSideValues},
	{"1171",&_1171},
	{"JustTable",&_JustTable},
	{"JustDirectionTable",&_JustDirectionTable},
	{"JustPostcompTable",&_JustPostcompTable},
	{"JustWidthDeltaGroup",&_JustWidthDeltaGroup},
	{"JustWidthDeltaEntry",&_JustWidthDeltaEntry},
	{"JustPCAction",&_JustPCAction},
	{"JustPCActionSubrecord",&_JustPCActionSubrecord},
	{"JustPCGlyphRepeatAddAction",&_JustPCGlyphRepeatAddAction},
	{"JustPCDuctilityAction",&_JustPCDuctilityAction},
	{"JustPCConditionalAddAction",&_JustPCConditionalAddAction},
	{"JustPCDecompositionAction",&_JustPCDecompositionAction},
	{"1170",&_1170},
	{"1169",&_1169},
	{"1168",&_1168},
	{"LcarCaretTable",&_LcarCaretTable},
	{"LcarCaretClassEntry",&_LcarCaretClassEntry},
	{"1167",&_1167},
	{"STXEntryTwo",&_STXEntryTwo},
	{"STXEntryOne",&_STXEntryOne},
	{"STXEntryZero",&_STXEntryZero},
	{"STXHeader",&_STXHeader},
	{"1166",&_1166},
	{"STEntryTwo",&_STEntryTwo},
	{"STEntryOne",&_STEntryOne},
	{"STEntryZero",&_STEntryZero},
	{"STClassTable",&_STClassTable},
	{"STHeader",&_STHeader},
	{"1165",&_1165},
	{"SFNTLookupTable",&_SFNTLookupTable},
	{"SFNTLookupFormatSpecificHeader",&_SFNTLookupFormatSpecificHeader},
	{"SFNTLookupSingleHeader",&_SFNTLookupSingleHeader},
	{"SFNTLookupSingle",&_SFNTLookupSingle},
	{"SFNTLookupSegmentHeader",&_SFNTLookupSegmentHeader},
	{"SFNTLookupSegment",&_SFNTLookupSegment},
	{"SFNTLookupTrimmedArrayHeader",&_SFNTLookupTrimmedArrayHeader},
	{"SFNTLookupArrayHeader",&_SFNTLookupArrayHeader},
	{"SFNTLookupBinarySearchHeader",&_SFNTLookupBinarySearchHeader},
	{"1164",&_1164},
	{"1163",&_1163},
	{"1162",&_1162},
	{"1161",&_1161},
	{"1160",&_1160},
	{"1159",&_1159},
	{"1158",&_1158},
	{"1157",&_1157},
	{"1156",&_1156},
	{"1155",&_1155},
	{"1154",&_1154},
	{"1153",&_1153},
	{"1152",&_1152},
	{"1151",&_1151},
	{"1150",&_1150},
	{"1149",&_1149},
	{"1148",&_1148},
	{"1147",&_1147},
	{"1146",&_1146},
	{"1145",&_1145},
	{"1144",&_1144},
	{"1143",&_1143},
	{"1142",&_1142},
	{"1141",&_1141},
	{"1140",&_1140},
	{"1139",&_1139},
	{"1138",&_1138},
	{"1137",&_1137},
	{"1136",&_1136},
	{"1135",&_1135},
	{"1134",&_1134},
	{"1133",&_1133},
	{"1132",&_1132},
	{"1131",&_1131},
	{"1130",&_1130},
	{"__CFHTTPMessage",&___CFHTTPMessage},
	{"1129",&_1129},
	{"1128",&_1128},
	{"1127",&_1127},
	{"1126",&_1126},
	{"1125",&_1125},
	{"1124",&_1124},
	{"1123",&_1123},
	{"1122",&_1122},
	{"1121",&_1121},
	{"1120",&_1120},
	{"1119",&_1119},
	{"KCCallbackInfo",&_KCCallbackInfo},
	{"1118",&_1118},
	{"1117",&_1117},
	{"SecKeychainAttributeList",&_SecKeychainAttributeList},
	{"SecKeychainAttribute",&_SecKeychainAttribute},
	{"OpaqueSecKeychainSearchRef",&_OpaqueSecKeychainSearchRef},
	{"OpaqueSecKeychainItemRef",&_OpaqueSecKeychainItemRef},
	{"OpaqueSecKeychainRef",&_OpaqueSecKeychainRef},
	{"NSLPluginData",&_NSLPluginData},
	{"NSLServicesListHeader",&_NSLServicesListHeader},
	{"NSLTypedData",&_NSLTypedData},
	{"NSLPluginAsyncInfo",&_NSLPluginAsyncInfo},
	{"NSLClientAsyncInfo",&_NSLClientAsyncInfo},
	{"1116",&_1116},
	{"1115",&_1115},
	{"1114",&_1114},
	{"NSLError",&_NSLError},
	{"1113",&_1113},
	{"1112",&_1112},
	{"1111",&_1111},
	{"1110",&_1110},
	{"1109",&_1109},
	{"1108",&_1108},
	{"Partition",&_Partition},
	{"1107",&_1107},
	{"DDMap",&_DDMap},
	{"Block0",&_Block0},
	{"1106",&_1106},
	{"1105",&_1105},
	{"1104",&_1104},
	{"1103",&_1103},
	{"1102",&_1102},
	{"1101",&_1101},
	{"1100",&_1100},
	{"1099",&_1099},
	{"1098",&_1098},
	{"1097",&_1097},
	{"1096",&_1096},
	{"1095",&_1095},
	{"1094",&_1094},
	{"1093",&_1093},
	{"1092",&_1092},
	{"1091",&_1091},
	{"1090",&_1090},
	{"1089",&_1089},
	{"1088",&_1088},
	{"1087",&_1087},
	{"1086",&_1086},
	{"1085",&_1085},
	{"SCSILoadDriverPB",&_SCSILoadDriverPB},
	{"SCSIDriverPB",&_SCSIDriverPB},
	{"SCSIGetVirtualIDInfoPB",&_SCSIGetVirtualIDInfoPB},
	{"SCSIReleaseQPB",&_SCSIReleaseQPB},
	{"SCSIResetDevicePB",&_SCSIResetDevicePB},
	{"SCSIResetBusPB",&_SCSIResetBusPB},
	{"SCSITerminateIOPB",&_SCSITerminateIOPB},
	{"SCSIAbortCommandPB",&_SCSIAbortCommandPB},
	{"SCSIBusInquiryPB",&_SCSIBusInquiryPB},
	{"SCSI_IO",&_SCSI_IO},
	{"SCSI_PB",&_SCSI_PB},
	{"SCSIHdr",&_SCSIHdr},
	{"SGRecord",&_SGRecord},
	{"CDB",&_CDB},
	{"DeviceIdentATA",&_DeviceIdentATA},
	{"1084",&_1084},
	{"DeviceIdent",&_DeviceIdent},
	{"1083",&_1083},
	{"1082",&_1082},
	{"1081",&_1081},
	{"1080",&_1080},
	{"SCSIInstr",&_SCSIInstr},
	{"1079",&_1079},
	{"PowerSourceParamBlock",&_PowerSourceParamBlock},
	{"1078",&_1078},
	{"1077",&_1077},
	{"1076",&_1076},
	{"1075",&_1075},
	{"1074",&_1074},
	{"1073",&_1073},
	{"StartupTime",&_StartupTime},
	{"WakeupTime",&_WakeupTime},
	{"BatteryTimeRec",&_BatteryTimeRec},
	{"PMgrQueueElement",&_PMgrQueueElement},
	{"HDQueueElement",&_HDQueueElement},
	{"SleepQRec",&_SleepQRec},
	{"BatteryInfo",&_BatteryInfo},
	{"ActivityInfo",&_ActivityInfo},
	{"1072",&_1072},
	{"1071",&_1071},
	{"1070",&_1070},
	{"1069",&_1069},
	{"1068",&_1068},
	{"1067",&_1067},
	{"1066",&_1066},
	{"1065",&_1065},
	{"1064",&_1064},
	{"1063",&_1063},
	{"1062",&_1062},
	{"1061",&_1061},
	{"1060",&_1060},
	{"PowerSummary",&_PowerSummary},
	{"DevicePowerInfo",&_DevicePowerInfo},
	{"1059",&_1059},
	{"1058",&_1058},
	{"1057",&_1057},
	{"1056",&_1056},
	{"1055",&_1055},
	{"1054",&_1054},
	{"1053",&_1053},
	{"1052",&_1052},
	{"1051",&_1051},
	{"1050",&_1050},
	{"1049",&_1049},
	{"1048",&_1048},
	{"OTGate",&_OTGate},
	{"OTHashList",&_OTHashList},
	{"OTClientList",&_OTClientList},
	{"OTPortCloseStruct",&_OTPortCloseStruct},
	{"1047",&_1047},
	{"1046",&_1046},
	{"trace_ids",&_trace_ids},
	{"1045",&_1045},
	{"log_ctl",&_log_ctl},
	{"strioctl",&_strioctl},
	{"strrecvfd",&_strrecvfd},
	{"strpmsg",&_strpmsg},
	{"strpeek",&_strpeek},
	{"str_list",&_str_list},
	{"str_mlist",&_str_mlist},
	{"strfdinsert",&_strfdinsert},
	{"1044",&_1044},
	{"1043",&_1043},
	{"1042",&_1042},
	{"1041",&_1041},
	{"1040",&_1040},
	{"1039",&_1039},
	{"bandinfo",&_bandinfo},
	{"1038",&_1038},
	{"1037",&_1037},
	{"1036",&_1036},
	{"1035",&_1035},
	{"1034",&_1034},
	{"1033",&_1033},
	{"1032",&_1032},
	{"1031",&_1031},
	{"1030",&_1030},
	{"LCPEcho",&_LCPEcho},
	{"CCMiscInfo",&_CCMiscInfo},
	{"PPPMRULimits",&_PPPMRULimits},
	{"1029",&_1029},
	{"1028",&_1028},
	{"1027",&_1027},
	{"1026",&_1026},
	{"1025",&_1025},
	{"1024",&_1024},
	{"1023",&_1023},
	{"1022",&_1022},
	{"1021",&_1021},
	{"1020",&_1020},
	{"1019",&_1019},
	{"1018",&_1018},
	{"1017",&_1017},
	{"1016",&_1016},
	{"1015",&_1015},
	{"1014",&_1014},
	{"1013",&_1013},
	{"1012",&_1012},
	{"1011",&_1011},
	{"1010",&_1010},
	{"1009",&_1009},
	{"1008",&_1008},
	{"1007",&_1007},
	{"OTISDNAddress",&_OTISDNAddress},
	{"1006",&_1006},
	{"1005",&_1005},
	{"1004",&_1004},
	{"1003",&_1003},
	{"1002",&_1002},
	{"1001",&_1001},
	{"1000",&_1000},
	{"999",&_999},
	{"998",&_998},
	{"997",&_997},
	{"996",&_996},
	{"995",&_995},
	{"994",&_994},
	{"993",&_993},
	{"992",&_992},
	{"991",&_991},
	{"T8022FullPacketHeader",&_T8022FullPacketHeader},
	{"T8022SNAPHeader",&_T8022SNAPHeader},
	{"T8022Header",&_T8022Header},
	{"EnetPacketHeader",&_EnetPacketHeader},
	{"990",&_990},
	{"989",&_989},
	{"988",&_988},
	{"T8022Address",&_T8022Address},
	{"987",&_987},
	{"986",&_986},
	{"985",&_985},
	{"984",&_984},
	{"983",&_983},
	{"982",&_982},
	{"981",&_981},
	{"980",&_980},
	{"979",&_979},
	{"978",&_978},
	{"AppleTalkInfo",&_AppleTalkInfo},
	{"DDPNBPAddress",&_DDPNBPAddress},
	{"NBPAddress",&_NBPAddress},
	{"DDPAddress",&_DDPAddress},
	{"NBPEntity",&_NBPEntity},
	{"977",&_977},
	{"976",&_976},
	{"975",&_975},
	{"974",&_974},
	{"973",&_973},
	{"972",&_972},
	{"971",&_971},
	{"970",&_970},
	{"969",&_969},
	{"968",&_968},
	{"967",&_967},
	{"966",&_966},
	{"965",&_965},
	{"InetDHCPOption",&_InetDHCPOption},
	{"964",&_964},
	{"InetInterfaceInfo",&_InetInterfaceInfo},
	{"963",&_963},
	{"962",&_962},
	{"DNSAddress",&_DNSAddress},
	{"DNSQueryInfo",&_DNSQueryInfo},
	{"InetMailExchange",&_InetMailExchange},
	{"InetSysInfo",&_InetSysInfo},
	{"InetHostInfo",&_InetHostInfo},
	{"961",&_961},
	{"InetAddress",&_InetAddress},
	{"960",&_960},
	{"TIPAddMulticast",&_TIPAddMulticast},
	{"959",&_959},
	{"958",&_958},
	{"957",&_957},
	{"956",&_956},
	{"955",&_955},
	{"954",&_954},
	{"953",&_953},
	{"952",&_952},
	{"951",&_951},
	{"950",&_950},
	{"OTList",&_OTList},
	{"OTLIFO",&_OTLIFO},
	{"OTLink",&_OTLink},
	{"949",&_949},
	{"OpaqueOTClientContextPtr",&_OpaqueOTClientContextPtr},
	{"TLookupBuffer",&_TLookupBuffer},
	{"TLookupReply",&_TLookupReply},
	{"TLookupRequest",&_TLookupRequest},
	{"TRegisterReply",&_TRegisterReply},
	{"TRegisterRequest",&_TRegisterRequest},
	{"TUnitReply",&_TUnitReply},
	{"TUnitRequest",&_TUnitRequest},
	{"TReply",&_TReply},
	{"TRequest",&_TRequest},
	{"TOptMgmt",&_TOptMgmt},
	{"TUDErr",&_TUDErr},
	{"TUnitData",&_TUnitData},
	{"TCall",&_TCall},
	{"TDiscon",&_TDiscon},
	{"TBind",&_TBind},
	{"948",&_948},
	{"OTBufferInfo",&_OTBufferInfo},
	{"947",&_947},
	{"OTBuffer",&_OTBuffer},
	{"946",&_946},
	{"OTData",&_OTData},
	{"strbuf",&_strbuf},
	{"TNetbuf",&_TNetbuf},
	{"OTPortRecord",&_OTPortRecord},
	{"945",&_945},
	{"944",&_944},
	{"943",&_943},
	{"942",&_942},
	{"941",&_941},
	{"940",&_940},
	{"939",&_939},
	{"TEndpointInfo",&_TEndpointInfo},
	{"938",&_938},
	{"937",&_937},
	{"936",&_936},
	{"t_linger",&_t_linger},
	{"t_kpalive",&_t_kpalive},
	{"935",&_935},
	{"TOption",&_TOption},
	{"TOptionHeader",&_TOptionHeader},
	{"OTConfiguration",&_OTConfiguration},
	{"934",&_934},
	{"933",&_933},
	{"932",&_932},
	{"OTScriptInfo",&_OTScriptInfo},
	{"931",&_931},
	{"930",&_930},
	{"929",&_929},
	{"928",&_928},
	{"927",&_927},
	{"926",&_926},
	{"925",&_925},
	{"924",&_924},
	{"923",&_923},
	{"922",&_922},
	{"921",&_921},
	{"920",&_920},
	{"OTAddress",&_OTAddress},
	{"919",&_919},
	{"918",&_918},
	{"917",&_917},
	{"916",&_916},
	{"TECPluginDispatchTable",&_TECPluginDispatchTable},
	{"915",&_915},
	{"TECSnifferContextRec",&_TECSnifferContextRec},
	{"TECConverterContextRec",&_TECConverterContextRec},
	{"TECPluginStateRec",&_TECPluginStateRec},
	{"TECBufferContextRec",&_TECBufferContextRec},
	{"TextChunk",&_TextChunk},
	{"CommentsChunk",&_CommentsChunk},
	{"Comment",&_Comment},
	{"ApplicationSpecificChunk",&_ApplicationSpecificChunk},
	{"AudioRecordingChunk",&_AudioRecordingChunk},
	{"MIDIDataChunk",&_MIDIDataChunk},
	{"InstrumentChunk",&_InstrumentChunk},
	{"AIFFLoop",&_AIFFLoop},
	{"MarkerChunk",&_MarkerChunk},
	{"Marker",&_Marker},
	{"SoundDataChunk",&_SoundDataChunk},
	{"ExtCommonChunk",&_ExtCommonChunk},
	{"CommonChunk",&_CommonChunk},
	{"FormatVersionChunk",&_FormatVersionChunk},
	{"ContainerChunk",&_ContainerChunk},
	{"ChunkHeader",&_ChunkHeader},
	{"914",&_914},
	{"913",&_913},
	{"912",&_912},
	{"911",&_911},
	{"910",&_910},
	{"BTHeaderRec",&_BTHeaderRec},
	{"909",&_909},
	{"BTNodeDescriptor",&_BTNodeDescriptor},
	{"HFSPlusVolumeHeader",&_HFSPlusVolumeHeader},
	{"HFSMasterDirectoryBlock",&_HFSMasterDirectoryBlock},
	{"908",&_908},
	{"907",&_907},
	{"906",&_906},
	{"HFSPlusAttrRecord",&_HFSPlusAttrRecord},
	{"HFSPlusAttrExtents",&_HFSPlusAttrExtents},
	{"HFSPlusAttrForkData",&_HFSPlusAttrForkData},
	{"HFSPlusAttrInlineData",&_HFSPlusAttrInlineData},
	{"905",&_905},
	{"HFSPlusCatalogThread",&_HFSPlusCatalogThread},
	{"HFSCatalogThread",&_HFSCatalogThread},
	{"HFSPlusCatalogFile",&_HFSPlusCatalogFile},
	{"HFSCatalogFile",&_HFSCatalogFile},
	{"HFSPlusCatalogFolder",&_HFSPlusCatalogFolder},
	{"HFSCatalogFolder",&_HFSCatalogFolder},
	{"904",&_904},
	{"903",&_903},
	{"HFSPlusCatalogKey",&_HFSPlusCatalogKey},
	{"HFSCatalogKey",&_HFSCatalogKey},
	{"902",&_902},
	{"HFSPlusPermissions",&_HFSPlusPermissions},
	{"HFSPlusForkData",&_HFSPlusForkData},
	{"HFSPlusExtentDescriptor",&_HFSPlusExtentDescriptor},
	{"HFSExtentDescriptor",&_HFSExtentDescriptor},
	{"901",&_901},
	{"HFSPlusExtentKey",&_HFSPlusExtentKey},
	{"HFSExtentKey",&_HFSExtentKey},
	{"900",&_900},
	{"899",&_899},
	{"XLibExportedSymbol",&_XLibExportedSymbol},
	{"898",&_898},
	{"XLibContainerHeader",&_XLibContainerHeader},
	{"897",&_897},
	{"896",&_896},
	{"895",&_895},
	{"894",&_894},
	{"893",&_893},
	{"892",&_892},
	{"891",&_891},
	{"890",&_890},
	{"889",&_889},
	{"888",&_888},
	{"887",&_887},
	{"886",&_886},
	{"PEFLoaderRelocationHeader",&_PEFLoaderRelocationHeader},
	{"885",&_885},
	{"884",&_884},
	{"PEFExportedSymbol",&_PEFExportedSymbol},
	{"883",&_883},
	{"882",&_882},
	{"PEFExportedSymbolKey",&_PEFExportedSymbolKey},
	{"PEFSplitHashWord",&_PEFSplitHashWord},
	{"881",&_881},
	{"PEFExportedSymbolHashSlot",&_PEFExportedSymbolHashSlot},
	{"880",&_880},
	{"879",&_879},
	{"PEFImportedSymbol",&_PEFImportedSymbol},
	{"878",&_878},
	{"PEFImportedLibrary",&_PEFImportedLibrary},
	{"PEFLoaderInfoHeader",&_PEFLoaderInfoHeader},
	{"877",&_877},
	{"876",&_876},
	{"875",&_875},
	{"874",&_874},
	{"PEFSectionHeader",&_PEFSectionHeader},
	{"873",&_873},
	{"872",&_872},
	{"PEFContainerHeader",&_PEFContainerHeader},
	{"AVLTreeStruct",&_AVLTreeStruct},
	{"871",&_871},
	{"870",&_870},
	{"869",&_869},
	{"868",&_868},
	{"MPAddressSpaceInfo",&_MPAddressSpaceInfo},
	{"MPNotificationInfo",&_MPNotificationInfo},
	{"MPCriticalRegionInfo",&_MPCriticalRegionInfo},
	{"MPEventInfo",&_MPEventInfo},
	{"MPSemaphoreInfo",&_MPSemaphoreInfo},
	{"MPQueueInfo",&_MPQueueInfo},
	{"867",&_867},
	{"TMTask",&_TMTask},
	{"866",&_866},
	{"MultiUserGestalt",&_MultiUserGestalt},
	{"865",&_865},
	{"864",&_864},
	{"863",&_863},
	{"862",&_862},
	{"861",&_861},
	{"FindFolderUserRedirectionGlobals",&_FindFolderUserRedirectionGlobals},
	{"860",&_860},
	{"FolderRouting",&_FolderRouting},
	{"FolderDesc",&_FolderDesc},
	{"859",&_859},
	{"858",&_858},
	{"857",&_857},
	{"856",&_856},
	{"855",&_855},
	{"854",&_854},
	{"853",&_853},
	{"852",&_852},
	{"851",&_851},
	{"850",&_850},
	{"849",&_849},
	{"848",&_848},
	{"847",&_847},
	{"846",&_846},
	{"845",&_845},
	{"SchedulerInfoRec",&_SchedulerInfoRec},
	{"844",&_844},
	{"843",&_843},
	{"842",&_842},
	{"841",&_841},
	{"840",&_840},
	{"839",&_839},
	{"838",&_838},
	{"837",&_837},
	{"836",&_836},
	{"835",&_835},
	{"834",&_834},
	{"833",&_833},
	{"832",&_832},
	{"UnicodeMapping",&_UnicodeMapping},
	{"831",&_831},
	{"OpaqueUnicodeToTextRunInfo",&_OpaqueUnicodeToTextRunInfo},
	{"OpaqueUnicodeToTextInfo",&_OpaqueUnicodeToTextInfo},
	{"OpaqueTextToUnicodeInfo",&_OpaqueTextToUnicodeInfo},
	{"TECConversionInfo",&_TECConversionInfo},
	{"OpaqueTECSnifferObjectRef",&_OpaqueTECSnifferObjectRef},
	{"OpaqueTECObjectRef",&_OpaqueTECObjectRef},
	{"830",&_830},
	{"829",&_829},
	{"828",&_828},
	{"decform",&_decform},
	{"827",&_827},
	{"decimal",&_decimal},
	{"826",&_826},
	{"825",&_825},
	{"824",&_824},
	{"823",&_823},
	{"OpaqueTextBreakLocatorRef",&_OpaqueTextBreakLocatorRef},
	{"822",&_822},
	{"821",&_821},
	{"820",&_820},
	{"819",&_819},
	{"818",&_818},
	{"817",&_817},
	{"OpaqueCollatorRef",&_OpaqueCollatorRef},
	{"816",&_816},
	{"815",&_815},
	{"814",&_814},
	{"813",&_813},
	{"812",&_812},
	{"UCKeySequenceDataIndex",&_UCKeySequenceDataIndex},
	{"UCKeyStateTerminators",&_UCKeyStateTerminators},
	{"UCKeyStateRecordsIndex",&_UCKeyStateRecordsIndex},
	{"UCKeyToCharTableIndex",&_UCKeyToCharTableIndex},
	{"UCKeyModifiersToTableNum",&_UCKeyModifiersToTableNum},
	{"UCKeyLayoutFeatureInfo",&_UCKeyLayoutFeatureInfo},
	{"UCKeyboardLayout",&_UCKeyboardLayout},
	{"UCKeyboardTypeHeader",&_UCKeyboardTypeHeader},
	{"UCKeyStateEntryRange",&_UCKeyStateEntryRange},
	{"UCKeyStateEntryTerminal",&_UCKeyStateEntryTerminal},
	{"811",&_811},
	{"UCKeyStateRecord",&_UCKeyStateRecord},
	{"810",&_810},
	{"NBreakTable",&_NBreakTable},
	{"BreakTable",&_BreakTable},
	{"ScriptRunStatus",&_ScriptRunStatus},
	{"809",&_809},
	{"808",&_808},
	{"FVector",&_FVector},
	{"807",&_807},
	{"806",&_806},
	{"805",&_805},
	{"NumFormatString",&_NumFormatString},
	{"804",&_804},
	{"803",&_803},
	{"802",&_802},
	{"801",&_801},
	{"InterruptSetMember",&_InterruptSetMember},
	{"OpaqueInterruptSetID",&_OpaqueInterruptSetID},
	{"800",&_800},
	{"799",&_799},
	{"PageInformation",&_PageInformation},
	{"798",&_798},
	{"797",&_797},
	{"796",&_796},
	{"795",&_795},
	{"794",&_794},
	{"IOPreparationTable",&_IOPreparationTable},
	{"MultipleAddressRange",&_MultipleAddressRange},
	{"AddressRange",&_AddressRange},
	{"793",&_793},
	{"792",&_792},
	{"791",&_791},
	{"PhysicalAddressRange",&_PhysicalAddressRange},
	{"LogicalAddressRange",&_LogicalAddressRange},
	{"790",&_790},
	{"OpaqueTimerID",&_OpaqueTimerID},
	{"OpaqueTaskID",&_OpaqueTaskID},
	{"OpaqueSoftwareInterruptID",&_OpaqueSoftwareInterruptID},
	{"OpaqueIOPreparationID",&_OpaqueIOPreparationID},
	{"ExceptionInformationPowerPC",&_ExceptionInformationPowerPC},
	{"ExceptionInfo",&_ExceptionInfo},
	{"789",&_789},
	{"MemoryExceptionInformation",&_MemoryExceptionInformation},
	{"788",&_788},
	{"VectorInformationPowerPC",&_VectorInformationPowerPC},
	{"Vector128",&_Vector128},
	{"FPUInformationPowerPC",&_FPUInformationPowerPC},
	{"RegisterInformationPowerPC",&_RegisterInformationPowerPC},
	{"MachineInformationPowerPC",&_MachineInformationPowerPC},
	{"OpaqueAreaID",&_OpaqueAreaID},
	{"OpaqueIOCommandID",&_OpaqueIOCommandID},
	{"787",&_787},
	{"786",&_786},
	{"DRVRHeader",&_DRVRHeader},
	{"785",&_785},
	{"784",&_784},
	{"783",&_783},
	{"782",&_782},
	{"781",&_781},
	{"780",&_780},
	{"779",&_779},
	{"778",&_778},
	{"777",&_777},
	{"OpaqueRegPropertyIter",&_OpaqueRegPropertyIter},
	{"OpaqueRegEntryIter",&_OpaqueRegEntryIter},
	{"776",&_776},
	{"775",&_775},
	{"774",&_774},
	{"773",&_773},
	{"772",&_772},
	{"771",&_771},
	{"770",&_770},
	{"769",&_769},
	{"768",&_768},
	{"RegEntryID",&_RegEntryID},
	{"OpaqueDeviceNodePtr",&_OpaqueDeviceNodePtr},
	{"767",&_767},
	{"766",&_766},
	{"765",&_765},
	{"764",&_764},
	{"763",&_763},
	{"LocaleAndVariant",&_LocaleAndVariant},
	{"762",&_762},
	{"OpaqueLocaleRef",&_OpaqueLocaleRef},
	{"AliasRecord",&_AliasRecord},
	{"761",&_761},
	{"760",&_760},
	{"759",&_759},
	{"758",&_758},
	{"757",&_757},
	{"CFragSystem7InitBlock",&_CFragSystem7InitBlock},
	{"756",&_756},
	{"CFragSystem7Locator",&_CFragSystem7Locator},
	{"CFragCFBundleLocator",&_CFragCFBundleLocator},
	{"CFragSystem7SegmentedLocator",&_CFragSystem7SegmentedLocator},
	{"CFragSystem7DiskFlatLocator",&_CFragSystem7DiskFlatLocator},
	{"CFragSystem7MemoryLocator",&_CFragSystem7MemoryLocator},
	{"755",&_755},
	{"754",&_754},
	{"753",&_753},
	{"OpaqueCFragContainerID",&_OpaqueCFragContainerID},
	{"OpaqueCFragClosureID",&_OpaqueCFragClosureID},
	{"OpaqueCFragConnectionID",&_OpaqueCFragConnectionID},
	{"752",&_752},
	{"CFragResource",&_CFragResource},
	{"751",&_751},
	{"CFragResourceSearchExtension",&_CFragResourceSearchExtension},
	{"CFragResourceExtensionHeader",&_CFragResourceExtensionHeader},
	{"CFragResourceMember",&_CFragResourceMember},
	{"750",&_750},
	{"CFragWhere2Union",&_CFragWhere2Union},
	{"CFragWhere1Union",&_CFragWhere1Union},
	{"749",&_749},
	{"CFragUsage2Union",&_CFragUsage2Union},
	{"CFragUsage1Union",&_CFragUsage1Union},
	{"748",&_748},
	{"747",&_747},
	{"746",&_746},
	{"745",&_745},
	{"744",&_744},
	{"743",&_743},
	{"742",&_742},
	{"741",&_741},
	{"740",&_740},
	{"739",&_739},
	{"MPTaskInfo",&_MPTaskInfo},
	{"MPTaskInfoVersion2",&_MPTaskInfoVersion2},
	{"738",&_738},
	{"737",&_737},
	{"736",&_736},
	{"735",&_735},
	{"734",&_734},
	{"733",&_733},
	{"732",&_732},
	{"731",&_731},
	{"730",&_730},
	{"729",&_729},
	{"728",&_728},
	{"727",&_727},
	{"OpaqueMPOpaqueID",&_OpaqueMPOpaqueID},
	{"OpaqueMPConsoleID",&_OpaqueMPConsoleID},
	{"OpaqueMPAreaID",&_OpaqueMPAreaID},
	{"OpaqueMPCpuID",&_OpaqueMPCpuID},
	{"OpaqueMPCoherenceID",&_OpaqueMPCoherenceID},
	{"OpaqueMPNotificationID",&_OpaqueMPNotificationID},
	{"OpaqueMPAddressSpaceID",&_OpaqueMPAddressSpaceID},
	{"OpaqueMPEventID",&_OpaqueMPEventID},
	{"OpaqueMPTimerID",&_OpaqueMPTimerID},
	{"OpaqueMPCriticalRegionID",&_OpaqueMPCriticalRegionID},
	{"OpaqueMPSemaphoreID",&_OpaqueMPSemaphoreID},
	{"OpaqueMPQueueID",&_OpaqueMPQueueID},
	{"OpaqueMPTaskID",&_OpaqueMPTaskID},
	{"OpaqueMPProcessID",&_OpaqueMPProcessID},
	{"726",&_726},
	{"725",&_725},
	{"724",&_724},
	{"723",&_723},
	{"722",&_722},
	{"721",&_721},
	{"ComponentMPWorkFunctionHeaderRecord",&_ComponentMPWorkFunctionHeaderRecord},
	{"720",&_720},
	{"719",&_719},
	{"718",&_718},
	{"717",&_717},
	{"RegisteredComponentInstanceRecord",&_RegisteredComponentInstanceRecord},
	{"RegisteredComponentRecord",&_RegisteredComponentRecord},
	{"ComponentInstanceRecord",&_ComponentInstanceRecord},
	{"ComponentRecord",&_ComponentRecord},
	{"ComponentParameters",&_ComponentParameters},
	{"ComponentAliasResource",&_ComponentAliasResource},
	{"ExtComponentResource",&_ExtComponentResource},
	{"ComponentPlatformInfoArray",&_ComponentPlatformInfoArray},
	{"ComponentResourceExtension",&_ComponentResourceExtension},
	{"ComponentPlatformInfo",&_ComponentPlatformInfo},
	{"ComponentResource",&_ComponentResource},
	{"ResourceSpec",&_ResourceSpec},
	{"ComponentDescription",&_ComponentDescription},
	{"716",&_716},
	{"715",&_715},
	{"714",&_714},
	{"713",&_713},
	{"712",&_712},
	{"711",&_711},
	{"710",&_710},
	{"709",&_709},
	{"708",&_708},
	{"707",&_707},
	{"706",&_706},
	{"705",&_705},
	{"704",&_704},
	{"703",&_703},
	{"702",&_702},
	{"701",&_701},
	{"700",&_700},
	{"699",&_699},
	{"698",&_698},
	{"697",&_697},
	{"696",&_696},
	{"695",&_695},
	{"694",&_694},
	{"693",&_693},
	{"692",&_692},
	{"691",&_691},
	{"690",&_690},
	{"689",&_689},
	{"688",&_688},
	{"687",&_687},
	{"686",&_686},
	{"685",&_685},
	{"684",&_684},
	{"683",&_683},
	{"682",&_682},
	{"681",&_681},
	{"680",&_680},
	{"679",&_679},
	{"678",&_678},
	{"677",&_677},
	{"676",&_676},
	{"675",&_675},
	{"674",&_674},
	{"673",&_673},
	{"672",&_672},
	{"671",&_671},
	{"670",&_670},
	{"669",&_669},
	{"668",&_668},
	{"667",&_667},
	{"666",&_666},
	{"665",&_665},
	{"664",&_664},
	{"663",&_663},
	{"662",&_662},
	{"661",&_661},
	{"660",&_660},
	{"659",&_659},
	{"658",&_658},
	{"657",&_657},
	{"656",&_656},
	{"655",&_655},
	{"654",&_654},
	{"653",&_653},
	{"652",&_652},
	{"651",&_651},
	{"650",&_650},
	{"649",&_649},
	{"648",&_648},
	{"647",&_647},
	{"646",&_646},
	{"645",&_645},
	{"644",&_644},
	{"643",&_643},
	{"642",&_642},
	{"641",&_641},
	{"640",&_640},
	{"639",&_639},
	{"638",&_638},
	{"637",&_637},
	{"636",&_636},
	{"635",&_635},
	{"634",&_634},
	{"633",&_633},
	{"632",&_632},
	{"631",&_631},
	{"630",&_630},
	{"629",&_629},
	{"628",&_628},
	{"627",&_627},
	{"626",&_626},
	{"625",&_625},
	{"624",&_624},
	{"623",&_623},
	{"622",&_622},
	{"621",&_621},
	{"620",&_620},
	{"619",&_619},
	{"618",&_618},
	{"617",&_617},
	{"616",&_616},
	{"615",&_615},
	{"614",&_614},
	{"613",&_613},
	{"612",&_612},
	{"611",&_611},
	{"610",&_610},
	{"609",&_609},
	{"608",&_608},
	{"607",&_607},
	{"606",&_606},
	{"605",&_605},
	{"604",&_604},
	{"603",&_603},
	{"602",&_602},
	{"601",&_601},
	{"600",&_600},
	{"599",&_599},
	{"598",&_598},
	{"597",&_597},
	{"596",&_596},
	{"595",&_595},
	{"594",&_594},
	{"593",&_593},
	{"592",&_592},
	{"591",&_591},
	{"590",&_590},
	{"589",&_589},
	{"588",&_588},
	{"587",&_587},
	{"586",&_586},
	{"585",&_585},
	{"584",&_584},
	{"583",&_583},
	{"582",&_582},
	{"581",&_581},
	{"580",&_580},
	{"579",&_579},
	{"578",&_578},
	{"577",&_577},
	{"576",&_576},
	{"575",&_575},
	{"574",&_574},
	{"573",&_573},
	{"572",&_572},
	{"571",&_571},
	{"570",&_570},
	{"569",&_569},
	{"568",&_568},
	{"567",&_567},
	{"566",&_566},
	{"565",&_565},
	{"564",&_564},
	{"563",&_563},
	{"562",&_562},
	{"561",&_561},
	{"560",&_560},
	{"559",&_559},
	{"558",&_558},
	{"557",&_557},
	{"556",&_556},
	{"555",&_555},
	{"554",&_554},
	{"553",&_553},
	{"552",&_552},
	{"551",&_551},
	{"550",&_550},
	{"549",&_549},
	{"548",&_548},
	{"547",&_547},
	{"546",&_546},
	{"545",&_545},
	{"544",&_544},
	{"543",&_543},
	{"542",&_542},
	{"541",&_541},
	{"540",&_540},
	{"539",&_539},
	{"538",&_538},
	{"537",&_537},
	{"536",&_536},
	{"535",&_535},
	{"534",&_534},
	{"533",&_533},
	{"532",&_532},
	{"531",&_531},
	{"530",&_530},
	{"529",&_529},
	{"528",&_528},
	{"527",&_527},
	{"526",&_526},
	{"525",&_525},
	{"524",&_524},
	{"523",&_523},
	{"522",&_522},
	{"521",&_521},
	{"520",&_520},
	{"519",&_519},
	{"518",&_518},
	{"517",&_517},
	{"516",&_516},
	{"515",&_515},
	{"514",&_514},
	{"513",&_513},
	{"512",&_512},
	{"511",&_511},
	{"510",&_510},
	{"509",&_509},
	{"508",&_508},
	{"507",&_507},
	{"506",&_506},
	{"OpaqueCollection",&_OpaqueCollection},
	{"505",&_505},
	{"504",&_504},
	{"503",&_503},
	{"502",&_502},
	{"TokenBlock",&_TokenBlock},
	{"TokenRec",&_TokenRec},
	{"501",&_501},
	{"500",&_500},
	{"499",&_499},
	{"498",&_498},
	{"497",&_497},
	{"496",&_496},
	{"495",&_495},
	{"494",&_494},
	{"493",&_493},
	{"492",&_492},
	{"491",&_491},
	{"490",&_490},
	{"489",&_489},
	{"488",&_488},
	{"487",&_487},
	{"486",&_486},
	{"485",&_485},
	{"484",&_484},
	{"483",&_483},
	{"482",&_482},
	{"481",&_481},
	{"480",&_480},
	{"479",&_479},
	{"478",&_478},
	{"477",&_477},
	{"476",&_476},
	{"475",&_475},
	{"474",&_474},
	{"473",&_473},
	{"472",&_472},
	{"471",&_471},
	{"470",&_470},
	{"469",&_469},
	{"468",&_468},
	{"467",&_467},
	{"466",&_466},
	{"465",&_465},
	{"464",&_464},
	{"463",&_463},
	{"462",&_462},
	{"461",&_461},
	{"460",&_460},
	{"459",&_459},
	{"458",&_458},
	{"457",&_457},
	{"456",&_456},
	{"455",&_455},
	{"454",&_454},
	{"453",&_453},
	{"452",&_452},
	{"451",&_451},
	{"ItlbExtRecord",&_ItlbExtRecord},
	{"ItlbRecord",&_ItlbRecord},
	{"ItlcRecord",&_ItlcRecord},
	{"RuleBasedTrslRecord",&_RuleBasedTrslRecord},
	{"Itl5Record",&_Itl5Record},
	{"TableDirectoryRecord",&_TableDirectoryRecord},
	{"NItl4Rec",&_NItl4Rec},
	{"Itl4Rec",&_Itl4Rec},
	{"NumberParts",&_NumberParts},
	{"WideCharArr",&_WideCharArr},
	{"WideChar",&_WideChar},
	{"UntokenTable",&_UntokenTable},
	{"Itl1ExtRec",&_Itl1ExtRec},
	{"Intl1Rec",&_Intl1Rec},
	{"Intl0Rec",&_Intl0Rec},
	{"OffPair",&_OffPair},
	{"450",&_450},
	{"449",&_449},
	{"448",&_448},
	{"447",&_447},
	{"446",&_446},
	{"445",&_445},
	{"444",&_444},
	{"440",&_440},
	{"439",&_439},
	{"438",&_438},
	{"__CFUserNotification",&___CFUserNotification},
	{"437",&_437},
	{"__CFWriteStream",&___CFWriteStream},
	{"__CFReadStream",&___CFReadStream},
	{"436",&_436},
	{"435",&_435},
	{"434",&_434},
	{"433",&_433},
	{"432",&_432},
	{"431",&_431},
	{"__CFNotificationCenter",&___CFNotificationCenter},
	{"430",&_430},
	{"429",&_429},
	{"428",&_428},
	{"427",&_427},
	{"__CFSocket",&___CFSocket},
	{"426",&_426},
	{"425",&_425},
	{"__CFMessagePort",&___CFMessagePort},
	{"424",&_424},
	{"__CFMachPort",&___CFMachPort},
	{"423",&_423},
	{"422",&_422},
	{"421",&_421},
	{"420",&_420},
	{"419",&_419},
	{"418",&_418},
	{"__CFRunLoopTimer",&___CFRunLoopTimer},
	{"__CFRunLoopObserver",&___CFRunLoopObserver},
	{"__CFRunLoopSource",&___CFRunLoopSource},
	{"__CFRunLoop",&___CFRunLoop},
	{"mach_port_qos",&_mach_port_qos},
	{"mach_port_limits",&_mach_port_limits},
	{"mach_port_status",&_mach_port_status},
	{"417",&_417},
	{"__CFPlugInInstance",&___CFPlugInInstance},
	{"416",&_416},
	{"__CFUUID",&___CFUUID},
	{"403",&_403},
	{"402",&_402},
	{"__CFByteOrder",&___CFByteOrder},
	{"__CFBundle",&___CFBundle},
	{"__CFBitVector",&___CFBitVector},
	{"__CFBinaryHeap",&___CFBinaryHeap},
	{"378",&_378},
	{"377",&_377},
	{"tm",&_tm},
	{"376",&_376},
	{"375",&_375},
	{"__sFILE",&___sFILE},
	{"__sbuf",&___sbuf},
	{"sigstack",&_sigstack},
	{"sigvec",&_sigvec},
	{"sigaltstack",&_sigaltstack},
	{"sigaction",&_sigaction},
	{"370",&_370},
	{"_opaque_pthread_cond_t",&__opaque_pthread_cond_t},
	{"_opaque_pthread_condattr_t",&__opaque_pthread_condattr_t},
	{"_opaque_pthread_mutex_t",&__opaque_pthread_mutex_t},
	{"_opaque_pthread_mutexattr_t",&__opaque_pthread_mutexattr_t},
	{"_opaque_pthread_attr_t",&__opaque_pthread_attr_t},
	{"_opaque_pthread_t",&__opaque_pthread_t},
	{"_pthread_handler_rec",&__pthread_handler_rec},
	{"fd_set",&_fd_set},
	{"_jmp_buf",&__jmp_buf},
	{"sigcontext",&_sigcontext},
	{"369",&_369},
	{"exception",&_exception},
	{"fdversion",&_fdversion},
	{"lconv",&_lconv},
	{"345",&_345},
	{"344",&_344},
	{"343",&_343},
	{"342",&_342},
	{"341",&_341},
	{"340",&_340},
	{"339",&_339},
	{"__CFXMLParser",&___CFXMLParser},
	{"338",&_338},
	{"337",&_337},
	{"336",&_336},
	{"335",&_335},
	{"334",&_334},
	{"333",&_333},
	{"332",&_332},
	{"331",&_331},
	{"330",&_330},
	{"329",&_329},
	{"328",&_328},
	{"327",&_327},
	{"326",&_326},
	{"__CFXMLNode",&___CFXMLNode},
	{"325",&_325},
	{"__CFURL",&___CFURL},
	{"324",&_324},
	{"323",&_323},
	{"OpaqueFNSubscriptionRef",&_OpaqueFNSubscriptionRef},
	{"322",&_322},
	{"FSVolumeInfoParam",&_FSVolumeInfoParam},
	{"FSVolumeInfo",&_FSVolumeInfo},
	{"321",&_321},
	{"320",&_320},
	{"FSForkCBInfoParam",&_FSForkCBInfoParam},
	{"FSForkInfo",&_FSForkInfo},
	{"FSForkIOParam",&_FSForkIOParam},
	{"319",&_319},
	{"FSCatalogBulkParam",&_FSCatalogBulkParam},
	{"FSSearchParams",&_FSSearchParams},
	{"318",&_318},
	{"317",&_317},
	{"OpaqueFSIterator",&_OpaqueFSIterator},
	{"FSRefParam",&_FSRefParam},
	{"FSCatalogInfo",&_FSCatalogInfo},
	{"316",&_316},
	{"315",&_315},
	{"314",&_314},
	{"FSPermissionInfo",&_FSPermissionInfo},
	{"FSRef",&_FSRef},
	{"313",&_313},
	{"DrvQEl",&_DrvQEl},
	{"VCB",&_VCB},
	{"FCBPBRec",&_FCBPBRec},
	{"WDPBRec",&_WDPBRec},
	{"CMovePBRec",&_CMovePBRec},
	{"HParamBlockRec",&_HParamBlockRec},
	{"CSParam",&_CSParam},
	{"ForeignPrivParam",&_ForeignPrivParam},
	{"FIDParam",&_FIDParam},
	{"WDParam",&_WDParam},
	{"CopyParam",&_CopyParam},
	{"ObjParam",&_ObjParam},
	{"AccessParam",&_AccessParam},
	{"XVolumeParam",&_XVolumeParam},
	{"XIOParam",&_XIOParam},
	{"HVolumeParam",&_HVolumeParam},
	{"HFileParam",&_HFileParam},
	{"HIOParam",&_HIOParam},
	{"DTPBRec",&_DTPBRec},
	{"AFPAlternateAddress",&_AFPAlternateAddress},
	{"AFPTagData",&_AFPTagData},
	{"312",&_312},
	{"311",&_311},
	{"310",&_310},
	{"AFPXVolMountInfo",&_AFPXVolMountInfo},
	{"AFPVolMountInfo",&_AFPVolMountInfo},
	{"309",&_309},
	{"VolumeMountInfoHeader",&_VolumeMountInfoHeader},
	{"VolMountInfoHeader",&_VolMountInfoHeader},
	{"308",&_308},
	{"FSSpec",&_FSSpec},
	{"CatPositionRec",&_CatPositionRec},
	{"XCInfoPBRec",&_XCInfoPBRec},
	{"CInfoPBRec",&_CInfoPBRec},
	{"DirInfo",&_DirInfo},
	{"HFileInfo",&_HFileInfo},
	{"MultiDevParam",&_MultiDevParam},
	{"SlotDevParam",&_SlotDevParam},
	{"CntrlParam",&_CntrlParam},
	{"VolumeParam",&_VolumeParam},
	{"FileParam",&_FileParam},
	{"IOParam",&_IOParam},
	{"ParamBlockRec",&_ParamBlockRec},
	{"GetVolParmsInfoBuffer",&_GetVolParmsInfoBuffer},
	{"307",&_307},
	{"306",&_306},
	{"305",&_305},
	{"304",&_304},
	{"303",&_303},
	{"302",&_302},
	{"301",&_301},
	{"300",&_300},
	{"299",&_299},
	{"298",&_298},
	{"297",&_297},
	{"296",&_296},
	{"295",&_295},
	{"294",&_294},
	{"293",&_293},
	{"292",&_292},
	{"291",&_291},
	{"290",&_290},
	{"289",&_289},
	{"288",&_288},
	{"287",&_287},
	{"286",&_286},
	{"285",&_285},
	{"284",&_284},
	{"283",&_283},
	{"HFSUniStr255",&_HFSUniStr255},
	{"DXInfo",&_DXInfo},
	{"DInfo",&_DInfo},
	{"FXInfo",&_FXInfo},
	{"FInfo",&_FInfo},
	{"ExtendedFolderInfo",&_ExtendedFolderInfo},
	{"ExtendedFileInfo",&_ExtendedFileInfo},
	{"FolderInfo",&_FolderInfo},
	{"FileInfo",&_FileInfo},
	{"282",&_282},
	{"281",&_281},
	{"280",&_280},
	{"279",&_279},
	{"278",&_278},
	{"277",&_277},
	{"276",&_276},
	{"275",&_275},
	{"274",&_274},
	{"RoutingResourceEntry",&_RoutingResourceEntry},
	{"273",&_273},
	{"CustomBadgeResource",&_CustomBadgeResource},
	{"272",&_272},
	{"271",&_271},
	{"270",&_270},
	{"269",&_269},
	{"LocalDateTime",&_LocalDateTime},
	{"UTCDateTime",&_UTCDateTime},
	{"268",&_268},
	{"267",&_267},
	{"266",&_266},
	{"265",&_265},
	{"264",&_264},
	{"263",&_263},
	{"262",&_262},
	{"261",&_261},
	{"260",&_260},
	{"259",&_259},
	{"258",&_258},
	{"257",&_257},
	{"256",&_256},
	{"255",&_255},
	{"254",&_254},
	{"253",&_253},
	{"252",&_252},
	{"251",&_251},
	{"250",&_250},
	{"249",&_249},
	{"248",&_248},
	{"247",&_247},
	{"246",&_246},
	{"245",&_245},
	{"244",&_244},
	{"243",&_243},
	{"242",&_242},
	{"241",&_241},
	{"240",&_240},
	{"239",&_239},
	{"238",&_238},
	{"237",&_237},
	{"236",&_236},
	{"235",&_235},
	{"234",&_234},
	{"233",&_233},
	{"232",&_232},
	{"231",&_231},
	{"230",&_230},
	{"229",&_229},
	{"228",&_228},
	{"227",&_227},
	{"226",&_226},
	{"225",&_225},
	{"224",&_224},
	{"223",&_223},
	{"222",&_222},
	{"221",&_221},
	{"220",&_220},
	{"219",&_219},
	{"218",&_218},
	{"217",&_217},
	{"216",&_216},
	{"215",&_215},
	{"214",&_214},
	{"213",&_213},
	{"212",&_212},
	{"211",&_211},
	{"210",&_210},
	{"209",&_209},
	{"208",&_208},
	{"207",&_207},
	{"206",&_206},
	{"205",&_205},
	{"204",&_204},
	{"203",&_203},
	{"202",&_202},
	{"201",&_201},
	{"200",&_200},
	{"199",&_199},
	{"198",&_198},
	{"197",&_197},
	{"196",&_196},
	{"195",&_195},
	{"194",&_194},
	{"193",&_193},
	{"192",&_192},
	{"191",&_191},
	{"190",&_190},
	{"189",&_189},
	{"188",&_188},
	{"187",&_187},
	{"186",&_186},
	{"185",&_185},
	{"184",&_184},
	{"183",&_183},
	{"182",&_182},
	{"181",&_181},
	{"180",&_180},
	{"179",&_179},
	{"178",&_178},
	{"177",&_177},
	{"176",&_176},
	{"175",&_175},
	{"174",&_174},
	{"173",&_173},
	{"172",&_172},
	{"171",&_171},
	{"170",&_170},
	{"169",&_169},
	{"168",&_168},
	{"167",&_167},
	{"166",&_166},
	{"165",&_165},
	{"164",&_164},
	{"163",&_163},
	{"162",&_162},
	{"161",&_161},
	{"160",&_160},
	{"159",&_159},
	{"158",&_158},
	{"157",&_157},
	{"156",&_156},
	{"155",&_155},
	{"154",&_154},
	{"153",&_153},
	{"152",&_152},
	{"151",&_151},
	{"150",&_150},
	{"149",&_149},
	{"148",&_148},
	{"147",&_147},
	{"146",&_146},
	{"145",&_145},
	{"144",&_144},
	{"143",&_143},
	{"142",&_142},
	{"141",&_141},
	{"140",&_140},
	{"139",&_139},
	{"138",&_138},
	{"137",&_137},
	{"136",&_136},
	{"135",&_135},
	{"134",&_134},
	{"133",&_133},
	{"132",&_132},
	{"131",&_131},
	{"130",&_130},
	{"129",&_129},
	{"128",&_128},
	{"127",&_127},
	{"126",&_126},
	{"125",&_125},
	{"124",&_124},
	{"123",&_123},
	{"122",&_122},
	{"TECInfo",&_TECInfo},
	{"121",&_121},
	{"ScriptCodeRun",&_ScriptCodeRun},
	{"TextEncodingRun",&_TextEncodingRun},
	{"120",&_120},
	{"119",&_119},
	{"118",&_118},
	{"117",&_117},
	{"116",&_116},
	{"115",&_115},
	{"114",&_114},
	{"113",&_113},
	{"112",&_112},
	{"111",&_111},
	{"110",&_110},
	{"109",&_109},
	{"108",&_108},
	{"107",&_107},
	{"106",&_106},
	{"105",&_105},
	{"104",&_104},
	{"103",&_103},
	{"102",&_102},
	{"101",&_101},
	{"100",&_100},
	{"99",&_99},
	{"98",&_98},
	{"97",&_97},
	{"96",&_96},
	{"95",&_95},
	{"94",&_94},
	{"93",&_93},
	{"92",&_92},
	{"91",&_91},
	{"90",&_90},
	{"89",&_89},
	{"88",&_88},
	{"87",&_87},
	{"86",&_86},
	{"85",&_85},
	{"SysEnvRec",&_SysEnvRec},
	{"84",&_84},
	{"83",&_83},
	{"MachineLocation",&_MachineLocation},
	{"DeferredTask",&_DeferredTask},
	{"QHdr",&_QHdr},
	{"QElem",&_QElem},
	{"SysParmType",&_SysParmType},
	{"82",&_82},
	{"81",&_81},
	{"80",&_80},
	{"79",&_79},
	{"TogglePB",&_TogglePB},
	{"78",&_78},
	{"77",&_77},
	{"LongDateRec",&_LongDateRec},
	{"76",&_76},
	{"LongDateCvt",&_LongDateCvt},
	{"DateTimeRec",&_DateTimeRec},
	{"DateCacheRecord",&_DateCacheRecord},
	{"75",&_75},
	{"74",&_74},
	{"73",&_73},
	{"72",&_72},
	{"71",&_71},
	{"70",&_70},
	{"VolumeVirtualMemoryInfo",&_VolumeVirtualMemoryInfo},
	{"69",&_69},
	{"LogicalToPhysicalTable",&_LogicalToPhysicalTable},
	{"MemoryBlock",&_MemoryBlock},
	{"Zone",&_Zone},
	{"68",&_68},
	{"67",&_67},
	{"66",&_66},
	{"65",&_65},
	{"64",&_64},
	{"63",&_63},
	{"62",&_62},
	{"61",&_61},
	{"60",&_60},
	{"MixedModeStateRecord",&_MixedModeStateRecord},
	{"RoutineDescriptor",&_RoutineDescriptor},
	{"59",&_59},
	{"RoutineRecord",&_RoutineRecord},
	{"58",&_58},
	{"57",&_57},
	{"56",&_56},
	{"55",&_55},
	{"54",&_54},
	{"53",&_53},
	{"52",&_52},
	{"51",&_51},
	{"50",&_50},
	{"49",&_49},
	{"48",&_48},
	{"47",&_47},
	{"46",&_46},
	{"45",&_45},
	{"44",&_44},
	{"43",&_43},
	{"__CFTree",&___CFTree},
	{"42",&_42},
	{"__CFSet",&___CFSet},
	{"41",&_41},
	{"40",&_40},
	{"__CFNumber",&___CFNumber},
	{"39",&_39},
	{"__CFBoolean",&___CFBoolean},
	{"38",&_38},
	{"37",&_37},
	{"36",&_36},
	{"__CFTimeZone",&___CFTimeZone},
	{"__CFDate",&___CFDate},
	{"35",&_35},
	{"__CFCharacterSet",&___CFCharacterSet},
	{"20",&_20},
	{"19",&_19},
	{"18",&_18},
	{"__CFDictionary",&___CFDictionary},
	{"17",&_17},
	{"16",&_16},
	{"__CFData",&___CFData},
	{"__CFBag",&___CFBag},
	{"15",&_15},
	{"__CFArray",&___CFArray},
	{"14",&_14},
	{"13",&_13},
	{"__CFAllocator",&___CFAllocator},
	{"9",&_9},
	{"8",&_8},
	{"7",&_7},
	{"__CFString",&___CFString},
	{"VersRec",&_VersRec},
	{"NumVersionVariant",&_NumVersionVariant},
	{"6",&_6},
	{"NumVersion",&_NumVersion},
	{"TimeRecord",&_TimeRecord},
	{"TimeBaseRecord",&_TimeBaseRecord},
	{"5",&_5},
	{"FixedRect",&_FixedRect},
	{"FixedPoint",&_FixedPoint},
	{"Rect",&_Rect},
	{"Point",&_Point},
	{"4",&_4},
	{"3",&_3},
	{"2",&_2},
	{"1",&_1},
	{"Float32Point",&_Float32Point},
	{"Float96",&_Float96},
	{"Float80",&_Float80},
	{"UnsignedWide",&_UnsignedWide},
	{"wide",&_wide},
	{"T*",&_T_44_},
	{"void",&_void},
	{"unsigned long long",&_unsigned_32_long_32_long},
	{"unsigned int",&_unsigned_32_int},
	{"unsigned short",&_unsigned_32_short},
	{"unsigned long",&_unsigned_32_long},
	{"unsigned char",&_unsigned_32_char},
	{"signed char",&_signed_32_char},
	{"short",&_short},
	{"long long int",&_long_32_long_32_int},
	{"long int",&_long_32_int},
	{"long double",&_long_32_double},
	{"int",&_int},
	{"float",&_float},
	{"double",&_double},
	{"char",&_char},
};

Nat4	VPX_MacOSX_Types_Number = 3487;

#pragma export on

Nat4	load_MacOSX_Types(V_Environment environment);
Nat4	load_MacOSX_Types(V_Environment environment)
{
		Nat4	result = 0;
        V_Dictionary	dictionary = NULL;
		
		dictionary = environment->externalTypesTable;
		result = add_nodes(dictionary,VPX_MacOSX_Types_Number,VPX_MacOSX_Types);
		
		return result;
}

#pragma export off
