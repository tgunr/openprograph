/*
	
	StandardUI.h
	Copyright 2003 Scott B. Anderson, All Rights Reserved.
	
*/

#ifndef VPXSTANDARDUI
#define VPXSTANDARDUI

Nat4	load_StandardUI(V_Environment environment);

#endif
