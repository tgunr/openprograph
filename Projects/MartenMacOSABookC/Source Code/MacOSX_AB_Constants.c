/*
	
	MacOSX_AB_Constants.c
	Copyright 2005 Andescotia LLC, All Rights Reserved.
	
*/

#ifdef __MWERKS__
#include "VPL_Compiler.h"
#endif	

	VPL_ExtConstant _kEventParamABPickerRef_C = {"kEventParamABPickerRef",1633841264,NULL};
	VPL_ExtConstant _kEventABPeoplePickerNameDoubleClicked_C = {"kEventABPeoplePickerNameDoubleClicked",6,NULL};
	VPL_ExtConstant _kEventABPeoplePickerGroupDoubleClicked_C = {"kEventABPeoplePickerGroupDoubleClicked",5,NULL};
	VPL_ExtConstant _kEventABPeoplePickerDisplayedPropertyChanged_C = {"kEventABPeoplePickerDisplayedPropertyChanged",4,NULL};
	VPL_ExtConstant _kEventABPeoplePickerValueSelectionChanged_C = {"kEventABPeoplePickerValueSelectionChanged",3,NULL};
	VPL_ExtConstant _kEventABPeoplePickerNameSelectionChanged_C = {"kEventABPeoplePickerNameSelectionChanged",2,NULL};
	VPL_ExtConstant _kEventABPeoplePickerGroupSelectionChanged_C = {"kEventABPeoplePickerGroupSelectionChanged",1,NULL};
	VPL_ExtConstant _kEventClassABPeoplePicker_C = {"kEventClassABPeoplePicker",1633841264,NULL};
	VPL_ExtConstant _kABPickerAllowMultipleSelection_C = {"kABPickerAllowMultipleSelection",8,NULL};
	VPL_ExtConstant _kABPickerAllowGroupSelection_C = {"kABPickerAllowGroupSelection",4,NULL};
	VPL_ExtConstant _kABPickerMultipleValueSelection_C = {"kABPickerMultipleValueSelection",2,NULL};
	VPL_ExtConstant _kABPickerSingleValueSelection_C = {"kABPickerSingleValueSelection",1,NULL};
	VPL_ExtConstant _kABSearchOr_C = {"kABSearchOr",1,NULL};
	VPL_ExtConstant _kABSearchAnd_C = {"kABSearchAnd",0,NULL};
	VPL_ExtConstant _kABNotWithinIntervalFromTodayYearless_C = {"kABNotWithinIntervalFromTodayYearless",24,NULL};
	VPL_ExtConstant _kABNotWithinIntervalFromToday_C = {"kABNotWithinIntervalFromToday",23,NULL};
	VPL_ExtConstant _kABWithinIntervalFromTodayYearless_C = {"kABWithinIntervalFromTodayYearless",22,NULL};
	VPL_ExtConstant _kABWithinIntervalFromToday_C = {"kABWithinIntervalFromToday",21,NULL};
	VPL_ExtConstant _kABNotWithinIntervalAroundTodayYearless_C = {"kABNotWithinIntervalAroundTodayYearless",20,NULL};
	VPL_ExtConstant _kABNotWithinIntervalAroundToday_C = {"kABNotWithinIntervalAroundToday",19,NULL};
	VPL_ExtConstant _kABWithinIntervalAroundTodayYearless_C = {"kABWithinIntervalAroundTodayYearless",18,NULL};
	VPL_ExtConstant _kABWithinIntervalAroundToday_C = {"kABWithinIntervalAroundToday",17,NULL};
	VPL_ExtConstant _kABSuffixMatchCaseInsensitive_C = {"kABSuffixMatchCaseInsensitive",16,NULL};
	VPL_ExtConstant _kABSuffixMatch_C = {"kABSuffixMatch",15,NULL};
	VPL_ExtConstant _kABNotEqualCaseInsensitive_C = {"kABNotEqualCaseInsensitive",14,NULL};
	VPL_ExtConstant _kABDoesNotContainSubStringCaseInsensitive_C = {"kABDoesNotContainSubStringCaseInsensitive",13,NULL};
	VPL_ExtConstant _kABDoesNotContainSubString_C = {"kABDoesNotContainSubString",12,NULL};
	VPL_ExtConstant _kABBitsInBitFieldMatch_C = {"kABBitsInBitFieldMatch",11,NULL};
	VPL_ExtConstant _kABPrefixMatchCaseInsensitive_C = {"kABPrefixMatchCaseInsensitive",10,NULL};
	VPL_ExtConstant _kABPrefixMatch_C = {"kABPrefixMatch",9,NULL};
	VPL_ExtConstant _kABContainsSubStringCaseInsensitive_C = {"kABContainsSubStringCaseInsensitive",8,NULL};
	VPL_ExtConstant _kABContainsSubString_C = {"kABContainsSubString",7,NULL};
	VPL_ExtConstant _kABEqualCaseInsensitive_C = {"kABEqualCaseInsensitive",6,NULL};
	VPL_ExtConstant _kABGreaterThanOrEqual_C = {"kABGreaterThanOrEqual",5,NULL};
	VPL_ExtConstant _kABGreaterThan_C = {"kABGreaterThan",4,NULL};
	VPL_ExtConstant _kABLessThanOrEqual_C = {"kABLessThanOrEqual",3,NULL};
	VPL_ExtConstant _kABLessThan_C = {"kABLessThan",2,NULL};
	VPL_ExtConstant _kABNotEqual_C = {"kABNotEqual",1,NULL};
	VPL_ExtConstant _kABEqual_C = {"kABEqual",0,NULL};
	VPL_ExtConstant _kABMultiDataProperty_C = {"kABMultiDataProperty",263,NULL};
	VPL_ExtConstant _kABMultiDictionaryProperty_C = {"kABMultiDictionaryProperty",262,NULL};
	VPL_ExtConstant _kABMultiArrayProperty_C = {"kABMultiArrayProperty",261,NULL};
	VPL_ExtConstant _kABMultiDateProperty_C = {"kABMultiDateProperty",260,NULL};
	VPL_ExtConstant _kABMultiRealProperty_C = {"kABMultiRealProperty",259,NULL};
	VPL_ExtConstant _kABMultiIntegerProperty_C = {"kABMultiIntegerProperty",258,NULL};
	VPL_ExtConstant _kABMultiStringProperty_C = {"kABMultiStringProperty",257,NULL};
	VPL_ExtConstant _kABDataProperty_C = {"kABDataProperty",7,NULL};
	VPL_ExtConstant _kABDictionaryProperty_C = {"kABDictionaryProperty",6,NULL};
	VPL_ExtConstant _kABArrayProperty_C = {"kABArrayProperty",5,NULL};
	VPL_ExtConstant _kABDateProperty_C = {"kABDateProperty",4,NULL};
	VPL_ExtConstant _kABRealProperty_C = {"kABRealProperty",3,NULL};
	VPL_ExtConstant _kABIntegerProperty_C = {"kABIntegerProperty",2,NULL};
	VPL_ExtConstant _kABStringProperty_C = {"kABStringProperty",1,NULL};
	VPL_ExtConstant _kABErrorInProperty_C = {"kABErrorInProperty",0,NULL};
	VPL_ExtConstant _kABLastNameFirst_C = {"kABLastNameFirst",16,NULL};
	VPL_ExtConstant _kABFirstNameFirst_C = {"kABFirstNameFirst",32,NULL};
	VPL_ExtConstant _kABDefaultNameOrdering_C = {"kABDefaultNameOrdering",0,NULL};
	VPL_ExtConstant _kABNameOrderingMask_C = {"kABNameOrderingMask",112,NULL};
	VPL_ExtConstant _kABShowAsCompany_C = {"kABShowAsCompany",1,NULL};
	VPL_ExtConstant _kABShowAsPerson_C = {"kABShowAsPerson",0,NULL};
	VPL_ExtConstant _kABShowAsMask_C = {"kABShowAsMask",7,NULL};


VPL_DictionaryNode VPX_MacOSAddressBook_Constants[] =	{
	{"kEventParamABPickerRef", &_kEventParamABPickerRef_C},
	{"kEventABPeoplePickerNameDoubleClicked", &_kEventABPeoplePickerNameDoubleClicked_C},
	{"kEventABPeoplePickerGroupDoubleClicked", &_kEventABPeoplePickerGroupDoubleClicked_C},
	{"kEventABPeoplePickerDisplayedPropertyChanged", &_kEventABPeoplePickerDisplayedPropertyChanged_C},
	{"kEventABPeoplePickerValueSelectionChanged", &_kEventABPeoplePickerValueSelectionChanged_C},
	{"kEventABPeoplePickerNameSelectionChanged", &_kEventABPeoplePickerNameSelectionChanged_C},
	{"kEventABPeoplePickerGroupSelectionChanged", &_kEventABPeoplePickerGroupSelectionChanged_C},
	{"kEventClassABPeoplePicker", &_kEventClassABPeoplePicker_C},
	{"kABPickerAllowMultipleSelection", &_kABPickerAllowMultipleSelection_C},
	{"kABPickerAllowGroupSelection", &_kABPickerAllowGroupSelection_C},
	{"kABPickerMultipleValueSelection", &_kABPickerMultipleValueSelection_C},
	{"kABPickerSingleValueSelection", &_kABPickerSingleValueSelection_C},
	{"kABSearchOr", &_kABSearchOr_C},
	{"kABSearchAnd", &_kABSearchAnd_C},
	{"kABNotWithinIntervalFromTodayYearless", &_kABNotWithinIntervalFromTodayYearless_C},
	{"kABNotWithinIntervalFromToday", &_kABNotWithinIntervalFromToday_C},
	{"kABWithinIntervalFromTodayYearless", &_kABWithinIntervalFromTodayYearless_C},
	{"kABWithinIntervalFromToday", &_kABWithinIntervalFromToday_C},
	{"kABNotWithinIntervalAroundTodayYearless", &_kABNotWithinIntervalAroundTodayYearless_C},
	{"kABNotWithinIntervalAroundToday", &_kABNotWithinIntervalAroundToday_C},
	{"kABWithinIntervalAroundTodayYearless", &_kABWithinIntervalAroundTodayYearless_C},
	{"kABWithinIntervalAroundToday", &_kABWithinIntervalAroundToday_C},
	{"kABSuffixMatchCaseInsensitive", &_kABSuffixMatchCaseInsensitive_C},
	{"kABSuffixMatch", &_kABSuffixMatch_C},
	{"kABNotEqualCaseInsensitive", &_kABNotEqualCaseInsensitive_C},
	{"kABDoesNotContainSubStringCaseInsensitive", &_kABDoesNotContainSubStringCaseInsensitive_C},
	{"kABDoesNotContainSubString", &_kABDoesNotContainSubString_C},
	{"kABBitsInBitFieldMatch", &_kABBitsInBitFieldMatch_C},
	{"kABPrefixMatchCaseInsensitive", &_kABPrefixMatchCaseInsensitive_C},
	{"kABPrefixMatch", &_kABPrefixMatch_C},
	{"kABContainsSubStringCaseInsensitive", &_kABContainsSubStringCaseInsensitive_C},
	{"kABContainsSubString", &_kABContainsSubString_C},
	{"kABEqualCaseInsensitive", &_kABEqualCaseInsensitive_C},
	{"kABGreaterThanOrEqual", &_kABGreaterThanOrEqual_C},
	{"kABGreaterThan", &_kABGreaterThan_C},
	{"kABLessThanOrEqual", &_kABLessThanOrEqual_C},
	{"kABLessThan", &_kABLessThan_C},
	{"kABNotEqual", &_kABNotEqual_C},
	{"kABEqual", &_kABEqual_C},
	{"kABMultiDataProperty", &_kABMultiDataProperty_C},
	{"kABMultiDictionaryProperty", &_kABMultiDictionaryProperty_C},
	{"kABMultiArrayProperty", &_kABMultiArrayProperty_C},
	{"kABMultiDateProperty", &_kABMultiDateProperty_C},
	{"kABMultiRealProperty", &_kABMultiRealProperty_C},
	{"kABMultiIntegerProperty", &_kABMultiIntegerProperty_C},
	{"kABMultiStringProperty", &_kABMultiStringProperty_C},
	{"kABDataProperty", &_kABDataProperty_C},
	{"kABDictionaryProperty", &_kABDictionaryProperty_C},
	{"kABArrayProperty", &_kABArrayProperty_C},
	{"kABDateProperty", &_kABDateProperty_C},
	{"kABRealProperty", &_kABRealProperty_C},
	{"kABIntegerProperty", &_kABIntegerProperty_C},
	{"kABStringProperty", &_kABStringProperty_C},
	{"kABErrorInProperty", &_kABErrorInProperty_C},
	{"kABLastNameFirst", &_kABLastNameFirst_C},
	{"kABFirstNameFirst", &_kABFirstNameFirst_C},
	{"kABDefaultNameOrdering", &_kABDefaultNameOrdering_C},
	{"kABNameOrderingMask", &_kABNameOrderingMask_C},
	{"kABShowAsCompany", &_kABShowAsCompany_C},
	{"kABShowAsPerson", &_kABShowAsPerson_C},
	{"kABShowAsMask", &_kABShowAsMask_C},
};

Nat4	VPX_MacOSAddressBook_Constants_Number = 61;

#pragma export on

Nat4	load_MacOSAddressBook_Constants(V_Environment environment,char *bundleID);
Nat4	load_MacOSAddressBook_Constants(V_Environment environment,char *bundleID)
{
		Nat4	result = 0;
        V_Dictionary	dictionary = NULL;
		
		dictionary = environment->externalConstantsTable;
		result = add_nodes(dictionary,VPX_MacOSAddressBook_Constants_Number,VPX_MacOSAddressBook_Constants);
		
		return result;
}

#pragma export off
